'use strict';
var logger=require('./utilities/logger.js');
var db = require('../db').pool;
var file='Admin_TempleContact.js';


exports.AddContact=function(req,res)
{
        var method='AddContact';
        var params="'"+req.session.TempleID+"','"+req.body.ContactName+"','"+req.body.TitleID+"','"+req.body.HouseNumber+"','"+req.body.HouseName+"','"+req.body.StreetName+"','"+req.body.PostOffice+"','"+req.body.PIN+"','"+req.body.Email+"','"+req.body.State+"','"+req.body.District+"','"+req.body.PhoneMobile+"','"+req.body.PhoneWired+"','"+req.body.Status+"'";
        var sql="call admin_add_management("+params+")";
        db.getConnection(function(err, con){
            logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
            con.query(sql,function(err,rows){
                con.release();
                if (err)
                {
                    res.end( '[{ "RESULT" : "0"}]');
                    logger.log('error',+err,req.session.sessionID,file+'/'+method);
                    return;
                }
                if (rows.length == 0)
                {
                    res.end( '[{ "RESULT" : "0}]');
                    logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                }
                else
                {
                    var results= JSON.stringify(rows);
                    res.end( '[{ "RESULT" : "1"}]');
                    logger.log('data',results,req.session.sessionID,file+'/'+method);
                }
            });
        });
}
exports.LoadDesignation=function(req,res)
{
    var method='Loadusers';
    var sql="SELECT `title`.* FROM `devaayanam`.`title`;";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
}
exports.loadusers=function(req,res)
{
    console.log("............");
    console.log("..............."+JSON.stringify(req.session));

    var method='LoadDesignation';
    var sql="SELECT * FROM `devaayanam`.`useradmin` where TempleID='"+req.session.TempleID+"';"
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
},
    exports.loadcontacts=function(req,res)
    {

        console.log('s..'+JSON.stringify(req.session));
        var method='LoadDesignation';
        var sql;
        if(req.body.mode=='load') {
            sql = "SELECT * FROM `devaayanam`.`templecontact` where TempleID='" + req.session.TempleID + "' and `templecontact`.`Status`=1 and `templecontact`.`LanguageID`=1;";
        }else  if(req.body.mode=='delete'){
            sql = "update `devaayanam`.`templecontact` set `templecontact`.`Status`=0  where TempleID='" + req.session.TempleID + "' and ContactID='" + req.body.ContactID + "' and Title='" + req.body.Title + "'";
        }
        db.getConnection(function(err, con){
            logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
            con.query(sql,function(err,rows){
                con.release();
                if (err)
                {
                    res.end( '[{ "RESULT" : "DB ERROR"}]');
                    logger.log('error',+err,req.session.sessionID,file+'/'+method);
                    return;
                }
                if (rows.length == 0)
                {
                    res.end( '[{ "RESULT" : "DB ERROR"}]');
                    logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                }
                else
                {
                    var results= JSON.stringify(rows);
                    res.end(results);
                    logger.log('data',results,req.session.sessionID,file+'/'+method);
                }
            });
        });
    }





