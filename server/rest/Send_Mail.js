// <copyright file="Notify.js" company="CronyCo">
// Copyright (c) 2014 All Right Reserved, http://cronyco.in/
//
// This source is subject to the CronyCo Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@cronyco.in</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Notify</summary>
'use strict';
var db = require('../db').pool;
var nodemailer = require("nodemailer");
var cryptojs=require('../rest/utilities/cryptography.js');

var prevDirName=__dirname;
prevDirName=prevDirName.substring(0,prevDirName.lastIndexOf('utilities'));
var dbConfigFile = prevDirName + 'server/database.json';
var fs = require('fs');
var env='configuration';
var data = fs.readFileSync(dbConfigFile, 'utf8');
var dbConfig = JSON.parse(data)[env];
var email_provider=dbConfig.emailprovider;
var email_username=dbConfig.emailusername;
var email_password=cryptojs.DecryptString(dbConfig.emailpassword);

exports.SendEmail=function(req,res){
    var smtpTransport = nodemailer.createTransport("SMTP",{
        service: email_provider,
        auth: {
            user: email_username,
            pass: email_password
        }
    });
    var mailOptions = {
        from: email_username, // sender address
        to: req.session.LoginID, // list of receivers
        subject:req.body.Subject, // Subject line//
        html: req.body.SendTemplate
    };
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log("Mail Send Error "+error);
            res.end( '[{ "RESULT" : "FAILED"}]');

        }
        else{
            res.end( '[{ "RESULT" : "SUCCESS"}]');
            console.log("Message sent: " + response.message);
        }
    });
};

exports.SendMailToEmail=function(req,res){
    var smtpTransport = nodemailer.createTransport("SMTP",{
        service: email_provider,
        auth: {
            user: email_username,
            pass: email_password
        }
    });
    var fromAddress=email_username;
    if(req.body.From!=undefined){
        fromAddress=req.body.From
    }
    var mailOptions = {
        from: fromAddress, // sender address
        to: req.body.CustomerEmail, // list of receivers
        subject:req.body.Subject, // Subject line//
        html: req.body.SendTemplate
    };
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log("Mail Send Error "+error);
            res.end( '[{ "RESULT" : "FAILED"}]');
        }
        else{
            res.end( '[{ "RESULT" : "SUCCESS"}]');
            console.log("Message sent: " + response.message);
        }
    });
};

exports.SentMail=function(data){
    var smtpTransport = nodemailer.createTransport("SMTP",{
        service: email_provider,
        auth: {
            user: email_username,
            pass: email_password
        }
    });
    var fromAddress=email_username;
    if(data.From!=undefined){
        fromAddress=data.From
    }
    var mailOptions = {
        from: fromAddress, // sender address
        to: data.CustomerEmail, // list of receivers
        subject:data.Subject, // Subject line//
        html: data.SendTemplate
    };
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log("Mail Send Error "+error);
        }
        else{
            console.log("Message sent: " + response.message);
        }
    });
};

exports.send_mail_from_data=function(data,callback){
    console.log('mailing data= '+JSON.stringify(data))
    if(data.email_provider==undefined || data.email_provider==''){
        data.email_provider=email_provider;
    }
    if(data.email_username==undefined || data.email_username==''){
        data.email_username=email_username;
    }
    if(data.email_password==undefined || data.email_password==''){
        data.email_password=email_password;
    }
    console.log('mailing data= '+JSON.stringify(data))
    var smtpTransport = nodemailer.createTransport("SMTP",{
        service: data.email_provider,
        auth: {
            user: data.email_username,
            pass: data.email_password
        }
    });
    var fromAddress=email_username;
    if(data.from!=undefined){
        fromAddress=data.from
    }
    var mailOptions = {
        from: fromAddress, // sender address
        to: data.to, // list of receivers
        subject:data.subject, // Subject line//
        html: data.template
    };
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log("Mail Send Error "+error);
        }
        else{
            console.log("Message sent: " + response.message);
        }
    });
};
exports.send_mail_with_attachment=function(data,callback){
    console.log('mailing data= '+JSON.stringify(data))
    if(data.email_provider==undefined || data.email_provider==''){
        data.email_provider=email_provider;
    }
    if(data.email_username==undefined || data.email_username==''){
        data.email_username=email_username;
    }
    if(data.email_password==undefined || data.email_password==''){
        data.email_password=email_password;
    }
    console.log('mailing data= '+JSON.stringify(data))
    var smtpTransport = nodemailer.createTransport("SMTP",{
        service: data.email_provider,
        auth: {
            user: data.email_username,
            pass: data.email_password
        }
    });
    var fromAddress=email_username;
    if(data.from!=undefined){
        fromAddress=data.from
    }
    var mailOptions = {
        from: fromAddress, // sender address
        to: data.to, // list of receivers
        subject:data.subject, // Subject line//
        html: data.template,
        attachments:data.attachment
    };
    console.log("maildata.."+JSON.stringify(mailOptions));
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log("Mail Send Error "+error);
            callback('f');
        }
        else{
            console.log("Message sent: " + response.message);
            callback('s');
        }
    });
};

