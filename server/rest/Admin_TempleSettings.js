'use strict';

var db = require('../db').pool;
var logger=require('./utilities/logger.js');
var file='Admin_TempleSettings.js';

exports.LoadKey=function(req,res)
{
    var method='LoadKey';
    var sql="SELECT `templecustomsettings`.`Key`FROM `devaayanam`.`templecustomsettings` where `templecustomsettings`.`TempleID`= '"+req.session.TempleID+"';";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
}


exports.GetKeyValue=function(req,res)
{
    var method='GetKeyValue';
    var sql="SELECT `templecustomsettings`.`Value`,`templecustomsettings`.`Status` FROM `devaayanam`.`templecustomsettings` where `templecustomsettings`.`TempleID`= '"+req.session.TempleID+"' and `templecustomsettings`.`Key`='"+req.body.Key+"'";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
}
exports.AddSettings=function(req,res)
{
        var method='AddSettings';
        var sql="INSERT INTO `devaayanam`.`templecustomsettings` (`TempleID`, `Key`, `Value`, `Status`) VALUES ('"+req.session.TempleID+"', '"+req.body.Key+"', '"+req.body.Value+"', '"+req.body.Status+"')";
        db.getConnection(function(err, con){
            logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
            con.query(sql,function(err,rows){
                con.release();
                if (err)
                {
                    res.end( '[{ "RESULT" : "DB ERROR"}]');
                    logger.log('error',+err,req.session.sessionID,file+'/'+method);
                    return;
                }
                if (rows.length == 0)
                {
                    res.end( '[{ "RESULT" : "DB ERROR"}]');
                    logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                }
                else
                {
                    var results= JSON.stringify(rows);
                    res.end(results);
                    logger.log('data',results,req.session.sessionID,file+'/'+method);
                }
            });
        });

}


exports.UpdateSettings=function(req,res)
{
    var method='UpdateSettings';
    var sql="UPDATE `devaayanam`.`templecustomsettings` SET `templecustomsettings`.`Value` = '"+req.body.Value+"',`templecustomsettings`.`Status`='"+req.body.Status+"' WHERE `templecustomsettings`.`TempleID`= '"+req.session.TempleID+"' and `templecustomsettings`.`Key`='"+req.body.Key+"';";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "0"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "0"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
}



