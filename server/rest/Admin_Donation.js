'use strict';

var db = require('../db').pool;
var logger=require('./utilities/logger.js');
var file='Admin_Donation.js';

exports.LoadDonation=function(req,res)
{
var method='LoadDonation';
    var sql="SELECT * FROM `devaayanam`.`donation` where `donation`.`TempleID`= '"+req.session.TempleID+"' and `donation`.`LanguageID`='"+req.body.LanguageID+"' ";
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "DB ERROR"}]');
            }
            else
            {
                results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.LoadDonationByLanguage=function(req,res)
{
    var method='GetDonationDetails';
    var sql="Select  languages.*,  donation.* From  donation Inner Join  languages On donation.LanguageID =    languages.languages_id Where  donation.TempleID = '"+req.session.TempleID+"' And  donation.DonationID = '"+req.body.DonationID+"' And  donation.LanguageID =  '"+req.body.LanguageID+"'";
    logger.log('data','Query :'+sql,req.session.sessionID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "DB ERROR"}]');
            }
            else
            {
                results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.AddDonation=function(req,res)
{
    var method='AddDonation';

    var params="'"+req.session.TempleID+"','"+req.body.DonationMasterID+"', '"+req.body.Desc+"', '"+req.body.Amount+"', '"+req.body.StartFromDate.substring(0,10)+"', '"+req.body.EndOnDate.substring(0,10)+"'";
    var sql="call admin_add_donation("+params+")";
    logger.log('data','Query: '+sql,req.session.sessionID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '{ "Result" : "Unable to add donation"}');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '{ "Result" : "Unable to add donation"}');
            }
            else
            {
                results= JSON.stringify(rows);
                res.end( '{ "Result" : "Donation added successfully"}');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.DonationUpdate=function(req,res)
{
    var method='DonationUpdate'
    var sql="call admin_update_donation('"+req.session.TempleID+"','"+req.body.DonationID+"','"+req.body.Amount+"','"+req.body.Status+"','"+req.body.LanguageID+"','"+req.body.Desc+"')";
console.log(sql);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                console.log('...error',+err,req.session.sessionID,file+'/'+method);
                res.end( '{ "Result" : "Unable to update donation"}');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '{ "Result" : "Unable to update donation"}');
            }
            else
            {
                results= JSON.stringify(rows);
                res.end( '{ "Result" : "Donation updated successfully"}');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });

    });
};







