'use strict';

var db = require('../db').pool;
var logger=require('./utilities/logger.js');
var file='Admin Deity.js';


exports.AddDeity=function(req,res)
{
    var method='AddDeity';
    db.getConnection(function(err, con){
        console.log("Database connection starts Deity Add");
        var params="'"+req.session.TempleID+"','"+req.body.DeityMasterID+"','"+req.body.Category+"'";

        con.query("Call admin_add_deity("+params+")",function(err,rows){
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "NoData"}]');
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "NoData"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                results=JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });


        console.log("Database connection Closed-Deity Add");



    });

}

exports.LoadDeity=function(req,res)
{
    var method='LoadDeity';
    var sql="SELECT `deity`.* FROM `devaayanam`.`deity` where `deity`.`TempleID`='"+req.session.TempleID+"' and `deity`.`LanguageID`='"+req.body.LanguageID+"';";
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "NoData"}]');
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "NoData"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                results=JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
}

exports.RemoveDeity=function(req,res)
{
    var method='RemoveDeity';
    var sql="update deity set Status="+req.body.NewStatus+" where TempleID='"+req.body.TempleID+"' and DeityID ='"+req.body.DeityID+"';";
    console.log (sql);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            var results;
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.send( '[{ "RESULT" : "0"}]');
            }
            else
            {
                logger.log('data',results,req.session.sessionID,file+'/'+method);
                results='[{ "RESULT" : "1"}]';
                res.send( results);
            }
        });
    });
}
