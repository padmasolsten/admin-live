'use strict';
var logger=require('./utilities/logger.js');
var db = require('../db').pool;
var cryptojs=require('./utilities/cryptography.js'); //DV156-Encrypt Admin user password --------- start -------- 
var file="Admin_NewUser.js";

exports.LoadAccessType=function(req,res)
{
    var method='LoadAccessType';
    var sql="SELECT `accesstype`.`AccessTypeID`,`accesstype`.`TypeName` FROM `devaayanam`.`accesstype`;";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.AddUserAdmin=function(req,res,callback)
{
         var method='AddUserAdmin';
		//DV156-Encrypt Admin user password --------- start --------
		// var params="'"+req.session.TempleID+"','"+req.body.HouseNumber+"','"+req.body.HouseName+"','"+req.body.StreetName+"','"+req.body.PostOffice+"','"+req.body.PIN+"','"+req.body.Country+"','"+req.body.State+"','"+req.body.District+"','"+req.body.PhoneMobile+"','"+req.body.PhoneWired+"','"+req.body.TitleID+"','"+req.body.AccessType+"','"+req.body.FirstName+"','"+req.body.LastName+"','"+req.body.LoginID+"','"+req.body.Password+"','"+req.body.Status+"'";		
		var cryptpassword=cryptojs.EncryptString(req.body.Password);
		 
        var params="'"+req.session.TempleID+"','"+req.body.HouseNumber+"','"+req.body.HouseName+"','"+req.body.StreetName+"','"+req.body.PostOffice+"','"+req.body.PIN+"','"+req.body.Country+"','"+req.body.State+"','"+req.body.District+"','"+req.body.PhoneMobile+"','"+req.body.PhoneWired+"','"+req.body.TitleID+"','"+req.body.AccessType+"','"+req.body.FirstName+"','"+req.body.LastName+"','"+req.body.LoginID+"','"+cryptpassword+"','"+req.body.Status+"'";
         var sql="call admin_add_user("+params+")";

    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "0"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "0"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
exports.UsernameAvailability=function(req,res)
{
    var method='UsernameAvailability';
    var sql="SELECT `useradmin`.`LoginID` FROM `devaayanam`.`useradmin`where `useradmin`.`LoginID`='"+req.body.LoginID+"' ;";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "0"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "0"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};



