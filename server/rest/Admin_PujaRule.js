'use strict';
var file='Admin_PujaRule.js';
var logger=require('./utilities/logger.js');
var db = require('../db').pool;

exports.LoadPuja=function(req,res)
{
    console.log("......."+req.session.TempleID);
    var method='LoadPuja';
    var sql="SELECT `puja`.`PujaID`,`puja`.`PujaName`FROM `devaayanam`.`puja` where `puja`.`TempleID`= '"+req.session.TempleID+"';";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "No Data"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};


exports.AddPujaRule=function(req,res)
{
    var method='AddPujaRule';
    var sql="Call pujarule('"+req.session.TempleID+"', '"+req.body.PujaID+"', '"+req.body.DisableOnlineListing+"', '"+req.body.DisableOnlineBooking+"', '"+req.body.CeilingNormalDay+"', '"+req.body.CeilingFestivalDay+"', '"+req.body.BookingStartDate+"', '"+req.body.BookingEndDate+"', '"+req.body.MinAdvanceBookingPeriod+"', '"+req.body.ReOccuringDay+"', '"+req.body.ReOccuringStar+"', '"+req.body.SkipDevoteeNameStar+"','"+req.body.NotAFixedRate+"','"+req.body.Duration+"', '"+req.body.Timing+"', '"+req.body.PujaStartTime+"', '"+req.body.PujaEndTime+"')"
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "No Data"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.GetPujaRuleDetails=function(req,res)
{
    var method='GetPujaRuleDetails';
    var sql="Select pujarule.TempleID,pujarule.PujaID,pujarule.DisableOnlineListing,pujarule.DisableOnlineBooking,pujarule.CeilingNormalDay,pujarule.CeilingFestivalDay,pujarule.BookingStartDate,pujarule.BookingEndDate,pujarule.MinAdvanceBookingPeriod,pujarule.ReOccuringDay,pujarule.ReOccuringStar,pujarule.SkipDevoteeNameStar,pujarule.NotAFixedRate,pujarule.Timing,pujarule.Duration,pujarule.PujaStartTime,pujarule.PujaEndTime ,star.StarName,pujarule.ReOccuringStar FROM `devaayanam`.`pujarule` ,`devaayanam`.`star` where `pujarule`.`TempleID`= '"+req.session.TempleID+"' and `pujarule`.`PujaID`='"+req.body.PujaID+"' and `star`.`StarID`=`pujarule`.`ReOccuringStar`;";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "No Data"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
}



exports.GetExclusionDetails=function(req,res)
{
    var method='GetExclusionDetails';
    var sql="Select pujaexclusion.TempleID,pujaexclusion.PujaID,pujaexclusion.ExcludedPujaiD,pujaexclusion.ExclusionEligibilityCount FROM `devaayanam`.`pujaexclusion` where `pujaexclusion`.`TempleID`= '"+req.session.TempleID+"' and `pujaexclusion`.`PujaID`='"+req.body.PujaID+"' and `pujaexclusion`.`ExcludedPujaID`='"+req.body.ExcludedPujaID+"';";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
}

exports.PujaRuleUpdate=function(req,res)
{
    var method='PujaRuleUpdate';
    var sql="UPDATE `devaayanam`.`pujarule` SET `pujarule`.`DisableOnlineListing` = '"+req.body.DisableOnlineListing+"',`pujarule`.`DisableOnlineBooking`='"+req.body.DisableOnlineBooking+"',`pujarule`.`CeilingNormalDay`='"+req.body.CeilingNormalDay+"',`pujarule`.`CeilingFestivalDay`='"+req.body.CeilingFestivalDay+"',`pujarule`.`BookingStartDate`='"+req.body.BookingStartDate+"',`pujarule`.`BookingEndDate`='"+req.body.BookingEndDate+"',`pujarule`.`MinAdvanceBookingPeriod`='"+req.body.MinAdvanceBookingPeriod+"',`pujarule`.`ReOccuringDay`='"+req.body.ReOccuringDay+"',`pujarule`.`ReOccuringStar`='"+req.body.ReOccuringStar+"',`pujarule`.`SkipDevoteeNameStar`='"+req.body.SkipDevoteeNameStar+"',`pujarule`.`NotAFixedRate`='"+req.body.NotAFixedRate+"',`pujarule`.`Duration`='"+req.body.Duration+"',`pujarule`.`Timing`='"+req.body.Timing+"',`pujarule`.`PujaStartTime`='"+req.body.PujaStartTime+"',`pujarule`.`PujaEndTime`='"+req.body.PujaEndTime+"' WHERE `pujarule`.`TempleID`= '"+req.session.TempleID+"' and `pujarule`.`PujaID`='"+req.body.PujaID+"';";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "Puja Rule Updation  Failed "}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "Puja Rule Updation  Failed "}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end( '[{ "RESULT" : "Puja Rule Updated Successfully"}]');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

}



exports.Block=function(req,res)
{
        var method='Block';
        var params="'"+req.session.TempleID+"', '"+req.body.PujaID+"', '"+req.body.UNAVAILABLE_DATE+"'";
        var sql="Call unavailabledate("+params+")";
        db.getConnection(function(err, con){
            logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
            con.query(sql,function(err,rows){
                con.release();
                if (err)
                {
                    res.end( '[{ "RESULT" : "DB ERROR"}]');
                    logger.log('error',+err,req.session.sessionID,file+'/'+method);
                    return;
                }
                if (rows.length == 0)
                {
                    res.end( '[{ "RESULT" : "No Data"}]');
                    logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                }
                else
                {
                    var results= JSON.stringify(rows);
                    res.end(results);
                    logger.log('data',results,req.session.sessionID,file+'/'+method);
                }
            });
        });
}

exports.Exclusion=function(req,res)
{
        var method='Exclusion';
        var params="'"+req.session.TempleID+"', '"+req.body.PujaID+"', '"+req.body.ExcludedPujaID+"','"+req.body.ExclusionEligibilityCount+"'";
        var sql="Call exclusionpuja("+params+")";
        db.getConnection(function(err, con){
            logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
            con.query(sql,function(err,rows){
                con.release();
                if (err)
                {
                    res.end( '[{ "RESULT" : "DB ERROR"}]');
                    logger.log('error',+err,req.session.sessionID,file+'/'+method);
                    return;
                }
                if (rows.length == 0)
                {
                    res.end( '[{ "RESULT" : "No Data"}]');
                    logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                }
                else
                {
                    var results= JSON.stringify(rows);
                    res.end(results);
                    logger.log('data',results,req.session.sessionID,file+'/'+method);
                }
            });
        });
}

exports.ExclusionUpdate=function(req,res)
{
    var method='ExclusionUpdate';
    var sql="UPDATE `devaayanam`.`pujaexclusion` SET `ExclusionEligibilityCount`='"+req.body.ExclusionEligibilityCount+"'WHERE `pujaexclusion`.`TempleID`= '"+req.session.TempleID+"' and `pujaexclusion`.`PujaID`='"+req.body.PujaID+"' and `pujaexclusion`.`ExcludedPujaID`='"+req.body.ExcludedPujaID+"';";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "No Data"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
}
