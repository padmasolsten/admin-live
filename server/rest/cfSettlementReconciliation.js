exports.processSettlement=function(req,res){
    var method='processSettlement';
    var stage="";

	var db = require('../db').pool;
	var request=require('request');

	db.getConnection(function(err, con){
		stage = "call `processOrderSettlement`();"
		var sql = "call `processOrderSettlement`();"
		console.log (sql);
		con.query(sql,function(err, rows, fields) {
		con.release();
		var orderArr = rows[0];
		console.log ('To Process - ' + orderArr.length);
		if (err) {
			var msg='Error in ' + stage;
			var resp={ "Error" : msg,message:err};
			console.log('error',err,0,file+'/'+method);
		}
		if (orderArr.length > 0) {
			orderArr.forEach(function(settlementRec){
				stage = 'Get Settlement Statius for Order Id - ' + settlementRec.GatewayTransactionID; 	console.log (stage);								
				var options = {
				  'method': 'POST',
				  'url': 'https://ces-api.cashfree.com/ces/v1/authorize',
				  'headers': {
					'X-Client-Id': 'CF12348XI38PD42EPEUAMA',
					'X-Client-Secret': '3c140394c494abd5ad7f4c5fc2d85dae2a4a77aa'
				  }
				};
				request(options, function (err, response, body) {
					//console.log ('Authorise call response - ' + JSON.stringify(JSON.parse(body)));					
					if ((JSON.parse(body).status) == 'SUCCESS') {
						var tokenDetails = JSON.parse(body).data;
						console.log ('Token -' +  tokenDetails.token);							
					
						var orderURL = 'https://ces-api.cashfree.com/ces/v1/getOrderSettlementStatus/'+ settlementRec.GatewayTransactionID;
						var optionsGet = {
						  'method': 'GET',
						  'url': orderURL,
						  'headers': {
							'Authorization': 'Bearer ' + tokenDetails.token,
							'Content-Type': 'application/x-www-form-urlencoded'
						  },
						  form: {
							'Authorization': tokenDetails.token
						  }
						};
						console.log ('Status call ' + optionsGet);
						request(optionsGet, function (err, response, body) {
							console.log ('Order response -' +  JSON.stringify(JSON.parse(body).vendorTransfers));															
							if (err) {
								var msg='Error in ' + stage;
								var resp={ "Error" : msg,message:err};
								console.log('error',err,0,file+'/'+method);
							}
							else {
								var transferArr = JSON.parse(body).vendorTransfers;
								var trasnferStatus = JSON.parse(body).settlementStatus;								
								console.log ('Transfer status, details ' + trasnferStatus + ', ' + JSON.stringify(transferArr));
								
								var vendorArr = [];
								if (transferArr != undefined) {
									vendorArr = transferArr.filter(function (e) {
										return e.vendorId == settlementRec.vendorId;
									});									
									console.log ('Vendor details ' + JSON.stringify(vendorArr));
								}	
								var var_addedon = '';
								var var_utr = '0';
								if (vendorArr != undefined) {
									vendorArr.forEach(function(transferRec){
										var_addedon = transferRec.addedOn;
										var_utr = transferRec.utr;
									})	
								}
								db.getConnection(function(err, con){
									var sql = "call `updateSettlementDetails`('"+settlementRec.GatewayTransactionID+"','"+settlementRec.vendorId+"','"+
													var_addedon+"','"+var_utr+"','"+trasnferStatus+"');"
									console.log (sql);
									con.query(sql,function(err, rows, fields) {
										con.release();
										console.log (rows);
										if(!err) {
											if(rows.affectedRows == 1) {
												console.log('data','updateSettlementDetails Success !!',1,stage);
											}
											else {
												console.log('warn','updateSettlementDetails Failed !!',0,stage);
											}
										}
										else{
											console.log('error','Invalid Query !!',0,stage);
										}
									});
								});
							}
						});		
					}
				});
			});	
		}
	});	
	});

/*
exports.processSettlement=function(req,res){
    var method='processSettlement';
    var stage="";
	var var_apiID = '12348ca589bd785fd8e40f3cb84321';
	var var_secretKey = '4239fd6b875664686a85eadb588ae64dd317d765';
	
	var db = require('../db').pool;
	
	var checkstartdate = new Date();
	checkstartdate.setDate(checkstartdate.getDate() - 2);
	var checkstartdate= [checkstartdate.getFullYear(), ('0' + (checkstartdate.getMonth() + 1)).slice(-2), ('0' + checkstartdate.getDate()).slice(-2)].join('-');
	console.log(checkstartdate.toString());
	
	var checkenddate = new Date();
	checkenddate.setDate(checkenddate.getDate() - 1);
	var checkenddate= [checkenddate.getFullYear(), ('0' + (checkenddate.getMonth() + 1)).slice(-2), ('0' + checkenddate.getDate()).slice(-2)].join('-');
	console.log(checkenddate.toString());

	var var_startDate = checkstartdate;
    var var_endDate = checkenddate;
	
	var request=require('request');
	var options={
		appId:var_apiID,
		secretKey:var_secretKey,
		startDate:var_startDate,
		endDate:var_endDate
	}
	stage = 'Settlement API call options -' + JSON.stringify(options)
	console.log (stage);							
	var postForm={
		url:'https://ces-api.cashfree.com/api/v1/settlements',
		form:options
	}	
	request.post(postForm, function (err, settleresponse, settlebody) {
		console.log ('Settlement response -' + JSON.stringify(JSON.parse(settlebody)));								
		if (err) {
			var msg='Error in ' + stage;
			var resp={ "Error" : msg,message:err};
			console.log('error',err,0,file+'/'+method);
			//res.end(JSON.stringify([{result:resp,message:msg}]));
			//return;
		}
		console.log ('Settlements List -' + JSON.parse(settlebody).settlements);	
		var settlementsArr = JSON.parse(settlebody).settlements;
		if (settlementsArr.length > 0) {
			settlementsArr.forEach(function(settlementRec){
				stage = 'Get Transactions in Settlement Id - ' + settlementRec.id; 	console.log (stage);
				var options={
					appId:var_apiID,
					secretKey:var_secretKey,
					settlementId:settlementRec.id
				}
				var postForm={
					url:'https://ces-api.cashfree.com/api/v1/settlement',
					form:options
				}	
				request.post(postForm, function (err, orderresponse, orderbody) {
					console.log ('Order response -' +  JSON.stringify(JSON.parse(orderbody)));								
					if (err) {
						var msg='Error in ' + stage;
						var resp={ "Error" : msg,message:err};
						console.log('error',err,0,file+'/'+method);
						//res.end(JSON.stringify([{result:resp,message:msg}]));
						//return;
					}
					//console.log ('Order List - ' + JSON.parse(settlebody).transactions);	
					var orderArr = JSON.parse(orderbody).transactions;

					if (orderArr.length > 0) {
						orderArr.forEach(function(orderRec){
							//stage = 'Update Order as settled - ' + orderRec.orderId; 	console.log (stage);
							db.getConnection(function(err, con){
							var sql = "call `updateSettlementDetails`('"+orderRec.orderId+"',"+settlementRec.id+",'"+settlementRec.settledOn+"',"+orderRec.referenceId+");"
							console.log (sql);
							con.query("call `updateSettlementDetails`('"+orderRec.orderId+"',"+settlementRec.id+",'"+settlementRec.settledOn+"',"+orderRec.referenceId+");",function(err, rows, fields) {
								con.release();
								console.log (rows);
								if(!err) {
									if(rows.affectedRows == 1) {
										console.log('data','updateSettlementDetails Success !!',1,stage);
										//res.end('[{"result":"1"}]');
									}
									else {
										console.log('warn','updateSettlementDetails Failed !!',0,stage);
										//res.end('[{"result":"0"}]');
									}
								}
								else{
									console.log('error','Invalid Query !!',0,stage);
									//res.end('[{"result":"0"}]');
								}
							});
						});
						});
					}	
				});					
			});
		}	
	});	
*/
};
/*
const nDate = new Date();
console.log ('Settlement Job - '+nDate);

var schedule = require('node-schedule');
let rule = new schedule.RecurrenceRule();

// your timezone
//rule.tz = 'Asia/Calcutta'; // doesnot work with timezone

// runs at 04:30:00 AM GMT = 10AM IST (India)
//rule.second = 0;
rule.minute = 30;
rule.hour = 04;
*/
var schedule = require('node-schedule');
var sss = schedule.scheduleJob('0 28 04 * * *', function() {
	console.log('Start Auto Settlement update.....');
	exports.processSettlement();
	console.log('End Auto Settlement update.....');		  
});

