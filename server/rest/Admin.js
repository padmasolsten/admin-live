'use strict';

var mysql = require('../db').pool;
var logger=require('./utilities/logger.js');
var url = require('url');
var file='admin.js';

var cryptojs=require('./utilities/cryptography.js'); //DV156-Encrypt Admin user password --------- start -------- 

exports.list = function(req, res){
    res.end('api user list');
};

exports.logout = function(req, res){
    var method='logout';
    var data = req.session.LoginID;
    req.session.LoginID = null;
    req.session.AuthenticatedCID=null;
    req.session.AuthenticatedCustomerID=null;
    req.session.TempleID=null;
    req.session.TempleName = null;
    req.session.AccessTypeID=null;
    res.end('[{"User":"api user logout"}]');
    logger.log('info','logout success',req.session.sessionID,file+'/'+method);
};

exports.Login=function(req,res){
    var method='Login';
    var url_parts=url.parse(req.body.PageURL,true);
    var host=url_parts.hostname;
    var wwwcheck=host.substring(0,2);
    if(wwwcheck.toUpperCase()=='WWW'){host=host.substring(4,host.length)}
    var loginPageHost=host;
        loginPageHost="admin."+loginPageHost; //remove this comment to run in local
	//DV156-Encrypt Admin user password --------- start --------
    // -- old code var sql="Select  useradmin.TempleID,  temple.TempleName,temple.Category,  useradmin.UserID,  useradmin.LoginID,  useradmin.FirstName,  temple.WebSite,  (Select    IfNull((Select      templeconfig.Value    From      templeconfig    Where      templeconfig.TempleID = useradmin.TempleID And      templeconfig.ConfigID = 15), 0)) As TypeID,  useradmin.LanguageID,  temple.CommunicationMailID As TempleEmail,IfNull((Select    templeimages.Url  From    templeimages  Where    templeimages.TempleID = temple.TempleID And    templeimages.TypeID = 3 and templeimages.LanguageID = 1  Limit 1), '/images/temple/uploads/thumbnail.jpg') As TempleIcon From  useradmin Inner Join  temple On useradmin.TempleID =    temple.TempleID And useradmin.LanguageID =    temple.LanguageID Where  useradmin.LoginID = '"+req.body.mail+"' And  useradmin.LoginPassword = '"+req.body.pass+"' And  useradmin.LanguageID = 1";
	var cryptpassword=cryptojs.EncryptString(req.body.pass);
	//var sql="Select  useradmin.TempleID,  temple.TempleName,temple.Category,  useradmin.UserID,  useradmin.LoginID,  useradmin.FirstName,  temple.WebSite,  (Select    IfNull((Select      templeconfig.Value    From      templeconfig    Where      templeconfig.TempleID = useradmin.TempleID And      templeconfig.ConfigID = 15), 0)) As TypeID,  useradmin.LanguageID,  temple.CommunicationMailID As TempleEmail,IfNull((Select    templeimages.Url  From    templeimages  Where    templeimages.TempleID = temple.TempleID And    templeimages.TypeID = 3 and templeimages.LanguageID = 1  Limit 1), '/images/temple/uploads/thumbnail.jpg') As TempleIcon From  useradmin Inner Join  temple On useradmin.TempleID =    temple.TempleID And useradmin.LanguageID =    temple.LanguageID Where  useradmin.LoginID = '"+req.body.mail+"' And  useradmin.LoginPassword = '"+req.body.pass+"' And  useradmin.LanguageID = 1";   
	//DV156-Encrypt Admin user password --------- end --------   
    var sql="Select  useradmin.TempleID,  temple.TempleName,temple.Category,  temple.registerno, useradmin.UserID,  useradmin.LoginID,  useradmin.FirstName,  temple.WebSite,  (Select    IfNull((Select      templeconfig.Value    From      templeconfig    Where      templeconfig.TempleID = useradmin.TempleID And      templeconfig.ConfigID = 15), 0)) As TypeID,  useradmin.LanguageID,  temple.CommunicationMailID As TempleEmail,IfNull((Select    templeimages.Url  From    templeimages  Where    templeimages.TempleID = temple.TempleID And    templeimages.TypeID = 3 and templeimages.LanguageID = 1  Limit 1), '/images/temple/uploads/thumbnail.jpg') As TempleIcon From  useradmin Inner Join  temple On useradmin.TempleID =    temple.TempleID And useradmin.LanguageID =    temple.LanguageID Where  useradmin.LoginID = '"+req.body.mail+"' And  useradmin.LoginPassword = '"+cryptpassword+"' And  useradmin.LanguageID = 1";
    logger.log('info','query :'+sql,req.session.sessionID,file+'/'+method);
    mysql.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('warn','',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var templeHost="admin."+rows[0].WebSite;
                console.log('RequestedHost : '+loginPageHost);
                console.log('templeHost : '+templeHost);
                //if(templeHost==loginPageHost)
                if(true)
                {
                    req.session.LoginID = rows[0].LoginID;
                    req.session.CustomerName=rows[0].FirstName;
                    req.session.UserID=rows[0].UserID;
                    req.session.TempleID=rows[0].TempleID;
                    req.session.Category=rows[0].Category;
                    req.session.TempleName=rows[0].TempleName;
                    req.session.TempleTypeID= rows[0].TypeID;
                    req.session.AccessTypeID= rows[0].AccessTypeID;
                    req.session.TempleEmail= rows[0].TempleEmail;
                    req.session.TempleIcon= rows[0].TempleIcon;
                    results= JSON.stringify(rows);
                    console.log(''+ results);
                    res.end(results);
                    logger.log('info',results,req.session.sessionID,file+'/'+method);
                }
                else
                {
                    logger.log('error','host not matching ;templeHost:'+templeHost+'- RequestedHost:'+loginPageHost,req.session.sessionID,file+'/'+method);
                    res.end( '[{ "RESULT" : "Domain not found", "RequestedHost": "'+loginPageHost+'", "templeHost": "'+templeHost+'"}]');
                }
            }
        });

    });

};





