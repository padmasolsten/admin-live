'use strict';

var db = require('../db').pool;
var logger=require('./utilities/logger.js');
var emailtemplate=require('../rest/emailtemplate.js');
var mail = require('./Send_Mail.js');
var file='Dashboard.js';
var imageurl='http://www.devaayanam.com/pages/template3/images/';
exports.loadDashboard=function(req, res) {
    var method='loadDashboard';
  var sql="call admin_load_dashboard('"+req.session.TempleID+"')";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};

exports.GetPujaReport=function(req,res){
    var method='GetPujaReport';
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
    var sql="call admin_puja_report('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+from+"','"+to+"')"
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};

exports.GetBookingReport=function(req,res){
    var method='GetBookingReport';
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
    var sql="call admin_booking_report('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+from+"','"+to+"',"+0+")" // last zero for txn id, proc changed to support admin app, to fetch by date range or single txn id
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                console.log(results);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};

exports.GetRevenueReport=function(req,res){
    var method='GetRevenueReport';
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
    var sql="call admin_revenue_report('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+from+"','"+to+"')"
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};

exports.GetPaymentReport=function(req,res){
    var method='GetPaymentReport';
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
    var sql="call admin_payment_report('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+from+"','"+to+"')"
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};

exports.GetApprovalReport=function(req,res){
    var method='GetApprovalReport';
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
    var sql="call admin_approval_report('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+from+"','"+to+"')"
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};

exports.GetPostalAddressReport=function(req,res){
        var method='GetPostalAddressReport';
        var from=req.body.from.substring(0,10);
        var to=req.body.to.substring(0,10);
        var sql="call admin_postaladdress_report('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+from+"','"+to+"')"
        logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
        db.getConnection(function(err, con){
            con.query(sql,function(err, rows, fields) {
                con.release();
                var results;
                if (err)
                {
                    logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                    res.end( '[[]]');
                    return;
                }
                if (rows.length == 0)
                {
                    logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                    res.end( '[[]]');
                }
                else
                {
                    results = JSON.stringify(rows);
                    res.end( results);
                    logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
                }
            });
        });
    };

exports.GetDonationReport=function(req,res){
    var method='GetDonationReport';
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
    var sql="call admin_donation_report('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+from+"','"+to+"')"
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};

exports.getdevoteelist=function(req,res){
    var method='getdevoteelist';
        console.log("session./+"+JSON.stringify(req.session));
    var sql="select FirstName,LastName,LoginID,PhoneMobile,customerimage from customer inner join address on address.AddressID=customer.AddressID inner join customerfavourites on customer.CustomerID= customerfavourites.CustomerID where customerfavourites.TempleID='"+req.session.TempleID+"';"
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};
exports.getreviews=function(req,res){
    var method='getreviews';
    console.log("SESSION.");
    console.log("SESSION.."+req.session.TempleID);
    if(req.body.type=='review') {
        var sql = "SELECT 'review' as type,`templecontent`.* FROM `devaayanam`.`templecontent` WHERE `templecontent`.ContenttypeID in (8) " +
            "and `templecontent`.`TempleID` = '" + req.session.TempleID + "' and templecontent.LanguageID=1 order by StartDate desc;"
    } else if(req.body.type=='downloads') {
        var sql = "SELECT 'downloads' as type, `templecontent`.* FROM `devaayanam`.`templecontent` WHERE `templecontent`.ContenttypeID in (9) " +
            "and `templecontent`.`TempleID` = '" + req.session.TempleID + "' and templecontent.LanguageID=1 order by StartDate desc;"
    }
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[]');   //DV-59
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[]'); //DV-59
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};
exports.updatereview=function(req,res){
    var method='updatereview';
//DV-59
    if(req.body.mode=='delete'&&req.body.type=='review') {
        var sql = "Delete FROM `devaayanam`.`templecontent` WHERE `templecontent`.ContenttypeID in (8) " +
            "and `templecontent`.`TempleID` = '" + req.session.TempleID + "' and templecontent.LanguageID=1 and templecontent.ContentID='" + req.body.ContentID + "';"
    } else if(req.body.mode=='update'&&req.body.type=='review') {
        var sql = "update `devaayanam`.`templecontent` set `templecontent`.MessageBody='"+req.body.MessageBody+"' where `templecontent`.ContenttypeID in (8) " +
            "and `templecontent`.`TempleID` = '" + req.session.TempleID + "' and templecontent.LanguageID=1 and templecontent.ContentID='" + req.body.ContentID + "';"
    }
    //DV-59
    //DV-10
    if(req.body.mode=='delete'&&req.body.type=='downloads') {
        var sql = "Delete FROM `devaayanam`.`templecontent` WHERE `templecontent`.ContenttypeID in (9) " +
            "and `templecontent`.`TempleID` = '" + req.session.TempleID + "' and templecontent.LanguageID=1 and templecontent.ContentID='" + req.body.ContentID + "';"
    } else if(req.body.mode=='update'&&req.body.type=='downloads') {
        var sql = "update `devaayanam`.`templecontent` set `templecontent`.MessageBody='"+req.body.MessageBody+"',`templecontent`.EndDate='"+req.body.EndDate+"' where `templecontent`.ContenttypeID in (9) " +
            "and `templecontent`.`TempleID` = '" + req.session.TempleID + "' and templecontent.LanguageID=1 and templecontent.ContentID='" + req.body.ContentID + "';"
    }
    //DV-10
        logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                var result={
                    result:''
                }
                res.end(results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};
exports.addreview=function(req,res){
    var method='addreview';
    console.log("session./+"+JSON.stringify(req.session));
    // var sql="Delete FROM `devaayanam`.`templecontent` WHERE `templecontent`.ContenttypeID in (8) " +
    //     "and `templecontent`.`TempleID` = '"+req.session.TempleID+"' and templecontent.LanguageID=1 and templecontent.ContentID='"+req.body.ContentID+"';"
    var sql="call writereview('"+req.session.TempleID+"','"+req.body.review+"','"+req.body.customername+"','"+req.body.customerimagepath+"','"+req.body.website+"')";
    console.log(sql);
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                console.log(err);
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                var result={
                    result:''
                }
                res.end(results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};
exports.sendnotification=function(req,res){
    console.log(JSON.stringify(req.body));
    var templename=req.session.TempleName;
    if(req.body.option=="mail" && req.body.mode=="one") {
        var template='<div class="col-md-12"align="center">'+templename+'</div><br><br><div style="padding-left: 70px;">'+req.body.message+'</div><br><br>';
        var newtemplate = '<!doctype html><html><head><meta charset="utf-8"><title>Untitled Document</title><link href= "https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"></head><body style="font-family: Open Sans, sans-serif; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;"><section class="header" style="width:100%; height:auto;"><table style="width:650px; margin:0px auto; border:0px; border-collapse:collapse;"><tr style="	width:650px; height:auto; text-align:center;"><td><img src="http://viralfever.in/dev/mail/images/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px;height:auto;background-image:url(http://viralfever.in/dev/mail/images/background.jpg);background-size:cover;text-align:left;height:auto;"><td><h5 style="font-weight:600; font-size:17px; color:#b74b03;  padding-left:25px;"></h5><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;"></p>' + template + '<p style="font-weight:400; font-size:14px; color:#2d2d2d; padding-left:70px; padding-right:25px; line-height:25px;">Visit <a href="#" style="color:#603; text-decoration:none;">www.devaayanam.in</a> to find the list of temples onboarded</p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">List of temples <a href="#" style="color:#603; text-decoration:none;">-http://www.devaayanam.in/List</a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;"></p><h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:20px;  padding-left:70px; padding-right:25px; line-height:25px;">Team Devaayanam</h4></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><TD><img src="http://viralfever.in/dev/mail/images/banner..jpg"/></TD></tr><tr style="width:650px;height:auto;margin:0px auto;background-color:#CCC;text-align:center;height:auto;"><td><h6 style="font-size:14px;padding-top:0px;padding-bottom:0px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';

        var emailinfo = {
            event: 'SENDNOTIFICATION',
            from: 'support@devaayanam.in',
            to: req.body.to,
            template: newtemplate
        };
        console.log("template.." + newtemplate);
        emailtemplate.prepare_mail_template(emailinfo);
        res.end("success");
    }
    if(req.body.option=="sms" && req.body.mode=="one") {
        var maildata = {
            mobileno: req.body.to,
            message: templename+"                       "+req.body.message
        }
        emailtemplate.sendsms(maildata,function(response){
            res.end(response);
        });
        res.end("success");
    }

    if(req.body.option=="mail" && req.body.mode=="all") {
        req.body.todata.forEach(function(row){
            var template='<div class="col-md-12"><span style="padding-left: 220px;size=25px;color=red"align="center">'+templename+'</span></div><br><br><div style="padding-left: 70px;">'+req.body.message+'</div><br><br>';
            var newtemplate = '<!doctype html><html><head><meta charset="utf-8"><title>Untitled Document</title><link href= "https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"></head><body style="font-family: Open Sans, sans-serif; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;"><section class="header" style="width:100%; height:auto;"><table style="width:650px; margin:0px auto; border:0px; border-collapse:collapse;"><tr style="	width:650px; height:auto; text-align:center;"><td><img src="http://viralfever.in/dev/mail/images/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px;height:auto;background-image:url(http://viralfever.in/dev/mail/images/background.jpg);background-size:cover;text-align:left;height:auto;"><td><h5 style="font-weight:600; font-size:17px; color:#b74b03;  padding-left:25px;"></h5><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;"></p>' + template + '<p style="font-weight:400; font-size:14px; color:#2d2d2d; padding-left:70px; padding-right:25px; line-height:25px;">Visit <a href="#" style="color:#603; text-decoration:none;">www.devaayanam.in</a> to find the list of temples onboarded</p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">List of temples <a href="#" style="color:#603; text-decoration:none;">-http://www.devaayanam.in/List</a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;"></p><h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:20px;  padding-left:70px; padding-right:25px; line-height:25px;">Team Devaayanam</h4></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><TD><img src="http://viralfever.in/dev/mail/images/banner..jpg"/></TD></tr><tr style="width:650px;height:auto;margin:0px auto;background-color:#CCC;text-align:center;height:auto;"><td><h6 style="font-size:14px;padding-top:0px;padding-bottom:0px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';

            var emailinfo = {
                event: 'SENDNOTIFICATION',
                from: 'support@devaayanam.in',
                to: row,
                template: newtemplate
            };
            console.log("template.." + newtemplate);
            emailtemplate.prepare_mail_template(emailinfo);
        })
        res.end("success");
    }
    if(req.body.option=="sms" && req.body.mode=="all") {
        req.body.todata.forEach(function(row) {
            var maildata = {
                mobileno: row,
                message: templename+"                       "+req.body.message
            }
            emailtemplate.sendsms(maildata, function (response) {
                res.end(response);
            });
        });
        res.end("success");
    }


};

exports.updatedevotee=function(req,res){
    console.log(JSON.stringify(req.body));
    if(req.body.option=="update") {
        var method='updatedevotee';
        var sql="update devaayanam.address set PhoneMobile='"+req.body.PhoneMobile+"' where AddressID=(select AddressID from devaayanam.customer where LoginID= '"+req.body.LoginID+"')";
        logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
        db.getConnection(function(err, con){
            con.query(sql,function(err, rows, fields) {
                con.release();
                var results;
                if (err)
                {
                    logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                    res.end( '[[]]');
                    return;
                }
                if (rows.length == 0)
                {
                    logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                    res.end( '[[]]');
                }
                else
                {
                    results = JSON.stringify(rows);
                    res.end( results);
                    logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
                }
            });
        });
    }
    if(req.body.option=="new") {

    }
};
exports.loaddashboard=function(req,res){
    var method='getdashboard';
    var sql ="call getdashboard()";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};