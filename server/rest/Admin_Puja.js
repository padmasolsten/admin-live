'use strict';

var db = require('../db').pool;
var nodemailer = require("nodemailer");
var cryptojs=require('./utilities/cryptography.js');
var logger=require('./utilities/logger.js');
var emailtemplate=require('./emailtemplate.js');
var prevDirName=__dirname;
prevDirName=prevDirName.substring(0,prevDirName.lastIndexOf('rest'));
var dbConfigFile = prevDirName + '/database.json';
var fs = require('fs');
var env='configuration';
var data = fs.readFileSync(dbConfigFile, 'utf8');
var dbConfig = JSON.parse(data)[env];
var email_provider=dbConfig.emailprovider;
var email_username=dbConfig.emailusername;
var email_password=cryptojs.DecryptString(dbConfig.emailpassword);
var file='admin_Puja.js';
//var emailtemplate=require('../modules/emailtemplate.js');
var imageurl='http://www.devaayanam.com/pages/template3/images/';
var PushOrderStatus=function(req,res,orderstatus)
{
    var method='PushOrderStatus';
    var sql="call push_order_status('"+orderstatus.TempleID+"','"+orderstatus.TrackingNo+"','"+orderstatus.StatusID+"','"+orderstatus.RefNo+"','"+orderstatus.Comments+"')"
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,'/'+method);
            }
        });
    });
};


exports.LoadPujaType=function(req,res) {
    var method='LoadPujaType';
	//DV203 - Get the common pujatypes under Dvnm (templeid 0) and those which are configured for the specific temple 
    var sql="select `pujatype`.`PujaTypeID` ,`pujatype`.`Type` FROM `devaayanam`.`pujatype` where `pujatype`.LanguageID='"+req.body.LanguageID+"'" +
				" and templeid = 0 and pujatypeid <> 0 " +
				" union " +
				" select `pujatype`.`PujaTypeID` ,`pujatype`.`Type` FROM `devaayanam`.`pujatype` where `pujatype`.LanguageID='"+req.body.LanguageID+"'" +
				" and templeid = " + req.session.TempleID + " ;" ;

    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.puja_check_update=function(req,res){

    var method='puja_check_update';
    var sql="SELECT `templeconfig`.`Value` FROM `devaayanam`.`templeconfig` where `templeconfig`.`TempleID`='"+req.session.TempleID+"' and `templeconfig`.`Key`='LAST_UPDATED_TIMESTAMP';";
    logger.log('info','Query :'+sql,req.session.sessionID,'TempleHomePage.js/CheckLastUpdatedDate ');
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            if( err ) {
                logger.log('error',err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var result = JSON.stringify(rows);
                res.end(result);
                logger.log('data',result,req.session.sessionID,file+'/'+method);
            }
        });


    });
};
exports.LoadPuja=function(req,res){
    var method='LoadPuja';
    console.log("....."+req.session.TempleID)
	//dv203
	/*
    var sql="SELECT  puja.*, pujarule.DisableOnlineBooking FROM `devaayanam`.`puja` " +
			 " left OUTER JOIN pujarule ON puja.templeid = pujarule.templeid and puja.pujaid = pujarule.pujaid " +
			" where `puja`.`TempleID`= '"+req.session.TempleID+"' and `puja`.`LanguageID`= '"+req.body.LanguageID+"'";
	var sql="SELECT  p.*, ifnull(r.DisableOnlineBooking,0) as DisableOnlineBooking " + 
			" FROM `devaayanam`.`puja` p " + 
			" left outer join pujarule r on r.TempleID = p.TempleID and r.PujaID = p.PujaID " +
			" where p.PujaTypeID = 0 and p.languageid = '" + req.body.LanguageID+"'"  + " and p.`TempleID`= '" + req.session.TempleID+"'";
	*/
			
	var sql= " select p.*, (select GROUP_CONCAT(distinct pujatypeid)  " +
			"				FROM puja t	where pujatypeid <> 0 " +
            "    		and p.templeid = t.TempleID and p.pujaid = t.pujaid and t.status = 1) as currentpujatags, " +
			" 		(select ifnull(r.DisableOnlineBooking,0) from pujarule r " +
            "  			where r.templeid = p.templeid and r.PujaID = p.pujaid ) as DisableOnlineBooking " +
			"		, d.DeityName, " +
			" (select GROUP_CONCAT(distinct ty.type) " +
			"	FROM pujatype ty " +	
			"	where ty.pujatypeid <> 0 " +
			"	and ty.pujatypeid in (select pujatypeid from puja where puja.pujaid = p.pujaid  " +
			"							and p.templeid = puja.templeid  " +
			"							and p.deityid = puja.deityid and puja.languageid = p.languageid and puja.status = 1)) as currentpujatagnames  " +
			" from puja p, deity d " +
			" where p.templeid = '" + req.session.TempleID+"'" +
			" and p.languageid = '" + req.body.LanguageID+"'"  + 
			" and p.pujatypeid = 0" +
			" and p.DeityID = d.DeityID and d.TempleID = p.TempleID and d.LanguageID = '" + req.body.LanguageID+"'";

    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "NO PUJA TO LIST"}]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });

    });
};
exports.LoadPujaByLanguage=function(req,res)
{
    var method='LoadPujaByLanguage';
    var sql="SELECT `puja`.`PujaName`,`puja`.`Desc` FROM `devaayanam`.`puja` Inner Join    `devaayanam`.`languages` On `devaayanam`.`puja`.`LanguageID` =    `devaayanam`.`languages`.`languages_id` where `puja`.`TempleID`= '"+req.session.TempleID+"' and `puja`.`PujaID`='"+req.body.PujaID+"' and `puja`.`LanguageID`='"+req.body.LanguageID+"'";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows) {
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : ""}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : " "}]');
            }
            else
            {
                var results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};
exports.PujaUpdate=function(req,res)
{
    var method='PujaUpdate';
    // DV199 16-Mar-2020, Allow puja type to be changed on update
	// DV203 - puja tags
	var sql="call admin_update_puja('"+req.session.TempleID+"','"+req.body.PujaID+"','"+req.body.DeityID+"','"+req.body.LanguageID+"','"+req.body.Desc+"','"+
			req.body.Amount+"','"+req.body.Status+"','"+req.body.EnableCount+"','"+req.body.PujaTags+"')";
    console.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows) {
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "Puja Updation  Failed "}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "Puja Updation  Failed "}]');
            }
            else
            {
                res.end( '[{ "RESULT" : "Puja Updated Successfully"}]');
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};
exports.AddSubPuja=function(req,res)
{
    var method='AddSubPuja';
    var sql="call subpuja('"+req.session.TempleID+"','"+req.body.PujaID+"','"+req.body.SubTypeName+"')";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows)
        {
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                res.end( '[]');
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

};
exports.AddPuja=function(req,res)
{
    var method='AddPuja';
    var sql="Call admin_add_puja('"+req.session.TempleID+"','"+req.body.DeityID+"','"+req.body.MasterPujaID+"','"+req.body.PujaTypeID+"','"+req.body.Amount+"','"+req.body.ListIndex+"', '"+req.body.Status+"', '"+req.body.EnableCount+"', '"+req.body.PujaTags+"')";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        console.log("............try"+sql)
        con.query(sql,function(err,rows)
        {
            con.release();
            if(err)
            {   console.log(".........err1")
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "unable to add new puja"},'+JSON.stringify(rows)+']');
                return
            }
            if (rows.length == 0)
            {   console.log(".........err2")
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "unable to add new puja"}]');
            }
            else
            {

                var result;
                result=JSON.stringify(rows);
                console.log(JSON.stringify(rows))
                res.end( '[{ "RESULT" : "New puja added"},'+JSON.stringify(rows)+']');
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};


exports.LoadPujaApproval=function(req,res)
{
    //req.body.from.imdkld(0,10);
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
    var method='LoadPujaApproval';
    console.log("cus.."+JSON.stringify(req.UserID));
    var sql="call admin_puja_approval_list('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+req.session.UserID+"','"+from+"','"+to+"')";
console.log(sql);
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });

    });
};
var schedule = require('node-schedule');
exports.autostatusupdate=function() {
    var method = 'autostatusupdate';

    var sql = "select * from templeconfig where ConfigID=27 and `Key`='ENABLE_AUTO_STATUS_UPDATE' and Value ='true';";
    console.log('info', 'Query :' + sql, '', file + '/' + method);
    db.getConnection(function (err, con) {
        con.query(sql, function (err, rows, fields) {
            con.release();
            var results;
			console.log('Temples for autostatus update' + JSON.stringify(rows));
            if (err) {
                console.log('error', 'Result :' + err,'', file + '/' + method);
                return;
            }
            if (rows.length != 0) {
                rows.forEach(function (row) {

                    var sql = "call admin_puja_pending_list('" + row.TempleID + "','" + 1 + "',"+0+")"; // admin app can see all txn or by single txn. admin report here will pass on 0 to ignore txn
                    console.log('info', 'Query :' + sql, '', file + '/' + method);
                    db.getConnection(function (err, con) {
                        con.query(sql, function (err, rows, fields) {
                            con.release();
                            var results;
                            if (err) {
                                logger.log('error', 'Result :' + err, '', file + '/' + method);
                                return;
                            }
                            if (rows.length == 0) {
                                logger.log('warn', 'Result:No match found for query', '', file + '/' + method);
                                return;
                            }
                            else {
                                results = JSON.stringify(rows);
                                rows[0].forEach(function(puja){
                                    console.log('p..'+puja.datepuja+","+puja.datestatus);
                                    if(puja.datepuja.toString()==puja.datestatus.toString()){
                                        console.log('pppppppp'+JSON.stringify(rows[1]));
                                        if(puja.PostalStatus==0){
                                            puja.PujaStatus=7;
                                        }else{
                                            puja.PujaStatus=1;
                                        }
										// -- DV-110 - 16-May-2019 
										// -- TypeError: Cannot read property 'Templename' of undefined
										//puja.TempleName=rows[2][0].TempleName
                                        puja.TempleName=rows[1][0].TempleName
                                        puja.TempleID=row.TempleID;
                                        exports.autochangepujastatus(puja);
                                    }
                                });
                                logger.log('data', 'Result:' + JSON.stringify(rows), '', file + '/' + method);
                                return;
                            }
                        });

                    });
                })
            }
        });
    });
}
var sss = schedule.scheduleJob('1 30 12 * * *', function() {
	var timeNow = new Date();
	console.log('start autostatus update job at '+ timeNow);
    console.log('a.p.b');
    exports.autostatusupdate();
	var timeNow = new Date();	
	console.log('end autostatus update job at '+ timeNow);	
});

exports.autochangepujastatus=function(puja)
{
    var method='autochangepujastatus';
    var d=new Date(puja.PujaDate)
    var PujaDate= d.getFullYear()+'-'+(((d.getMonth())*1)+(1*1)) +'-'+d.getDate();
    var sql="call admin_order_puja_status_change('"+puja.PujaStatus+"','"+puja.TrackingNo+"','"+puja.PujaID+"','"+PujaDate+"','"+puja.DevoteeName+"','"+puja.TempleID+"')"
    console.log('info','Query :'+sql,'',file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                console.log('error','Result :'+err,'',file+'/'+method);
                return;
            }
            else if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                return;
            }
            else
            {
                results = JSON.stringify(rows);
                var TempleAddress=rows[0][0];
                var email_subject,email_text,email_message;

//    get booking details from body
if(puja.PujaStatus==1){
    email_text='PUJA COMPLETED';
}else{
    email_text='PUJA COMPLETE POSTAGE COMPLETE';
}
                var DevoteeName=puja.DevoteeName;
                var PujaName=puja.PujaName;
                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var d = new Date(puja.PujaDate);
                var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                var TrackingNo=puja.TrackingNo;
//    end

//    get temple details from session

                var TempleName=puja.TempleName;
                var TempleHouseName=TempleAddress.HouseName;
                var TempleStreetName=TempleAddress.StreetName;
                var TemplePostOffice=TempleAddress.PostOffice;
                var TempleDistrict=TempleAddress.District;
                var TempleState=TempleAddress.State;
                var TemplePostalCode=TempleAddress.PostalCode;
                var TemplePhoneMobile=TempleAddress.PhoneMobile;
                var TemplePhoneWired=TempleAddress.PhoneWired;
                var TempleCommunicationMailID=TempleAddress.Email;

//    end




                if(puja.PujaStatus=='7')
                {
//      shipping completed
                    email_subject='Postal delivery has been completed for your puja,'+PujaName+' at '+TempleName+' for date '+PujaDate+'' ;
                    //DV-49
                    //var data={mobileno:req.body.mobile,message:email_subject}
					// -- DV-110 - 16-May-2019 
					// -- ReferenceError: req is not defined
					//var data={mobileno:req.body.mobile,message:email_subject}
					var data={mobileno:puja.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>postal delivery Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                    TempleName.toUpperCase()+
                    '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                    'PRASADAM SHIPMENT COMPLETED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                    TrackingNo+
                    '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                    'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                    'Prasadam shipment completed for the  puja '+PujaName+' for date '+PujaDate+' booked by you.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                    'You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p>' +
                    '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                    'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                    'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                    '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                    TempleName.toUpperCase() +' </span> Temple management</p></td></tr>'+
                    '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +

                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                    '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                    TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+TempleStreetName.toLowerCase()+',<br>'+TemplePostOffice.toLowerCase()+',<br>' +
                    TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                    +'</h6></td></tr>' +
                    '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +
                    '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>'
                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Shipping Completed</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}</style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Shipping Completed</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>Prasadam shipment completed for the  puja '+PujaName+' for date '+PujaDate+' booked by you.</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic"></p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic">You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr></table></td></tr><tr><td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr></table></td></tr></table></td></tr><tr><td height="20"></td></tr><tr><td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;"><p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a></p></td></tr><tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li></ul></td></tr></table></td></tr></table> </div></body></html>'

                }
                else if(puja.PujaStatus=='1')
                {
//      puja and if postal that also completed
                    email_subject='Your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+' has been completed ' ;
                    //DV-49
                    //var data={mobileno:req.body.mobile,message:email_subject}
					// -- DV-110 - 16-May-2019 
					// -- ReferenceError: req is not defined
					//var data={mobileno:req.body.mobile,message:email_subject}
					var data={mobileno:puja.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>Puja Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                    TempleName.toUpperCase()+
                    '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                    'PUJA COMPLETED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                    TrackingNo+
                    '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                    'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                    'Your puja '+PujaName+' for date '+PujaDate+' has been completed .</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                    'If you have opted for prasadam shipping by postal delivery,<br>normally it takes up to "one week" from the today onwards .<br>We will notify you about the shipping status by mail.</p>' +
                    '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                    'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                    'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                    '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                    TempleName.toUpperCase() +' </span> Temple management</p></td></tr>' +
                    '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +
                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                    '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                    TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+TempleStreetName.toLowerCase()+',<br>'+TemplePostOffice.toLowerCase()+',<br>' +
                    TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                    +'</h6></td></tr>' +
                    '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +

                    '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Pooja completed</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}    </style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2>    <tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td>    <table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr>    </table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Pooja completed</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>Your puja '+PujaName+' for date '+PujaDate+' has been completed .</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic">if you have opted for prasadam shipping by postal delivery,normally it takes up to "one week" from the today onwards .We will notify you about the shipping status by mail.</p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic">You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr>    </table>    </td></tr><tr><td>    <table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr>    </table></td></tr></table></td></tr>    <tr><td height="20"></td>    </tr>    <tr>    <td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;">    <p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a>    </p></td></tr><tr><td style="font-style: italic">    <ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li>    </ul></td></tr></table></td>    </tr></table> </div></body></html>'

                }

                if(true) {
                    var smtpTransport = nodemailer.createTransport("SMTP",{
                        service: email_provider,
                        auth: { user: email_username,
                            pass: email_password
                        }
                    });					
					var fromAddress="'\"" + TempleName +  "\"" + " <" + email_username + ">'"; //DV178 Dynamic display names for emails
                    var mailOptions = {
                        //from: email_username, // sender address
						from: fromAddress, // sender address
                        to: puja.CustomerEmail, // list of receivers
                        subject:email_subject, // Subject line
                        text: email_text, // plaintext body
                        html: email_message // html body

                    }
                    console.log(JSON.stringify(mailOptions));
                    smtpTransport.sendMail(mailOptions, function(error, response){
                        if(error){
                            logger.log('error','Result:'+error,'',file+'/'+method+'Email');
                        }
                        else{
                            logger.log('data','Result:'+response.message+'',file+'/'+method+'Email');
                        }
                    });
                }
                else
                {
                    logger.log('data','Result: Not need to send mail to customer','',file+'/'+method);
                }

            }
        });

    });
};
//DV161 - Postal status update ---------- start -----------
exports.PostalAction=function(req,res){
    var method='PostalAction';
	var puja = {
		PujaStatus: req.body.PujaStatus,
		TrackingNo: req.body.TrackingNo,
		PujaID: req.body.PujaID,
		PujaDate: req.body.PujaDate,
		DevoteeName: req.body.DevoteeName,
		TempleID: req.body.TempleID,
		PujaName:req.body.PujaName,
		DevoteeName:req.body.DevoteeName,
		TempleName:req.body.TempleName,
		mobile:req.body.mobile,
		CustomerEmail:req.body.CustomerEmail
	}
	console.log ("To call autochangepujastatus with Puja record -" +JSON.stringify(puja));
	// puja.PujaStatus+"','"+puja.TrackingNo+"','"+puja.PujaID+"','"+PujaDate+"','"+puja.DevoteeName+"','"+puja.TempleID+"')"    
	exports.autochangepujastatus(puja);
};
//DV161 - Postal status update ---------- end --------

exports.LoadIncompletedPuja=function(req,res){
    var method='Load InCompleted Puja';
    var sql="call admin_puja_pending_list('"+req.session.TempleID+"','"+req.body.LanguageID+"',"+0+")"; // admin app can see all txn or by single txn. admin report here will pass on 0 to ignore txn
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[[]]');
                return;
            }
            else
            {
                results = JSON.stringify(rows);
                res.end( results);
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
                return;
            }
        });

    });
};

exports.ApprovalActionOLD=function(req,res)
{
    var method='admin_order_approve';
    var sql="call admin_order_approve('"+req.session.TempleID+"','"+req.body.ApprovedPujaList+"','"+req.body.NotApprovedPujaList+"','"+req.body.CustomerID+"','"+req.body.TrackingNo+"','"+req.body.ReceiptNo+"' )";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            else if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                results = JSON.stringify(rows);
                var TempleAddress=rows[0][0];
                var DevoteeName=req.body.FirstName+''+req.body.LastName;
                var TrackingNo=req.body.TrackingNo;
                var ReceiptNo=req.body.ReceiptNo;
                var Message=req.body.ApprovalMessage;
                var TempleName=req.session.TempleName;
                var TempleHouseName=TempleAddress.HouseName;
                var TempleStreetName=TempleAddress.StreetName;
                var TemplePostOffice=TempleAddress.PostOffice;
                var TempleDistrict=TempleAddress.District;
                var TempleState=TempleAddress.State;
                var TemplePostalCode=TempleAddress.PostalCode;
                var TemplePhoneMobile=TempleAddress.PhoneMobile;
                var TemplePhoneWired=TempleAddress.PhoneWired;
                var TempleCommunicationMailID=TempleAddress.Email;
                if(req.body.ApprovedPujaListArray.length!=0){
                    var TemplateApproved;
                    //TemplateApproved='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Puja Approved</title><style type="text/css"> body, .header h1, .header h2, p{margin: 0; padding: 0;} .top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;} .header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;} .header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px} .header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;} h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif} h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif} h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;} p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;} /*li {}*/ h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;} .border{ background-color: #F2F2F2; border-bottom: 2px solid #444444; border-top: 2px solid #444444; border-right: 2px solid #444444; border-left: 2px solid #444444; margin-top: 5px; margin-bottom: 5px; margin-left: 5px; margin-right: 5px; /*color: white;*/ min-height: 11.693in; }</style></head><body><div class="border"><table width="100%" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td> <table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"bgcolor="E7EDF1" style="background-color: #F2F2F2"> <tr><td><table class="header" width="100%"> <tr> <td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName+'</h1> </td></tr></table></td> </tr> <tr><td></td> </tr> <tr><td><table width="100%" class="header"> <tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Puja Approved</h1></td> </tr></table></td> </tr> </table></td><tr><tr><td> <table class="main" align="center" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF"> <tr><td><table bgcolor="F8D8DF" width="95%" class="header"> <tr> <td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b> </td> </tr> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 0px;padding-left: 30px">Congratulations !! Following Puja request by you is approved. </td> </tr> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 0px;padding-left: 30px"><b>Tracking No:'+TrackingNo+'</b> </td> </tr> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>For payment please transfer the money to bank detailsor pay at temple mentioning this booking id. Please make the paymentwithin 15 days post which we will release the bookingPlease send a mail to <b>contact@devaayanam.in</b> with the details of money transfer along with booking and puja id in that mail. <br><p>'+Message+'</p></td> </tr></table></td> </tr> <tr><td><TABLE WIDTH=65% BORDER=1 CELLPADDING=0 CELLSPACING=0 style="margin-left: 30px"> <TR> <TH COLSPAN=5 align="center" bgcolor="88CFE0" WIDTH=64% HEIGHT=26>ReceiptNo : '+ReceiptNo+' </TH></TR> <TR bgcolor="F0FBE3"><th width=30%><p>Pooja</p> </th> <th width=30%><p>Beneficiary</p> </th> <th width=20%><p>Date</p> </th> <th width=10%><p>Amount</p> </th> <th width=10%><p>Shipment</p> </th> </TR>';
                    TemplateApproved='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Puja Approved</title><style type="text/css"> body, .header h1, .header h2, p{margin: 0; padding: 0;} .top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;} .header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;} .header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px} .header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;} h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif} h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif} h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;} p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;} /*li {}*/ h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;} .border{ background-color: #F2F2F2; border-bottom: 2px solid #444444; border-top: 2px solid #444444; border-right: 2px solid #444444; border-left: 2px solid #444444; margin-top: 5px; margin-bottom: 5px; margin-left: 5px; margin-right: 5px; /*color: white;*/ min-height: 11.693in; }</style></head><body><div class="border"><table width="100%" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td> <table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"bgcolor="E7EDF1" style="background-color: #F2F2F2"> <tr><td><table class="header" width="100%"> <tr> <td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName+'</h1> </td></tr></table></td> </tr> <tr><td></td> </tr> <tr><td><table width="100%" class="header"> <tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Puja Approved</h1></td> </tr></table></td> </tr> </table></td><tr><tr><td> <table class="main" align="center" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF"> <tr><td><table bgcolor="F8D8DF" width="95%" class="header"> <tr> <td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b> </td> </tr> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 0px;padding-left: 30px">Congratulations !! Following Puja requested by you is approved. </td> </tr> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 0px;padding-left: 30px"><b>Tracking No:'+TrackingNo+'</b> </td> </tr> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Complete your booking please proceed with your payment by Logging in to temple website. Once you login please visit "user area" , check for your transaction status and proceed with online payment. Alternatively you can pay at temple mentioning this booking id. Please make the payment within 15 days post which we will release the booking. Please send a mail to <b>contact@devaayanam.in</b> with payment details along with booking id and puja details in that mail. <br><p>'+Message+'</p></td> </tr></table></td> </tr> <tr><td><TABLE WIDTH=65% BORDER=1 CELLPADDING=0 CELLSPACING=0 style="margin-left: 30px"> <TR> <TH COLSPAN=5 align="center" bgcolor="88CFE0" WIDTH=64% HEIGHT=26>ReceiptNo : '+ReceiptNo+' </TH></TR> <TR bgcolor="F0FBE3"><th width=30%><p>Puja</p> </th> <th width=30%><p>Beneficiary</p> </th> <th width=20%><p>Date</p> </th> <th width=10%><p>Amount</p> </th> <th width=10%><p>Shipment</p> </th> </TR>';
                    req.body.ApprovedPujaListArray.forEach(function(puja){
                        var Postal='No';
                        var DT=new Date(puja.PujaDate);
                        var PujaDate=DT.getDate() +"-"+(+DT.getMonth()+1)+"-"+DT.getFullYear()
                        if(puja.PostalStatus=='0'){Postal='Yes';}
                        var amount= +puja.Rate + +puja.PostalCharge;
                        amount=amount* +puja.Quantity;
                        TemplateApproved+='<TR bgcolor=e0eaf0> <TD align=left><p>'+puja.PujaName+'</p></TD> <TD align=left><p>'+puja.DevoteeName+'</p></TD> <TD align=center><p>'+PujaDate+'</p></TD> <TD align=right><p>'+amount+'.00</p></TD> <TD align=center><p>'+Postal+'</p> </TD> </TR>'
                    });
                    TemplateApproved+='</TABLE></td> </tr> <tr><td><table bgcolor="F8D8DF" width="95%" class="header"> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"><p>Please provide your booking id for any future correspondences.</p> </td> </tr> <tr> <td width="95%" style="padding-top: 0px; padding-bottom: 5px;padding-left: 30px"><p>You can also see the status of this booking in your “user area” on the website.</p> </td> </tr> <tr> <td width="95%" style="padding-top: 20px;padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessingsin your life,</p> </td> </tr> <tr> <td width="95%" style="padding-bottom: 10px;padding-left: 30px"><p>'+TempleName+'Temple management</p> </td> </tr> <tr> <td width="95%" style="padding-bottom: 5px;padding-left: 30px"><b>Contact Details:</b><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address> </td> </tr></table></td> </tr> </table></td></tr><tr><td height="20"></td></tr><tr><td> <table class="top-message" cellpadding="0" cellspacing="0" width="100%"> <tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td> </tr> </table> <table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"> <tr><td align="center" style="padding-top: 10px;"><p> <a href="#"> Terms and Conditions</a> | <a href="#">Privacy Policy </a>| <a href="#">Contact Us</a></p></td> </tr> <tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"> <li> Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in. </li></ul></td> </tr> </table></td></tr></table></div></body></html>';
                    var smtpTransport = nodemailer.createTransport("SMTP",{
                        service: email_provider,
                        auth: {user: email_username, pass: email_password }
                    });
                    var mailOptions = {
                        from: email_username, // sender address
                        to: req.body.Email, // list of receivers
                        subject:'Devaayanam Puja Approved', // Subject line
                        text: Message, // plaintext body
                        html: TemplateApproved // html body              }
                    }
                    smtpTransport.sendMail(mailOptions, function(error, response){
                        if(error){logger.log('error','Result:'+error,req.session.UserID,file+'/'+method+'/Email');}
                        else{ logger.log('data','Result:'+response.message,req.session.UserID,file+'/'+method+'/Email');}
                    });
                }
                if(req.body.NotApprovedPujaListArray.length!=0){
                    var TemplateNoApproved;
                    TemplateNoApproved='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Puja Not Approved</title><style type="text/css"> body, .header h1, .header h2, p{margin: 0; padding: 0;} .top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;} .header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;} .header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px} .header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;} h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif} h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif} h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;} p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;} /*li {}*/ h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;} .border{ background-color: #F2F2F2; border-bottom: 2px solid #444444; border-top: 2px solid #444444; border-right: 2px solid #444444; border-left: 2px solid #444444; margin-top: 5px; margin-bottom: 5px; margin-left: 5px; margin-right: 5px; /*color: white;*/ min-height: 11.693in; }</style></head><body><div class="border"><table width="100%" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td> <table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"bgcolor="E7EDF1" style="background-color: #F2F2F2"> <tr><td><table class="header" width="100%"> <tr> <td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName+'</h1> </td></tr></table></td> </tr> <tr><td></td> </tr> <tr><td><table width="100%" class="header"> <tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Puja Not Approved</h1></td> </tr></table></td> </tr> </table></td><tr><tr><td> <table class="main" align="center" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF"> <tr><td><table bgcolor="F8D8DF" width="95%" class="header"> <tr> <td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b> </td> </tr> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 0px;padding-left: 30px">We regret to inform you that following Puja request made by you could not be approved. </td> </tr> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 0px;padding-left: 30px"><b>Tracking No:'+TrackingNo+'</b> </td> </tr> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>'+Message+'</p> </td> </tr></table></td> </tr> <tr><td><TABLE WIDTH=65% BORDER=1 CELLPADDING=0 CELLSPACING=0 style="margin-left: 30px"> <TR> <TH COLSPAN=5 align="center" bgcolor="88CFE0" WIDTH=64% HEIGHT=26>ReceiptNo : '+ReceiptNo+' </TH></TR> <TR bgcolor="F0FBE3"><th width=30%><p>Puja</p> </th> <th width=30%><p>Beneficiary</p> </th> <th width=20%><p>Date</p> </th> <th width=10%><p>Amount</p> </th> <th width=10%><p>Shipment</p> </th> </TR>';
                    req.body.NotApprovedPujaListArray.forEach(function(puja){
                        var Postal='No';
                        var DT=new Date(puja.PujaDate);
                        var PujaDate=DT.getDate() +"-"+(+DT.getMonth()+1)+"-"+DT.getFullYear()
                        if(puja.PostalStatus=='0'){Postal='Yes';}
                        var amount= +puja.Rate + +puja.PostalCharge;
                        amount=amount* +puja.Quantity;
                        TemplateNoApproved+='<TR bgcolor=e0eaf0> <TD align=left><p>'+puja.PujaName+'</p></TD> <TD align=left><p>'+puja.DevoteeName+'</p></TD> <TD align=center><p>'+PujaDate+'</p></TD> <TD align=right><p>'+amount+'</p></TD> <TD align=center><p>'+Postal+'</p> </TD> </TR>'
                    });

                    TemplateNoApproved+='</TABLE></td> </tr> <tr><td><table bgcolor="F8D8DF" width="95%" class="header"> <tr> <td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"><p>Please provide your booking id for any future correspondences.</p> </td> </tr> <tr> <td width="95%" style="padding-top: 0px; padding-bottom: 5px;padding-left: 30px"><p>You can also see the status of this booking in your “user area” on the website.</p> </td> </tr> <tr> <td width="95%" style="padding-top: 20px;padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessingsin your life,</p> </td> </tr> <tr> <td width="95%" style="padding-bottom: 10px;padding-left: 30px"><p>'+TempleName+'Temple management</p> </td> </tr> <tr> <td width="95%" style="padding-bottom: 5px;padding-left: 30px"><b>Contact Details:</b><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address> </td> </tr></table></td> </tr> </table></td></tr><tr><td height="20"></td></tr><tr><td> <table class="top-message" cellpadding="0" cellspacing="0" width="100%"> <tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td> </tr> </table> <table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"> <tr><td align="center" style="padding-top: 10px;"><p> <a href="#"> Terms and Conditions</a> | <a href="#">Privacy Policy </a>| <a href="#">Contact Us</a></p></td> </tr> <tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"> <li> Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in. </li></ul></td> </tr> </table></td></tr></table></div></body></html>';
                    var smtpTransport2 = nodemailer.createTransport("SMTP",{
                        service: email_provider,
                        auth: {user: email_username, pass: email_password }
                    });
                    var mailOptionsNotApproved = {
                        from: email_username, // sender address
                        to: req.body.Email, // list of receivers
                        subject:'Devaayanam Puja Not Approved', // Subject line
                        text: Message, // plaintext body
                        html: TemplateApproved // html body              }
                    }
                    console.log (JSON.stringify(mailOptionsNotApproved));
                    smtpTransport2.sendMail(mailOptionsNotApproved, function(error, response){
                        if(error){logger.log('error','Result:'+error,req.session.UserID,file+'/'+method+'Email');}
                        else{ logger.log('data','Result:'+response.message,req.session.UserID,file+'/'+method+'Email');}
                    });
                }
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

    if (req.body.NotApprovedPujaList != 0) {
        var orderstatusjson = {
            TempleID: req.session.TempleID,
            TrackingNo: req.body.TrackingNo,
            StatusID:3,
            RefNo: req.body.ReceiptNo,
            Comments: req.body.ApprovalMessage+ 'puja list : '+req.body.NotApprovedPujaList
        }
        PushOrderStatus(req, res, orderstatusjson);
    }

    if (req.body.ApprovedPujaList != 0) {
        var orderstatusjson = {
            TempleID: req.session.TempleID,
            TrackingNo: req.body.TrackingNo,
            StatusID:11,
            RefNo: req.body.ReceiptNo,
            Comments: req.body.ApprovalMessage + 'puja list : '+req.body.ApprovedPujaList
        }
        PushOrderStatus(req, res, orderstatusjson);
    }
};
function jsUcfirst(p_string) 
{
	return p_string.charAt(0).toUpperCase() + p_string.slice(1);
};
exports.ApprovalAction=function(req,res)
{var method='admin_order_approve';
    console.log('r..'+JSON.stringify(req.body));
    var sql="call approvalaction('"+req.body.RequestID+"','"+req.body.ApprovalMessage+"','"+req.body.ApprovalStatus+"','"+req.session.TempleID+"')";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                console.log(err);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            else if (rows.length == 0)
            {
                console.log('no match');
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else {
                console.log('cost'+req.body.itemCost);
                if(req.body.itemCost){
                    if(req.body.itemCost>0){
                        db.getConnection(function(err, con) {
                            var sql2="call admin_add_temple_shippingcharge('"+req.session.TempleID+"','"+req.body.itemCost+"','','"+req.body.PujaID+"')";
                            console.log(sql2);
                            con.query(sql2, function (err, rows, fields) {
                                con.release();
                                var results;
                                if (err) {
                                    console.log('itemCost error'+err);
                                    return;
                                }
                            });
                        });

                    }
                }
                console.log('ok');
                if (rows.affectedRows != 0) {
                    var TempleAddress=rows[1][0];
                    var TempleName=req.session.TempleName;
                    var TempleHouseName=TempleAddress.HouseName;
                    var TempleStreetName=TempleAddress.StreetName;
                    var TemplePostOffice=TempleAddress.PostOffice;
                    var TempleDistrict=TempleAddress.District;
                    var TempleState=TempleAddress.State;
                    var TemplePostalCode=TempleAddress.PostalCode;
                    var TemplePhoneMobile=TempleAddress.PhoneMobile;
                    var TemplePhoneWired=TempleAddress.PhoneWired;
                    var TempleCommunicationMailID=TempleAddress.Email;
                    if (rows[0][0].Status == 1) {
                        var Message = ' ';
                        var TemplateApproved;
                        if (req.body.ApprovalMessage != null && req.body.ApprovalMessage != undefined) {
                            Message = req.body.ApprovalMessage;
                        }
                        //new template
                        var message = 'puja booking received';
                        var subject = "";
                        var content = '';
                        var ItemsContent = '';
                        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        console.log('Puja Booking Approval..' + JSON.stringify(rows[0][0].CustomerID));
                        var customer_name = rows[0][0].FirstName + ' ' + rows[0][0].LastName;
						var customer_mobile = rows[0][0].PhoneMobile;
                        var enCustomerId = cryptojs.EncryptString('' + rows[0][0].CustomerID);
                        enCustomerId = enCustomerId.replace(/\//g, "110111");
                        var enRequestId = cryptojs.EncryptString('' + rows[0][0].RequestID);
                        enRequestId = enRequestId.replace(/\//g, "110111");
                        if (req.body.category == 'priest') {
                            content += '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                            content += TempleName;
                            content += '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>PUJA REQUEST APPROVED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">RequestID:<span style="font-weight:700; color:#000;">';
                            content += rows[0][0].RequestID;
                            content += '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                            content += 'Dear ' + customer_name + ' ';
                            content += ',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"> Congratulations! Following puja requested by you is approved by the priest '+TempleName.toUpperCase()+'.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                            '</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"> ' + Message + '</p><table border="3px " bordercolor="#7b0b0b" style="width:98%;margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="3" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Request ID : ';
                            content += rows[0][0].RequestID + ' ';
                            content += '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Puja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Location</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td></tr>';
                            var shipment_stats_for_booking = 0;
                            var Shipment = '-';
                            var d = new Date(rows[0][0].PujaDate);
                            var PujaDate = d.getDate() + '-' + monthNames[d.getMonth()] + '-' + d.getFullYear();
                            if (rows[0][0].PostalStatus == '0') {
                                Shipment = 'included';
                                shipment_stats_for_booking = 1
                            }
                            ItemsContent += '<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].PujaName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].DevoteeName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + PujaDate + '</td></tr>';
                            content += ItemsContent;
                            content += '</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">You can track the status of your booking in your user profile.</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"> <span style="color:#b74b03;">';
                            content += '</span></p>' +
                            //'<p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;">Choose Book with puja items if you want Pandit to arrange for the puja items.</p>' +
                            '</td></tr>' +
                            '    <tr style="text-align:center;padding-bottom: 20px;"><td><a href="http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + '/1"><button style=" margin-right:5px;background-color: #4CAF50; /* Green */border: none;color: white;padding: 9px 12px;text-align: center;text-decoration: none;display: inline-block;margin-bottom: 10px;font-size: 16px;">Book with puja items</button></a><a href="http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + '/0"><button style=" background-color: #4CAF50; /* Green */border: none;color: white;padding: 9px 12px;text-align: center;text-decoration: none;display: inline-block;margin-bottom: 10px;font-size: 16px;">Book without puja items</button></a></td></tr>';
                            content +=' <tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>';
                            content += '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                            content += TempleName.toUpperCase() + ',<br>' + TempleHouseName.toLowerCase() + ',' + TempleStreetName.toLowerCase() + ',<br>' + TemplePostOffice.toLowerCase() + ',<br>'
                            content += TempleDistrict.toLowerCase() + ',<br>' + TempleState.toLowerCase() + ',<br>' + TemplePostalCode + ',<br>' + TemplePhoneMobile + ',' + TemplePhoneWired + '.<br>' + TempleCommunicationMailID.toLowerCase();
                            content += '</h6></td></tr>' +
                            '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                        } else {
							//DV-117 - Template changes
							//The link to pay for puja should be prominent (bigger font, separate line)
							//24 hours time line message should be prominent
                            content += '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                            content += TempleName;
                            content += '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>PUJA REQUEST APPROVED <hr style="width:10%; border:solid #C00 1px; margin-top:10px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:250px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">RequestID:<span style="font-weight:700; color:#000;">';
                            content += rows[0][0].RequestID;
                            content += '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                            content += 'Dear ' + customer_name + ',';
                            content += '</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">Congratulations !! Following Puja requested by you is approved.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:4px;padding-left:70px;padding-right:2px;line-height:20px;">' +
                            'For complete your booking please proceed with your payment by <br> <span style="font-weight:800;font-size:18px;"><a href="http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + '">clicking here</a>.</p>';
							content += '<p style="font-weight:800;font-size:14px;color:#2d2d2d;padding-top:4px;padding-left:70px;padding-right:2px;line-height:12px;">Please make the payment within 2 days post which we will release the booking.</p>' +
										'<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"> </p>' + '<table border="3px " bordercolor="#7b0b0b" style="margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="4" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Request ID : ';
                            content += rows[0][0].RequestID + ' ';
                            content += '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Puja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Beneficiary</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td><td class="shipment" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Shipment</td></tr>';

                            var shipment_stats_for_booking = 0;
                            var Shipment = '-';
                            var d = new Date(rows[0][0].PujaDate);
                            var PujaDate = d.getDate() + '-' + monthNames[d.getMonth()] + '-' + d.getFullYear();

                            if (rows[0][0].PostalStatus == '0') {
                                Shipment = 'included';
                                shipment_stats_for_booking = 1
                            }
                            ItemsContent += '<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].PujaName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].DevoteeName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + PujaDate + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + Shipment + '</td></tr>';


                            content += ItemsContent;
                            content += '</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">You can track the status of your booking in your “user area” on temple website.</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">May <span style="color:#b74b03;">';
                            content += TempleName;
                            content += '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">';
                            content += TempleName.toUpperCase() + ' </span> Temple management</p></td></tr>';
                            content += '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is an online platform for temples. For complete list of temples in Devaayanam visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' ;
                            // content+='<tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                            content += '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                            content += TempleName.toUpperCase() + ',<br>' + jsUcfirst(TempleHouseName) + ',' + jsUcfirst(TempleStreetName) + ',<br>' + jsUcfirst(TemplePostOffice) + ',<br>'
                            content += jsUcfirst(TempleDistrict) + ',<br>' + jsUcfirst(TempleState) + ',<br>' + TemplePostalCode + ',<br>' + TemplePhoneMobile + ',' + TemplePhoneWired + '.<br>' + TempleCommunicationMailID.toLowerCase();
                            content += '</h6></td></tr>' +
                            '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                        }
                        console.log(content);
                        //ene template
                        console.log(req.body.filepath);
                        if (rows[0][0].Status == 1 && req.body.filepath != null && req.body.filepath != undefined && req.body.filepath != '') {
                            fs.readFile(__dirname + '/../../../ordering/client/' + req.body.filepath, function (err, data) {
                                console.log(err);
                                if (err) {
                                    console.log(err);
                                } else {
                                    var mailer = require('nodemailernew');
                                    var smtpTransport = mailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                            user: email_username,
                                            pass: email_password
                                        }
                                    });
									var mailto;
									mailto = rows[0][0].LoginID+ ', ' + rows[1][0].Email;
									console.log ('Copy temple on puja action - ' + mailto);
									var fromAddress="'\"" + TempleName +  "\"" + " <" + email_username + ">'"; //DV178 Dynamic display names for emails
                                    var mailOptions = {
                                        //from: email_username, // sender address
										from: fromAddress, // sender address
                                        to: rows[0][0].LoginID+ ', ' + rows[1][0].Email, // list of receivers // DV191 copy temple on puja approval/rejects
                                        subject: 'Puja request approved', // Subject line
                                        attachments: [{'filename': 'attachement1.jpg', 'content': data}],
                                        html: content // html body              }
                                    }
                                    console.log(mailOptions);
                                    smtpTransport.sendMail(mailOptions, function (error, response) {
                                        if (error) {
                                            console.log('e..' + error);
                                            logger.log('error', 'Result:' + error, req.session.UserID, file + '/' + method + '/Email');
                                        }
                                        else {
                                            console.log('s..' + error + response.message);
                                            logger.log('data', 'Result:' + response.message, req.session.UserID, file + '/' + method + '/Email');
                                        }
                                    });
									// DV154 - SMS to Devotee on all approval action -- start ------- approved -------
									console.log('SMS for Puja Booking Approval..' + customer_name);
				                    var smsinfo={
										mobileno:customer_mobile,
										message:'Dear ' + customer_name + ', Congratulations !! Puja requested by you at ' + TempleName + ' is approved. ' +
											' To complete your booking please proceed with your payment by ' + 
											'http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + ' clicking here.' +
											'. Please make the payment within 2 days post which we will release the booking. Thank You'									
									};
									emailtemplate.sendsms(smsinfo, function(res){});
									// DV154 - SMS to Devotee on all approval action -- start ------- approved -------									
                                }
                            });
                        } else {
                            var mailer = require('nodemailernew');
                            var smtpTransport = mailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: email_username,
                                    pass: email_password
                                }
                            });
							var fromAddress="'\"" + TempleName +  "\"" + " <" + email_username + ">'"; //DV178 Dynamic display names for emails							
                            var mailOptions = {								
                                //from: email_username, // sender address
								from: fromAddress, // sender address								
                                to: rows[0][0].LoginID+ ', ' + rows[1][0].Email, // list of receivers // DV191 copy temple on puja approval/rejects
                                subject: 'Puja request approved', // Subject line
                                // text: Message, // plaintext body
                                html: content // html body              }
                            }
                            console.log(mailOptions); 
                            smtpTransport.sendMail(mailOptions, function (error, response) {
                                if (error) {
                                    console.log('e..' + error);
                                    logger.log('error', 'Result:' + error, req.session.UserID, file + '/' + method + '/Email');
                                }
                                else {
                                    console.log('s..' + error + response.message);
                                    logger.log('data', 'Result:' + response.message, req.session.UserID, file + '/' + method + '/Email');
                                }
                            });
							// DV154 - SMS to Devotee on all approval action -- start ------- approved -------
							console.log('SMS for Puja Booking Approval (last else)..' + customer_name);
							var smsinfo={
								mobileno:customer_mobile, 
								message:'Dear ' + customer_name + ', Congratulations !! Puja requested by you at ' + TempleName + ' is approved. ' +
										' To complete your booking please proceed with your payment by ' + 
										'http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + ' clicking here.' +
										' Please make the payment within 2 days post which we will release the booking. Thank You'
							};
							emailtemplate.sendsms(smsinfo, function(res){});
							// DV154 - SMS to Devotee on all approval action -- start ------- approved -------									
                        }
                    } else {
                        var Message=' ';
                        if(req.body.ApprovalMessage!=null && req.body.ApprovalMessage!=undefined) {
                            Message = req.body.ApprovalMessage;
                        }
                        //new template
                        var message='request puja approved';
                        var subject="";
                        var content='';
                        var ItemsContent='';
                        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        if(req.body.category=='priest') {
                            var customer_name = rows[0][0].FirstName + ' ' + rows[0][0].LastName;
                            content += '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                            content += TempleName;
                            content += '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>PUJA REQUEST NOT APPROVED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">RequestID:<span style="font-weight:700; color:#000;">';
                            content += rows[0][0].RequestID;
                            content += '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                            content += 'Dear ' + customer_name + ' ';
                            content += ',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">We regret to inform you that following Puja request made by you could not be approved.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;">   ' + Message + '  </p><table border="3px " bordercolor="#7b0b0b" style="margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="4" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Request ID : ';
                            content += rows[0][0].RequestID + ' ';
                            content += '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Puja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Beneficiary</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td><td class="shipment" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Shipment</td></tr>';

                            var shipment_stats_for_booking = 0;
                            var Shipment = '-';
                            var d = new Date(rows[0][0].PujaDate);
                            var PujaDate = d.getDate() + '-' + monthNames[d.getMonth()] + '-' + d.getFullYear();

                            if (rows[0][0].PostalStatus == '0') {
                                Shipment = 'included';
                                shipment_stats_for_booking = 1
                            }
                            ItemsContent += '<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].PujaName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].DevoteeName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + PujaDate + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + Shipment + '</td></tr>';


                            content += ItemsContent;
                            content += '</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">';
                            //content += TempleName;
                            content += '</p><p style="font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:20px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">';
                            content +='If you want Devaayanam to help arrange for a priest, please give your details <a href="http://priests.devaayanam.in/devaayanam">here</a> and we will reach out to you</span></p></td></tr>';
                            content +='<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>';
                            // content+=<tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                            content += '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                            content += TempleName.toUpperCase() + ',<br>' + TempleHouseName.toLowerCase() + ',' + TempleStreetName.toLowerCase() + ',<br>' + TemplePostOffice.toLowerCase() + ',<br>'
                            content += TempleDistrict.toLowerCase() + ',<br>' + TempleState.toLowerCase() + ',<br>' + TemplePostalCode + ',<br>' + TemplePhoneMobile + ',' + TemplePhoneWired + '.<br>' + TempleCommunicationMailID.toLowerCase();
                            content += '</h6></td></tr>' +
                            '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                        } else {
                            var customer_name=rows[0][0].FirstName+' '+rows[0][0].LastName;
							var customer_mobile =rows[0][0].PhoneMobile;
                            content+= '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                            content+= TempleName;
                            content+= '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>PUJA REQUEST NOT APPROVED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">RequestID:<span style="font-weight:700; color:#000;">';
                            content+= rows[0][0].RequestID ;
                            content+= '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                            content+= 'Dear '+customer_name+' ';
                            content+= ',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">We regret to inform you that following Puja request made by you could not be approved.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;">   '+Message+'  </p><table border="3px " bordercolor="#7b0b0b" style="margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="4" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Request ID : ';
                            content+= rows[0][0].RequestID  +' ';
                            content+= '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Puja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Beneficiary</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td><td class="shipment" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Puja Items</td></tr>';

                            var  shipment_stats_for_booking=0;
                            var Shipment='-';
                            var d = new Date(rows[0][0].PujaDate);
                            var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                            if(rows[0][0].PostalStatus=='0'){Shipment='included';  shipment_stats_for_booking=1}
                            ItemsContent+='<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+rows[0][0].PujaName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+rows[0][0].DevoteeName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+PujaDate+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+Shipment+'</td></tr>';
                            content+=ItemsContent;

                            content+='</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">May <span style="color:#b74b03;">';
                            content+= TempleName;
                            content+='</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">';
                            content+= TempleName.toUpperCase() +' </span> Temple management</p></td></tr>';
                            content+='<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' ;
                            // content+=<tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                            content+='<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                            content+=TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+TempleStreetName.toLowerCase()+',<br>'+TemplePostOffice.toLowerCase()+',<br>'
                            content+=TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase();
                            content+='</h6></td></tr>' +
                            '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                        }
                        console.log(content);
                        console.log(TemplateApproved);
                        //ene template

                        var mailer = require('nodemailernew');
                        var smtpTransport = mailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: email_username,
                                pass: email_password
                            }
                        });
						var fromAddress="'\"" + TempleName +  "\"" + " <" + email_username + ">'"; //DV178 Dynamic display names for emails						
                        var mailOptions = {
                            //from: email_username, // sender address
							from: fromAddress, // sender address	
                            to: rows[0][0].LoginID + ', ' + rows[1][0].Email, // list of receivers //DV191 copy temple on puja approval/rejects
                            subject: 'Puja request not approved', // Subject line
                            // text: Message, // plaintext body
                            html: content // html body              }
                        }
                        console.log(mailOptions);
                        smtpTransport.sendMail(mailOptions, function (error, response) {
                            if (error) {
                                logger.log('error', 'Result:' + error, req.session.UserID, file + '/' + method + '/Email');
                            }
                            else {
                                logger.log('data', 'Result:' + response.message, req.session.UserID, file + '/' + method + '/Email');
                            }
                        });
						// DV154 - SMS to Devotee on all approval action -- start ------- Rejected/Cancelled -------
						console.log('SMS for Puja Booking Rejected/Cancelled..' + customer_name);
				        var smsinfo={
							mobileno:customer_mobile,
							message:'Dear ' + customer_name +', We regret to inform you that Puja request made by you at ' + TempleName + 
									' could not be approved. Request ID-' + rows[0][0].RequestID +
									'. ' + Message								
						};
						emailtemplate.sendsms(smsinfo, function(res){});
						// DV154 - SMS to Devotee on all approval action -- end ------- Rejected/Cancelled -------									
                    }
                }
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

};
exports.ChangePujaStatus=function(req,res)
{
    var method='admin_order_puja_status_change';
    var sql="call admin_order_puja_status_change('"+req.body.PujaStatus+"','"+req.body.TrackingNo+"','"+req.body.PujaID+"','"+req.body.PujaDate+"','"+req.body.DevoteeName+"','"+req.session.TempleID+"')"
console.log(sql);
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            else if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            else
            {
                results = JSON.stringify(rows);
                var TempleAddress=rows[0][0];
                var email_subject,email_text,email_message;

//    get booking details from body

                email_text=req.body.PujaStatusMessage;
                var DevoteeName=req.body.DevoteeName;
                var PujaName=req.body.PujaName;
                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var d = new Date(req.body.PujaDate);
                var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                var TrackingNo=req.body.TrackingNo;
//    end

//    get temple details from session

                var TempleName=req.session.TempleName;
                var TempleHouseName=TempleAddress.HouseName;
                var TempleStreetName=TempleAddress.StreetName;
                var TemplePostOffice=TempleAddress.PostOffice;
                var TempleDistrict=TempleAddress.District;
                var TempleState=TempleAddress.State;
                var TemplePostalCode=TempleAddress.PostalCode;
                var TemplePhoneMobile=TempleAddress.PhoneMobile;
                var TemplePhoneWired=TempleAddress.PhoneWired;
                var TempleCommunicationMailID=TempleAddress.Email;

//    end



                if(req.body.PujaStatus=='6')
                {
                    //DV-49
                    email_subject='Your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+' has been completed and postal delivery is pending' ;
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
//      puja completed and postal delivery is pending
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>postal delivery Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                        TempleName.toUpperCase()+
                        '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                        'PRASADAM COMPLETED POSTAGE PENDING <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                        TrackingNo+
                        '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                        'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Your puja '+PujaName+' for date '+PujaDate+' has been completed and postal delivery is pending.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p>' +
                        '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                        '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                        TempleName.toUpperCase() +' </span> Temple management</p></td></tr>'+
                    '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +
                        '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +

                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                        '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                        TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+TempleStreetName.toLowerCase()+',<br>'+TemplePostOffice.toLowerCase()+',<br>' +
                        TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                        +'</h6></td></tr>' +
                        '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';

                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Pooja completed and  Postage Pending</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}    </style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2>    <tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td>    <table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr>    </table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Pooja completed and  Postage Pending</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>Your puja '+PujaName+' for date '+PujaDate+' has been completed and postal delivery is pending.</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic">you have opted for prasadam shipping by postal delivery,normally it takes up to "one week" from the date of puja.We will notify you about the shipping status by mail.</p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic">You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr>    </table>    </td></tr><tr><td>    <table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr>    </table></td></tr></table></td></tr>    <tr><td height="20"></td>    </tr>    <tr>    <td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;">    <p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a>    </p></td></tr><tr><td style="font-style: italic">    <ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li>    </ul></td></tr></table></td>    </tr></table> </div></body></html>'

                }
                else if(req.body.PujaStatus=='7')
                {
//      shipping completed
                    email_subject='postal delivery has been completed for your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+'' ;
                    //DV-49
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>postal delivery Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                        TempleName.toUpperCase()+
                        '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                        'PRASADAM SHIPMENT COMPLETED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                        TrackingNo+
                        '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                        'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Prasadam shipment completed for the  puja '+PujaName+' for date '+PujaDate+' booked by you.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p>' +
                        '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                        '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                        TempleName.toUpperCase() +' </span> Temple management</p></td></tr>'+
                        '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +

                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                        '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                        TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+TempleStreetName.toLowerCase()+',<br>'+TemplePostOffice.toLowerCase()+',<br>' +
                        TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                        +'</h6></td></tr>' +
                        '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +
                        '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>'
                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Shipping Completed</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}</style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Shipping Completed</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>Prasadam shipment completed for the  puja '+PujaName+' for date '+PujaDate+' booked by you.</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic"></p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic">You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr></table></td></tr><tr><td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr></table></td></tr></table></td></tr><tr><td height="20"></td></tr><tr><td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;"><p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a></p></td></tr><tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li></ul></td></tr></table></td></tr></table> </div></body></html>'

                }
                else if(req.body.PujaStatus=='1')
                {
//      puja and if postal that also completed
                    email_subject='Your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+' has been completed ' ;
                    //DV-49
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>Puja Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                        TempleName.toUpperCase()+
                        '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                        'PUJA COMPLETED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                        TrackingNo+
                        '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                        'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Your puja '+PujaName+' for date '+PujaDate+' has been completed .</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'If you have opted for prasadam shipping by postal delivery,<br>normally it takes up to "one week" from the today onwards .<br>We will notify you about the shipping status by mail.</p>' +
                        '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                        '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                        TempleName.toUpperCase() +' </span> Temple management</p></td></tr>' +
                    '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +
                        '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +
                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                        '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                        TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+TempleStreetName.toLowerCase()+',<br>'+TemplePostOffice.toLowerCase()+',<br>' +
                        TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                        +'</h6></td></tr>' +

                        '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';



                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Pooja completed</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}    </style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2>    <tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td>    <table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr>    </table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Pooja completed</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>Your puja '+PujaName+' for date '+PujaDate+' has been completed .</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic">if you have opted for prasadam shipping by postal delivery,normally it takes up to "one week" from the today onwards .We will notify you about the shipping status by mail.</p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic">You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr>    </table>    </td></tr><tr><td>    <table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr>    </table></td></tr></table></td></tr>    <tr><td height="20"></td>    </tr>    <tr>    <td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;">    <p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a>    </p></td></tr><tr><td style="font-style: italic">    <ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li>    </ul></td></tr></table></td>    </tr></table> </div></body></html>'

                }
                else if(req.body.PujaStatus=='16')
                {
//      puja delayed
                    email_subject='Your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+' has been delayed ' ;
                    //DV-49
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>postal delivery Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                        TempleName.toUpperCase()+
                        '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                        'PUJA DELAYED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                        TrackingNo+
                        '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                        'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'We regret to inform you that following Puja booked by you had to be delayed due to unforeseen reasons.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        '<br><b>Puja :'+PujaName+' <br> Puja Date :'+PujaDate+'</b><br>  We can do the same puja for another day. For that please send the details (beneficiary name, star, puja date)<br> to this email id Please also mention this booking and puja id in that mail.<br>We will notify you by mail when the puja is completed .</p>' +
                        '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                        '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                        TempleName.toUpperCase() +' </span> Temple management</p></td></tr>'+
                        '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +

                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                        '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                        TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+TempleStreetName.toLowerCase()+',<br>'+TemplePostOffice.toLowerCase()+',<br>' +
                        TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                        +'</h6></td></tr>' +
                        '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +
                        '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>'


                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Pooja Delayed</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}</style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Pooja Delayed</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p> We regret to inform you that following Puja booked by you had to be delayed due to unforeseen reasons.<br><b>Puja :'+PujaName+' <br> Puja Date :'+PujaDate+'</b><br>  We can do the same puja for another day. For that please send the details (beneficiary name, star, puja date) to this email id Please also mention this booking and puja id in that mail.</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p> We will notify you by mail when the puja is completed </p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic"></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr></table></td></tr><tr><td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr></table></td></tr></table></td></tr><tr><td height="20"></td></tr><tr><td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;"><p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a></p></td></tr><tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li></ul></td></tr></table></td></tr></table> </div></body></html>'

                }
                else if(req.body.PujaStatus=='3')
                {
//      puja cancelled
                    email_subject='Your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+' has been cancelled ' ;
                    //DV-49
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Puja Cancelled</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}</style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Puja Cancelled</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p> We regret to inform you that following Puja booked by you had to be cancelled due to unforeseen reasons.<br><b>Puja :'+PujaName+' <br> Puja Date :'+PujaDate+'</b><br>  We are able to present you two options to proceed with this booking .</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic">  <ul style="font-size: 12px;color: #000000;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 30px"><li style="">We can consider your booking amount towards donation to Temple . </li> <li> We can do the same puja for another day. For that please send the details (beneficiary name, star, puja date) to this   email idPlease also mention this booking and puja id in that mail.</li></ul></p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic"></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr></table></td></tr><tr><td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr></table></td></tr></table></td></tr><tr><td height="20"></td></tr><tr><td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;"><p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a></p></td></tr><tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li></ul></td></tr></table></td></tr></table> </div></body></html>'
                } else if(req.body.PujaStatus=='17') {
                    //DV-49
                    console.log(req.body);
                    var data={mobileno:req.body.mobile,message:'Your puja booking has been completed, Tracking No:'+req.body.TrackingNo}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                }

                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
                if((req.body.PujaStatus!='0')&&(req.body.PujaStatus!='2')&&(req.body.PujaStatus!='17')) {
                    var mailer = require('nodemailernew');
                    var smtpTransport = mailer.createTransport({
                        service: email_provider,
                        auth: {
                            user: email_username,
                            pass: email_password
                        }
                    });
					var fromAddress="'\"" + TempleName +  "\"" + " <" + email_username + ">'"; //DV178 Dynamic display names for emails										
                    var mailOptions = {
                        //from: email_username, // sender address
						from: fromAddress, // sender address							
                        to: req.body.Email, // list of receivers
                        subject:email_subject, // Subject line
                        text: email_text, // plaintext body
                        html: email_message // html body

                    }
                    smtpTransport.sendMail(mailOptions, function(error, response){
                        if(error){
                            logger.log('error','Result:'+error,req.session.UserID,file+'/'+method+'Email');
                        }
                        else{
                            logger.log('data','Result:'+response.message,req.session.UserID,file+'/'+method+'Email');
                        }
                    });
                }
                else
                {
                    logger.log('data','Result: Not need to send mail to customer',req.session.UserID,file+'/'+method);
                }

            }
        });

    });
};

exports.sendreceipt=function(req,res)
{
    var method='admin_order_puja_status_change';
    var sql="select * from bookingaddress where TrackingNo=(select SessionID from sessiontrackingdetails where TrackingNo=(select TrackingNo from bookingpujadetails where PujaID='"+req.body.puja.PujaID+"' limit 1))";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            else if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            else
            {
                res.end(JSON.stringify(rows));
            }
        });

    });
};



//Devaayanam 	DV-87
//Puja pakage

exports.manage_puja_packages=function(req,res)
{
    var file='Admin Puja.js';
    var method='manage_puja_packages';
    var sql;
    console.log(JSON.stringify(req.body));
    sql = "call manage_puja_packages('0','"+req.session.UserID+"','"+req.body.type+"','"+req.body.data+"')";
    console.log('manage_puja_packages query '+sql);
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {


                var results= JSON.stringify(rows);
                res.end(JSON.stringify(rows));


                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

}