'use strict';

var mysql = require('../db').pool;
var logger=require('./utilities/logger.js');
var url = require('url');
var file='Masterbox.js';
var emailtemplate=require('./emailtemplate.js');
var sendMail=require('./Send_Mail.js');
var async = require('async');
var schedule = require('node-schedule');


var imageurl='http://www.devaayanam.com/pages/template3/images/';
exports.loadPuja=function(req,res) {
    var method='loadPuja';
    var sql="Select devaayanam.masterboxpuja.* From devaayanam.masterboxpuja where masterboxpuja.LanguageID='"+req.body.LanguageID+"'";
    logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
    mysql.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "NoData"}]');
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "NoData"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);

            }
            else
            {
                results=JSON.stringify(rows);
                res.end(results);
                //logger.log('data',results,req.session.sessionID,file+'/'+method);
				logger.log('data','log becomes too big, just loading the count-'+rows.length,req.session.sessionID,file+'/'+method);
            }
        });
    });

}
exports.loadDonation=function(req,res) {
    var method='loadDonation';
    var sql="Select devaayanam.masterboxdonation.* From devaayanam.masterboxdonation where masterboxdonation.LanguageID='"+req.body.LanguageID+"'";
    logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
    mysql.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "NoData"}]');
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "NoData"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                results=JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

}
exports.loadDeity=function(req,res) {
    var method='loadDeity';
    var sql="Select devaayanam.masterboxdeity.* From devaayanam.masterboxdeity where masterboxdeity.LanguageID='"+req.body.LanguageID+"'";
    logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
    mysql.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "NoData"}]');
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "NoData"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                results=JSON.stringify(rows);
                res.end(results);
                //logger.log('data',results,req.session.sessionID,file+'/'+method);
				logger.log('data','log becomes too big, just loading the count-'+rows.length,req.session.sessionID,file+'/'+method);
            }
        });
    });

}
exports.addmasterpuja=function(req,res) {
    var method='addmasterpuja';
	var v_templeid = '';
	if (req.body.templeid == undefined || req.session.TempleID =='')
	{
		v_templeid = 0;
	}
	else 
	{
		v_templeid = req.session.TempleID;
	}
    var sql="call addmasterpuja('"+req.body.pujaname+"','"+req.body.description+"','"+req.body.type+"','"+req.body.templeid+"')";
    console.log(sql);
    logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
    mysql.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "NoData"}]');
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "NoData"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                results=JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

}
exports.deletemasterpuja=function(req,res) {
    var method='loadDeity';
    var sql;
    console.log(JSON.stringify(req.body));
    if(req.body.type=='puja') {
        sql = "delete from devaayanam.masterboxpuja where masterboxpuja.PujaID='" + req.body.PujaID + "'";
    }else if(req.body.type=='deity'){
        sql = "delete from devaayanam.masterboxdeity where masterboxdeity.DeityID='" + req.body.DeityID + "'";
    }
    logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
    mysql.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "NoData"}]');
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "NoData"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                results=JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

}
exports.updatepuja=function(req,res) {
    var method='updatepuja';
    var sql;
    console.log(JSON.stringify(req.body));
    if(req.body.type=='puja') {
        sql = "update masterboxpuja set PujaName='" + req.body.PujaName + "',Description='" + req.body.Description + "' where masterboxpuja.PujaID='" + req.body.PujaID + "'";
    }else if(req.body.type=='deity'){
        sql = "update masterboxdeity set DeityName='" + req.body.DeityName + "' and Description='" + req.body.Description + "' where masterboxdeity.DeityID='" + req.body.DeityID + "'";
    }
    logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
    mysql.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "NoData"}]');
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "NoData"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                results=JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

}
exports.LoadTemples=function(req,res){
    var templecategory=req.body.Category;
    var sql="call listtemple('"+req.body.LanguageID+"')";

    console.log('temple list query ' + sql)
    mysql.getConnection(function(err, con){
        logger.log('info','Database connection open',req.session.sessionID,'Search.js/LoadTemples ');
        con.query(sql,function(err, rows){
            con.release();
            var results;
            if (err){
                res.end( '[]');
                logger.log('error',err,req.session.sessionID,'Search.js/LoadTemples ');
                return;
            }
            if (rows.length == 0){
                res.end( '[]');
                logger.log('info','No match found for query',req.session.sessionID,'Search.js/LoadTemples ');
            }
            else{
                results=JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,'Search.js/LoadTemples ');
            }
        });
    });
};
exports.autocheckincompletetransaction=function(req,res){
    var sql="select * from paymentgatewaytransactions where TrackingNo='"+req.TrackingNo+"' and TransactionStatus='S';";
    console.log(sql);
    mysql.getConnection(function (err, con) {
        console.log('info', 'Database connection open', 'Search.js/complete_transaction ');
        con.query(sql, function (err, rows) {
            con.release();
            var results;
            if (err) {
                console.log('error', err,'Search.js/complete_transaction ');
                return;
            }
            if (rows.length == 0) {
                var sql = "select paymentgatewaytransactions.GatewayTransactionID,paymentgatewaytransactions.SubmittedData,paymentgatewaytransactions.AttemptNo from paymentgatewaytransactions where TrackingNo='" + req.TrackingNo + "' and TransactionStatus='" + req.TransactionStatus + "' order by paymentgatewaytransactions.AttemptNo desc limit 1;";
                mysql.getConnection(function (err, con) {
                    console.log('info', 'Database connection open', 'Search.js/complete_transaction ');
                    con.query(sql, function (err, rows) {
                        con.release();
                        var results;
                        if (err) {
                            console.log('error', err, 'Search.js/complete_transaction ');
                            return;
                        }
                        if (rows.length == 0) {
                            console.log('info', 'No match found for query', 'Search.js/complete_transaction ');
                        }
                        else {
                            results = JSON.stringify(rows);
                            var request = require('request');
                            var options = {};
                            var passcode = JSON.parse(rows[0].SubmittedData).pass_code;
                            passcode=passcode.replace('#', '%23');
                            passcode=passcode.replace('$', '%24');
                            console.log('check status: https://epay.federalbank.co.in/easypaytranstatus/GetEasyPayTranStatus.htm?user_code=' + JSON.parse(rows[0].SubmittedData).user_code + '&pass_code=' + passcode + '&tran_id=' + JSON.parse(rows[0].SubmittedData).tran_id);

                            request.get('https://epay.federalbank.co.in/easypaytranstatus/GetEasyPayTranStatus.htm?user_code=' + JSON.parse(rows[0].SubmittedData).user_code + '&pass_code=' + passcode + '&tran_id=' + JSON.parse(rows[0].SubmittedData).tran_id, options, function (err, ress, body) {
                                console.log('RES..'+JSON.stringify(ress));
                                if (err) {
                                    console.log('r..' + err);
                                }
                                if (ress.statusCode == 200) {
                                    //var parser = require('xml2json-light');
                                    //var json = parser.xml2json(ress.body);
                                    var json;
                                    var to_json = require('xmljson').to_json;
                                    to_json(ress.body, function (error, data) {
                                        json=data;
                                    });

                                    //json = JSON.parse(json);
                                    var response = json['ns1:SQL_CFG_1']['ns1:SP']['ns1:V_STATUS'];
                                    var result = [];
                                    var count = 0;
                                    response.split('|').forEach(function (x) {
                                        result.push(x);
                                    });
                                    if (result[4] == 'S' || result[4] == 's') {
                                        console.log('success');
                                        req.body=req;
                                        exports.scruborder(req,res);
                                    } else
                                    if (result[4] == 'F' || result[4] == 'f') {
                                        console.log('failed');
                                        request.get('http://www.devaayanam.in/offlinemail?tid=' + req.TrackingNo, options, function (err, res, body) {
                                            if (err) {
                                                console.log('offlinemail..' + err);
                                            } else {
                                                //res.end('[]');
                                            }
                                        });
                                    } else
                                    if (result[4] == 'A' || result[4] == 'a') {
                                        console.log('pending');
                                    } else
                                    if (result[4] == 'P' || result[4] == 'p') {
                                        console.log('pending');
                                    } else {
//check with fonepaisa
                                        const crypto = require('crypto');
                                        const sign = crypto.createSign('RSA-SHA512');
                                        var request = require('request');
                                        var privatekey ='-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAg0Q0qeAWYJOHflO9VmLLKWc0Y/pzy+LJE6n+f/4HcWYJ3wpD\nfT17018AYLmkwTPtUFAYWkJxhaSV8RVwtPx/kzabBBukqzCw4zucxf6i3BgUZR2w\noy0J2HzTp/SAWHDrSu2nsj0IDqkcdQ0dFORLhzc4hDQb80uWnE0IvZcDjLMia12v\nNrgf8VcNANiv4eEcz/2pQ1vm3aBkwvqpFK1FXdDu/EQ/+4eJ6vqSvuW2aY/k41QO\nTJhEStuFmNR1bOKNYUGfxy7XkZpaCIpSXrOWOdxN26qz3nPtGZl0SuGGgjKj6tlh\niFXhYDiis7Wt2wqPC4KY97JD/nmBUToAmNEdcQIDAQABAoIBAEFTZT4RRjGj2e9V\nb0mZeu/M67bBwnT6FQWTOROrcTrPBCSasAGY4pHLmp2IZeS8meK5KZVpsaNHyFMl\nT2TSsA6sQzMlKqsDXVSIqzmhwpzz4tp7jxd154t0e8T/Ggxpjb9JTIaYdvUM1huu\nfgWSo7/UDG1GnY5T1M1OTQZFLEANf9HKIFpl+P+agjLpB38mvxPG+e3Z/LSQBo4U\nikkQYeHJuxpt4wupMcpDa97LbMUP9zg0c5XHX/9vXz3DmJR4DOy8BUmE6y3S0PIM\n6m07kyRPcAK6CTcRpsCPeBzXnHIyJ+5XjhnZlUOIp/KZYYkorvyzkZgRGRCfQf7n\n/BQmA8ECgYEA4NkWUidKpBQit0IPkfDFHq2Iq0QKk9yh+ljN/svhQWWP39Er52An\n3QixXMnOcEFddM73zd8yYH9BVePzvRh0u9x0kGS2upeGrCFiFwVGooY6iMwqMKG5\nxu+a92S/bUoAGIHST0nXfKSDHT69jSQG/OwUOU/e1rtNi6U0bmcHt/kCgYEAlXP3\nPQAOdXxtpKRA8lyoDxmwZ5nkQYT+vJAn39OWlDRRoTFRzlcBeWHPN97yNrrILoGj\niAyMNAnl6N3uHadpv94h8s87tWDCbT4ieYNvKZhH6wSLUkX9fQNh5F8xWuERMUpP\nYcrEsop61lkmN4k2YpHR/iP+dnCzvLuiRYfiHzkCgYEAvBxCtVaT9rO125WUCyfg\n8mqCVa6mHtBOX3DeXlXZzWmXtct8rKVglGaKZDOd14KIGopw/TbHIb41PhNovoHw\nX57vkkIAMhlFHUsMkm/iiWQ9ImeUwHz9PPcGsgUCtf9cFyS9e4IumuPToar00Fkc\n04r63Hv61bF8DeGX/OmLppkCgYAmQ3F6SahqBfhgXblPE8/nRdF3RNw+thkuF85N\nrPNxcE+x8FmcdYQoGpWeHVNwFbQozAIdU/EVZa9aRm+vYexeNjYE32/PCT3eEDlt\nWOw9/wHyttAguVCkEBihCootVVA8iWT3iZ2AhJnchSiWsWeeJu1XnsmIc2RhHrz2\nBksraQKBgAyI0TjJy5dlAWRVe5pli+w7JH2zhrTr6CGnLVx1c8O1B3TDdKhjF/+G\nNoRjL7Mv/dLwruFS6IKwDkqi4KzYALKTYQv05Q5kGNXvmx2wflzt40jOfnlGz/Aa\nbVYgoX+n9kn9W1OPK/lyVNH09CxM2+MLBiYMZxjIpiX8YlJnXuu3\n-----END RSA PRIVATE KEY-----\n';
                                        var api_key = '48I0045LG60O18DX819126N4U6O3K50';
                                        var $hashinput = api_key + "#" + 'A844' + "#" + 'A844' + "#" +JSON.parse(rows[0].SubmittedData).tran_id+ "#";
                                        sign.write($hashinput);
                                        sign.end();
                                        var encryptedData = sign.sign(privatekey, 'hex');
                                        var options={
                                            id:'A844',
                                            merchant_id:'A844',
                                            invoice:JSON.parse(rows[0].SubmittedData).tran_id,
                                            sign:encryptedData
                                        };
                                        var postForm= {
                                            url: 'https://secure.fonepaisa.com/portal/payment/inquire',
                                            json: options
                                        };
                                        request.post(postForm, function (err, resss, body) {
                                            if (err) {
                                                console.log('offlinemail..' + err);
                                            } else {
                                                 if (body.status == 'C' || body.status == 'c') {
                                                    console.log('success');
                                                    req.body=req;
                                                    exports.scruborder(req,res);
                                                } else
                                                if (body.status == 'F' || body.status == 'X') {
                                                    console.log('failed');
                                                    request.get('http://www.devaayanam.in/offlinemail?tid=' + req.TrackingNo, options, function (err, res, body) {
                                                        if (err) {
                                                            console.log('offlinemail..' + err);
                                                        } else {
                                                            //res.end('[]');
                                                        }
                                                    });
                                                } else {
                                                    //res.end('Payment details sent to support');
                                                    //var template='<html><body><div style="height: 100px;text-align: center;padding-top: 30px;padding-bottom: 15px;font-size: 27px;">PAYMENT STATUS OF '+JSON.parse(rows[0].SubmittedData).tran_id+'</div>' +
                                                    //    '<table><tr><th>Status</th><th>payment_reference</th><th>error_msg</th></tr><tr><td>'+body.status+'</td><td>'+body.payment_reference+'</td><td>'+body.error_msg+'</td></tr>'
                                                    //    +'</table>' +
                                                    //    '</body></html>';
                                                    //console.log(template);
                                                    //var emailData={
                                                    //    to:'support@devaayanam.in',
                                                    //    subject:'Payment status of, Id:'+JSON.parse(rows[0].SubmittedData).tran_id,
                                                    //    template:template
                                                    //}
                                                    //sendMail.send_mail_from_data(emailData,function(resp){
                                                    //    console.log(resp);
                                                    //});
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                            logger.log('data', results, 'Search.js/complete_transaction ');
                        }
                    });
                });
            }
        });
    });
}
exports.updatesettlmentstatus=function(req,res){
    console.log(JSON.stringify(req.body));
    if(req.body.TransactionStatus=='S') {
        var sql="call updatesettlementstatus('"+req.body.TrackingNo+"','"+req.body.SettlementStatus+"')";
        console.log('update settlement.. ' + sql);
        mysql.getConnection(function (err, con) {
            logger.log('info', 'Database connection open', req.session.sessionID, 'Search.js/LoadTemples ');
            con.query(sql, function (err, rows) {
                con.release();
                var results;
                if (err) {
                    res.end('[]');
                    logger.log('error', err, req.session.sessionID, 'Search.js/LoadTemples ');
                    return;
                }
                if (rows.length == 0) {
                    res.end('[]');
                    logger.log('info', 'No match found for query', req.session.sessionID, 'Search.js/LoadTemples ');
                }
                else {
                    if (req.body.SettlementStatus == 'Complete') {
                        // var trandate=new Date(rows[0][0].TransactionRequestedTime);
                        // var transactiondate =trandate.getFullYear()+"-"+trandate.getMonth()+1+"-"+trandate.getDate();
                        // var template='<table><thead><tr><th>TrackingNo</th><th>Transaction Date</th><th>Amount</th><th>Customer Name</th><th>Email</th></tr></thead>' +
                        //     '<tbody><tr>' +
                        //     '<td>'+rows[0][0].TrackingNo+'</td>' +
                        //     '<td>'+transactiondate+'</td>' +
                        //     '<td>'+rows[0][0].GatewayTransactionAmount+'</td>' +
                        //     '<td>'+rows[0][0].FirstName+'</td>' +
                        //     '<td>'+rows[0][0].LoginID+'</td>' +
                        //     '</tr></tbody></table>'
                        // var newtemplate='<!doctype html><html><head><meta charset="utf-8"><title>Untitled Document</title><link href= "https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"></head><body style="font-family: Open Sans, sans-serif; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;"><section class="header" style="width:100%; height:auto;"><table style="width:650px; margin:0px auto; border:0px; border-collapse:collapse;"><tr style="	width:650px; height:auto; text-align:center;"><td><img src="http://viralfever.in/dev/mail/images/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px;height:auto;background-image:url(http://viralfever.in/dev/mail/images/background.jpg);background-size:cover;text-align:left;height:auto;"><td><h5 style="font-weight:600; font-size:17px; color:#b74b03;  padding-left:25px;"></h5><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;"></p>'+template+'<p style="font-weight:400; font-size:14px; color:#2d2d2d; padding-left:70px; padding-right:25px; line-height:25px;">Visit <a href="#" style="color:#603; text-decoration:none;">www.devaayanam.in</a> to find the list of temples onboarded</p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">List of temples <a href="#" style="color:#603; text-decoration:none;">-http://www.devaayanam.in/List</a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;"></p><h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:20px;  padding-left:70px; padding-right:25px; line-height:25px;">Team Devaayanam</h4></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://viralfever.in/dev/mail/images/ad1.jpg"/><img src="http://viralfever.in/dev/mail/images/ad2.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://viralfever.in/dev/mail/images/ad2.jpg"/><img src="http://viralfever.in/dev/mail/images/ad1.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><TD><img src="http://viralfever.in/dev/mail/images/banner..jpg"/></TD></tr><tr style="width:650px;height:auto;margin:0px auto;background-color:#CCC;text-align:center;height:auto;"><td><h6 style="font-size:14px;padding-top:0px;padding-bottom:0px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';

                        var tonumber;
                        tonumber = req.body.tono;
                        if (tonumber == undefined || tonumber == '') {

                        }
                        var maildata = {
                            mobileno: rows[1][0].PhoneMobile,
                            message: 'Payment of Rs.' + ((rows[0][0].GatewayTransactionAmount / 100) - 10) + ' against the  Booking ' + rows[0][0].TrackingNo + ', has been credited in Temples bank account'
                        }
                        emailtemplate.sendsms(maildata, function (response) {
                            res.end(response);
                        });
                        console.log("umobile.." + JSON.stringify(rows[1][0].PhoneMobile));
                        // var emailinfo= {
                        //     event:'SENDFORSETTLEMENT',
                        //     from:"support@devaayanam.in",
                        //     to:"toaddress",
                        //     template:newtemplate
                        // };
                        //emailtemplate.prepare_mail_template(req,res,emailinfo);
                    }
                    results = JSON.stringify(rows);
                    res.end(results);
                    logger.log('data', results, req.session.sessionID, 'Search.js/LoadTemples ');
                }
            });
        });
    }
    else {
        if(req.body.SettlementStatus=='Complete') {
            var sql="select * from paymentgatewaytransactions where TrackingNo='"+req.body.TrackingNo+"' and TransactionStatus='S';";
            mysql.getConnection(function (err, con) {
                logger.log('info', 'Database connection open', req.session.sessionID, 'Search.js/complete_transaction ');
                con.query(sql, function (err, rows) {
                    con.release();
                    var results;
                    if (err) {
                        res.end('[]');
                        logger.log('error', err, req.session.sessionID, 'Search.js/complete_transaction ');
                        return;
                    }
                    if (rows.length == 0) {
                        var sql = "select paymentgatewaytransactions.GatewayTransactionID,paymentgatewaytransactions.SubmittedData,paymentgatewaytransactions.AttemptNo from paymentgatewaytransactions where TrackingNo='" + req.body.TrackingNo + "' and TransactionStatus='" + req.body.TransactionStatus + "' order by paymentgatewaytransactions.AttemptNo desc limit 1;";
                        mysql.getConnection(function (err, con) {
                            logger.log('info', 'Database connection open', req.session.sessionID, 'Search.js/complete_transaction ');
                            con.query(sql, function (err, rows) {
                                con.release();
                                var results;
                                if (err) {
                                    res.end('[]');
                                    logger.log('error', err, req.session.sessionID, 'Search.js/complete_transaction ');
                                    return;
                                }
                                if (rows.length == 0) {
                                    res.end('[]');
                                    logger.log('info', 'No match found for query', req.session.sessionID, 'Search.js/complete_transaction ');
                                }
                                else {
                                    results = JSON.stringify(rows);
                                    var request = require('request');
                                    var options = {};
                                    var passcode = JSON.parse(rows[0].SubmittedData).pass_code;
                                    passcode=passcode.replace('#', '%23');
                                    passcode=passcode.replace('$', '%24');
                                    console.log('check status:'+passcode);
                                    request.get('https://epay.federalbank.co.in/easypaytranstatus/GetEasyPayTranStatus.htm?user_code=' + JSON.parse(rows[0].SubmittedData).user_code + '&pass_code=' + passcode + '&tran_id=' + JSON.parse(rows[0].SubmittedData).tran_id, options, function (err, ress, body) {
                                        if (err) {
                                            console.log('r..' + err);
                                        }
                                        if (ress.statusCode == 200) {
                                            //var parser = require('xml2json-light');
                                            //var json = parser.xml2json(ress.body);
                                            //console.log(json);
                                            //json = JSON.parse(json);
                                            var json;
                                            var to_json = require('xmljson').to_json;
                                            to_json(ress.body, function (error, data) {
                                                json=data;
                                            });
                                            var response = json['ns1:SQL_CFG_1']['ns1:SP']['ns1:V_STATUS'];
                                            var result = [];
                                            var count = 0;
                                            response.split('|').forEach(function (x) {
                                                result.push(x);
                                            });
                                            if (result[4] == 'S' || result[4] == 's') {
                                                console.log('success');
                                                exports.scruborder(req,res);
                                                res.end('Order Completed Successfully');
                                            } else
                                            if (result[4] == 'F' || result[4] == 'f') {
                                                console.log('failed');
                                                res.end('Payment Failed, Mail Sent');
                                                request.get('http://www.devaayanam.in/offlinemail?tid=' + req.body.TrackingNo, options, function (err, res, body) {
                                                    if (err) {
                                                        console.log('offlinemail..' + err);
                                                    } else {
                                                        //res.end('[]');
                                                    }
                                                });
                                            } else
                                            if (result[4] == 'A' || result[4] == 'a') {
                                                console.log('pending');
                                                res.end('Payment Pending, Try Again Later');
                                            } else
                                            if (result[4] == 'P' || result[4] == 'p') {
                                                console.log('pending');
                                                res.end('Payment Pending, Try Again Later');
                                            } else {
                                                const crypto = require('crypto');
                                                const sign = crypto.createSign('RSA-SHA512');
                                                var request = require('request');
                                                var privatekey ='-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAg0Q0qeAWYJOHflO9VmLLKWc0Y/pzy+LJE6n+f/4HcWYJ3wpD\nfT17018AYLmkwTPtUFAYWkJxhaSV8RVwtPx/kzabBBukqzCw4zucxf6i3BgUZR2w\noy0J2HzTp/SAWHDrSu2nsj0IDqkcdQ0dFORLhzc4hDQb80uWnE0IvZcDjLMia12v\nNrgf8VcNANiv4eEcz/2pQ1vm3aBkwvqpFK1FXdDu/EQ/+4eJ6vqSvuW2aY/k41QO\nTJhEStuFmNR1bOKNYUGfxy7XkZpaCIpSXrOWOdxN26qz3nPtGZl0SuGGgjKj6tlh\niFXhYDiis7Wt2wqPC4KY97JD/nmBUToAmNEdcQIDAQABAoIBAEFTZT4RRjGj2e9V\nb0mZeu/M67bBwnT6FQWTOROrcTrPBCSasAGY4pHLmp2IZeS8meK5KZVpsaNHyFMl\nT2TSsA6sQzMlKqsDXVSIqzmhwpzz4tp7jxd154t0e8T/Ggxpjb9JTIaYdvUM1huu\nfgWSo7/UDG1GnY5T1M1OTQZFLEANf9HKIFpl+P+agjLpB38mvxPG+e3Z/LSQBo4U\nikkQYeHJuxpt4wupMcpDa97LbMUP9zg0c5XHX/9vXz3DmJR4DOy8BUmE6y3S0PIM\n6m07kyRPcAK6CTcRpsCPeBzXnHIyJ+5XjhnZlUOIp/KZYYkorvyzkZgRGRCfQf7n\n/BQmA8ECgYEA4NkWUidKpBQit0IPkfDFHq2Iq0QKk9yh+ljN/svhQWWP39Er52An\n3QixXMnOcEFddM73zd8yYH9BVePzvRh0u9x0kGS2upeGrCFiFwVGooY6iMwqMKG5\nxu+a92S/bUoAGIHST0nXfKSDHT69jSQG/OwUOU/e1rtNi6U0bmcHt/kCgYEAlXP3\nPQAOdXxtpKRA8lyoDxmwZ5nkQYT+vJAn39OWlDRRoTFRzlcBeWHPN97yNrrILoGj\niAyMNAnl6N3uHadpv94h8s87tWDCbT4ieYNvKZhH6wSLUkX9fQNh5F8xWuERMUpP\nYcrEsop61lkmN4k2YpHR/iP+dnCzvLuiRYfiHzkCgYEAvBxCtVaT9rO125WUCyfg\n8mqCVa6mHtBOX3DeXlXZzWmXtct8rKVglGaKZDOd14KIGopw/TbHIb41PhNovoHw\nX57vkkIAMhlFHUsMkm/iiWQ9ImeUwHz9PPcGsgUCtf9cFyS9e4IumuPToar00Fkc\n04r63Hv61bF8DeGX/OmLppkCgYAmQ3F6SahqBfhgXblPE8/nRdF3RNw+thkuF85N\nrPNxcE+x8FmcdYQoGpWeHVNwFbQozAIdU/EVZa9aRm+vYexeNjYE32/PCT3eEDlt\nWOw9/wHyttAguVCkEBihCootVVA8iWT3iZ2AhJnchSiWsWeeJu1XnsmIc2RhHrz2\nBksraQKBgAyI0TjJy5dlAWRVe5pli+w7JH2zhrTr6CGnLVx1c8O1B3TDdKhjF/+G\nNoRjL7Mv/dLwruFS6IKwDkqi4KzYALKTYQv05Q5kGNXvmx2wflzt40jOfnlGz/Aa\nbVYgoX+n9kn9W1OPK/lyVNH09CxM2+MLBiYMZxjIpiX8YlJnXuu3\n-----END RSA PRIVATE KEY-----\n';
                                                var api_key = '48I0045LG60O18DX819126N4U6O3K50';
                                                var $hashinput = api_key + "#" + 'A844' + "#" + 'A844' + "#" +JSON.parse(rows[0].SubmittedData).tran_id+ "#";
                                                sign.write($hashinput);
                                                sign.end();
                                                var encryptedData = sign.sign(privatekey, 'hex');
                                                var options={
                                                    id:'A844',
                                                    merchant_id:'A844',
                                                    invoice:JSON.parse(rows[0].SubmittedData).tran_id,
                                                    sign:encryptedData
                                                };
                                                var postForm= {
                                                    url: 'https://secure.fonepaisa.com/portal/payment/inquire',
                                                    json: options
                                                };
                                                request.post(postForm, function (err, resss, body) {
                                                    if (err) {
                                                        var options={
                                                            appId:'12348ca589bd785fd8e40f3cb84321',
                                                            secretKey:'4239fd6b875664686a85eadb588ae64dd317d765',
                                                            orderId:JSON.parse(rows[0].SubmittedData).tran_id
                                                        }
                                                        var postForm= {
                                                            url: 'https://api.cashfree.com/api/v1/order/info/status',
                                                            json: options
                                                        };
                                                        request.post(postForm, function (err, resss, body) {
                                                            if (err) {
                                                                console.log('offlinemail..' + err);
                                                                res.send(JSON.stringify(err));
                                                            } else {
                                                                if(ress.body.status=='OK'){
                                                                                                        if(ress.body.txStatus=='SUCCESS'){
                                                                                                            exports.scruborder(req,res);
                                                                                                        } else {
                                                                                                            var template='<html><body>'+JSON.stringify(ress.body)+'</body></html>';
                                                                                                            console.log(template);
                                                                                                            var emailData={
                                                                                                                to:'support@devaayanam.in',
                                                                                                                subject:'Payment status of, Id:'+JSON.parse(rows[0].SubmittedData).tran_id,
                                                                                                                template:template
                                                                                                            }
                                                                                                            sendMail.send_mail_from_data(emailData,function(resp){
                                                                                                                console.log(resp);
                                                                                                            });
                                                                                                        }
                                                                } else {
                                                                    var template='<html><body>'+JSON.stringify(ress.body)+'</body></html>';
                                                                    console.log(template);
                                                                    var emailData={
                                                                        to:'support@devaayanam.in',
                                                                        subject:'Payment status of, Id:'+JSON.parse(rows[0].SubmittedData).tran_id,
                                                                        template:template
                                                                    }
                                                                    sendMail.send_mail_from_data(emailData,function(resp){
                                                                        console.log(resp);
                                                                    });
                                                                }

                                                            }
                                                            });
                                                    } else {
                                                        if (resss.body.status == 'C' || resss.body.status == 'c') {
                                                            console.log('success');
                                                            //req.body=req;
                                                            exports.scruborder(req,res);
                                                        } else
                                                        if (resss.body.status == 'F' || resss.body.status == 'X') {
                                                            res.end('Payment Failed, Mail Sent');
                                                            console.log('failed');
                                                            request.get('http://www.devaayanam.in/offlinemail?tid=' + JSON.parse(rows[0].SubmittedData).tran_id, options, function (err, res, body) {
                                                                if (err) {
                                                                    console.log('offlinemail..' + err);
                                                                } else {
                                                                    //res.end('[]');
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            //res.end('Payment details sent to support');
                                                            //var template='<html><body><div style="height: 100px;text-align: center;padding-top: 30px;padding-bottom: 15px;font-size: 27px;">PAYMENT STATUS OF '+JSON.parse(rows[0].SubmittedData).tran_id+'</div>' +
                                                            //    '<table><tr><th>Status</th><th>payment_reference</th><th>error_msg</th></tr><tr><td>'+body.status+'</td><td>'+body.payment_reference+'</td><td>'+body.error_msg+'</td></tr>'
                                                            //    +'</table>' +
                                                            //    '</body></html>';
                                                            //console.log(template);
                                                            //var emailData={
                                                            //    to:'support@devaayanam.in',
                                                            //    subject:'Payment status of, Id:'+JSON.parse(rows[0].SubmittedData).tran_id,
                                                            //    template:template
                                                            //}
                                                            //sendMail.send_mail_from_data(emailData,function(resp){
                                                            //    console.log(resp);
                                                            //});


                                                            var options1={
                                                                appId:"12348ca589bd785fd8e40f3cb84321",
                                                                secretKey:"4239fd6b875664686a85eadb588ae64dd317d765",
                                                                orderId:JSON.parse(rows[0].SubmittedData).tran_id
                                                            }


                                                            var postForm1= {
                                                                url: 'https://api.cashfree.com/api/v1/order/info/status',
                                                                form: options1
                                                            };
                                                            console.log(JSON.stringify(postForm1));
                                                            request.post(postForm1, function (err, ress, body) {
                                                                if (err) {
                                                                    console.log('offlinemail..' + err);
                                                                    res.send(JSON.stringify(err));
                                                                } else {
                                                                    if(JSON.parse(body).status=='OK'){
                                                                        if(JSON.parse(body).txStatus=='SUCCESS'){
                                                                            exports.scruborder(req,res);
                                                                        } else {
                                                                            res.end('Payment details sent to support');
                                                                            var template='<html><body>'+ress.body+'</body></html>';
                                                                            console.log(template);
                                                                            var emailData={
                                                                                to:'support@devaayanam.in',
                                                                                subject:'Payment status of, Id:'+JSON.parse(rows[0].SubmittedData).tran_id,
                                                                                template:template
                                                                            }
                                                                            sendMail.send_mail_from_data(emailData,function(resp){
                                                                                console.log(resp);
                                                                            });
                                                                        }
                                                                    } else {
                                                                        res.end('Payment details sent to support');
                                                                        var template='<html><body>'+ress.body+'</body></html>';
                                                                        console.log(template);
                                                                        var emailData={
                                                                            to:'support@devaayanam.in',
                                                                            subject:'Payment status of, Id:'+JSON.parse(rows[0].SubmittedData).tran_id,
                                                                            template:template
                                                                        }
                                                                        sendMail.send_mail_from_data(emailData,function(resp){
                                                                            console.log(resp);
                                                                        });
                                                                    }

                                                                }
                                                            });
                                                        }
                                                        //if (body.status == 'P' || body.status == 'I') {
                                                        //    console.log('waiting or partially paid');
                                                        //} else {
                                                        //
                                                        //}
                                                    }
                                                });
                                            }
                                        }
                                    });
                                    logger.log('data', results, req.session.sessionID, 'Search.js/complete_transaction ');
                                }
                            });
                        });
                    } else {
                        res.end('Allready a Successful Order');
                    }
                })});
        }
    }
};
exports.scruborder=function(req,res) {
    var sql = "select sessiontrackingdetails.SessionID,sessionbooking.TempleID from sessiontrackingdetails inner join sessionbooking on sessionbooking.SessionID=sessiontrackingdetails.SessionID  where TrackingNo='" + req.body.TrackingNo + "';";
    console.log(sql);
    mysql.getConnection(function (err, con) {
        logger.log('info', 'Database connection open', 'Search.js/complete_transaction ');
        con.query(sql, function (err, rows) {
            con.release();
            var results;
            if (err) {
                //res.end('[]');
                logger.log('error', err, 'Search.js/complete_transaction ');
                return;
            }
            if (rows.length == 0) {
                //res.end('[]');
                logger.log('info', 'No match found for query', 'Search.js/complete_transaction ');
            }
            else {
                var sql = "call scruborder('" + rows[0].TempleID + "','" + rows[0].SessionID + "','" + req.body.TrackingNo + "','" + req.body.LoginID + "')";
                console.log(sql);
                mysql.getConnection(function (err, con) {
                    logger.log('info', 'Database connection open', 'Search.js/complete_transaction ');
                    con.query(sql, function (err, rows) {
                        con.release();
                        var results;
                        if (err) {
                            //res.end('[]');
                            console.log('error', err, 'Search.js/complete_transaction ');
                            return;
                        }
                        if (rows.length == 0) {
                            //res.end('[]');
                            console.log('info', 'No match found for query', 'Search.js/complete_transaction ');
                        }
                        else {
                            console.log('send-mail');
                            res.end('Success Mail Sent');
                            var request=require('request');
                            var options={};
                            request.get('http://www.devaayanam.in/offlinemail?tid=' + req.body.TrackingNo, options, function (err, res, body) {
                                if (err) {
                                    console.log('offlinemail..' + err);
                                } else {
                                    //res.end('[]');
                                }
                            });
                        }
                    });});
            }
        });
    });
}
exports.checkTransferStatus=function(data,callback) {
    var request = require('request');
    const crypto = require('crypto');
    const sign = crypto.createSign('RSA-SHA512');
    var request = require('request');
    var privatekey ='-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAkXxYpKPClp/sl2BihnlPXXWK2L2gFYUf1F4G4VxpelyhNMAA\nrkxIl21lBYBMGW0y6k2pHu5iWorfcKl53s5Vhqs+wesv/EvhowCLjMg5ZNvc5fch\nzRA5D7iXvzTO3+01YT5SsQ+hBvus7ZVwlDj+OgqqjuPTcfC4xO1Ch3+Yh+9wjKVp\nSszkZUeu/fG2wogq6mHlJdZOkGUawYaqbcnxYPLxKXlBow1A4Q1eehpHNNGqJfFc\nzXgHenJRD6e/ddFjRAgYvc8ayRfhReNHs9X5QBgv+dZt5R+mPFWDOp8fl6Sv6QLu\n5MHH4odIVBsbeUqg54DeUgseClYl/WVLnQxrDQIDAQABAoIBAAtuJpTA3P/yjqcS\nBoukKk0geAkxVMt8CxUnUgkQtqTLdErtruAC1E05Zg95lgEFaFOqSFhKyGVutcFn\nP1D3CxcqiyK3NKzw1Uh2OYCVFpVLBN74fKpc1O7cROfb4UkMnP4H5H1Oygr/aQW9\nkPvEQo0S23WghrNUA2BNd8Wni7daRTBKOQ5GZCr8eDjDyhTnfQIjkke3BNj2mrQ0\n5Ykm7t8V4RLzYpLnK+VJB/j3jeQHJzBEhOn284bu+4H6/kzDjak8NJmGGAw2XTrt\njfQuRgTOq114H2PpHtmeXDvGbmVqhgnxTFrBhBlWauyxR39fezL/PckoAqW4Apf1\nOHAIA6ECgYEAzHSDIwjgSgGycKVhKYCwO7wWHvNFM39fNNZl/RKHbd6jwQ1hQFJs\nr6wkOsp99vggLphhURFq5SI7tadkYg5ZLDxy0LBAJxAkIGOeVtJhTvc/9Bb5UeZ9\ncFYr9skGKyPSeg8EbKwuhncSObFZFdOqPb9aeV2EWTcK+ZI/9gRIhyUCgYEAtin2\nWx1PLDG7w9QDzoDCNjJ+RGsU7lIhtQ4ABmZS3rEOmIbdtYeg4CMEBjP8B0VxCW2o\nctOTJobS5lWxeidKWn1F8Gmzql9jjFEm6rqQjBn5yQehjb+qLWkCudR0Tc3QvvOh\n0Z5a7ovlzjH8h/nmdjmUSGWlWv0GP4/MK2xLY8kCgYEAy59Gc4AFkO87vgCXUfQ+\nkF90UILL6svyD/dvP6pSgtP3lu8yahMs95CjzIGbDnpz4rQUR97m7Sk4+mOqEBZm\nLS5O0xSV79GdiTxtl96S10hbw0eWK0E4sMbBpljy8cgNeU63g1vAQl8YIalFJf3W\nUQTgoHrIsumk3oYPeX8ulJkCgYEAss5jtDIDyeSTYvhUODno6JwNlwjpBbl2vqqc\nWavMwixXoF4ZeBtGTVvxkkkqpNSDzPZndza4ZQPINjF9QOMaN9JpeT+4DI8PPudV\nZOh2BB9nMO2dpW6yE1rVQyUIeGqC8Txh5UkkBq0piIlGUQ56KigApkOn2LUukeuL\nREBLjAkCgYBLOjnfRWg/EhM/KtjPX9mYCeVOccZ5QZXr7ZgOiZN16vNvl94oD8u4\nGjKqoSigh+csxFI0rcPtFVBUNTYcUKqRW5B8OWFQARxP4O8+2L7gF+Gn/NiicNjC\nJh5WDm4CMhceNvGyvomntUZzmU6/jpXU84IdkLJnqVQZcadxGySjYg==\n-----END RSA PRIVATE KEY-----';
    var api_key = '2OBI840030A19I0KBHJ4RA0CM7H5Q9X';
    var req_id=new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate();
    var $hashinput = api_key + "#" + 'A935' + "#" + 'A935' + "#" + 'CSHOT' + "#";
    sign.write($hashinput);
    sign.end();
    var encryptedData = sign.sign(privatekey, 'hex');
    var requests=[];
    if(data.req_id!=undefined) {
        req_id =data.req_id;
    }
    var options;
    if(data.from) {
         options =
        {
            id: 'A935',
            merchant_id: 'A935',
            sign: encryptedData,
            event_id: 'CSHOT',
            from_date: '',
            to_date: ''
        }
    } else {
        options =
        {
            id: 'A935',
            merchant_id: 'A935',
            request_id:req_id,
            sign: encryptedData,
            event_id: 'CSHOT'
        }
    }
    if(data.from!=undefined){
        options.from_date=data.from;
    }
    if(data.to!=undefined) {
        options.to_date=data.to;
    }
    console.log(options);
    var postForm= {
        url: 'https://secure.fonepaisa.com/portal/funds/enquire',
        json: options
    };
    console.log(options);
        request.post(postForm, function (err, ress, body) {
            console.log('RES..' + JSON.stringify(ress));
            callback(ress);
            if (err) {
                console.log('r..' + err);
            }
            if (ress.statusCode == 200) {
                //callback(body);
            }
        });
}
var sql = 'select paymentgatewaytransactions.TrackingNo,(paymentgatewayorder.GatewayTransactionAmount/100) as GatewayTransactionAmount,booking.BillAmount as Amount,templesettlementdetails.BenficiaryName,templesettlementdetails.BenficiaryBankACNumber,templesettlementdetails.BenficiaryBankIFSC ' +
    'from paymentgatewaytransactions ' +
    'inner join booking ON paymentgatewaytransactions.TrackingNo= booking.TrackingNo ' +
    'inner join sessiontrackingdetails on sessiontrackingdetails.TrackingNo=paymentgatewaytransactions.TrackingNo ' +
    'inner join bookingaddress on sessiontrackingdetails.SessionID=bookingaddress.TrackingNo and `bookingaddress`.`Type` in(' + "'guest'" + ',' + "'customer'" + ') ' +
    'inner join paymentgatewayorder ON paymentgatewaytransactions.TrackingNo=paymentgatewayorder.TrackingNo ' +
    'inner join templesettlementdetails on templesettlementdetails.TempleID=paymentgatewayorder.TempleID ' +
    'inner join temple on temple.TempleID=booking.TempleID and temple.LanguageID=1 where paymentgatewaytransactions.TrackingNo in(select TrackingNo from booking) and Date(TransactionRequestedTime) < subdate(curdate(),interval 1 day) and TransactionStatus=' + "'S'" + ' and temple.BankName=' + "'fonepaisa_temple'" + ' and ' +
    'paymentgatewaytransactions.TrackingNo not in(select TrackingNo from paymentgatewayorderstatus);';
console.log(sql);
exports.autoTransferAmount=function() {
    var request = require('request');
    const crypto = require('crypto');
    const sign = crypto.createSign('RSA-SHA512');
    var request = require('request');
    var privatekey ='-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAkXxYpKPClp/sl2BihnlPXXWK2L2gFYUf1F4G4VxpelyhNMAA\nrkxIl21lBYBMGW0y6k2pHu5iWorfcKl53s5Vhqs+wesv/EvhowCLjMg5ZNvc5fch\nzRA5D7iXvzTO3+01YT5SsQ+hBvus7ZVwlDj+OgqqjuPTcfC4xO1Ch3+Yh+9wjKVp\nSszkZUeu/fG2wogq6mHlJdZOkGUawYaqbcnxYPLxKXlBow1A4Q1eehpHNNGqJfFc\nzXgHenJRD6e/ddFjRAgYvc8ayRfhReNHs9X5QBgv+dZt5R+mPFWDOp8fl6Sv6QLu\n5MHH4odIVBsbeUqg54DeUgseClYl/WVLnQxrDQIDAQABAoIBAAtuJpTA3P/yjqcS\nBoukKk0geAkxVMt8CxUnUgkQtqTLdErtruAC1E05Zg95lgEFaFOqSFhKyGVutcFn\nP1D3CxcqiyK3NKzw1Uh2OYCVFpVLBN74fKpc1O7cROfb4UkMnP4H5H1Oygr/aQW9\nkPvEQo0S23WghrNUA2BNd8Wni7daRTBKOQ5GZCr8eDjDyhTnfQIjkke3BNj2mrQ0\n5Ykm7t8V4RLzYpLnK+VJB/j3jeQHJzBEhOn284bu+4H6/kzDjak8NJmGGAw2XTrt\njfQuRgTOq114H2PpHtmeXDvGbmVqhgnxTFrBhBlWauyxR39fezL/PckoAqW4Apf1\nOHAIA6ECgYEAzHSDIwjgSgGycKVhKYCwO7wWHvNFM39fNNZl/RKHbd6jwQ1hQFJs\nr6wkOsp99vggLphhURFq5SI7tadkYg5ZLDxy0LBAJxAkIGOeVtJhTvc/9Bb5UeZ9\ncFYr9skGKyPSeg8EbKwuhncSObFZFdOqPb9aeV2EWTcK+ZI/9gRIhyUCgYEAtin2\nWx1PLDG7w9QDzoDCNjJ+RGsU7lIhtQ4ABmZS3rEOmIbdtYeg4CMEBjP8B0VxCW2o\nctOTJobS5lWxeidKWn1F8Gmzql9jjFEm6rqQjBn5yQehjb+qLWkCudR0Tc3QvvOh\n0Z5a7ovlzjH8h/nmdjmUSGWlWv0GP4/MK2xLY8kCgYEAy59Gc4AFkO87vgCXUfQ+\nkF90UILL6svyD/dvP6pSgtP3lu8yahMs95CjzIGbDnpz4rQUR97m7Sk4+mOqEBZm\nLS5O0xSV79GdiTxtl96S10hbw0eWK0E4sMbBpljy8cgNeU63g1vAQl8YIalFJf3W\nUQTgoHrIsumk3oYPeX8ulJkCgYEAss5jtDIDyeSTYvhUODno6JwNlwjpBbl2vqqc\nWavMwixXoF4ZeBtGTVvxkkkqpNSDzPZndza4ZQPINjF9QOMaN9JpeT+4DI8PPudV\nZOh2BB9nMO2dpW6yE1rVQyUIeGqC8Txh5UkkBq0piIlGUQ56KigApkOn2LUukeuL\nREBLjAkCgYBLOjnfRWg/EhM/KtjPX9mYCeVOccZ5QZXr7ZgOiZN16vNvl94oD8u4\nGjKqoSigh+csxFI0rcPtFVBUNTYcUKqRW5B8OWFQARxP4O8+2L7gF+Gn/NiicNjC\nJh5WDm4CMhceNvGyvomntUZzmU6/jpXU84IdkLJnqVQZcadxGySjYg==\n-----END RSA PRIVATE KEY-----';
    var api_key = '2OBI840030A19I0KBHJ4RA0CM7H5Q9X';
    var  req_id = new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate();
    var $hashinput = api_key + "#" + 'A935' + "#" + 'A935' + "#" + req_id + "#";
    sign.write($hashinput);
    sign.end();
    var encryptedData = sign.sign(privatekey, 'hex');
    var requests=[];
    var transactionList;
    var options =
    {
        id: 'A935',
        merchant_id: 'A935',
        request_id:req_id,
        sign: encryptedData,
        requests: requests
    }
        async.series([function(callback) {
            var sql ='select * from templeconfig where templeconfig.Key='+"'ENABLE_AUTO_SETTLEMENT'"+' and  templeconfig.Value='+"'true'"+' and templeconfig.ConfigID=1 and templeconfig.TempleID=0;';
            mysql.getConnection(function(err, con){
                logger.log('info','Database connection open','','Search.js/load settilement ');
                con.query(sql,function(err, rows){
                    con.release();
                    var results;
                    if (err){
                        // res.end( '[]');
                        console.log('error',err,'','Search.js/load settilement ');
                        return;
                    }
                    if (rows.length == 0){
                        // res.end( '[]');
                        console.log('info','No match found for query','','Search.js/load settilement ');
                    }
                    else{
                        callback();
                    }
                });
            });
        },function(callback)
        {
            var sql = 'select paymentgatewaytransactions.TrackingNo,(paymentgatewayorder.GatewayTransactionAmount/100) as GatewayTransactionAmount,(select sum(devaayanam.bookingpujadetails.Rate*devaayanam.bookingpujadetails.Quantity)+sum(devaayanam.bookingpujadetails.PostalCharge) from devaayanam.bookingpujadetails Where devaayanam.bookingpujadetails.TrackingNo = paymentgatewaytransactions.TrackingNo) as Amount,templesettlementdetails.BenficiaryName,templesettlementdetails.BenficiaryBankACNumber,templesettlementdetails.BenficiaryBankIFSC ' +
                'from paymentgatewaytransactions ' +
                'inner join booking ON paymentgatewaytransactions.TrackingNo= booking.TrackingNo ' +
                'inner join sessiontrackingdetails on sessiontrackingdetails.TrackingNo=paymentgatewaytransactions.TrackingNo ' +
                'inner join bookingaddress on sessiontrackingdetails.SessionID=bookingaddress.TrackingNo and `bookingaddress`.`Type` in(' + "'guest'" + ',' + "'customer'" + ') ' +
                'inner join paymentgatewayorder ON paymentgatewaytransactions.TrackingNo=paymentgatewayorder.TrackingNo ' +
                'inner join templesettlementdetails on templesettlementdetails.TempleID=paymentgatewayorder.TempleID ' +
                'inner join temple on temple.TempleID=booking.TempleID and temple.LanguageID=1 where paymentgatewaytransactions.TrackingNo in(select TrackingNo from booking) and Date(TransactionRequestedTime) < subdate(curdate(),interval 1 day) and Date(TransactionRequestedTime) > subdate(curdate(),interval 3 day) and TransactionStatus=' + "'S'" + ' and temple.BankName=' + "'fonepaisa_temple'" + ' and ' +
                'paymentgatewaytransactions.TrackingNo not in(select TrackingNo from paymentgatewayorderstatus);';

                mysql.getConnection(function (err, con) {
                    con.query(sql, function (err, setrow) {
                        con.release();
                        var results;
                        if (err) {
                            callback();
                            return;
                        }
                        if (setrow.length == 0) {
                            callback();
                            console.log('info', 'No match found for query', "", 'Search.js/auto settilement ');
                        }
                        else {
                            transactionList=setrow;
                            results = JSON.stringify(setrow);
                            setrow.forEach(function (row) {
                                var sql = "insert into paymentgatewayorderstatus value ('" + row.TrackingNo + "',now(),'" + "New" + "');";
                                mysql.getConnection(function (err, con) {
                                    con.query(sql, function (err, rows) {
                                        con.release();
                                        var results;
                                        if (err) {
                                            console.log('error', err, 'MasterBox.js.js/auto transfer ');
                                            return;
                                        } else {
                                            results = JSON.stringify(rows);
                                        }
                                    });
                                });
                            });
                            setrow.forEach(function (row) {
                                var sql = "call generatesettlementdata('" + row.TrackingNo + "')";
                                mysql.getConnection(function (err, con) {
                                    con.query(sql, function (err, rows) {
                                        con.release();
                                        var results;
                                        if (err) {
                                            logger.log('error', err, 'MasterBox.js.js/auto transfer ');
                                            return;
                                        }
                                        if (rows.length == 0) {
                                            callback();
                                            logger.log('info', 'No match found for query',  'MasterBox.js.js/auto transfer ');
                                        } else {
                                            results = JSON.stringify(rows);
                                        }
                                    });
                                });
                            });
                            setrow.forEach(function (row) {
                                if(row.BenficiaryName.length>35) {
                                    row.BenficiaryName=row.BenficiaryName.substring(0,34);
                                }
                                requests.push(
                                    {
                                        'request_id':row.TrackingNo,
                                        'acct_name':row.BenficiaryName,
                                        'acct_number':row.BenficiaryBankACNumber,
                                        'ifsc_code':row.BenficiaryBankIFSC,
                                        'transfer_type':'N',
                                        'amount':row.Amount
                                    }
                                );
                                //requests.push(
                                //    {
                                //        'request_id':row.TrackingNo+'-DEV',
                                //        'acct_name':'Devaayanam Network Pvt Ltd',
                                //        'acct_number':'10940200012933',
                                //        'ifsc_code':'FDRL0001094',
                                //        'transfer_type':'N',
                                //        'amount':row.GatewayTransactionAmount-row.Amount
                                //    }
                                //);
                            });
                            setTimeout(function(){callback();},1000);
                        }
                    });
                });
            }
        ,function(callback) {
            exports.checkTransferStatus({},function(data) {
                if(data.body.tran_list.length==0) {
                    var postForm = {
                        url: 'https://secure.fonepaisa.com/portal/funds/transfer',
                        json: options
                    };
                    console.log(JSON.stringify(options));
                    if (requests.length > 0) {
                        request.post(postForm, function (err, ress, body) {
                            console.log('fp res:'+JSON.stringify(ress));
                            if(ress.body.resp_code!='0000') {
                                transactionList.forEach(function (row) {
                                    var sql = "delete from paymentgatewayorderstatus where paymentgatewayorderstatus.TrackingNo= '"+row.TrackingNo+"';";
                                    mysql.getConnection(function (err, con) {
                                        con.query(sql, function (err, rows) {
                                            con.release();
                                            var results;
                                            if (err) {
                                                console.log('error', err, 'MasterBox.js.js/auto transfer ');
                                                return;
                                            } else {
                                                results = JSON.stringify(rows);
                                            }
                                        });
                                    });
                                });
                            }
                            var tableRow='';
                            ress.body.requests.forEach(function(row){
                                tableRow=tableRow+'<tr><td>'+row.request_id+'</td><td>'+row.acct_name+'</td><td>'+row.amount+'</td><td>'+row.resp_code+'</td><td>'+row.resp_msg+'</td><tr>';
                            });
                            var template='<html><body><div style="height: 100px;text-align: center;padding-top: 30px;padding-bottom: 15px;font-size: 27px;">'+ress.body.resp_msg+'('+ress.body.resp_code+')</div>' +
                                '<table><tr><th>TrackingNo</th><th>AccountName</th><th>Amount</th><th>ResponseCode</th><th>ResponseMsg</th></tr>'+tableRow+'</table>' +
                                '</body></html>';
                            console.log(template);
                            var emailData={
                                to:'support@devaayanam.in',
                                subject:'Transfer Request Status, Id:'+ress.body.request_id,
                                template:template
                            }
                            sendMail.send_mail_from_data(emailData,function(resp){
                                console.log(resp);
                            });
                            callback();
                            if (err) {
                                console.log('autoTransferError' + err);
                            }
                            if (ress.statusCode == 200) {

                            }
                        });
                    }
                } else {
                    //res.send('Allready have one transfer request today');
                }
            });
        }]);
    }
exports.transferAmount=function(req,res) {
    var trackingNos='';
    var request = require('request');
    const crypto = require('crypto');
    const sign = crypto.createSign('RSA-SHA512');
    var request = require('request');
    var transactionList;
    var privatekey ='-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAkXxYpKPClp/sl2BihnlPXXWK2L2gFYUf1F4G4VxpelyhNMAA\nrkxIl21lBYBMGW0y6k2pHu5iWorfcKl53s5Vhqs+wesv/EvhowCLjMg5ZNvc5fch\nzRA5D7iXvzTO3+01YT5SsQ+hBvus7ZVwlDj+OgqqjuPTcfC4xO1Ch3+Yh+9wjKVp\nSszkZUeu/fG2wogq6mHlJdZOkGUawYaqbcnxYPLxKXlBow1A4Q1eehpHNNGqJfFc\nzXgHenJRD6e/ddFjRAgYvc8ayRfhReNHs9X5QBgv+dZt5R+mPFWDOp8fl6Sv6QLu\n5MHH4odIVBsbeUqg54DeUgseClYl/WVLnQxrDQIDAQABAoIBAAtuJpTA3P/yjqcS\nBoukKk0geAkxVMt8CxUnUgkQtqTLdErtruAC1E05Zg95lgEFaFOqSFhKyGVutcFn\nP1D3CxcqiyK3NKzw1Uh2OYCVFpVLBN74fKpc1O7cROfb4UkMnP4H5H1Oygr/aQW9\nkPvEQo0S23WghrNUA2BNd8Wni7daRTBKOQ5GZCr8eDjDyhTnfQIjkke3BNj2mrQ0\n5Ykm7t8V4RLzYpLnK+VJB/j3jeQHJzBEhOn284bu+4H6/kzDjak8NJmGGAw2XTrt\njfQuRgTOq114H2PpHtmeXDvGbmVqhgnxTFrBhBlWauyxR39fezL/PckoAqW4Apf1\nOHAIA6ECgYEAzHSDIwjgSgGycKVhKYCwO7wWHvNFM39fNNZl/RKHbd6jwQ1hQFJs\nr6wkOsp99vggLphhURFq5SI7tadkYg5ZLDxy0LBAJxAkIGOeVtJhTvc/9Bb5UeZ9\ncFYr9skGKyPSeg8EbKwuhncSObFZFdOqPb9aeV2EWTcK+ZI/9gRIhyUCgYEAtin2\nWx1PLDG7w9QDzoDCNjJ+RGsU7lIhtQ4ABmZS3rEOmIbdtYeg4CMEBjP8B0VxCW2o\nctOTJobS5lWxeidKWn1F8Gmzql9jjFEm6rqQjBn5yQehjb+qLWkCudR0Tc3QvvOh\n0Z5a7ovlzjH8h/nmdjmUSGWlWv0GP4/MK2xLY8kCgYEAy59Gc4AFkO87vgCXUfQ+\nkF90UILL6svyD/dvP6pSgtP3lu8yahMs95CjzIGbDnpz4rQUR97m7Sk4+mOqEBZm\nLS5O0xSV79GdiTxtl96S10hbw0eWK0E4sMbBpljy8cgNeU63g1vAQl8YIalFJf3W\nUQTgoHrIsumk3oYPeX8ulJkCgYEAss5jtDIDyeSTYvhUODno6JwNlwjpBbl2vqqc\nWavMwixXoF4ZeBtGTVvxkkkqpNSDzPZndza4ZQPINjF9QOMaN9JpeT+4DI8PPudV\nZOh2BB9nMO2dpW6yE1rVQyUIeGqC8Txh5UkkBq0piIlGUQ56KigApkOn2LUukeuL\nREBLjAkCgYBLOjnfRWg/EhM/KtjPX9mYCeVOccZ5QZXr7ZgOiZN16vNvl94oD8u4\nGjKqoSigh+csxFI0rcPtFVBUNTYcUKqRW5B8OWFQARxP4O8+2L7gF+Gn/NiicNjC\nJh5WDm4CMhceNvGyvomntUZzmU6/jpXU84IdkLJnqVQZcadxGySjYg==\n-----END RSA PRIVATE KEY-----';
    var api_key = '2OBI840030A19I0KBHJ4RA0CM7H5Q9X';
    var req_id = req.body.settlementtrackingno[0]+'-'+req.body.settlementtrackingno[(req.body.settlementtrackingno.length-1)];
    var $hashinput = api_key + "#" + 'A935' + "#" + 'A935' + "#" + req_id + "#";
    sign.write($hashinput);
    sign.end();
    var encryptedData = sign.sign(privatekey, 'hex');
    var requests=[];
    var options =
    {
        id: 'A935',
        merchant_id: 'A935',
        request_id:req_id,
        sign: encryptedData,
        requests: requests
    }
    var sql='';
    console.log(sql);
    async.series([function(doCallback){
            async.series([
                function(callback){
                var index=0;
                req.body.settlementtrackingno.forEach(function(trackingNo){
                    index=index+1;
                    if(req.body.settlementtrackingno.length!=index){
                        trackingNos=trackingNos+trackingNo+',';
                    } else {
                        trackingNos=trackingNos+trackingNo;
                        callback();
                    }
                });
                if(req.body.settlementtrackingno==0){
                    callback();
                }
            },function(callback) {
                sql = 'select paymentgatewaytransactions.TrackingNo,(paymentgatewayorder.GatewayTransactionAmount/100) as GatewayTransactionAmount,(select sum(devaayanam.bookingpujadetails.Rate*devaayanam.bookingpujadetails.Quantity)+sum(devaayanam.bookingpujadetails.PostalCharge) from devaayanam.bookingpujadetails Where devaayanam.bookingpujadetails.TrackingNo = paymentgatewaytransactions.TrackingNo) as Amount,templesettlementdetails.BenficiaryName,templesettlementdetails.BenficiaryBankACNumber,templesettlementdetails.BenficiaryBankIFSC ' +
                'from paymentgatewaytransactions ' +
                'inner join booking ON paymentgatewaytransactions.TrackingNo= booking.TrackingNo ' +
                'inner join sessiontrackingdetails on sessiontrackingdetails.TrackingNo=paymentgatewaytransactions.TrackingNo ' +
                'inner join bookingaddress on sessiontrackingdetails.SessionID=bookingaddress.TrackingNo and `bookingaddress`.`Type` in(' + "'guest'" + ',' + "'customer'" + ') ' +
                'inner join paymentgatewayorder ON paymentgatewaytransactions.TrackingNo=paymentgatewayorder.TrackingNo ' +
                'inner join templesettlementdetails on templesettlementdetails.TempleID=paymentgatewayorder.TempleID ' +
                'inner join temple on temple.TempleID=booking.TempleID and temple.LanguageID=1 ' +
                'where paymentgatewaytransactions.TrackingNo in(select TrackingNo from booking) and TransactionStatus=' + "'S'" + ' and temple.BankName=' + "'fonepaisa_temple'" + ' and paymentgatewaytransactions.TrackingNo in('+trackingNos+') and ' +
                'paymentgatewaytransactions.TrackingNo not in(select TrackingNo from paymentgatewayorderstatus);';
                callback();
                doCallback();
            }]);
    },
        function(callback){
            console.log(sql);
            mysql.getConnection(function (err, con) {
                con.query(sql, function (err, setrow) {
                    con.release();
                    var results;
                    if (err) {
                        callback();
                        console.log('error', err, "", 'Masterbox.js/transferAmount ');
                        return;
                    }
                    if (setrow.length == 0) {
                        callback();
                            res.send('No transactions or allready requested for transfer');
                    }
                    else {
                        results = JSON.stringify(setrow);
                        transactionList=setrow;
                        setrow.forEach(function (row) {
                            var sql = "insert into paymentgatewayorderstatus value ('" + row.TrackingNo + "',now(),'" + "New" + "');";
                            mysql.getConnection(function (err, con) {
                                con.query(sql, function (err, rows) {
                                    con.release();
                                    var results;
                                    if (err) {
                                        console.log('error', err, 'Masterbox.js/transferAmount ');
                                        return;
                                    } else {
                                        results = JSON.stringify(rows);
                                    }
                                });
                            });
                        });
                        setrow.forEach(function (row) {
                            var sql = "call generatesettlementdata('" + row.TrackingNo + "')";
                            mysql.getConnection(function (err, con) {
                                con.query(sql, function (err, rows) {
                                    con.release();
                                    var results;
                                    if (err) {
                                        console.log('error', err, "", 'Masterbox.js/transferAmount ');
                                        return;
                                    } else {
                                        results = JSON.stringify(rows);
                                    }
                                });
                            });
                        });
                        setrow.forEach(function (row) {
                            requests.push(
                                {
                                    'request_id':row.TrackingNo,
                                    'acct_name':row.BenficiaryName,
                                    'acct_number':row.BenficiaryBankACNumber,
                                    'ifsc_code':row.BenficiaryBankIFSC,
                                    'transfer_type':'N',
                                    'amount':row.Amount
                                }
                            );
                            //requests.push(
                            //    {
                            //        'request_id':row.TrackingNo+'-DEV',
                            //        'acct_name':'Devaayanam Network Pvt Ltd',
                            //        'acct_number':'10940200012933',
                            //        'ifsc_code':'FDRL0001094',
                            //        'transfer_type':'I',
                            //        'amount':row.GatewayTransactionAmount-row.Amount
                            //    }
                            //);
                        });
                        setTimeout(function(){callback();},1000);
                    }
                });
            });
        },function(callback) {
            exports.checkTransferStatus({},function(data) {
                    var postForm = {
                        url: 'https://secure.fonepaisa.com/portal/funds/transfer',
                        json: options
                    };
                    console.log(JSON.stringify(options));
                    if (requests.length > 0) {
                        request.post(postForm, function (err, ress, body) {
                            res.end(ress.body.resp_msg);
                            console.log('fp res:'+JSON.stringify(ress));
                            if(ress.body.resp_code!='0000') {
                                transactionList.forEach(function (row) {
                                    var sql = "delete from paymentgatewayorderstatus where paymentgatewayorderstatus.TrackingNo= '"+row.TrackingNo+"';";
                                    mysql.getConnection(function (err, con) {
                                        con.query(sql, function (err, rows) {
                                            con.release();
                                            var results;
                                            if (err) {
                                                console.log('error', err, 'MasterBox.js.js/auto transfer ');
                                                return;
                                            } else {
                                                results = JSON.stringify(rows);
                                            }
                                        });
                                    });
                                });
                            }
                            var tableRow='';
                            ress.body.requests.forEach(function(row){
                                tableRow=tableRow+'<tr><td>'+row.request_id+'</td><td>'+row.acct_name+'</td><td>'+row.amount+'</td><td>'+row.resp_code+'</td><td>'+row.resp_msg+'</td><tr>';
                            });
                            var template='<html><body><div style="height: 100px;text-align: center;padding-top: 30px;padding-bottom: 15px;font-size: 27px;">'+ress.body.resp_msg+'('+ress.body.resp_code+')</div>' +
                                '<table><tr><th>TrackingNo</th><th>AccountName</th><th>Amount</th><th>ResponseCode</th><th>ResponseMsg</th></tr>'+tableRow+'</table>' +
                                '</body></html>';
                            console.log(template);
                            var emailData={
                                to:'support@devaayanam.in',
                                subject:'Transfer Request Status, Id:'+ress.body.request_id,
                                template:template
                            }
                            sendMail.send_mail_from_data(emailData,function(resp){
                                console.log(resp);
                            });
                            callback();
                            if (err) {
                                console.log('r..' + err);
                            }
                            if (ress.statusCode == 200) {

                            }
                        });
                    } else
                    {
                        res.end('no new transactions');
                    }
            });
        }]);
}
exports.transferServiceCharge=function(req,res) {
    var trackingNos='';
    var request = require('request');
    const crypto = require('crypto');
    const sign = crypto.createSign('RSA-SHA512');
    var request = require('request');
    var transactionList;
    var privatekey ='-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAkXxYpKPClp/sl2BihnlPXXWK2L2gFYUf1F4G4VxpelyhNMAA\nrkxIl21lBYBMGW0y6k2pHu5iWorfcKl53s5Vhqs+wesv/EvhowCLjMg5ZNvc5fch\nzRA5D7iXvzTO3+01YT5SsQ+hBvus7ZVwlDj+OgqqjuPTcfC4xO1Ch3+Yh+9wjKVp\nSszkZUeu/fG2wogq6mHlJdZOkGUawYaqbcnxYPLxKXlBow1A4Q1eehpHNNGqJfFc\nzXgHenJRD6e/ddFjRAgYvc8ayRfhReNHs9X5QBgv+dZt5R+mPFWDOp8fl6Sv6QLu\n5MHH4odIVBsbeUqg54DeUgseClYl/WVLnQxrDQIDAQABAoIBAAtuJpTA3P/yjqcS\nBoukKk0geAkxVMt8CxUnUgkQtqTLdErtruAC1E05Zg95lgEFaFOqSFhKyGVutcFn\nP1D3CxcqiyK3NKzw1Uh2OYCVFpVLBN74fKpc1O7cROfb4UkMnP4H5H1Oygr/aQW9\nkPvEQo0S23WghrNUA2BNd8Wni7daRTBKOQ5GZCr8eDjDyhTnfQIjkke3BNj2mrQ0\n5Ykm7t8V4RLzYpLnK+VJB/j3jeQHJzBEhOn284bu+4H6/kzDjak8NJmGGAw2XTrt\njfQuRgTOq114H2PpHtmeXDvGbmVqhgnxTFrBhBlWauyxR39fezL/PckoAqW4Apf1\nOHAIA6ECgYEAzHSDIwjgSgGycKVhKYCwO7wWHvNFM39fNNZl/RKHbd6jwQ1hQFJs\nr6wkOsp99vggLphhURFq5SI7tadkYg5ZLDxy0LBAJxAkIGOeVtJhTvc/9Bb5UeZ9\ncFYr9skGKyPSeg8EbKwuhncSObFZFdOqPb9aeV2EWTcK+ZI/9gRIhyUCgYEAtin2\nWx1PLDG7w9QDzoDCNjJ+RGsU7lIhtQ4ABmZS3rEOmIbdtYeg4CMEBjP8B0VxCW2o\nctOTJobS5lWxeidKWn1F8Gmzql9jjFEm6rqQjBn5yQehjb+qLWkCudR0Tc3QvvOh\n0Z5a7ovlzjH8h/nmdjmUSGWlWv0GP4/MK2xLY8kCgYEAy59Gc4AFkO87vgCXUfQ+\nkF90UILL6svyD/dvP6pSgtP3lu8yahMs95CjzIGbDnpz4rQUR97m7Sk4+mOqEBZm\nLS5O0xSV79GdiTxtl96S10hbw0eWK0E4sMbBpljy8cgNeU63g1vAQl8YIalFJf3W\nUQTgoHrIsumk3oYPeX8ulJkCgYEAss5jtDIDyeSTYvhUODno6JwNlwjpBbl2vqqc\nWavMwixXoF4ZeBtGTVvxkkkqpNSDzPZndza4ZQPINjF9QOMaN9JpeT+4DI8PPudV\nZOh2BB9nMO2dpW6yE1rVQyUIeGqC8Txh5UkkBq0piIlGUQ56KigApkOn2LUukeuL\nREBLjAkCgYBLOjnfRWg/EhM/KtjPX9mYCeVOccZ5QZXr7ZgOiZN16vNvl94oD8u4\nGjKqoSigh+csxFI0rcPtFVBUNTYcUKqRW5B8OWFQARxP4O8+2L7gF+Gn/NiicNjC\nJh5WDm4CMhceNvGyvomntUZzmU6/jpXU84IdkLJnqVQZcadxGySjYg==\n-----END RSA PRIVATE KEY-----';
    var api_key = '2OBI840030A19I0KBHJ4RA0CM7H5Q9X';
    var req_id = '2018-12-19-ALL';
    var $hashinput = api_key + "#" + 'A935' + "#" + 'A935' + "#" + req_id + "#";
    sign.write($hashinput);
    sign.end();
    var encryptedData = sign.sign(privatekey, 'hex');
    var requests=[{
        "request_id": '5265-1',
        "acct_name": "Panthalayani Sree Aghora Siva temple",
        "acct_number": "4343000100092198",
        "ifsc_code": "PUNB0434300",
        "transfer_type": "N",
        "amount": 40
    },
        {
            "request_id": '5266-1',
            "acct_name": "Sree Randumoorthy Devaswam",
            "acct_number": "854610110006036",
            "ifsc_code": "BKID0008546",
            "transfer_type": "N",
            "amount": 100
        },
        {
            "request_id": '5270-1',
            "acct_name": "Ezhipuram Bhagavathi Temple",
            "acct_number": "857010100003232",
            "ifsc_code": "BKID0008570",
            "transfer_type": "N",
            "amount": 75
        },
        {
            "request_id": '5304-1',
            "acct_name": "pookulangara devaswam kunisery",
            "acct_number": "002900100000802",
            "ifsc_code": "dlxb0000029",
            "transfer_type": "N",
            "amount": 290
        },
        {
            "request_id": '5341-1',
            "acct_name": "Sree Randumoorthy Devaswam",
            "acct_number": "854610110006036",
            "ifsc_code": "BKID0008546",
            "transfer_type": "N",
            "amount": 202
        },
        {
            "request_id": '5342-1',
            "acct_name": "Sree Randumoorthy Devaswam",
            "acct_number": "854610110006036",
            "ifsc_code": "BKID0008546",
            "transfer_type": "N",
            "amount": 230
        }];
    var options =
    {
        id: 'A935',
        merchant_id: 'A935',
        request_id:req_id,
        sign: encryptedData,
        requests: requests
    }
    var postForm = {
        url: 'https://secure.fonepaisa.com/portal/funds/transfer',
        json: options
    };
    //requests.push({
    //        'request_id':'5265-DEV-2',
    //        'acct_name':'Devaayanam Network Pvt Ltd',
    //        'acct_number':'10940200012933',
    //        'ifsc_code':'FDRL0001094',
    //        'transfer_type':'N',
    //        'amount':'10'
    //    });
    //requests.push({
    //        'request_id':'5266-DEV-2',
    //        'acct_name':'Devaayanam Network Pvt Ltd',
    //        'acct_number':'10940200012933',
    //        'ifsc_code':'FDRL0001094',
    //        'transfer_type':'N',
    //        'amount':'20'
    //    });
    //requests.push({
    //        'request_id':'5270-DEV-2',
    //        'acct_name':'Devaayanam Network Pvt Ltd',
    //        'acct_number':'10940200012933',
    //        'ifsc_code':'FDRL0001094',
    //        'transfer_type':'N',
    //        'amount':'10'
    //    });
    if (requests.length > 0) {
        request.post(postForm, function (err, ress, body) {
            console.log('fp res:'+JSON.stringify(ress));
            if(ress.body.resp_code!='0000') {
                console.log('error');
            }
            if (err) {
                console.log( err);
            }else {
                var tableRow='';
                ress.body.requests.forEach(function(row){
                    tableRow=tableRow+'<tr><td>'+row.request_id+'</td><td>'+row.acct_name+'</td><td>'+row.amount+'</td><td>'+row.resp_code+'</td><td>'+row.resp_msg+'</td><tr>';
                });
                var template='<html><body><div style="height: 100px;text-align: center;padding-top: 30px;padding-bottom: 15px;font-size: 27px;">'+ress.body.resp_msg+'('+ress.body.resp_code+')</div>' +
                    '<table><tr><th>TrackingNo</th><th>AccountName</th><th>Amount</th><th>ResponseCode</th><th>ResponseMsg</th></tr>'+tableRow+'</table>' +
                    '</body></html>';
                console.log(template);
                var emailData={
                    to:'support@devaayanam.in',
                    subject:'Transfer Request Status, Id:'+ress.body.request_id,
                    template:template
                }
                sendMail.send_mail_from_data(emailData,function(resp){
                    console.log(resp);
                });
            }
            if (ress.statusCode == 200) {
            }
        });
    } else
    {
        res.end('no new transactions');
    }
}

exports.logAllTransactions=function() {
    var request = require('request');
    const crypto = require('crypto');
    const sign = crypto.createSign('RSA-SHA512');
    var request = require('request');
    var privatekey ='-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEAkXxYpKPClp/sl2BihnlPXXWK2L2gFYUf1F4G4VxpelyhNMAA\nrkxIl21lBYBMGW0y6k2pHu5iWorfcKl53s5Vhqs+wesv/EvhowCLjMg5ZNvc5fch\nzRA5D7iXvzTO3+01YT5SsQ+hBvus7ZVwlDj+OgqqjuPTcfC4xO1Ch3+Yh+9wjKVp\nSszkZUeu/fG2wogq6mHlJdZOkGUawYaqbcnxYPLxKXlBow1A4Q1eehpHNNGqJfFc\nzXgHenJRD6e/ddFjRAgYvc8ayRfhReNHs9X5QBgv+dZt5R+mPFWDOp8fl6Sv6QLu\n5MHH4odIVBsbeUqg54DeUgseClYl/WVLnQxrDQIDAQABAoIBAAtuJpTA3P/yjqcS\nBoukKk0geAkxVMt8CxUnUgkQtqTLdErtruAC1E05Zg95lgEFaFOqSFhKyGVutcFn\nP1D3CxcqiyK3NKzw1Uh2OYCVFpVLBN74fKpc1O7cROfb4UkMnP4H5H1Oygr/aQW9\nkPvEQo0S23WghrNUA2BNd8Wni7daRTBKOQ5GZCr8eDjDyhTnfQIjkke3BNj2mrQ0\n5Ykm7t8V4RLzYpLnK+VJB/j3jeQHJzBEhOn284bu+4H6/kzDjak8NJmGGAw2XTrt\njfQuRgTOq114H2PpHtmeXDvGbmVqhgnxTFrBhBlWauyxR39fezL/PckoAqW4Apf1\nOHAIA6ECgYEAzHSDIwjgSgGycKVhKYCwO7wWHvNFM39fNNZl/RKHbd6jwQ1hQFJs\nr6wkOsp99vggLphhURFq5SI7tadkYg5ZLDxy0LBAJxAkIGOeVtJhTvc/9Bb5UeZ9\ncFYr9skGKyPSeg8EbKwuhncSObFZFdOqPb9aeV2EWTcK+ZI/9gRIhyUCgYEAtin2\nWx1PLDG7w9QDzoDCNjJ+RGsU7lIhtQ4ABmZS3rEOmIbdtYeg4CMEBjP8B0VxCW2o\nctOTJobS5lWxeidKWn1F8Gmzql9jjFEm6rqQjBn5yQehjb+qLWkCudR0Tc3QvvOh\n0Z5a7ovlzjH8h/nmdjmUSGWlWv0GP4/MK2xLY8kCgYEAy59Gc4AFkO87vgCXUfQ+\nkF90UILL6svyD/dvP6pSgtP3lu8yahMs95CjzIGbDnpz4rQUR97m7Sk4+mOqEBZm\nLS5O0xSV79GdiTxtl96S10hbw0eWK0E4sMbBpljy8cgNeU63g1vAQl8YIalFJf3W\nUQTgoHrIsumk3oYPeX8ulJkCgYEAss5jtDIDyeSTYvhUODno6JwNlwjpBbl2vqqc\nWavMwixXoF4ZeBtGTVvxkkkqpNSDzPZndza4ZQPINjF9QOMaN9JpeT+4DI8PPudV\nZOh2BB9nMO2dpW6yE1rVQyUIeGqC8Txh5UkkBq0piIlGUQ56KigApkOn2LUukeuL\nREBLjAkCgYBLOjnfRWg/EhM/KtjPX9mYCeVOccZ5QZXr7ZgOiZN16vNvl94oD8u4\nGjKqoSigh+csxFI0rcPtFVBUNTYcUKqRW5B8OWFQARxP4O8+2L7gF+Gn/NiicNjC\nJh5WDm4CMhceNvGyvomntUZzmU6/jpXU84IdkLJnqVQZcadxGySjYg==\n-----END RSA PRIVATE KEY-----';
    var api_key = '2OBI840030A19I0KBHJ4RA0CM7H5Q9X';
    var  req_id = new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate();
    var $hashinput = api_key + "#" + 'A935' + "#" + 'A935' + "#" + req_id + "#";
    sign.write($hashinput);
    sign.end();
    var encryptedData = sign.sign(privatekey, 'hex');
    var requests=[];
    var transactionList;
    var options =
    {
        id: 'A935',
        merchant_id: 'A935',
        request_id:req_id,
        sign: encryptedData,
        requests: requests
    }
    async.series([function(callback) {
        var sql = 'select paymentgatewaytransactions.TrackingNo,(paymentgatewayorder.GatewayTransactionAmount/100) as GatewayTransactionAmount,booking.BillAmount as Amount,templesettlementdetails.BenficiaryName,templesettlementdetails.BenficiaryBankACNumber,templesettlementdetails.BenficiaryBankIFSC ' +
            'from paymentgatewaytransactions ' +
            'inner join booking ON paymentgatewaytransactions.TrackingNo= booking.TrackingNo ' +
            'inner join sessiontrackingdetails on sessiontrackingdetails.TrackingNo=paymentgatewaytransactions.TrackingNo ' +
            'inner join bookingaddress on sessiontrackingdetails.SessionID=bookingaddress.TrackingNo and `bookingaddress`.`Type` in(' + "'guest'" + ',' + "'customer'" + ') ' +
            'inner join paymentgatewayorder ON paymentgatewaytransactions.TrackingNo=paymentgatewayorder.TrackingNo ' +
            'inner join templesettlementdetails on templesettlementdetails.TempleID=paymentgatewayorder.TempleID ' +
            'inner join temple on temple.TempleID=booking.TempleID and temple.LanguageID=1 where paymentgatewaytransactions.TrackingNo in(select TrackingNo from booking) and Date(TransactionRequestedTime) > subdate(curdate(),interval 100 day) and TransactionStatus=' + "'S'" + ' and temple.BankName=' + "'fonepaisa_temple'"+' and paymentgatewaytransactions.TrackingNo>5093;';
        console.log(sql);
        mysql.getConnection(function (err, con) {
            con.query(sql, function (err, setrow) {
                con.release();
                var results;
                if (err) {
                    callback();
                    return;
                }
                if (setrow.length == 0) {
                    callback();
                    console.log('info', 'No match found for query', "", 'Search.js/auto settilement ');
                }
                else {
                    transactionList=setrow;
                    results = JSON.stringify(setrow);
                    setrow.forEach(function (row) {
                        requests.push(
                            {
                                'request_id':row.TrackingNo,
                                'acct_name':row.BenficiaryName,
                                'acct_number':row.BenficiaryBankACNumber,
                                'ifsc_code':row.BenficiaryBankIFSC,
                                'transfer_type':'N',
                                'amount':row.Amount
                            }
                        );
                        //requests.push(
                        //    {
                        //        'request_id':row.TrackingNo+'-DEV',
                        //        'acct_name':'Devaayanam Network Pvt Ltd',
                        //        'acct_number':'10940200012933',
                        //        'ifsc_code':'FDRL0001094',
                        //        'transfer_type':'N',
                        //        'amount':row.GatewayTransactionAmount-row.Amount
                        //    }
                        //);
                    });
                    setTimeout(function(){callback();},1000);
                }
            });
        });
    },function(callback) {
console.log('ar..'+JSON.stringify(requests));
    }]);
}
exports.transferStatus=function(req,res) {
    var data={}
    if(req.query.date) {
        data.req_id=req.query.date;
    }
    if(req.query.from) {
        data.from=req.query.from;
    }
    if(req.query.to) {
        data.to=req.query.to;
    }
    exports.checkTransferStatus(data,function(result){
        res.send(JSON.stringify(result));
    });
}
exports.autosendsettlement=function() {
    exports.autoTransferAmount();
    var xldata=new Array();
    mysql.getConnection(function(err, con){
        var sql ='select paymentgatewayorder.SettlementStatus,TransactionRequestedTime,paymentgatewaytransactions.TrackingNo,TransactionStatus,GatewayTransactionAmount,FirstName,bookingaddress.Email as LoginID,' +
            'SettlementRequestedDate,SettlementCompleteDate,Commission ' +
            'from paymentgatewaytransactions ' +
            'inner join booking ON paymentgatewaytransactions.TrackingNo= booking.TrackingNo ' +
            'inner join sessiontrackingdetails on sessiontrackingdetails.TrackingNo=paymentgatewaytransactions.TrackingNo ' +
            'inner join bookingaddress on sessiontrackingdetails.SessionID=bookingaddress.TrackingNo and `bookingaddress`.`Type` in(' + "'guest'" + ',' + "'customer'" + ') ' +
            'inner join paymentgatewayorder ON paymentgatewaytransactions.TrackingNo=paymentgatewayorder.TrackingNo ' +
            'inner join templesettlementdetails on templesettlementdetails.TempleID=paymentgatewayorder.TempleID ' +
            'inner join temple on temple.TempleID=booking.TempleID and temple.LanguageID=1 where paymentgatewaytransactions.TrackingNo in(select TrackingNo from booking) and Date(TransactionRequestedTime) = subdate(curdate(),interval 2 day) and TransactionStatus=' + "'S'" + ' and temple.BankName=' + "'fed_temple'" + ';';
        logger.log('info','Database connection open','','Search.js/load settilement ');
        con.query(sql,function(err, setrow){
            con.release();
            var results;
            if (err){
                // res.end( '[]');
                console.log('error',err,"",'Search.js/auto settilement ');
                return;
            }
            if (setrow.length == 0){
                // res.end( '[]');
                console.log('info','No match found for query',"",'Search.js/auto settilement ');
            }
            else{
                results=JSON.stringify(setrow);
                // res.end(results);
                logger.log('data',results,"",'Search.js/auto settilement ');


                console.log(JSON.stringify(setrow));
                setrow.forEach(function(row){
                    var sql="insert into paymentgatewayorderstatus value ('"+row.TrackingNo+"',now(),'"+"New"+"');";
                    console.log('temple list query ' + sql)
                    mysql.getConnection(function(err, con){
                        logger.log('info','Database connection open','','Search.js/load settilement ');
                        con.query(sql,function(err, rows){
                            con.release();
                            var results;
                            if (err){
                                // res.end( '[]');
                                logger.log('error',err,'','Search.js/load settilement ');
                                return;
                            }
                            if (rows.length == 0){
                                // res.end( '[]');
                                logger.log('info','No match found for query','','Search.js/load settilement ');
                            }
                            else{
                                results=JSON.stringify(rows);
                                // res.end(results);
                                logger.log('data',results,'','Search.js/load settilement ');
                            }
                        });
                    });
                })

                var expirydate;
                var data='';
                var transactiondate;
                var tablerows='';
                var toaddresses="";
                async.series([
                    function(callback) {
                        var index=1;
                        setrow.forEach(function (row) {
                            var sql = "call generatesettlementdata('" + row.TrackingNo + "')";
                            console.log('temple list query ' + sql)
                            mysql.getConnection(function (err, con) {
                                logger.log('info', 'Database connection open' + sql,"", 'Search.js/load settilement ');
                                con.query(sql, function (err, rows) {
                                    con.release();
                                    var results;
                                    if (err) {
                                        // res.end('[]');
                                        logger.log('error', err,"", 'Search.js/load settilement ');
                                        callback();
                                        return;
                                    }
                                    if (rows.length == 0) {
                                        // res.end('[]');
                                        callback();
                                        logger.log('info', 'No match found for query',"", 'Search.js/load settilement ');
                                    }
                                    else {
                                        results = JSON.stringify(rows);
                                        // res.end(results);
                                        var now = new Date();
                                        console.log("fulldata.." + results)
                                        //console.log("expirydate"+expirydate+"now:"+now.getFullYear()+"-"+now.getMonth()+"-"+now.getDay());

                                        xldata.push(rows[0][0]);
                                        console.log('push..');
                                        // res.end(JSON.stringify(rows[0]));

                                        rows[0].forEach(function (row) {
                                            console.log("transactiondate.." + row.TransactionDate);
                                            var date = new Date(row.TransactionDate);
                                            transactiondate=new Date(row.TransactionDate);
                                            var month =(date.getMonth()+1);
                                            var day =(date.getDate());
                                            if((date.getMonth()+1)<10) {
                                                month ='0'+(date.getMonth()+1);
                                            }
                                            if((date.getDate()+1)<10) {
                                                day ='0'+(date.getDate());
                                            }
                                            expirydate = day + "/" +month+'/'+date.getFullYear();
                                            row.TransactionDate=expirydate;
                                            console.log("transactiondate.." + new Date() + "Expiry DATE" + date);


                                            tablerows += '<tr>' +
                                            '<td>' + row.UniqueTransactionIdentificationNumber + '</td>' +
                                            '<td>' + row.BenficiaryIdentification + '</td>' +
                                            '<td>' + row.RemitterDetails + '</td>' +
                                            '<td>' + row.RemitterAccountNo + '</td>' +
                                            '<td>' + row.SettlementAmount + '</td>' +
                                            '<td>' + expirydate + '</td>' +
                                            '<td>' + row.BenficiaryName + '</td>' +
                                            '<td>' + row.BenficiaryBankIFSC + '</td>' +
                                            '<td>' + row.BenficiaryBankName + '</td>' +
                                            '<td>' + row.BenficiaryBankBranch + '</td>' +
                                            '<td>' + row.BenficiaryBankACType + '</td>' +
                                            '<td>' + row.BenficiaryBankACNumber + '</td>' +
                                            '<td>' + row.NatureofPayment + '</td>' +
                                                // '<td>' + row.GatewayTransactionID + '</td>' +
                                                // '<td>' + row.StatusofPayment + '</td>' +
                                                // '<td>' + row.ReasonforRejection + '</td>' +
                                                // '<td>' + row.CreditDate + '</td>' +
                                                // '<td>' + row.ModeofPayment + '</td>' +
                                            '<td></td>' +
                                            '<td></td>' +
                                            '<td></td>' +
                                            '<td></td>' +
                                            '<td></td>' +
                                            '</tr>';
                                        });
                                        if(setrow.length==index){
                                            callback();
                                        }
                                        index=index+1;
                                        //logger.log('data', results, req.session.sessionID, 'Search.js/load settilement ');
                                    }
                                });
                            });
                        });
                    },
                    function(callback) {
                        var template = '<html><body><table border="1px black;"><thead><tr>' +
                            '<th>Unique Transaction Identification Number</th>' +
                            '<th>Benficiary Identification</th>' +
                            '<th>Remitter Details</th>' +
                            '<th>Remitter Account Number </th>' +
                            '<th>Settlement Amount</th>' +
                            '<th>Payment Due Date/Value Date</th>' +
                            '<th>Benficiary Name</th>' +
                            '<th>Benficiary Bank IFSC</th>' +
                            '<th>Benficiary Bank Name</th>' +
                            '<th>Benficiary Bank</th>' +
                            '<th>Benficiary Bank AC/Type</th>' +
                            '<th>Benficiary Bank A/C Number</th>' +
                            '<th>Nature of Payment</th>' +
                            '<th>Unique Transaction Reference(UTR)</th>' +
                            '<th>Status of the Payment</th>' +
                            '<th>Reason for Rejection</th>' +
                            '<th>Credit Date</th>' +
                            '<th>Mode of Payment</th>' +
                            '</tr></thead><tbody>' + tablerows + '</tbody></table></body></html>';
                        var newtemplate = '<!doctype html><html><head><meta charset="utf-8"><title>Untitled Document</title><link href= "https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"></head><body style="font-family: Open Sans, sans-serif; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;"><section class="header" style="width:100%; height:auto;"><table style="width:650px; margin:0px auto; border:0px; border-collapse:collapse;"><tr style="	width:650px; height:auto; text-align:center;"><td><img src="http://viralfever.in/dev/mail/images/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px;height:auto;background-image:url(http://viralfever.in/dev/mail/images/background.jpg);background-size:cover;text-align:left;height:auto;"><td><h5 style="font-weight:600; font-size:17px; color:#b74b03;  padding-left:25px;"></h5><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;"></p>' + template + '<p style="font-weight:400; font-size:14px; color:#2d2d2d; padding-left:70px; padding-right:25px; line-height:25px;">Visit <a href="#" style="color:#603; text-decoration:none;">www.devaayanam.in</a> to find the list of temples onboarded</p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">List of temples <a href="#" style="color:#603; text-decoration:none;">-http://www.devaayanam.in/List</a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;"></p><h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:20px;  padding-left:70px; padding-right:25px; line-height:25px;">Team Devaayanam</h4></td></tr><tr style="width:650px; height:auto;text-align:center;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:center;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:center;"><TD><img src="http://viralfever.in/dev/mail/images/banner..jpg"/></TD></tr><tr style="width:650px;height:auto;margin:0px auto;background-color:#CCC;text-align:center;height:auto;"><td><h6 style="font-size:14px;padding-top:0px;padding-bottom:0px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                        if(true) {
                            async.series([
                                function(callback) {
                                    var Excel = require('exceljs');
                                    var workbook = new Excel.Workbook();
                                    var sheet1 = workbook.addWorksheet('Txns');
                                    var sheet2 = workbook.addWorksheet('Svc charge');
                                    sheet1.columns = [
                                        {header: '', key: 'n', width: 10},
                                        {
                                            header: 'Unique Transaction Identification Number',
                                            key: 'UniqueTransactionIdentificationNumber',
                                            width: 32
                                        },
                                        {header: 'Beneficiary Identification', key: 'BenficiaryIdentification', width: 20},
                                        {header: 'Remitter Details', key: 'RemitterDetails', width: 30},
                                        {header: 'Remitter Account No', key: 'RemitterAccountNo', width: 22},
                                        {header: 'Transaction Amount', key: 'SettlementAmount', width: 17},
                                        {header: 'Payment Due Date / Value Date', key: 'TransactionDate', width: 23},
                                        {header: 'Beneficiary Name', key: 'BenficiaryName', width: 34},
                                        {header: 'Beneficiary Bank IFSC', key: 'BenficiaryBankIFSC', width: 20},
                                        {header: 'Beneficiary Bank Name', key: 'BenficiaryBankName', width: 25},
                                        {header: 'Beneficiary Bank Branch', key: 'BenficiaryBankBranch', width: 25},
                                        {header: 'Beneficiary Bank A/C Type', key: 'BenficiaryBankACType', width: 25},
                                        {header: 'Beneficiary Bank A/C Number', key: 'BenficiaryBankACNumber', width: 28},
                                        {header: 'Nature of Payment', key: 'NatureofPayment', width: 25},
                                        {header: 'Unique Transaction Reference (UTR)', key: 'utr', width: 15},
                                        {header: 'Status of the Payment', key: 'sop', width: 15},
                                        {header: 'Reason for Rejection', key: 'rfr', width: 15},
                                        {header: 'Credit Date', key: 'cd', width: 15},
                                        {header: 'Mode of Payment', key: 'mop', width: 15},

                                    ];
                                    sheet2.columns = [
                                        {header: '', key: 'n', width: 10},
                                        {
                                            header: 'Unique Transaction Identification Number',
                                            key: 'UniqueTransactionIdentificationNumber',
                                            width: 32
                                        },
                                        {header: 'Beneficiary Identification', key: 'BenficiaryIdentification', width: 20},
                                        {header: 'Remitter Details', key: 'RemitterDetails', width: 30},
                                        {header: 'Remitter Account No', key: 'RemitterAccountNo', width: 22},
                                        {header: 'Transaction Amount', key: 'SettlementAmount', width: 17},
                                        {header: 'Payment Due Date / Value Date', key: 'TransactionDate', width: 23},
                                        {header: 'Beneficiary Name', key: 'BenficiaryName', width: 34},
                                        {header: 'Beneficiary Bank IFSC', key: 'BenficiaryBankIFSC', width: 20},
                                        {header: 'Beneficiary Bank Name', key: 'BenficiaryBankName', width: 25},
                                        {header: 'Beneficiary Bank Branch', key: 'BenficiaryBankBranch', width: 25},
                                        {header: 'Beneficiary Bank A/C Type', key: 'BenficiaryBankACType', width: 25},
                                        {header: 'Beneficiary Bank A/C Number', key: 'BenficiaryBankACNumber', width: 28},
                                        {header: 'Nature of Payment', key: 'NatureofPayment', width: 25},
                                        {header: 'Unique Transaction Reference (UTR)', key: 'utr', width: 15},
                                        {header: 'Status of the Payment', key: 'sop', width: 15},
                                        {header: 'Reason for Rejection', key: 'rfr', width: 15},
                                        {header: 'Credit Date', key: 'cd', width: 15},
                                        {header: 'Mode of Payment', key: 'mop', width: 15},

                                    ];
                                    sheet1.addRows([{}]);
                                    sheet2.addRows([{}]);
                                    sheet1.addRows(xldata);
                                    xldata.forEach(function (row) {
                                        row.BenficiaryIdentification = '63839752';
                                        row.RemitterDetails = 'Devaayanam Networks Private Limited';
                                        row.RemitterAccountNo = '10940200012933';
                                        row.SettlementAmount = '10';
                                        row.BenficiaryName = 'Devaayanam Network Pvt Ltd';
                                        row.BenficiaryBankIFSC = 'FDRL0001094';
                                        row.BenficiaryBankName = 'Federal Bank';
                                        row.BenficiaryBankBranch = 'Tripunithura, Ernakulam';
                                        row.BenficiaryBankACType = '11';
                                        row.BenficiaryBankACNumber = '10940200012958';
                                        row.NatureofPayment = 'Card Processing Settlement';
                                    })
                                    sheet2.addRows(xldata);
                                    workbook.xlsx.writeFile(__dirname + '/../../client/files/settlementfile.xlsx').then(function() {
                                        callback();
                                    });;

                                },
                                function(callback) {
                                    var sql ='select * from templeconfig where templeconfig.Key='+"'ENABLE_AUTO_SETTLEMENT'"+' and  templeconfig.Value='+"'true'"+' and templeconfig.ConfigID=1 and templeconfig.TempleID=0;';
                                    mysql.getConnection(function(err, con){
                                        logger.log('info','Database connection open','','Search.js/load settilement ');
                                        con.query(sql,function(err, rows){
                                            con.release();
                                            var results;
                                            if (err){
                                                // res.end( '[]');
                                                console.log('error',err,'','Search.js/load settilement ');
                                                return;
                                            }
                                            if (rows.length == 0){
                                                // res.end( '[]');
                                                console.log('info','No match found for query','','Search.js/load settilement ');
                                            }
                                            else{
                                                callback();
                                                toaddresses=rows[0].Description;
                                                console.log("toa.."+JSON.stringify(rows));
                                                console.log("toa.."+toaddresses);
                                                console.log('data','','','Search.js/load settilement ');
                                            }
                                        });
                                    });

                                },
                                function(callback) {
                                    // var date = transactiondate;
                                    var date = new Date();
                                    var month = (date.getMonth() + 1);
                                    var day = (date.getDate());
                                    if ((date.getMonth() + 1) < 10) {
                                        month = '0' + (date.getMonth() + 1);
                                    }
                                    if ((date.getDate() + 1) < 10) {
                                        day = '0' + (date.getDate());
                                    }
                                    var attachments = [{
                                        filename: 'settlementfile ' + date.getFullYear() + month + day + '.xlsx',
                                        path: __dirname + '/../../client/files/settlementfile.xlsx'
                                    }];
                                    var emailinfo = {
                                        event: 'SENDFORSETTLEMENT',
                                        from: 'support@devaayanam.in',
                                        to: toaddresses,
                                        template: template,
                                        attachment: attachments

                                    };
                                    console.log("template.." + JSON.stringify(emailinfo));
                                    var mailer = require('nodemailernew');
                                    var transport = mailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                            user: 'support@devaayanam.in',
                                            pass: '@Temple123'
                                        }
                                    });
                                    fs.readFile(__dirname + '/../../client/files/settlementfile.xlsx', function (err, data) {
                                        console.log(err);
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            //res.end('Success');
                                            transport.sendMail({
                                                sender: 'support@devaayanam.in',
                                                to: toaddresses,
                                                subject: 'Settlement file-' + day + '/' + month + '/' + date.getFullYear(),
                                                // html:template,
                                                attachments: [{
                                                    'filename': 'settlementfile ' + date.getFullYear() + month + day + '.xlsx',
                                                    'content': data
                                                }]
                                            }), function (err, success) {
                                                if (err) {
                                                    console.log("mailerror.." + err);
                                                } else {
                                                    // res.end('Success');
                                                }

                                            }
                                        }
                                    });
                                    callback();
                                } ]);

                            // emailtemplate.prepare_mail_template(emailinfo,function(response){
                            //     if(response=='S') {
                            //         res.end('Success');
                            //     }
                            // });

                        }
                    }]);
            }
        });
    });
};
exports.sendsettlementdata=function(req,res) {
    if(req.body.mode=='transferAmount') {
        exports.transferAmount(req,res);
    } else
    {
    var xldata = new Array();
    console.log(JSON.stringify(req.body.settlementtrackingno));
    req.body.settlementtrackingno.forEach(function (row) {
        var sql = "insert into paymentgatewayorderstatus value ('" + row + "',now(),'" + "New" + "');";
        console.log('temple list query ' + sql)
        mysql.getConnection(function (err, con) {
            logger.log('info', 'Database connection open', req.session.sessionID, 'Search.js/load settilement ');
            con.query(sql, function (err, rows) {
                con.release();
                var results;
                if (err) {
                    // res.end( '[]');
                    logger.log('error', err, req.session.sessionID, 'Search.js/load settilement ');
                    return;
                }
                if (rows.length == 0) {
                    // res.end( '[]');
                    logger.log('info', 'No match found for query', req.session.sessionID, 'Search.js/load settilement ');
                }
                else {
                    results = JSON.stringify(rows);
                    // res.end(results);
                    logger.log('data', results, req.session.sessionID, 'Search.js/load settilement ');
                }
            });
        });
    })

    console.log(JSON.stringify(req.body))
    var expirydate;
    var data = '';
    var transactiondate;
    var tablerows = '';
    async.series([
        function (callback) {
            var index = 1;
            req.body.settlementtrackingno.forEach(function (row) {


                var sql = "call generatesettlementdata('" + row + "')";
                console.log('temple list query ' + sql)
                mysql.getConnection(function (err, con) {
                    logger.log('info', 'Database connection open' + sql, req.session.sessionID, 'Search.js/load settilement ');
                    con.query(sql, function (err, rows) {
                        con.release();
                        var results;
                        if (err) {
                            // res.end('[]');
                            logger.log('error', err, req.session.sessionID, 'Search.js/load settilement ');
                            callback();
                            return;
                        }
                        if (rows.length == 0) {
                            //callback();
                            if (req.body.settlementtrackingno.length == index) {
                                callback();
                            }
                            logger.log('info', 'No match found for query', req.session.sessionID, 'Search.js/load settilement ');
                        }
                        else {
                            results = JSON.stringify(rows);
                            // res.end(results);
                            var now = new Date();
                            console.log("fulldata.." + results)
                            console.log("fulldata.." + rows[0].length)
                            //console.log("expirydate"+expirydate+"now:"+now.getFullYear()+"-"+now.getMonth()+"-"+now.getDay());
                            if (rows[0].length != 0) {
                                xldata.push(rows[0][0]);
                            }
                            console.log('push..');
                            // res.end(JSON.stringify(rows[0]));

                            rows[0].forEach(function (row) {
                                console.log("transactiondate.." + row.TransactionDate);
                                var date = new Date(row.TransactionDate);
                                transactiondate = new Date(row.TransactionDate);
                                var month = (date.getMonth() + 1);
                                var day = (date.getDate());
                                if ((date.getMonth() + 1) < 10) {
                                    month = '0' + (date.getMonth() + 1);
                                }
                                if ((date.getDate() + 1) < 10) {
                                    day = '0' + (date.getDate());
                                }
                                expirydate = day + "/" + month + '/' + date.getFullYear();
                                row.TransactionDate = expirydate;
                                console.log("transactiondate.." + new Date() + "Expiry DATE" + date);


                                tablerows += '<tr>' +
                                '<td>' + row.UniqueTransactionIdentificationNumber + '</td>' +
                                '<td>' + row.BenficiaryIdentification + '</td>' +
                                '<td>' + row.RemitterDetails + '</td>' +
                                '<td>' + row.RemitterAccountNo + '</td>' +
                                '<td>' + row.SettlementAmount + '</td>' +
                                '<td>' + expirydate + '</td>' +
                                '<td>' + row.BenficiaryName + '</td>' +
                                '<td>' + row.BenficiaryBankIFSC + '</td>' +
                                '<td>' + row.BenficiaryBankName + '</td>' +
                                '<td>' + row.BenficiaryBankBranch + '</td>' +
                                '<td>' + row.BenficiaryBankACType + '</td>' +
                                '<td>' + row.BenficiaryBankACNumber + '</td>' +
                                '<td>' + row.NatureofPayment + '</td>' +
                                    // '<td>' + row.GatewayTransactionID + '</td>' +
                                    // '<td>' + row.StatusofPayment + '</td>' +
                                    // '<td>' + row.ReasonforRejection + '</td>' +
                                    // '<td>' + row.CreditDate + '</td>' +
                                    // '<td>' + row.ModeofPayment + '</td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '</tr>';
                            });
                            if (req.body.settlementtrackingno.length == index) {
                                callback();
                            }
                            index = index + 1;
                            logger.log('data', results, req.session.sessionID, 'Search.js/load settilement ');
                        }
                    });
                });
            });
        },
        function (callback) {
            var template = '<html><body><table border="1px black;"><thead><tr>' +
                '<th>Unique Transaction Identification Number</th>' +
                '<th>Benficiary Identification</th>' +
                '<th>Remitter Details</th>' +
                '<th>Remitter Account Number </th>' +
                '<th>Settlement Amount</th>' +
                '<th>Payment Due Date/Value Date</th>' +
                '<th>Benficiary Name</th>' +
                '<th>Benficiary Bank IFSC</th>' +
                '<th>Benficiary Bank Name</th>' +
                '<th>Benficiary Bank</th>' +
                '<th>Benficiary Bank AC/Type</th>' +
                '<th>Benficiary Bank A/C Number</th>' +
                '<th>Nature of Payment</th>' +
                '<th>Unique Transaction Reference(UTR)</th>' +
                '<th>Status of the Payment</th>' +
                '<th>Reason for Rejection</th>' +
                '<th>Credit Date</th>' +
                '<th>Mode of Payment</th>' +
                '</tr></thead><tbody>' + tablerows + '</tbody></table></body></html>';
            var newtemplate = '<!doctype html><html><head><meta charset="utf-8"><title>Untitled Document</title><link href= "https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"></head><body style="font-family: Open Sans, sans-serif; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;"><section class="header" style="width:100%; height:auto;"><table style="width:650px; margin:0px auto; border:0px; border-collapse:collapse;"><tr style="	width:650px; height:auto; text-align:center;"><td><img src="http://viralfever.in/dev/mail/images/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px;height:auto;background-image:url(http://viralfever.in/dev/mail/images/background.jpg);background-size:cover;text-align:left;height:auto;"><td><h5 style="font-weight:600; font-size:17px; color:#b74b03;  padding-left:25px;"></h5><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;"></p>' + template + '<p style="font-weight:400; font-size:14px; color:#2d2d2d; padding-left:70px; padding-right:25px; line-height:25px;">Visit <a href="#" style="color:#603; text-decoration:none;">www.devaayanam.in</a> to find the list of temples onboarded</p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">List of temples <a href="#" style="color:#603; text-decoration:none;">-http://www.devaayanam.in/List</a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;"></p><h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:20px;  padding-left:70px; padding-right:25px; line-height:25px;">Team Devaayanam</h4></td></tr><tr style="width:650px; height:auto;text-align:center;"><td><img src="' + imageurl + 'email_ad_akshaya.jpg"/><img src="' + imageurl + 'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:center;"><td><img src="' + imageurl + 'email_ad_tesori.jpg"/><img src="' + imageurl + 'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:center;"><TD><img src="http://viralfever.in/dev/mail/images/banner..jpg"/></TD></tr><tr style="width:650px;height:auto;margin:0px auto;background-color:#CCC;text-align:center;height:auto;"><td><h6 style="font-size:14px;padding-top:0px;padding-bottom:0px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
            if (req.body.mode == 'createfile') {
                res.end(JSON.stringify(xldata));
            }
            if (req.body.mode == 'previewmail') {
                res.end(template);
            }
            if (req.body.mode == 'sendmail' && tablerows.length > 2) {
                async.series([
                    function (callback) {
                        var Excel = require('exceljs');
                        var workbook = new Excel.Workbook();
                        var sheet1 = workbook.addWorksheet('Txns');
                        var sheet2 = workbook.addWorksheet('Svc charge');
                        sheet1.columns = [
                            {header: '', key: 'n', width: 10},
                            {
                                header: 'Unique Transaction Identification Number',
                                key: 'UniqueTransactionIdentificationNumber',
                                width: 32
                            },
                            {header: 'Beneficiary Identification', key: 'BenficiaryIdentification', width: 20},
                            {header: 'Remitter Details', key: 'RemitterDetails', width: 30},
                            {header: 'Remitter Account No', key: 'RemitterAccountNo', width: 22},
                            {header: 'Transaction Amount', key: 'SettlementAmount', width: 17},
                            {header: 'Payment Due Date / Value Date', key: 'TransactionDate', width: 23},
                            {header: 'Beneficiary Name', key: 'BenficiaryName', width: 34},
                            {header: 'Beneficiary Bank IFSC', key: 'BenficiaryBankIFSC', width: 20},
                            {header: 'Beneficiary Bank Name', key: 'BenficiaryBankName', width: 25},
                            {header: 'Beneficiary Bank Branch', key: 'BenficiaryBankBranch', width: 25},
                            {header: 'Beneficiary Bank A/C Type', key: 'BenficiaryBankACType', width: 25},
                            {header: 'Beneficiary Bank A/C Number', key: 'BenficiaryBankACNumber', width: 28},
                            {header: 'Nature of Payment', key: 'NatureofPayment', width: 25},
                            {header: 'Unique Transaction Reference (UTR)', key: 'utr', width: 15},
                            {header: 'Status of the Payment', key: 'sop', width: 15},
                            {header: 'Reason for Rejection', key: 'rfr', width: 15},
                            {header: 'Credit Date', key: 'cd', width: 15},
                            {header: 'Mode of Payment', key: 'mop', width: 15},

                        ];
                        sheet2.columns = [
                            {header: '', key: 'n', width: 10},
                            {
                                header: 'Unique Transaction Identification Number',
                                key: 'UniqueTransactionIdentificationNumber',
                                width: 32
                            },
                            {header: 'Beneficiary Identification', key: 'BenficiaryIdentification', width: 20},
                            {header: 'Remitter Details', key: 'RemitterDetails', width: 30},
                            {header: 'Remitter Account No', key: 'RemitterAccountNo', width: 22},
                            {header: 'Transaction Amount', key: 'SettlementAmount', width: 17},
                            {header: 'Payment Due Date / Value Date', key: 'TransactionDate', width: 23},
                            {header: 'Beneficiary Name', key: 'BenficiaryName', width: 34},
                            {header: 'Beneficiary Bank IFSC', key: 'BenficiaryBankIFSC', width: 20},
                            {header: 'Beneficiary Bank Name', key: 'BenficiaryBankName', width: 25},
                            {header: 'Beneficiary Bank Branch', key: 'BenficiaryBankBranch', width: 25},
                            {header: 'Beneficiary Bank A/C Type', key: 'BenficiaryBankACType', width: 25},
                            {header: 'Beneficiary Bank A/C Number', key: 'BenficiaryBankACNumber', width: 28},
                            {header: 'Nature of Payment', key: 'NatureofPayment', width: 25},
                            {header: 'Unique Transaction Reference (UTR)', key: 'utr', width: 15},
                            {header: 'Status of the Payment', key: 'sop', width: 15},
                            {header: 'Reason for Rejection', key: 'rfr', width: 15},
                            {header: 'Credit Date', key: 'cd', width: 15},
                            {header: 'Mode of Payment', key: 'mop', width: 15},

                        ];
                        sheet1.addRows([{}]);
                        sheet2.addRows([{}]);
                        sheet1.addRows(xldata);
                        xldata.forEach(function (row) {
                            row.BenficiaryIdentification = '63839752';
                            row.RemitterDetails = 'Devaayanam Networks Private Limited';
                            row.RemitterAccountNo = '10940200012933';
                            row.SettlementAmount = '10';
                            row.BenficiaryName = 'Devaayanam Network Pvt Ltd';
                            row.BenficiaryBankIFSC = 'FDRL0001094';
                            row.BenficiaryBankName = 'Federal Bank';
                            row.BenficiaryBankBranch = 'Tripunithura, Ernakulam';
                            row.BenficiaryBankACType = '11';
                            row.BenficiaryBankACNumber = '10940200012958';
                            row.NatureofPayment = 'Card Processing Settlement';
                        })
                        sheet2.addRows(xldata);
                        workbook.xlsx.writeFile(__dirname + '/../../client/files/settlementfile.xlsx').then(function () {
                            callback();
                        });
                        ;

                    },
                    function (callback) {
                        // var date = transactiondate;
                        var date = new Date();
                        var month = (date.getMonth() + 1);
                        var day = (date.getDate());
                        if ((date.getMonth() + 1) < 10) {
                            month = '0' + (date.getMonth() + 1);
                        }
                        if ((date.getDate() + 1) < 10) {
                            day = '0' + (date.getDate());
                        }
                        var attachments = [{
                            filename: 'settlementfile ' + date.getFullYear() + month + day + '.xlsx',
                            path: __dirname + '/../../client/files/settlementfile.xlsx'
                        }];
                        var emailinfo = {
                            event: 'SENDFORSETTLEMENT',
                            from: 'support@devaayanam.in',
                            to: req.body.to,
                            template: template,
                            attachment: attachments

                        };
                        console.log("template.." + JSON.stringify(emailinfo));
                        var mailer = require('nodemailernew');
                        var transport = mailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: 'support@devaayanam.in',
                                pass: '@Temple123'
                            }
                        });
                        fs.readFile(__dirname + '/../../client/files/settlementfile.xlsx', function (err, data) {
                            console.log(err);
                            if (err) {
                                console.log(err);
                            } else {
                                res.end('Success');
                                transport.sendMail({
                                    sender: 'support@devaayanam.in',
                                    to: [req.body.to],
                                    subject: 'Settlement file-' + day + '/' + month + '/' + date.getFullYear(),
                                    // html:template,
                                    attachments: [{
                                        'filename': 'settlementfile ' + date.getFullYear() + month + day + '.xlsx',
                                        'content': data
                                    }]
                                }), function (err, success) {
                                    if (err) {
                                        console.log("mailerror.." + err);
                                    } else {
                                        // res.end('Success');
                                    }

                                }
                            }
                        });
                        callback();
                    }]);

                // emailtemplate.prepare_mail_template(emailinfo,function(response){
                //     if(response=='S') {
                //         res.end('Success');
                //     }
                // });

            }
        }]);
    }
};
exports.loadtransactions=function(req,res){
    var templecategory=req.body.Category;
    var from=req.body.from.substring(0,10);
    if(req.body.TempleID=="all"){
        req.body.Mode=1;
        req.body.TempleID=0;
        console.log("all..");
    }else{
        req.body.Mode=0;
        console.log("spe..");
    }
    var sql="call settlementdata('"+req.body.TempleID+"','"+req.body.from.substring(0,10)+"','"+req.body.to.substring(0,10)+"','"+req.body.Mode+"')";
    console.log('temple list query ' + sql)
    mysql.getConnection(function(err, con){
        logger.log('info','Database connection open',req.session.sessionID,'Search.js/load settilement ');
        con.query(sql,function(err, rows){
            con.release();
            var results;
            if (err){
                res.end( '[]');
                logger.log('error',err,req.session.sessionID,'Search.js/load settilement ');
                return;
            }
            if (rows.length == 0){
                res.end( '[]');
                logger.log('info','No match found for query',req.session.sessionID,'Search.js/load settilement ');
            }
            else{
                results=JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,'Search.js/load settilement ');
            }
        });
    });
};
exports.sendreminder=function(req,res){
    console.log(req.body);
    var message="",quantity="";
    var pujadate=new Date(req.body.PujaDate).getDate()+'-'+((new Date(req.body.PujaDate).getMonth()*1)+(1*1))+'-'+new Date(req.body.PujaDate).getFullYear();
    if(req.body.Quantity!=1){
        quantity=',Quantity:'+req.body.Quantity;
    }
    if(req.body.PostalCharge!=0){
        message='Online Booking,TrackingNo:'+req.body.TrackingNo+',PujaDate:'+pujadate+',PujaName:'+req.body.PujaName+',Deity:'+req.body.Deity+',Benficiary:'+req.body.DevoteeName+',Star:'+req.body.StarName+''+quantity+',Postage Amt:'+req.body.PostalCharge+',Mobile:'+req.body.cphonemobile+',Address:'+req.body.chousename+','+req.body.cstreetname+','+req.body.cpostoffice+','+req.body.cdistrict+','+req.body.cpostalcode+','+req.body.cstate;
    }else{
        message='Online Booking,TrackingNo:'+req.body.TrackingNo+',PujaDate:'+pujadate+',PujaName:'+req.body.PujaName+',Deity:'+req.body.Deity+',Benficiary:'+req.body.DevoteeName+',Star:'+req.body.StarName+',Mobile:'+req.body.cphonemobile+',No Shippment';
    }
    var maildata = {
        mobileno: req.body.PhoneMobile,
        message: message
    }
    console.log(JSON.stringify(maildata));
    var qry="insert into remindersms(`templeid`,`smsdate`,`pujaid`,`devoteename`,`devoteestar`,`phonemobile`,`pujadate`,`trackingno`,`addressid`,`postalcharge`)" +
        "values('"+req.body.TempleID+"',now(),'"+req.body.PujaID+"','"+req.body.DevoteeName+"','"+req.body.DevoteeStar+"','"+req.body.cphonemobile+"',date('"+req.body.PujaDate+"'),'"+req.body.TrackingNo+"','"+req.body.AddressID+"','"+req.body.PostalCharge+"')";
    console.log(qry);
    emailtemplate.sendsms(maildata,function(response){
        if(response=="success"){
            mysql.getConnection(function(err, con) {
                con.query(qry, function (err, rows, fields) {
                    con.release();
                    //console.log (rows.length);
                    if (err) {
                        logger.log('error', err, req.session.sessionID, 'report.js/getpujalist');
                        res.end('[{"RESULT":"ERROR","usermessage":"system error. please contact support","systemmessage":"invalid query"}]');
                        return;
                    }
                    if (rows.length == 0) {
                        logger.log('warn', 'No match found for query', req.session.sessionID, 'report.js/getpujalist');
                        res.end('[{"RESULT":"NODATA","usermessage":"no data available","systemmessage":"no data available"}]');
                    }
                    else {
                        var str = JSON.stringify(rows);
                        res.end("Success");
                        logger.log('data', str, req.session.sessionID, 'report.js/getpujalist');
                    }
                });
            });
        }else{
            res.end(response);
        }

    });
}
exports.sendremindertoall=function(){

    var qry="Select temple.TempleName,temple.TempleID,bookingpujadetails.TrackingNo,bookingpujadetails.PujaID,bookingpujadetails.AddressID,bookingpujadetails.PostalCharge,count(remindersms.templeid) as smscount, bookingpujadetails.PujaDate, puja.PujaName,star.StarName , deity.DeityName As Deity,bookingpujadetails.PostalCharge,bookingpujadetails.Quantity, Sum(bookingpujadetails.Quantity) As PujaCount,address.StreetName, address.PostOffice, address.District,address.State, address.PostalCode, address.PhoneMobile, address.PhoneWired, address.Email, " +
        "bookingpujadetails.DevoteeName,bookingpujadetails.DevoteeStar,bookingaddress.HouseName as chousename,bookingaddress.StreetName cstreetname, bookingaddress.PostOffice as cpostoffice, bookingaddress.District as cdistrict,bookingaddress.State as cstate, bookingaddress.PostalCode as cpostalcode, bookingaddress.PhoneMobile as cphonemobile, bookingaddress.PhoneWired as cphonewired, bookingaddress.Email as cemail " +
        "From  booking   Inner Join bookingpujadetails On booking.TrackingNo = bookingpujadetails.TrackingNo And booking.TempleID = bookingpujadetails.TempleID " +
        "inner join bookingaddress on bookingaddress.AddressID=bookingpujadetails.AddressID " +
        "Inner Join puja On bookingpujadetails.TempleID = puja.TempleID And bookingpujadetails.PujaID = puja.PujaID Left Join deity    On puja.TempleID = deity.TempleID And puja.DeityID = deity.DeityID    Inner Join    temple    On temple.TempleID = booking.TempleID Inner Join    address    On temple.AddressID = address.AddressID And temple.LanguageID = address.LanguageID " +
        "inner join templeconfig on templeconfig.TempleID=temple.TempleID and templeconfig.ConfigID=26 and templeconfig.Value='true' and templeconfig.Key='ENABLE_PUJA_REMINDER'" +
        " left join remindersms on remindersms.templeid=temple.templeid and remindersms.pujaid=bookingpujadetails.PujaID and remindersms.pujadate=bookingpujadetails.PujaDate and remindersms.devoteename=bookingpujadetails.DevoteeName and remindersms.devoteestar=bookingpujadetails.DevoteeStar " +
        "and  remindersms.phonemobile=bookingaddress.PhoneMobile and smsdate between subdate(curdate(),interval 1 day) and curdate()" +
        "and remindersms.trackingno=bookingpujadetails.TrackingNo inner join star on star.StarID=bookingpujadetails.DevoteeStar and star.LanguageID=1 Where  temple.Status = 1 And     temple.LanguageID = 1 And     puja.LanguageID = 1 And     deity.LanguageID = 1 And     Date(bookingpujadetails.PujaDate) =curdate() " +
        " Group By temple.TempleName, bookingpujadetails.PujaDate, puja.PujaName,  deity.DeityName,bookingpujadetails.DevoteeName,bookingpujadetails.Quantity,bookingpujadetails.AddressID,bookingpujadetails.DevoteeStar, address.StreetName, address.PostOffice, address.District,  address.State, address.PostalCode, address.PhoneMobile, address.PhoneWired,  address.Email " +
        "Order By temple.TempleName, bookingpujadetails.PujaDate;"
    console.log(qry);
    mysql.getConnection(function(err, con){
        con.query(qry,function(err, rows, fields){
            con.release();
            //console.log (rows.length);
            if( err ){
                logger.log('error',err,"reminder error",'report.js/reminder');
                return;
            }
            if (rows.length==0){
                logger.log('warn','No match found for query','','report.js/reminder');
            }
            else{
                var str=JSON.stringify(rows);
                rows.forEach(function(row){
                    console.log(JSON.stringify(row));
                    var message="",quantity="";
                    var pujadate=new Date(row.PujaDate).getDate()+'-'+((new Date(row.PujaDate).getMonth()*1)+(1*1))+'-'+new Date(row.PujaDate).getFullYear();
                    if(row.Quantity!=1){
                        quantity=',Quantity:'+row.Quantity;
                    }
                    if(row.PostalCharge!=0){
                        message='Online Booking,TrackingNo:'+row.TrackingNo+',PujaDate:'+pujadate+',PujaName:'+row.PujaName+',Deity:'+row.Deity+',Benficiary:'+row.DevoteeName+',Star:'+row.StarName+''+quantity+',Postage Amt:'+row.PostalCharge+',Mobile:'+row.cphonemobile+',Address:'+row.chousename+','+row.cstreetname+','+row.cpostoffice+','+row.cdistrict+','+row.cpostalcode+','+row.cstate;
                    }else{
                        message='Online Booking,TrackingNo:'+row.TrackingNo+',PujaDate:'+pujadate+',PujaName:'+row.PujaName+',Deity:'+row.Deity+',Benficiary:'+row.DevoteeName+',Star:'+row.StarName+',Mobile:'+row.cphonemobile+',No Shippment';
                    }
                    var maildata = {
                        mobileno: row.PhoneMobile,
                        message: message
                    }
                    console.log(JSON.stringify(maildata));
                    var qry="insert into remindersms(`templeid`,`smsdate`,`pujaid`,`devoteename`,`devoteestar`,`phonemobile`,`pujadate`,`trackingno`,`addressid`,`postalcharge`)" +
                        "values('"+row.TempleID+"',curdate(),'"+row.PujaID+"','"+row.DevoteeName+"','"+row.DevoteeStar+"','"+row.cphonemobile+"',curdate(),'"+row.TrackingNo+"','"+row.AddressID+"','"+row.PostalCharge+"')";
                    console.log(qry);
                    emailtemplate.sendsms(maildata,function(response){
                        if(response=="success"){
                            mysql.getConnection(function(err, con) {
                                con.query(qry, function (err, rows, fields) {
                                    con.release();
                                    //console.log (rows.length);
                                    if (err) {
                                        logger.log('error', err,'', 'report.js/getpujalist');
                                        //res.end('[{"RESULT":"ERROR","usermessage":"system error. please contact support","systemmessage":"invalid query"}]');
                                        return;
                                    }
                                    if (rows.length == 0) {
                                        logger.log('warn', 'No match found for query','', 'report.js/getpujalist');
                                        //res.end('[{"RESULT":"NODATA","usermessage":"no data available","systemmessage":"no data available"}]');
                                    }
                                    else {
                                        var str = JSON.stringify(rows);
                                        //res.end("Success");
                                        logger.log('data', str, '', 'report.js/getpujalist');
                                    }
                                });
                            });
                        }else{
                            //res.end(response);
                        }

                    });
                })
                //res.end(str);
                logger.log('data',str,"",'report.js/remainder');
            }
        });
    });
};
exports.checkincompletetransactions=function(req,res) {
    var qry='select sessionbooking.BillAmount,paymentgatewayorder.SettlementStatus,' +
        'pgt.TransactionRequestedTime as TransactionRequestedTime,' +
        'pgt.TrackingNo,pgt.TransactionStatus,' +
        'paymentgatewayorder.GatewayTransactionAmount,TempleName,Email as LoginID,' +
        ' paymentgatewayorder.SettlementRequestedDate,paymentgatewayorder.SettlementCompleteDate,' +
        'paymentgatewayorder.Commission' +
        ' from paymentgatewaytransactions pgt' +
        ' inner join sessiontrackingdetails on sessiontrackingdetails.TrackingNo=pgt.TrackingNo' +
        " inner join bookingaddress on sessiontrackingdetails.SessionID=bookingaddress.TrackingNo " +
        "and bookingaddress.Type in('customer','guest')" +
    ' inner join paymentgatewayorder on paymentgatewayorder.TrackingNo=pgt.TrackingNo' +
    ' inner join sessionbooking on sessionbooking.SessionID=sessiontrackingdetails.SessionID' +
     ' inner join temple on sessionbooking.TempleID=temple.TempleID and temple.LanguageID=1'+
    ' where pgt.TrackingNo in(select std.TrackingNo from sessiontrackingdetails std)' +
    " and pgt.TransactionStatus='I' and pgt.TransactionRequestedTime between subdate(now(),interval 12 hour) " +
        'and now() order by Date(pgt.TransactionRequestedTime) DESC;';
    mysql.getConnection(function(err, con) {
        con.query(qry, function (err, rows, fields) {
            con.release();
            //console.log (rows.length);
            if (err) {
                console.log('errorrrr', err, '', 'report.js/getincompletelist');
                //res.end('[{"RESULT":"ERROR","usermessage":"system error. please contact support","systemmessage":"invalid query"}]');
                return;
            }
            if (rows.length == 0) {
                console.log('warn', 'No match found for query', '', 'report.js/getincompletelist');
                //res.end('[{"RESULT":"NODATA","usermessage":"no data available","systemmessage":"no data available"}]');
            }
            else {
                var timesRun = 0;
                var interval = setInterval(function(){
                    console.log('ch..'+rows.length+JSON.stringify(rows));
                    exports.autocheckincompletetransaction(rows[timesRun],res);
                    timesRun += 1;
                    if(timesRun === rows.length){
                        clearInterval(interval);
                    }
                    //do whatever here..
                }, 10000);
            }
        });});
}
var a = schedule.scheduleJob('1 30 6 * * *', function(){
    console.log('a..a');
    exports.checkincompletetransactions();
});
var b = schedule.scheduleJob('1 30 18 * * *', function(){
    console.log('a..a');
    exports.checkincompletetransactions();
});
var j = schedule.scheduleJob('1 0 3 * * *', function(){
    console.log('a..a');
    exports.sendremindertoall();
});
var k = schedule.scheduleJob('0 30 4 * * *', function() {
    console.log('a..b');
    exports.autosendsettlement();
});
exports.getpujalist=function(req,res){
    //var qry="Select temple.TempleName, bookingpujadetails.PujaDate, puja.PujaName, deity.DeityName As Deity, Sum(bookingpujadetails.Quantity) As PujaCount,address.StreetName, address.PostOffice, address.District,address.State, address.PostalCode, address.PhoneMobile, address.PhoneWired, address.Email " +
    //    "From  booking Inner Join bookingpujadetails On booking.TrackingNo = bookingpujadetails.TrackingNo And booking.TempleID = bookingpujadetails.TempleID Inner Join puja On bookingpujadetails.TempleID = puja.TempleID And bookingpujadetails.PujaID = puja.PujaID Left Join deity    On puja.TempleID = deity.TempleID And puja.DeityID = deity.DeityID    Inner Join    temple    On temple.TempleID = booking.TempleID Inner Join    address    On temple.AddressID = address.AddressID And temple.LanguageID = address.LanguageID " +
    //    "Where     temple.Status = 1 And     temple.LanguageID = 1 And     puja.LanguageID = 1 And     deity.LanguageID = 1 And     Date(bookingpujadetails.PujaDate) >= CurDate() - Interval 100 Day And     Date(bookingpujadetails.PujaDate) <= CurDate() + Interval 100 Day " +
    //    "Group By temple.TempleName, bookingpujadetails.PujaDate, puja.PujaName,  deity.DeityName, address.StreetName, address.PostOffice, address.District,  address.State, address.PostalCode, address.PhoneMobile, address.PhoneWired,  address.Email " +
    //    "Order By temple.TempleName, bookingpujadetails.PujaDate;"
    //console.log(req.body.fdate);
    var qry="Select temple.TempleName,temple.TempleID,'Send SMS' as smsstatus,'Resend SMS' as smsresendstatus,count(remindersms.templeid) as smscount, bookingpujadetails.PujaDate, puja.PujaName,star.StarName , deity.DeityName As Deity,bookingpujadetails.PostalCharge,bookingpujadetails.PujaID,bookingpujadetails.TrackingNo,bookingpujadetails.AddressID,bookingpujadetails.Quantity, Sum(bookingpujadetails.Quantity) As PujaCount,address.StreetName, address.PostOffice, address.District,address.State, address.PostalCode, address.PhoneMobile, address.PhoneWired, address.Email, " +
        "bookingpujadetails.DevoteeName,bookingpujadetails.DevoteeStar,bookingaddress.Housename as chousename ,bookingaddress.StreetName cstreetname, bookingaddress.PostOffice as cpostoffice, bookingaddress.District as cdistrict,bookingaddress.State as cstate, bookingaddress.PostalCode as cpostalcode, bookingaddress.PhoneMobile as cphonemobile, bookingaddress.PhoneWired as cphonewired, bookingaddress.Email as cemail From  booking Inner Join sessiontrackingdetails on sessiontrackingdetails.TrackingNo=booking.TrackingNo  Inner Join bookingpujadetails On booking.TrackingNo = bookingpujadetails.TrackingNo And booking.TempleID = bookingpujadetails.TempleID inner join bookingaddress on bookingaddress.AddressID=bookingpujadetails.AddressID Inner Join puja On bookingpujadetails.TempleID = puja.TempleID And bookingpujadetails.PujaID = puja.PujaID Left Join deity    On puja.TempleID = deity.TempleID And puja.DeityID = deity.DeityID    Inner Join    temple    On temple.TempleID = booking.TempleID Inner Join    address    On temple.AddressID = address.AddressID And temple.LanguageID = address.LanguageID " +
        "left join remindersms on remindersms.postalcharge=bookingpujadetails.PostalCharge and remindersms.addressid=bookingpujadetails.AddressID and remindersms.templeid=temple.templeid and remindersms.pujaid=bookingpujadetails.PujaID and remindersms.pujadate=bookingpujadetails.PujaDate and remindersms.devoteename=bookingpujadetails.DevoteeName and remindersms.devoteestar=bookingpujadetails.DevoteeStar " +
        "and  remindersms.phonemobile=bookingaddress.PhoneMobile and smsdate between subdate(curdate(),interval 1 day) and curdate()" +
        "and remindersms.trackingno=bookingpujadetails.TrackingNo inner join star on star.StarID=bookingpujadetails.DevoteeStar and star.LanguageID=1 Where  temple.Status = 1 And     temple.LanguageID = 1 And     puja.LanguageID = 1 And     deity.LanguageID = 1 And     Date(bookingpujadetails.PujaDate) between '"+ req.body.fdate +"' and '" + req.body.tdate + "' " +
        " Group By temple.TempleName, bookingpujadetails.PujaDate, puja.PujaName,  deity.DeityName,bookingpujadetails.DevoteeName,bookingpujadetails.Quantity,bookingpujadetails.AddressID,bookingpujadetails.DevoteeStar, address.StreetName, address.PostOffice, address.District,  address.State, address.PostalCode, address.PhoneMobile, address.PhoneWired,  address.Email " +
        "Order By temple.TempleName, bookingpujadetails.PujaDate;"
    console.log(qry);
    mysql.getConnection(function(err, con){
        con.query(qry,function(err, rows, fields){
            con.release();
            //console.log (rows.length);
            if( err ){
                logger.log('error',err,req.session.sessionID,'report.js/getpujalist');
                res.end( '[{"RESULT":"ERROR","usermessage":"system error. please contact support","systemmessage":"invalid query"}]');
                return;
            }
            if (rows.length==0){
                logger.log('warn','No match found for query',req.session.sessionID,'report.js/getpujalist');
                res.end('[{"RESULT":"NODATA","usermessage":"no data available","systemmessage":"no data available"}]');
            }
            else{
                var str=JSON.stringify(rows);
                res.end(str);
                logger.log('data',str,req.session.sessionID,'report.js/getpujalist');
            }
        });
    });
};



