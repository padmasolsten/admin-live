'use strict';

var db = require('../db').pool;
var logger=require('./utilities/logger.js');
var file='Admin History.js';


exports.AddHistory=function(req,res)
{

    db.getConnection(function(err, con){
        var params="'"+req.session.TempleID+"','"+req.body.CategoryName+"', '"+req.body.DisplaySequence+"','"+req.body.Description+"'";
        var sql="Call admin_add_about("+params+")";
        logger.log('data','Query :'+sql,req.session.sessionID,'Admin History.js/Add History ');
        con.query(sql,function(err,rows)
        {
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,'Admin History.js/Add History ');
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,'Admin History.js/Add History ');
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {


                results= JSON.stringify(rows);
                res.end('[{ "RESULT" : "1"}]')

                logger.log('data',results,req.session.sessionID,'Admin History.js/Add History ');
            }

        });

        con.release();




    });

};
exports.UpdateTempleHistory=function(req,res)
{

    var method='UpdateTempleHistory';
    var params="'"+req.session.TempleID+"','"+req.body.Status+"','"+req.body.CategoryName+"','"+req.body.PlaceHolderID+"','"+req.body.Description+"','"+req.body.LanguageID+"'";

    var sql="Call admin_update_about("+params+")";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
exports.LoadTempleHistory=function(req,res)
{
    var method='LoadTempleHistory';
    var sql="Select  templehistory.*,  languages.languages_code,  languages.languages_name From  templehistory Inner Join  languages On templehistory.LanguageID =    languages.languages_id where  `templehistory`.`TempleID`='"+req.session.TempleID+"' order by PlaceholderID;"
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.imageUpload=function(req,res)
{
    console.log("..UPLOADING");
    if(req.files)
    {
        // console.log("....."+JSON.stringify(req.files));
        res.json(req.files);
    }
};
exports.SaveImage=function(req,res)
{
    db.getConnection(function(err, con){
        var sql="call admin_add_image('"+req.session.TempleID+"','"+req.body.TypeID+"','"+req.body.Title+"','"+req.body.Desc+"','"+req.body.imageUrl+"')";
        console.log(sql);
        con.query(sql,function(err,rows)
        {

            if(!err)
            {
                if(rows.affectedRows!=0)
                {
                    res.end('[{"result":1}]');
                    console.log("Database connection opened image save");

                }
                else
                {
                    res.end('[{"result":0}]');
                    console.log("Database connection Closed-image save");

                }
            }
            else
            {
                res.end('[{"result":0}]');
                console.log("Invalid Query"+err);
            }
        });

        con.release();
    });

};
exports.LoadImage=function(req,res)
{
    var method='LoadImage';
    var sql= "Select  languages.*,  templeimages.* From  templeimages Inner Join  languages On templeimages.LanguageID =    languages.languages_id  WHERE  `templeimages`.`TempleID`='"+req.session.TempleID+"'";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "DB ERROR"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
exports.UpdateImage=function(req,res)
{
    var method='UpdateImage';
    var sql= "DELETE  FROM `devaayanam`.`templeimages` WHERE  `templeimages`.`TempleID`='"+req.session.TempleID+"' AND `templeimages`.`ImageID`='"+req.body.ImageID+"' AND `templeimages`.`TypeID`='"+req.body.ImageType+"' AND `templeimages`.`Url`='"+req.body.ImagePath+"'";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.AddTempleRule=function(req,res)
{
    var method="AddTempleRule";
    db.getConnection(function(err, con){
        var params="'"+req.session.TempleID+"','"+req.body.ruleType+"','"+req.body.description+"'";
        var sql="Call admin_add_temple_rule("+params+")";
        console.log(sql);
        logger.log('data','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {


                results= JSON.stringify(rows);
                res.end('[{ "RESULT" : "1"}]')

                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }

        });


    });

};
exports.LoadTempleRule=function(req,res)
{
    var method='LoadTempleRule';
    var sql="SELECT * FROM `devaayanam`.`templecontent` where `templecontent`.`TempleID`='"+req.session.TempleID+"'and `templecontent`.`ContenttypeID`=4 ";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                console.log("rules.");
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.UpdateTempleRule=function(req,res)
{

    var method='UpdateTempleRule';
    var params="'"+req.session.TempleID+"','"+req.body.status+"','"+req.body.ContentID+"','"+req.body.ContentID+"','"+req.body.MessageBody+"','"+req.body.LanguageID+"'";

    var sql="Call admin_update_temple_rule("+params+")";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};

exports.AddTempleTimings=function(req,res)
{
    var method="AddTempleTimings";
    db.getConnection(function(err, con){
        var params="'"+req.session.TempleID+"','"+req.body.MessageHeader+"','"+req.body.MessageBody+"'";
        var sql="Call admin_add_temple_timings("+params+")";
        logger.log('data','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {


                results= JSON.stringify(rows);
                res.end('[{ "RESULT" : "1"}]')

                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }

        });


    });

};
exports.LoadTempleTimings=function(req,res)
{
    var method='LoadTempleTimings';
    var sql="SELECT * FROM `devaayanam`.`templecontent` where `templecontent`.`TempleID`='"+req.session.TempleID+"' and `templecontent`.`ContenttypeID`=5 ";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.UpdateTempleTimings=function(req,res)
{

    var method='UpdateTempleTimings';
    var params="'"+req.session.TempleID+"','"+req.body.status+"','"+req.body.ContentID+"','"+req.body.MessageHeader+"','"+req.body.MessageBody+"','"+req.body.LanguageID+"'";

    var sql="Call admin_update_temple_timings("+params+")";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};



exports.addshippingcharge=function(req,res)
{
    console.log('req..'+JSON.stringify(req.body));
    var method="Addshippingcharge";
    db.getConnection(function(err, con){
        var params="'"+req.session.TempleID+"','"+req.body.Amount+"','"+req.body.ShippingName+"','"+req.body.PujaID+"'";
        var sql="Call admin_add_temple_shippingcharge("+params+")";
        console.log(sql);
        logger.log('data','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }

            else
            {


                results= JSON.stringify(rows);
                res.end('[{ "RESULT" : "1"}]')

                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }

        });


    });

};
exports.loadshippingcharge=function(req,res)
{
    var method='LoadTempleTimings';

    var sql="SELECT * FROM `devaayanam`.`pujashippingcharges` where `pujashippingcharges`.`TempleID`='"+req.session.TempleID+"' and `pujashippingcharges`.`Status`=1 and type=0 and LanguageID=1; ";
    console.log('ls..'+JSON.stringify(req.session));
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });
};
exports.updateshippingcharge=function(req,res)
{
    var method='UpdateTempleTimings';
    var params="'"+req.session.TempleID+"','"+req.body.status+"','"+req.body.ShippingID+"','"+
					req.body.Amount+"','"+req.body.ShippingName+"','"+req.body.LanguageID+"','"+req.body.Type+"'";
    console.log(params);
    var sql="Call admin_update_temple_shippingcharge("+params+")";
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
exports.generaltab=function(req,res)
{
    var method='generaltab';
    var sql;
    console.log(JSON.stringify(req.body));
    sql = "call generaltab('" + req.session.TempleID +"','"+req.body.generaldata+"','"+req.body.templeconfigvalue+"')";
    console.log(sql);
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(JSON.stringify(rows));
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
//DV-41
//Messages for users in payment page
exports.manage_pg_messages=function(req,res)
{
    var method='manage_pg_messages';
    var sql;
    console.log(JSON.stringify(req.body));
    sql = "call manage_pg_messages('"+req.body.data+"')";
    console.log(sql);
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(JSON.stringify(rows));
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
// DV98 -- Temple Puja Links -- start --		
exports.loadTemplePujaLinks=function(req,res)
{
    var method='loadtemplepujalinks';
    var sql;
    console.log(JSON.stringify(req.body));
    sql = "call admin_load_templepujalinks('"+req.body.TempleName+"','"+req.body.LanguageID+"');";
    console.log(sql);
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(JSON.stringify(rows));
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
exports.updateTemplePujaLink=function(req,res)
{
    var method='admin_update_templepujalink';
    var sql="call admin_update_templepujalink('"+req.body.TempleID+"','"+req.body.PujaID+"','"
						+req.body.PujaTypeID+"','"+req.body.DeityID+"','"+req.body.Status+"')";
    console.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows) {
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "Puja Link Update Failed "}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "Puja Link Update Failed "}]');
            }
            else
            {
                res.end( '[{ "RESULT" : "Puja Link Updated Successfully"}]');
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};
exports.createTemplePujaLink=function(req,res)
{
    var method='admin_create_templepujalink';
    var sql="call admin_create_templepujalink('"+req.body.TempleID+"','"+req.body.PujaID+"','"+
					+req.body.DeityID+"','"+req.body.PujaTypeID+"','"+
					req.body.LanguageID+"','"+req.body.PujaLink+"')";
    console.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows) {
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "Create Puja Link Failed "}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "Create Puja Link Failed "}]');
            }
            else
            {
                res.end( '[{ "RESULT" : "Puja Link Created Successfully"}]');
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });
};

exports.listTemples=function(req,res)
{
    var method='listtemples';
    var sql;
    console.log(JSON.stringify(req.body));
    sql = "call admin_list_temples('"+req.body.LanguageID+"');";
    console.log(sql);
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(JSON.stringify(rows));
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
exports.listTemplePujas=function(req,res)
{
    var method='listtemplepujas';
    var sql;
    console.log(JSON.stringify(req.body));
    sql = "call admin_list_templepujas('"+req.body.TempleID+"','"+req.body.LanguageID+"');";
    console.log(sql);
    db.getConnection(function(err, con){
        logger.log('info','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                var results= JSON.stringify(rows);
                res.end(JSON.stringify(rows));
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
// DV98 -- Temple Puja Links -- end --		