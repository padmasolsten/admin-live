'use strict';
var logger=require('./utilities/logger.js');
var db = require('../db').pool;
exports.SetSession=function(req,res)
{
    req.session.LoginID =    req.body.LoginID;
    req.session.CustomerName=req.body.FirstName;
    req.session.UserID=      req.body.UserID;
    req.session.TempleID=    req.body.TempleID;
    req.session.TempleName=  req.body.TempleName;
    req.session.TempleTypeID=req.body.TypeID;
    req.session.AccessTypeID=req.body.AccessTypeID;
    req.session.TempleEmail= req.body.TempleEmail;
    req.session.TempleIcon=  req.body.TempleIcon;
    res.end('{"Status" :"SessionUpdated"}');
};
exports.create_session=function(req,res)
{
    if((req.session.sessionID==null)||(req.session.sessionID==undefined)){
        logger.log('info','session id expired','','GenerateSessionID.js/GenerateSessionID ');
        db.getConnection(function(err, con){
            con.query("call Ordering_GenerateSessionID()",function(err, rows, fields) {
                con.release();
                var results;
                if (err)
                {
                    res.end('{"Status" :"Failed"}');
                    logger.log('error',err,'0','GenerateSessionID.js/GenerateSessionID  ');
                }
                else if (rows.length == 0)
                {
                    res.end('{"Status" :"Failed"}');
                    logger.log('info','No match found for query Generate Session ID','0','GenerateSessionID.js/GenerateSessionID ');
                }
                else
                {
                    results=rows[0];
                    req.session.sessionID=results[0].SessionID;
                    res.send('')
                    req.session.sessionStartDateTime=new Date();
                    res.end('{"Status" :"SessionExpired","NewSessionID":"'+req.session.sessionID+'"}');
                    logger.log('info','New Session ID Generated',req.session.sessionID,'GenerateSessionID '+'sessionStartDateTime :'+req.session.sessionStartDateTime);
                }
            });
        });
    }
    else
    {
        req.session.lastSessionDateTime=new Date();
        res.end('[{"Status" :"SessionNotExpired","CurrentSessionID":"'+req.session.sessionID+'"}]');
        logger.log('info','SessionID Not Expired-Current SessionID :'+req.session.sessionID,req.session.sessionID,'GenerateSessionID '+'LastSessionDateTime :'+req.session.lastSessionDateTime);
    }

};
