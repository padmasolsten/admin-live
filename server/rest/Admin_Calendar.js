'use strict';

var db = require('../db').pool;
var logger=require('./utilities/logger.js');
var file='Admin Calendar.js';


exports.AddEvent=function(req,res)
{
    var method ='AddEvent';
    db.getConnection(function(err, con){
        var tp=new Date(req.body.EndDate);
        var EndDate=tp.getFullYear() +"-"+(+tp.getMonth()+1)+"-"+tp.getDate();
        var tps=new Date(req.body.StartDate);
        var StartDate=tps.getFullYear() +"-"+(+tps.getMonth()+1)+"-"+tps.getDate();
        var params="'"+req.session.TempleID+"','"+req.body.EventName+"','"+req.body.Desc+"','"+StartDate+"','"+EndDate+"','"+req.body.StartTime+"','"+req.body.EndTime+"'";
        var sql="Call admin_add_event("+params+")";
        console.log(sql);
        logger.log('data','Query :'+sql,req.session.sessionID,file+'/'+method);
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                console.log("error undefined")
                logger.log('error',+err);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};
exports.LoadEvent=function(req,res)
{
    var method='LoadEvent';
    var sql="SELECT * FROM `devaayanam`.`templecontent` where `templecontent`.`TempleID`='"+req.session.TempleID+"' and `templecontent`.`LanguageID`='"+req.body.LanguageID+"' and `templecontent`.`ContenttypeID`=2 ";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                res.end(JSON.stringify(rows));
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

};
exports.LoadEventByLanguage=function(req,res)
{
    var method='LoadEventByLanguage';
    var sql="Select  event.*,  languages.* From  languages Inner Join  event On event.LanguageID =    languages.languages_id Where `event`.`TempleID`='"+req.session.TempleID+"' and `event`.`LanguageID`='"+req.body.LanguageID+"' and `event`.`EventID`='"+req.body.EventID+"'";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                res.end(JSON.stringify(rows));
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

};
exports.UpdateEvent=function(req,res)
{
    var method='UpdateEvent';
    var sd=new Date(req.body.StartDate);
        sd=sd.getFullYear() +"-"+(+sd.getMonth()+1)+"-"+sd.getDate();
    var ed=new Date(req.body.EndDate);
        ed=ed.getFullYear() +"-"+(+ed.getMonth()+1)+"-"+ed.getDate();
    var sql="call admin_update_event('"+req.session.TempleID+"','"+req.body.ContentID+"','"+req.body.LanguageID+"','"+req.body.Mode+"','"+sd+"','"+ed+"','"+req.body.MessageHeader+"','"+req.body.MessageBody+"')";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows)
        {
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                res.end(JSON.stringify(rows));
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

};

exports.AddFestival=function(req,res)
{
    var method ='AddFestival';
    db.getConnection(function(err, con){
        var params="'"+req.session.TempleID+"', '"+req.body.FestivalName+"', '"+req.body.FestivalDate+"','"+req.body.FestivalDesc+"'";
        var sql="Call festivaldays("+params+")"
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "DB ERROR"}]');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "DB ERROR"}]');
            }
            else
            {
                results= JSON.stringify(rows);
                res.end(results);
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });

    });

};
exports.AddSubEvent=function(req,res)
{
    var method ='AddSubEvent';
    db.getConnection(function(err, con){
        var params="'"+req.session.TempleID+"', '"+req.body.EventID+"', '"+req.body.SubEvent+"', '"+req.body.Desc+"', '"+req.body.StartTime+"', '"+req.body.EndTime+"'";
        con.query("Call subevent("+params+")",function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                logger.log('error',+err,req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {


                results= JSON.stringify(rows);
                res.end( '[{ "RESULT" : "'+results+'"}]');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }

        });
    });

};




exports.AddAnnouncement=function(req,res)
{
    var method ='AddAnnouncement';
    db.getConnection(function(err, con){
        var tp=new Date(req.body.AlertEndDate);
        var AlertEndDate=tp.getFullYear() +"-"+(+tp.getMonth()+1)+"-"+tp.getDate();
        var tps=new Date(req.body.AlertStartDate);
        var AlertStartDate=tps.getFullYear() +"-"+(+tps.getMonth()+1)+"-"+tps.getDate();
        var sql="Call admin_add_announcement('"+req.session.TempleID+"','"+req.body.AlertName+"','"+req.body.AlertMessage+"','"+AlertStartDate+"','"+AlertEndDate+"')";
        con.query(sql,function(err,rows) {
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
            }
            else
            {
                res.end( '[{ "RESULT" : "1"}]');
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

};
exports.LoadAnnouncement=function(req,res)
{
    var method='LoadAnnouncement';
    var sql="SELECT * FROM `devaayanam`.`templecontent` where `templecontent`.`TempleID`='"+req.session.TempleID+"' and `templecontent`.`LanguageID`='"+req.body.LanguageID+"' and `templecontent`.`ContenttypeID`=1 ";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                res.end(JSON.stringify(rows));
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

};
exports.LoadAnnouncementByLanguage=function(req,res)
{
    var method='LoadAnnouncementByLanguage';
    var sql="Select  templealerts.*,  languages.* From  languages Inner Join  templealerts On templealerts.LanguageID =    languages.languages_id Where `templealerts`.`TempleID`='"+req.session.TempleID+"' and `templealerts`.`LanguageID`='"+req.body.LanguageID+"' and `templealerts`.`AlertID`='"+req.body.ContentID+"'";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows){
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                res.end(JSON.stringify(rows));
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

};
exports.UpdateAnnouncement=function(req,res)
{
    var method='UpdateAnnouncement';
    var tp=new Date(req.body.EndDate);
    var date=tp.getFullYear() +"-"+(+tp.getMonth()+1)+"-"+tp.getDate();
    var sql="call admin_update_announcement('"+req.session.TempleID+"','"+req.body.ContentID+"','"+req.body.LanguageID+"','"+req.body.Mode+"','"+date+"','"+req.body.MessageHeader+"','"+req.body.MessageBody+"')"
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err,rows)
        {
            con.release();
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                console.log(err);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                console.log("no match");
                logger.log('warn','Result:No match found for query',req.session.UserID,file+'/'+method);
                res.end( '[]');
            }
            else
            {
                console.log(JSON.stringify(rows));
                res.end(JSON.stringify(rows));
                logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
            }
        });
    });

};












