// <copyright file="Notify.js" company="CronyCo">
// Copyright (c) 2014 All Right Reserved, http://cronyco.in/
//
// This source is subject to the CronyCo Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@cronyco.in</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Notify</summary>

'use strict';
var db = require('../db').pool;
var cryptojs=require('../rest/utilities/cryptography.js');
var url = require('url');
var logger=require('../rest/utilities/logger.js');
var request = require('request');
var async = require('async');
var mail = require('./Send_Mail.js');
var mailTemplt = require('./emailtemplate.js');
var crypto=require('crypto');
var prevDirName=__dirname;
prevDirName=prevDirName.substring(0,prevDirName.lastIndexOf('utilities'));
var dbConfigFile = prevDirName + 'server/database.json';
var fs = require('fs');
var env='configuration';
var data = fs.readFileSync(dbConfigFile, 'utf8');
var dbConfig = JSON.parse(data)[env];
var email_provider=dbConfig.emailprovider;
var email_username=dbConfig.emailusername;
var email_password=cryptojs.DecryptString(dbConfig.emailpassword);
var file='emailtemplate.js';
var request = require("request");

// email
var imageurl='http://www.devaayanam.com/pages/template3/images/';
exports.getbookingdetails =function(orderinfo,callback){
    var method='getbookingdetails';
    var sql="call Ordering_get_booking_data_by_tracking_no('"+orderinfo.TrackingNo+"','"+orderinfo.addressid+"')";
    logger.log('info','Query :'+sql,'',file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err) {
                logger.log('error','Result :'+err,'',file+'/'+method);
                callback(false)
            }
            if (rows.length == 0) {
                logger.log('warn','Result:No match found for query','',file+'/'+method);
                //return false;
                callback(false)
            }
            else {
                logger.log('data','Result:'+JSON.stringify(rows),'',file+'/'+method);
                results =rows;
                callback(rows);
            }
        });
    });
};

exports.prepare_mail_template =function(orderinfo){
    var method=' prepare_mail_template ';
    switch(orderinfo.event) {
        case 'SENDNOTIFICATION':
            method=method+'/'+orderinfo.event;
            // var subject='Puja Enquiry';
            // var message='Settlement file';

            if(orderinfo.to==undefined){orderinfo.to=orderinfo.LoginID;}
            if(orderinfo.from==undefined){orderinfo.from='support@devaayanam.in';}
            if(orderinfo.message==undefined){orderinfo.message=message;}
            if(orderinfo.subject==undefined){orderinfo.subject=message;}
            orderinfo.email_provider=email_provider;
            orderinfo.email_username=email_username;
            orderinfo.email_password=email_password;
            mail.send_mail_from_data(orderinfo);
            break;

        case 'SENDFORSETTLEMENT':
            method=method+'/'+orderinfo.event;
            var subject='Puja Enquiry';
            var message='Settlement file';

            if(orderinfo.to==undefined){orderinfo.to=orderinfo.LoginID;}
            if(orderinfo.from==undefined){orderinfo.from='support@devaayanam.in';}
            if(orderinfo.message==undefined){orderinfo.message=message;}
            if(orderinfo.subject==undefined){orderinfo.subject=message;}
            orderinfo.email_provider=email_provider;
            orderinfo.email_username=email_username;
            orderinfo.email_password=email_password;
            mail.send_mail_with_attachment(orderinfo);
            break;

        case 'SENDAPPROVELREQUEST':
            method=method+'/'+orderinfo.event;
            var subject='Puja Enquiry';
            var message='you have a puja request fom devotee. please sign in to temple admin site and visit TASK / APPROVALS';
            var template='<!doctype html><html><head><meta charset="utf-8"><title>Untitled Document</title><link href= "https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"></head><body style="font-family: Open Sans, sans-serif; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;"><section class="header" style="width:100%; height:auto;"><table style="width:650px; margin:0px auto; border:0px; border-collapse:collapse;"><tr style="	width:650px; height:auto; text-align:center;"><td><img src="http://viralfever.in/dev/mail/images/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px;height:auto;background-image:url(http://viralfever.in/dev/mail/images/background.jpg);background-size:cover;text-align:left;height:auto;"><td><h5 style="font-weight:600; font-size:17px; color:#b74b03;  padding-left:25px;">Dear  '+orderinfo.firstname+',</h5><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;">you have a puja request fom devotee. please sign in to temple admin site and visit TASK / APPROVALS</p><p style="font-weight:400; font-size:14px; color:#2d2d2d; padding-left:70px; padding-right:25px; line-height:25px;">Visit <a href="#" style="color:#603; text-decoration:none;">www.devaayanam.in</a> to find the list of temples onboarded</p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">List of temples <a href="#" style="color:#603; text-decoration:none;">-http://www.devaayanam.in/List</a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">To get updates from your favourite temple ,please add to you favorite list</p><h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:20px;  padding-left:70px; padding-right:25px; line-height:25px;">Team Devaayanam</h4></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><TD><img src="http://viralfever.in/dev/mail/images/banner..jpg"/></TD></tr><tr style="width:650px;height:auto;margin:0px auto;background-color:#CCC;text-align:center;height:auto;"><td><h6 style="font-size:14px;padding-top:0px;padding-bottom:0px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';

            if(orderinfo.to==undefined){orderinfo.to=orderinfo.LoginID;}
            if(orderinfo.from==undefined){orderinfo.from='support@devaayanam.in';}
            if(orderinfo.message==undefined){orderinfo.message=message;}
            if(orderinfo.subject==undefined){orderinfo.subject=message;}

            orderinfo.template=template;
            orderinfo.email_provider=email_provider;
            orderinfo.email_username=email_username;
            orderinfo.email_password=email_password;
            mail.send_mail_from_data(orderinfo);
            break;

        case 'SENDAPPROVEDPUJA':
            method=method+'/'+orderinfo.event;
            var subject='Puja approved by temple';
            var message='Complete the puja booking procees';
            var template='<!doctype html><html><head><meta charset="utf-8"><title>Untitled Document</title><link href= "https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"></head><body style="font-family: Open Sans, sans-serif; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;"><section class="header" style="width:100%; height:auto;"><table style="width:650px; margin:0px auto; border:0px; border-collapse:collapse;"><tr style="	width:650px; height:auto; text-align:center;"><td><img src="http://viralfever.in/dev/mail/images/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px;height:auto;background-image:url(http://viralfever.in/dev/mail/images/background.jpg);background-size:cover;text-align:left;height:auto;"><td><h5 style="font-weight:600; font-size:17px; color:#b74b03;  padding-left:25px;">Dear  '+orderinfo.firstname+',</h5><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;">Welcome to devaayanam, network of temples. You can login to any temple hosted by devaayanam using your id <a href="#" style="color:#603; text-decoration:none;">'+orderinfo.to+' </a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d; padding-left:70px; padding-right:25px; line-height:25px;">Visit <a href="#" style="color:#603; text-decoration:none;">www.devaayanam.in</a> to find the list of temples onboarded</p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">List of temples <a href="#" style="color:#603; text-decoration:none;">-http://www.devaayanam.in/List</a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">To get updates from your favourite temple ,please add to you favorite list</p><h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:20px;  padding-left:70px; padding-right:25px; line-height:25px;">Team Devaayanam</h4></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><TD><img src="http://viralfever.in/dev/mail/images/banner..jpg"/></TD></tr><tr style="width:650px;height:auto;margin:0px auto;background-color:#CCC;text-align:center;height:auto;"><td><h6 style="font-size:14px;padding-top:0px;padding-bottom:0px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';

            if(orderinfo.to==undefined){orderinfo.to=orderinfo.LoginID;}
            if(orderinfo.from==undefined){orderinfo.from='support@devaayanam.in';}
            if(orderinfo.message==undefined){orderinfo.message=message;}
            if(orderinfo.subject==undefined){orderinfo.subject=message;}

            orderinfo.template=template;
            orderinfo.email_provider=email_provider;
            orderinfo.email_username=email_username;
            orderinfo.email_password=email_password;
            mail.send_mail_from_data(orderinfo);
            break;

        case 'SENDWELCOMEEMAIL':
            method=method+'/'+orderinfo.event;
            var subject='welcome to devaayanam';
            var message='welcome to devaayanam';
            var template='<!doctype html><html><head><meta charset="utf-8"><title>Untitled Document</title><link href= "https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"></head><body style="font-family: Open Sans, sans-serif; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;"><section class="header" style="width:100%; height:auto;"><table style="width:650px; margin:0px auto; border:0px; border-collapse:collapse;"><tr style="	width:650px; height:auto; text-align:center;"><td><img src="http://viralfever.in/dev/mail/images/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px;height:auto;background-image:url(http://viralfever.in/dev/mail/images/background.jpg);background-size:cover;text-align:left;height:auto;"><td><h5 style="font-weight:600; font-size:17px; color:#b74b03;  padding-left:25px;">Dear  '+orderinfo.firstname+',</h5><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;">Welcome to devaayanam, network of temples. You can login to any temple hosted by devaayanam using your id <a href="#" style="color:#603; text-decoration:none;">'+orderinfo.to+' </a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d; padding-left:70px; padding-right:25px; line-height:25px;">Visit <a href="#" style="color:#603; text-decoration:none;">www.devaayanam.in</a> to find the list of temples onboarded</p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">List of temples <a href="#" style="color:#603; text-decoration:none;">-http://www.devaayanam.in/List</a></p><p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:25px;">To get updates from your favourite temple ,please add to you favorite list</p><h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:20px;  padding-left:70px; padding-right:25px; line-height:25px;">Team Devaayanam</h4></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://viralfever.in/dev/mail/images/ad1.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><TD><img src="http://viralfever.in/dev/mail/images/banner..jpg"/></TD></tr><tr style="width:650px;height:auto;margin:0px auto;background-color:#CCC;text-align:center;height:auto;"><td><h6 style="font-size:14px;padding-top:0px;padding-bottom:0px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';

            if(orderinfo.to==undefined){orderinfo.to=orderinfo.LoginID;}
            if(orderinfo.from==undefined){orderinfo.from='support@devaayanam.in';}
            if(orderinfo.message==undefined){orderinfo.message=message;}
            if(orderinfo.subject==undefined){orderinfo.subject=message;}

            orderinfo.template=template;
            orderinfo.email_provider=email_provider;
            orderinfo.email_username=email_username;
            orderinfo.email_password=email_password;
            mail.send_mail_from_data(orderinfo);
            break;

        case 'SENDVERIFICATIONEMAIL':
            method=method+'/'+orderinfo.event;
            var subject='Devaayanam verification code :'+ orderinfo.verificationcode;
            var message='Devaayanam verification code :'+ orderinfo.verificationcode;
            var template='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">    <title>Devaayanam verification</title>    <style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;  12px;color: rgba(21, 24, 80, 0.85);font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;margin-left: 0px}h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}</style></head><body><table width="60%" align="center" cellpadding="0" cellspacing="0" bgcolor=e4e4e4><tr><td>    <table class="top-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr><td align="center"></td></tr>    </table>    <table class="main" cellpadding="0" cellspacing="15" bgcolor="ffffff" width="600" align="center" bgcolor="white" style="background-color: #E7EDF1"><tr><td width="570" align="center" style="background-color: #f5f5f5"><img src="http://www.devaayanam.in/images/DevaayanamCompanyName.png" width= "200"height="40px"></td></tr> <tr><td><table class="header"><tr><td width="570" bgcolor="E7EDF1">        <h5 style="font-size: 15px">Dear '+orderinfo.firstname+',</h5>        <h5>Welcome to Devaayanam</h5>    </td></tr><tr><td width="570" bgcolor="E7EDF1" style="padding-top: 5px;padding-bottom: 10px">  to complete your  registration  please provide below verification code on devaayanam signup</p></td></tr><tr><td width="570" bgcolor="E7EDF1"><h1>Verification  Code : '+orderinfo.verificationcode+'</h1></td></tr></table></td></tr><tr><td></td></tr></table><table class="bottom-message" cellpadding="20" cellspacing="0" width="600" align="center"><tr><td align="center"></td></tr></table></td></tr></table></body></html>';

            if(orderinfo.to==undefined){orderinfo.to=orderinfo.LoginID;}
            if(orderinfo.from==undefined){orderinfo.from='support@devaayanam.in';}
            if(orderinfo.message==undefined){orderinfo.message=message;}
            if(orderinfo.subject==undefined){orderinfo.subject=message;}

            orderinfo.template=template;
            orderinfo.email_provider=email_provider;
            orderinfo.email_username=email_username;
            orderinfo.email_password=email_password;
            mail.send_mail_from_data(orderinfo);
            break;

        case 'BOOKONLYPUJA':
            method=method+'/'+orderinfo.event;
            mailTemplt.getbookingdetails(orderinfo,function(data){
                var bookingdetails=data;
                logger.log('info','prepare_mail_template, BOOKONLYPUJA:  orderinfo :'+JSON.stringify(orderinfo) +' bookingdetails :'+JSON.stringify(bookingdetails),'',file+'/'+method);
                if(bookingdetails){
                    var BookingSummary=bookingdetails[0];
                    var BookingDetails=bookingdetails[1];
                    var CustomerDetails=bookingdetails[2];

                    var message='puja request submitted to temple.';
                    var subject="";
                    var content='';
                    var ItemsContent='';
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                    var customer_name=CustomerDetails[0].FirstName+' '+CustomerDetails[0].LastName;
                    BookingSummary.forEach(function(details){
                        content+= '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                        content+= details.TempleName.toUpperCase();
                        content+= '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>POOJA REQUEST RECEIVED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">';
                        content+= details.TrackingNo ;
                        content+= '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                        content+= 'Dear '+customer_name+' ';
                        content+= ',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">Your request for puja has been received. Once temple authority approves your request, you will be notified via email.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;">Your Please provide your booking id for any future correspondence.</p><table border="3px " bordercolor="#7b0b0b" style="margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="4" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Receipt No : ';
                        content+= details.ReceiptNo +' ';
                        content+= '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Pooja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Beneficiary</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td><td class="shipment" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Shipment</td></tr>';

                        var  shipment_stats_for_booking=0;
                        BookingDetails.forEach(function(item){
                            var Shipment='-';
                            var d = new Date(item.PujaDate);
                            var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                            if(item.PostalStatus=='0'){Shipment='included';  shipment_stats_for_booking=1}
                            if(details.TempleID==item.TempleID){
                                ItemsContent+='<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+item.ItemName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+item.DevoteeName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+PujaDate+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+Shipment+'</td></tr>';
                            }
                        });

                        content+=ItemsContent;
                        content+='</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">Once temple approve your request you may complete the order from user area of temple website.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">You can track the status of your booking in your “user area” on temple website.</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">May <span style="color:#b74b03;">';
                        content+= details.TempleName.toUpperCase();
                        content+='</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">';
                        content+= details.TempleName.toUpperCase() +' </span> Temple management</p></td></tr>';
                        content+='<tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr><tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                        content+=details.TempleName.toUpperCase()+',<br>'+details.HouseName.toLowerCase()+','+details.StreetName.toLowerCase()+',<br>'+details.PostOffice.toLowerCase()+',<br>'
                        content+=details.District.toLowerCase()+',<br>'+details.State.toLowerCase()+',<br>'+details.PostalCode+',<br>'+details.PhoneMobile+','+details.PhoneWired+'.<br>'+details.CommunicationMailID.toLowerCase();
                        content+='</h6></td></tr><tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                    });

                    if(orderinfo.to==undefined){
                        orderinfo.to=[CustomerDetails[0].Customer_Email,BookingSummary[0].CommunicationMailID]
                    }
                    orderinfo.to=[CustomerDetails[0].Customer_Email,BookingSummary[0].CommunicationMailID];
                    if(orderinfo.from==undefined){
                        orderinfo.from=BookingSummary[0].CommunicationMailID;
                    }
                    if(orderinfo.message==undefined){
                        orderinfo.message=message;
                    }
                    if(orderinfo.subject==undefined){
                        orderinfo.subject=message;
                    }
                    orderinfo.template=template;
                    orderinfo.email_provider=email_provider;
                    orderinfo.email_username=email_username;
                    orderinfo.email_password=email_password;
                    mail.send_mail_from_data(orderinfo);
                }
                else{
                    logger.log('error','no record found in booking  for :'+JSON.stringify(orderinfo),'',file+'/'+method);
                }
            });
            break;

        case 'PAYMENTSUCCESSBOOKINGSUCCESS':
            method=method+'/'+orderinfo.event;
            mailTemplt.getbookingdetails(orderinfo,function(data){
                var bookingdetails=data;
                logger.log('info','prepare_mail_template, payment complete successfully. order completed successfully:  orderinfo :'+JSON.stringify(orderinfo) +' bookingdetails :'+JSON.stringify(bookingdetails),'',file+'/'+method);
                if(bookingdetails){
                    var BookingSummary=bookingdetails[0];
                    var BookingDetails=bookingdetails[1];
                    var CustomerDetails=bookingdetails[2];

                    var message='pooja booking received';
                    var subject="";
                    var content='';
                    var ItemsContent='';
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                    var customer_name=CustomerDetails[0].FirstName+' '+CustomerDetails[0].LastName;
                    BookingSummary.forEach(function(details){
                        //content+= '<table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2>';
                        //content+= '<tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2">';
                        //content+= '<tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT="50"><h1 style="color: #ffffff">';
                        //content+= details.TempleName.toLowerCase();
                        //content+= '</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td><table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>pooja request received</h1></td>';
                        //content+= '</tr></table></td></tr></table></td><tr><tr><td><table class="main"  cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%"><tr>';
                        //content+= '<td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px">';
                        //content+= '<b>Dear '+customer_name+',</b>';
                        //content+= '</td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p> Your request for puja has been received.</p><p>Once temple authority approves your request, you will be notified via email</p>';
                        //content+= '<p>Your  Tracking Number is <b>'+details.TrackingNo+'</b></p></td></tr>';
                        //content+= '<tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your reference id for any future correspondence.</p></td></tr></table></td></tr><tr><td>';
                        //content+= '<TABLE width="95%" border=1 cellpadding=0 cellspacing=0 style="margin-left: 30px"><TR><TH COLSPAN=4 align="center" bgcolor="88CFE0" WIDTH=64% HEIGHT=26>';
                        //content+= 'Receipt No : '+details.ReceiptNo+'</TH></TR><TR bgcolor="F0FBE3">';
                        //content+= '<th width=30%><p>pooja</p></th><th width=30%><p>beneficiary</p></th><th width=20%><p>date</p></th><th width=20%><p>shipment</p></th></TR>';

                        content+= '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                        content+= details.TempleName.toUpperCase();
                        content+= '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>POOJA BOOKING RECEIVED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">';
                        content+= details.TrackingNo ;
                        content+= '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                        content+= 'Dear '+customer_name+' ';
                        content+= ',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">your booking is confirmed. receipt attached with this mail.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;">Your Please provide your booking id for any future correspondence.</p><table border="3px " bordercolor="#7b0b0b" style="margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="4" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Receipt No : ';
                        content+= details.ReceiptNo +' ';
                        content+= '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Pooja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Beneficiary</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td><td class="shipment" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Shipment</td></tr>';

                        var  shipment_stats_for_booking=0;
                        BookingDetails.forEach(function(item){
                            var Shipment='-';
                            var d = new Date(item.PujaDate);
                            var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                            if(item.PostalStatus=='0'){Shipment='included';  shipment_stats_for_booking=1}
                            if(details.TempleID==item.TempleID){
                                ItemsContent+='<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+item.ItemName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+item.DevoteeName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+PujaDate+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+Shipment+'</td></tr>';
                            }
                        });

                        content+=ItemsContent;
                        content+='</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">We will notify you by mail when the puja is completed</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">You can track the status of your booking in your “user area” on temple website.</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">May <span style="color:#b74b03;">';
                        content+= details.TempleName.toUpperCase();
                        content+='</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">';
                        content+= details.TempleName.toUpperCase() +' </span> Temple management</p></td></tr>';
                        content+='<tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr><tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                        content+=details.TempleName.toUpperCase()+',<br>'+details.HouseName.toLowerCase()+','+details.StreetName.toLowerCase()+',<br>'+details.PostOffice.toLowerCase()+',<br>'
                        content+=details.District.toLowerCase()+',<br>'+details.State.toLowerCase()+',<br>'+details.PostalCode+',<br>'+details.PhoneMobile+','+details.PhoneWired+'.<br>'+details.CommunicationMailID.toLowerCase();
                        content+='</h6></td></tr><tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                    });

                    var template=content;

                    if(orderinfo.to==undefined){
                        //orderinfo.to=[CustomerDetails[0].Customer_Email, BookingSummary[0].CommunicationMailID ]
                    }
                    orderinfo.to=[CustomerDetails[0].Customer_Email,BookingSummary[0].CommunicationMailID]

                    if(orderinfo.from==undefined){
                        orderinfo.from=BookingSummary[0].CommunicationMailID;
                    }
                    if(orderinfo.message==undefined){
                        orderinfo.message=message;
                    }
                    if(orderinfo.subject==undefined){
                        orderinfo.subject=message;
                    }

                    orderinfo.template=template;

                    orderinfo.email_provider=email_provider;
                    orderinfo.email_username=email_username;
                    orderinfo.email_password=email_password;

                    mail.send_mail_from_data(orderinfo);
                }
                else{
                    logger.log('error','no record found in booking  for :'+JSON.stringify(orderinfo),'',file+'/'+method);
                }
            });
            break;
        case 'BOOKINGCOMPLETED':
            method=method+'/'+orderinfo.event;
            mailTemplt.getbookingdetails(orderinfo,function(data){
                var bookingdetails=data;
                logger.log('info','booking completed successfully:  orderinfo :'+JSON.stringify(orderinfo) +' bookingdetails :'+JSON.stringify(bookingdetails),'',file+'/'+method);
                if(bookingdetails){
                    var BookingSummary=bookingdetails[0];
                    var BookingDetails=bookingdetails[1];
                    var CustomerDetails=bookingdetails[2];

                    var message='pooja booking completed';
                    var subject="";
                    var content='';
                    var ItemsContent='';
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    console.log("smsdata.."+JSON.stringify(bookingdetails));
                    console.log("smsdata..0"+JSON.stringify(bookingdetails[0]));
                    console.log("smsdata..1"+JSON.stringify(bookingdetails[1]));
                    console.log("smsdata..2"+JSON.stringify(bookingdetails[2]));
                    console.log("smsdata..3"+JSON.stringify(bookingdetails[3]));

                    var customer_name=CustomerDetails[0].FirstName+' '+CustomerDetails[0].LastName;
                    BookingSummary.forEach(function(details){
                        //content+= '<table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2>';
                        //content+= '<tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2">';
                        //content+= '<tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT="50"><h1 style="color: #ffffff">';
                        //content+= details.TempleName.toLowerCase();
                        //content+= '</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td><table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>pooja request received</h1></td>';
                        //content+= '</tr></table></td></tr></table></td><tr><tr><td><table class="main"  cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%"><tr>';
                        //content+= '<td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px">';
                        //content+= '<b>Dear '+customer_name+',</b>';
                        //content+= '</td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p> Your request for puja has been received.</p><p>Once temple authority approves your request, you will be notified via email</p>';
                        //content+= '<p>Your  Tracking Number is <b>'+details.TrackingNo+'</b></p></td></tr>';
                        //content+= '<tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your reference id for any future correspondence.</p></td></tr></table></td></tr><tr><td>';
                        //content+= '<TABLE width="95%" border=1 cellpadding=0 cellspacing=0 style="margin-left: 30px"><TR><TH COLSPAN=4 align="center" bgcolor="88CFE0" WIDTH=64% HEIGHT=26>';
                        //content+= 'Receipt No : '+details.ReceiptNo+'</TH></TR><TR bgcolor="F0FBE3">';
                        //content+= '<th width=30%><p>pooja</p></th><th width=30%><p>beneficiary</p></th><th width=20%><p>date</p></th><th width=20%><p>shipment</p></th></TR>';

                        content+= '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                        content+= details.TempleName.toUpperCase();
                        content+= '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>POOJA BOOKING COMPLETED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">';
                        content+= details.TrackingNo ;
                        content+= '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                        content+= 'Dear '+customer_name+' ';
                        content+= ',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">your booking is completed. receipt attached with this mail.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;">Your Please provide your booking id for any future correspondence.</p><table border="3px " bordercolor="#7b0b0b" style="margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="4" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Receipt No : ';
                        content+= details.ReceiptNo +' ';
                        content+= '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Pooja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Beneficiary</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td><td class="shipment" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Shipment</td></tr>';

                        var  shipment_stats_for_booking=0;
                        BookingDetails.forEach(function(item){
                            var Shipment='-';
                            var d = new Date(item.PujaDate);
                            var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                            if(item.PostalStatus=='0'){Shipment='included';  shipment_stats_for_booking=1}
                            if(details.TempleID==item.TempleID){
                                ItemsContent+='<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+item.ItemName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+item.DevoteeName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+PujaDate+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+Shipment+'</td></tr>';
                            }
                        });

                        content+=ItemsContent;
                        content+='</table><table><tr><th>Total Amount:</th><th>'+(bookingdetails[0][0].BillAmount+10)+'</th></tr></table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">We will notify you by mail when the puja is completed</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">You can track the status of your booking in your “user area” on temple website.</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">May <span style="color:#b74b03;">';
                        content+= details.TempleName.toUpperCase();
                        content+='</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">';
                        content+= details.TempleName.toUpperCase() +' </span> Temple management</p></td></tr>';
                        content+='<tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr><tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                        content+=details.TempleName.toUpperCase()+',<br>'+details.HouseName.toLowerCase()+','+details.StreetName.toLowerCase()+',<br>'+details.PostOffice.toLowerCase()+',<br>'
                        content+=details.District.toLowerCase()+',<br>'+details.State.toLowerCase()+',<br>'+details.PostalCode+',<br>'+details.PhoneMobile+','+details.PhoneWired+'.<br>' + details.Customer_Email;
                        content+='</h6></td></tr><tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                    });

                    var template=content;
                    console.log('emailtemplate..'+template);

                    if(orderinfo.to==undefined){
                        //orderinfo.to=[CustomerDetails[0].Customer_Email, BookingSummary[0].CommunicationMailID ]
                    }
                    orderinfo.to=[CustomerDetails[0].Customer_Email,BookingSummary[0].CommunicationMailID]

                    if(orderinfo.from==undefined){
                        orderinfo.from=BookingSummary[0].CommunicationMailID;
                    }
                    if(orderinfo.message==undefined){
                        orderinfo.message=message;
                    }
                    if(orderinfo.subject==undefined){
                        orderinfo.subject=message;
                    }


                    orderinfo.email_provider=email_provider;
                    orderinfo.email_username=email_username;
                    orderinfo.email_password=email_password;
                    console.log('complete mail..'+JSON.stringify(orderinfo));
                    var mailer = require('nodemailernew');
                    var transport = mailer.createTransport({
                        service: orderinfo.email_provider,
                        auth: {
                            user: orderinfo.email_username,
                            pass: orderinfo.email_password
                        }
                    });
                    fs.readFile(__dirname + '/../../client/files/receipt.jpg', function (err, data) {
                        console.log(err);
                        if(err) {
                            console.log(err);
                        }else{
                            transport.sendMail({
                                sender: 'support@devaayanam.in',
                                to: orderinfo.to,
                                subject: 'Puja Booking Completed',
                                html:template,
                                attachments: [{'filename': 'receipt.jpg', 'content': data}]
                            }), function (err, success) {
                                if (err) {
                                    console.log("mailerror.."+err);
                                }else{
                                    console.log("mailsuccess");
                                }

                            }
                        }
                    });
                }
                else{
                    logger.log('error','no record found in booking  for :'+JSON.stringify(orderinfo),'',file+'/'+method);
                }
            });
            break;

        case 'PAYMENTSUCCESSBOOKINGFAILED':
            method=method+'/'+orderinfo.event;
            mailTemplt.getbookingdetails(orderinfo,function(data){
                var bookingdetails=data;
                logger.log('info','prepare_mail_template, PAYMENTSUCCESSBOOKINGFAILED:  orderinfo :'+JSON.stringify(orderinfo) +' bookingdetails :'+JSON.stringify(bookingdetails),'',file+'/'+method);
                if(bookingdetails){

                    var BookingSummary=bookingdetails[0];
                    var BookingDetails=bookingdetails[1];
                    var CustomerDetails=bookingdetails[2];

                    var message='payment has been completed, booking failed. please contact devaayanam';

                    var subject="";
                    var content='';
                    var ItemsContent='';
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                    var customer_name=CustomerDetails[0].FirstName+' '+CustomerDetails[0].LastName;
                    BookingSummary.forEach(function(details){
                        content+= '<table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2>';
                        content+= '<tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2">';
                        content+= '<tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT="50"><h1 style="color: #ffffff">';
                        content+= details.TempleName.toLowerCase();
                        content+= '</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td><table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>pooja booking failed</h1></td>';
                        content+= '</tr></table></td></tr></table></td><tr><tr><td><table class="main"  cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%"><tr>';
                        content+= '<td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px">';
                        content+= '<b>Dear '+customer_name+',</b>';
                        content+= '</td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>we received your booking and your payment has been completed.</p><p>we experienced a technical problem in backend while processing your booking, please contact temple to complete your booking</p>';
                        content+= '<p>your  tracking Number is <b>'+details.TrackingNo+'</b></p></td></tr>';
                        content+= '<tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>please provide your reference id for any future correspondence.</p></td></tr></table></td></tr><tr><td>';
                        content+= '<TABLE width="95%" border=1 cellpadding=0 cellspacing=0 style="margin-left: 30px"><TR><TH COLSPAN=4 align="center" bgcolor="88CFE0" WIDTH=64% HEIGHT=26>';
                        content+= 'Receipt No : '+details.ReceiptNo+'</TH></TR><TR bgcolor="F0FBE3">';
                        content+= '<th width=30%><p>pooja</p></th><th width=30%><p>beneficiary</p></th><th width=20%><p>date</p></th><th width=20%><p>shipment</p></th></TR>';


                        var  shipment_stats_for_booking=0;
                        BookingDetails.forEach(function(item){
                            var Shipment='-';
                            var d = new Date(item.PujaDate);
                            var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                            if(item.PostalStatus=='0'){Shipment='included';  shipment_stats_for_booking=1}
                            if(details.TempleID==item.TempleID){
                                ItemsContent+='<TR bgcolor=e0eaf0><TD align=center><p>'+item.ItemName+'</p></TD><TD align=center><p>'+item.DevoteeName+'</p></TD><TD align=center><p>'+PujaDate+'</p></TD><TD align=center>'+Shipment+'</TD></TR>';
                            }
                        });

                        content+=ItemsContent;
                        content+='</TABLE></td></tr><tr><td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="100%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"><p style="font-style: italic">We will notify you by mail when the puja is completed</p></td></tr>';
                        if(shipment_stats_for_booking!=0) {
                            content += '<tr><td width="100%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic">normally prasadam shipment takes up to "one week" from the date of puja. you will be notified on shipping status at your registered email.</p></td></tr>';
                        }
                        content+='<tr><td  width="100%" style="padding-left: 30px"><p style="font-style: italic">you can track the status of your booking in your “user area” on temple website.</p></td></tr>';
                        content+='<tr><td  width="100%" style="padding-top: 20px;padding-left: 30px"><p>may <b>'+details.TempleName+'</b> showers blessings  in your life,</p></td></tr>';
                        content+='<tr><td  width="100%" style="padding-bottom: 5px;padding-left: 30px"><p>'+details.TempleName+' temple management</p></td></tr>';
                        content+='<tr><td  width="100%" style="padding-bottom: 5px;padding-left: 30px"><b>temple contact :</b><address style="margin-left: 8%">'
                        content+=details.TempleName.toLowerCase()+',<br>'+details.HouseName.toLowerCase()+','+details.StreetName.toLowerCase()+',<br>'+details.PostOffice.toLowerCase()+',<br>'
                        content+=details.District.toLowerCase()+',<br>'+details.State.toLowerCase()+',<br>'+details.PostalCode+',<br>'+details.PhoneMobile+','+details.PhoneWired+'.<br>'+details.CommunicationMailID.toLowerCase();
                        content+='</address></td></tr></table></td></tr></table></td></tr><tr><td height="20"></td></tr><tr><td>'
                        content+='<table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://devaayanam.in/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table>';
                        content+='<table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;"><p><a href="#"> Terms and Conditions</a> | <a href="#">Privacy Policy </a>| <a href="#">Contact Us</a></p></td></tr>';
                        content+='<tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"></ul></td></tr></table></td></tr></table>';
                    });

                    var style='<style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}    .header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;} .border{ background-color: #F2F2F2; border-bottom: 2px solid #444444;  border-top: 2px solid #444444; border-right: 2px solid #444444; border-left: 2px solid #444444; margin-top: 5px; margin-bottom: 5px; margin-left: 5px;  margin-right: 5px; min-height: 11.693in;}</style>';
                    var template='<html><head>'+style+'</head><body>'+content+'</body></html>';


                    if(orderinfo.to==undefined){
                        orderinfo.to=[CustomerDetails[0].Customer_Email,'contact@devaayanam.in']
                    }
                    orderinfo.to=[CustomerDetails[0].Customer_Email,BookingSummary[0].CommunicationMailID]

                    if(orderinfo.from==undefined){
                        orderinfo.from=BookingSummary[0].CommunicationMailID;
                    }

                    if(orderinfo.message==undefined){
                        orderinfo.message=message;
                    }

                    if(orderinfo.subject==undefined){
                        orderinfo.subject=message;
                    }

                    orderinfo.template=template;

                    orderinfo.email_provider=email_provider;
                    orderinfo.email_username=email_username;
                    orderinfo.email_password=email_password;

                    mail.send_mail_from_data(orderinfo);
                }
                else{
                    logger.log('error','no record found in booking  for :'+JSON.stringify(orderinfo),'',file+'/'+method);
                }
            });
            break;

        case 'PAYMENTSUCCESSHASHFAILED':
            method=method+'/'+orderinfo.event;

            var message='hash mismatch';
            var subject="";
            var content= '<p>Tracking no : ' +orderinfo.TrackingNo+'</p>';
            content+= '<p>hash_generated : ' +orderinfo.hash_generated+'</p>';
            content+= '<p>hash_received : ' +orderinfo.hash_received+'</p>';
            content+= '<p>data_sent : ' +orderinfo.data_sent+'</p>';
            content+= '<p>data_received : ' +orderinfo.data_received+'</p>';

            var style='<style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}    .header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;} .border{ background-color: #F2F2F2; border-bottom: 2px solid #444444;  border-top: 2px solid #444444; border-right: 2px solid #444444; border-left: 2px solid #444444; margin-top: 5px; margin-bottom: 5px; margin-left: 5px;  margin-right: 5px; min-height: 11.693in;}</style>';
            var template='<html><head>'+style+'</head><body>'+content+'</body></html>';

            if(orderinfo.to==undefined){
                orderinfo.to=['contact@devaayanam.in']
            }
            orderinfo.to=[CustomerDetails[0].Customer_Email,BookingSummary[0].CommunicationMailID]

            if(orderinfo.from==undefined){
                orderinfo.from='contact@devaayanam.in';
            }

            if(orderinfo.message==undefined){
                orderinfo.message=message;
            }

            if(orderinfo.subject==undefined){
                orderinfo.subject=message;
            }

            orderinfo.template=template;

            orderinfo.email_provider=email_provider;
            orderinfo.email_username=email_username;
            orderinfo.email_password=email_password;

            mail.send_mail_from_data(orderinfo);
            break;

        case 'PAYMENTFAILED':
            method=method+'/'+orderinfo.event;
            mailTemplt.getbookingdetails(orderinfo,function(data){
                var bookingdetails=data;
                logger.log('info','prepare_mail_template, payment complete successfully. order failed:  orderinfo :'+JSON.stringify(orderinfo) +' bookingdetails :'+JSON.stringify(bookingdetails),'',file+'/'+method);
                if(bookingdetails){
                    var BookingSummary=bookingdetails[0];
                    var BookingDetails=bookingdetails[1];
                    var CustomerDetails=bookingdetails[2];

                    var message='payment failed. unable to complete booking';
                    var subject="";
                    var content='';
                    var ItemsContent='';
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                    var customer_name=CustomerDetails[0].FirstName+' '+CustomerDetails[0].LastName;
                    BookingSummary.forEach(function(details){
                        content+= '<table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2>';
                        content+= '<tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2">';
                        content+= '<tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT="50"><h1 style="color: #ffffff">';
                        content+= details.TempleName;
                        content+= '</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td><table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Pooja booking failed</h1></td>';
                        content+= '</tr></table></td></tr></table></td><tr><tr><td><table class="main"  cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%"><tr>';
                        content+= '<td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px">';
                        content+= '<b>Dear '+customer_name+',</b>';
                        content+= '</td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>unable to complete your booking. error while processing your payment.</p>';
                        content+= '<p>your booking tracking Number : <b>'+orderinfo.TrackingNo+'</b></p></td></tr>';
                        content+= '<p>your payment transaction id : <b>'+orderinfo.pg_response_transactionid+'</b></p></td></tr>';
                        content+= '<p>Payment error message : <b>'+orderinfo.pg_response_message+'</b></p></td></tr>';
                        content+= '<tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your booking id for any future correspondence.</p></td></tr></table></td></tr><tr><td>';
                        content+= '<TABLE width="95%" border=1 cellpadding=0 cellspacing=0 style="margin-left: 30px"><TR><TH COLSPAN=4 align="center" bgcolor="88CFE0" WIDTH=64% HEIGHT=26>';
                        content+= 'Receipt No : '+details.ReceiptNo+'</TH></TR><TR bgcolor="F0FBE3">';
                        content+= '<th width=30%><p>pooja</p></th><th width=30%><p>beneficiary</p></th><th width=20%><p>date</p></th><th width=20%><p>shipment</p></th></TR>';


                        var  shipment_stats_for_booking=0;
                        BookingDetails.forEach(function(item){
                            var Shipment='-';
                            var d = new Date(item.PujaDate);
                            var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                            if(item.PostalStatus=='0'){Shipment='included';  shipment_stats_for_booking=1}
                            if(details.TempleID==item.TempleID){
                                ItemsContent+='<TR bgcolor=e0eaf0><TD align=center><p>'+item.ItemName+'</p></TD><TD align=center><p>'+item.DevoteeName+'</p></TD><TD align=center><p>'+PujaDate+'</p></TD><TD align=center>'+Shipment+'</TD></TR>';
                            }
                        });

                        content+=ItemsContent;
                        content+='</TABLE></td></tr><tr><td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="100%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"><p style="font-style: italic">We will notify you by mail when the puja is completed</p></td></tr>';

                        content+='<tr><td  width="100%" style="padding-bottom: 5px;padding-left: 30px"><p>'+details.TempleName+' temple management</p></td></tr>';
                        content+='<tr><td  width="100%" style="padding-bottom: 5px;padding-left: 30px"><b>temple contact :</b><address style="margin-left: 8%">'
                        content+=details.TempleName+',<br>'+details.HouseName+','+details.StreetName+',<br>'+details.PostOffice+',<br>'
                        content+=details.District+',<br>'+details.State+',<br>'+details.PostalCode+',<br>'+details.PhoneMobile+','+details.PhoneWired+'.<br>'+details.CommunicationMailID;
                        content+='</address></td></tr></table></td></tr></table></td></tr><tr><td height="20"></td></tr><tr><td>'
                        content+='<table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://devaayanam.in/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table>';
                        content+='<table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;"><p><a href="#"> Terms and Conditions</a> | <a href="#">Privacy Policy </a>| <a href="#">Contact Us</a></p></td></tr>';
                        content+='<tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"></ul></td></tr></table></td></tr></table>';
                    });

                    var style='<style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}    .header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;} .border{ background-color: #F2F2F2; border-bottom: 2px solid #444444;  border-top: 2px solid #444444; border-right: 2px solid #444444; border-left: 2px solid #444444; margin-top: 5px; margin-bottom: 5px; margin-left: 5px;  margin-right: 5px; min-height: 11.693in;}</style>';
                    var template='<html><head>'+style+'</head><body>'+content+'</body></html>';

                    if(orderinfo.to==undefined){
                        orderinfo.to=[CustomerDetails[0].Customer_Email ]
                    }

                    if(orderinfo.from==undefined){
                        orderinfo.from=BookingSummary[0].CommunicationMailID;
                    }

                    if(orderinfo.message==undefined){
                        orderinfo.message=message;
                    }

                    if(orderinfo.subject==undefined){
                        orderinfo.subject=message;
                    }

                    orderinfo.template=template;



                    orderinfo.email_provider=email_provider;
                    orderinfo.email_username=email_username;
                    orderinfo.email_password=email_password;

                    mail.send_mail_from_data(orderinfo);
                }
                else{
                    logger.log('error','no record found in booking  for :'+JSON.stringify(orderinfo),'',file+'/'+method);
                }
            });
            break;

        case 'TEMPLEREGISTRATION':
            method=method+'/'+orderinfo.event;
            var subject='EOI - ' + orderinfo.tname;
            var message='Expression of Interest';
            var template='<!doctype html><html><head> <meta charset="utf-8"> <title>Temple Regisration EOI</title> <link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"></head><body style="font-family: Open Sans, sans-serif; width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;"><section class="header" style="width:100%; height:auto;"> <table style="width:650px; margin:0px auto; border:0px; border-collapse:collapse;"> <tr style="width:650px; height:auto; text-align:center;"> <td><img src="http://viralfever.in/dev/mail/images/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px;height:auto;background-image:url(http://viralfever.in/dev/mail/images/background.jpg);background-size:cover;text-align:left;height:auto;"> <td> <h5 style="font-weight:600; font-size:17px; color:#b74b03; padding-left:25px;"> Temple : '+orderinfo.tname+'</h5> <h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:10px; padding-left:25px; line-height:15px;"> Location : '+orderinfo.tlocation+'</h4> <h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:10px; padding-left:25px; line-height:15px;"> Contact : '+orderinfo.tcontact+'</h4> <h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:10px; padding-left:25px; line-height:15px;"> Email : '+orderinfo.temail+'</h4> <h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:10px; padding-left:25px; line-height:15px;"> Phone : '+orderinfo.tphone+'</h4> <h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:10px; padding-left:25px; line-height:15px;"> Address : '+orderinfo.taddress+'</h4> <h4 style="font-weight:600; font-size:17px; color:#b74b03; padding-top:10px; padding-left:25px; line-height:15px;"> State : '+orderinfo.tstate+'</h4> </td></tr></table> </div></section></body></html>';

            if(orderinfo.to==undefined){orderinfo.to='support@devaayanam.in';}
            if(orderinfo.from==undefined){orderinfo.from='support@devaayanam.in';}
            if(orderinfo.message==undefined){orderinfo.message=message;}
            if(orderinfo.subject==undefined){orderinfo.subject=message;}

            orderinfo.template=template;
            orderinfo.email_provider=email_provider;
            orderinfo.email_username=email_username;
            orderinfo.email_password=email_password;
            mail.send_mail_from_data(orderinfo);
            break;

        default:
            logger.log('error','no case found for this event :'+JSON.stringify(orderinfo),'',file+'/'+method);
            break;
    }
};

exports.send_notification=function(data){
    mail.send_mail_from_data(data);
};

// sms
exports.getsmsdetails =function(orderinfo,callback){
    var method='getsmsdetails';
    var sql="call Ordering_get_sms_data ('"+orderinfo.notificationtype + "','" + orderinfo.transactionid + "','" +
        orderinfo.loginid + "','" + orderinfo.sessionid + "')";
    logger.log('data', ' Ordering_get_sms_data' + sql,'completeorder');
    logger.log('info','Query :'+sql,'',file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err){
                logger.log('error','Result :'+ err ,'',file+'/'+method);
                callback(false);
            }
            if (rows.length == 0){
                logger.log('warn','Result:No match found for query','',file+'/'+method);
                callback(false)
            }
            else{
                results = JSON.stringify(rows);
                logger.log('data','Result:'+ results,'',file+'/'+method);
                callback(results);
            }
        });
    });
};

exports.prepare_sms_template =function(orderinfo){
    var method=' prepare_sms_template ';
    switch(orderinfo.notificationtype) {
        case 'SENDWELCOMEMESSAGE':
            method=method+'/'+orderinfo.event;
            mailTemplt.getsmsdetails(orderinfo,function(data){
                var temp= JSON.parse(data);
                //var smsdetails= temp[0][0];
                var messagetemplate='welcome to devaayanam. find your favourite temples, book puja, get live updates.';
                var smsinfo={
                    mobileno:orderinfo.mobileno,
                    message:messagetemplate
                };
                mailTemplt.sendsms(smsinfo);

                //if(smsdetails){
                //
                //}
                //else{
                //   logger.log('error','no record found for  :'+JSON.stringify(data),'',file+'/'+method);
                //}
            });
            break;

        case 'SENDVERIFICATIONMESSAGE':
            method=method+'/'+orderinfo.event;
            mailTemplt.getsmsdetails(orderinfo,function(data){
                var temp= JSON.parse(data);
                var smsdetails= temp[0][0];
                var messagetemplate='Devaayanam sign in activation code '+ smsdetails.vcode +'. After sign in please complete your profile.';
                if(smsdetails){
                    var smsinfo={
                        mobileno:smsdetails.mobileno,
                        message:messagetemplate
                    };
                    mailTemplt.sendsms(smsinfo);
                }
                else{
                    logger.log('error','no record found for  :'+JSON.stringify(data),'',file+'/'+method);
                }
            });
            break;

        case 'PAYMENTSUCCESSBOOKINGSUCCESS':
            method=method+'/'+orderinfo.event;
            mailTemplt.getsmsdetails(orderinfo,function(data){
                var temp= JSON.parse(data);
                console.log("emaildata.."+JSON.stringify(data));
                var smsdetails= temp[0][0];
                console.log("emaildata..");
                var messagetemplate='Online Puja booking for ' + smsdetails.temple +'. Tracking No:'+ smsdetails.transactionid   +' booked on '+ smsdetails.transactiondate +
                    '. Bill amount rs. '+ smsdetails.billamount +'. Please carry your ID if you are collecting prasadam at temple.';

                if(smsdetails){
                    var smsinfo={
                        mobileno:smsdetails.mobileno,
                        message:messagetemplate
                    };
                    console.log(JSON.stringify(smsinfo))
                    mailTemplt.sendsms(smsinfo);
                }
                else{
                    logger.log('error','no record found for  :'+JSON.stringify(data),'',file+'/'+method);
                }
            });
            break;

        case 'BOOKONLYPUJA':
            method=method+'/'+orderinfo.event;
            mailTemplt.getsmsdetails(orderinfo,function(data){
                var temp= JSON.parse(data);
                var smsdetails= temp[0][0];
                var messagetemplate='';
                if (smsdetails.status == 0){
                    messagetemplate='Your request for puja at ' + smsdetails.temple +', tracking No:'+ smsdetails.transactionid +', requested on '+ smsdetails.transactiondate +' is approved. Please login to website and complete your booking from profile region.'
                }
                else{
                    messagetemplate='Your request for puja at ' + smsdetails.temple +', tracking No:'+ smsdetails.transactionid +', requested on '+ smsdetails.transactiondate +' is rejected. Please contact temple for details.'
                }

                if(smsdetails){
                    var smsinfo={
                        mobileno:smsdetails.mobileno,
                        message:messagetemplate
                    };
                    mail.sendsms(smsinfo);
                }
                else{
                    logger.log('error','no record found for  :'+JSON.stringify(data),'',file+'/'+method);
                }
            });
            break;

        default:
            logger.log('error','no case found for this event :'+JSON.stringify(orderinfo),'',file+'/'+method);
            break;
    }
};

exports.sendsms=function(data,callback){
	console.log ('^^ SMS Calls currently Switched off ^^');	
	/*
    console.log(JSON.stringify(data));
    var url = 'http://bhashsms.com/api/sendmsg.php';
    var mobileno = data.mobileno;
    var message= data.message;
    if (message==''){
        message = 'welcome to devaayanam. find your favourite temples, book puja, live information';
    }

    //return;
    //console.log('sms method called ');
    //console.log(mobileno);
    //console.log(message);

    request({
        url: url,
        qs:{
            user : '9886310567',
            pass : '584bac9',
            sender : 'DEVYNM',
            phone : +mobileno,
            priority : 'ndnd',
            text : message,
            stype : 'normal'
        },
        method: 'GET'
    }, function(error, response, body) {
        if (!error){
            console.log("Response.statusCode:" + response.statusCode);
            console.log("Body:" + body);
            callback("success");
            // update data base on sms status
        }
        else{

            console.log(error);
            callback("failed");
        }
        console.log('sms called');
        return;
    });
	*/
};
