'use strict';

var db = require('../db').pool;
var logger=require('./utilities/logger.js');
var file='Admin Calendar.js';
var crypto=require('./utilities/cryptography.js');
console.log('en..:'+crypto.EncryptString(" "));
function copyFile(source, target, cb) {
    var cbCalled = false;

    var rd = fs.createReadStream(source);
    rd.on("error", done);

    var wr = fs.createWriteStream(target);
    wr.on("error", done);
    wr.on("close", function(ex) {
        done();
    });
    rd.pipe(wr);

    function done(err) {
        if (!cbCalled) {
            cb(err);
            cbCalled = true;
        }
    }
}
//DV-34
exports.onboardtemple=function(req,res)
{
    var method ='onboardtemple';
    req.body.usercode=crypto.EncryptString(req.body.usercode);
    req.body.passcode=crypto.EncryptString(req.body.passcode);
    req.body.hashkey=crypto.EncryptString(req.body.hashkey);
    db.getConnection(function(err, con) {
        var sql="Call onboardtemple('"+req.body.xmldata+"','"+req.body.usercode+"','"+req.body.passcode+"','"+req.body.hashkey+"');";
        console.log(sql);
        con.query(sql,function(err,rows) {
            con.release();
            var results;
            if (err) {
                console.log(JSON.stringify(err));
                res.end( '[]');
                return;
            }
            if (rows.length == 0) {
                res.end( '[]');
            } else {
                 if(req.body.mode=='writetofile') {
                    var fs = require('fs');
                    var os = require("os");
                    fs.open(__dirname + '/../../client/files/server_names', 'a', 666, function( e, id ) {
                            fs.write(id, ';' + os.EOL, null, 'utf8', function () {
                                fs.close(id, function () {
                                    //console.log('last is updated' );
                                });
                            });
                        });
                }
                else if(req.body.mode=='createonboard') {
                    if(rows[0][0].status==1) {
                        var lineReader = require('line-reader');
                        var os = require("os");
                        var fs = require('fs');
                        copyFile(__dirname + '/../../client/files/server_names', __dirname + '/../../client/files/server_names-old', function (err) {
                            if (err) {
                                throw err
                            }
                            else {
                                fs.writeFile(__dirname + '/../../client/files/server_names', "server_name" + os.EOL, function (err) {
                                    if (err) {
                                        return console.log(err);
                                    } else {
                                        var count = 0;
                                        lineReader.eachLine(__dirname + '/../../client/files/server_names-old', function (line, last) {
                                            fs.open(__dirname + '/../../client/files/server_names', 'a', 666, function (e, id) {
                                                if (line != ';' && count != 0 && line != 'server_name') {
                                                    fs.write(id, line + os.EOL, null, 'utf8', function () {
                                                        fs.close(id, function () {
                                                            //console.log('line is updated'+line);
                                                        });
                                                    });
                                                }
                                                if (last) {
                                                    fs.write(id, " " + rows[0][0].Website + " " + 'www.' + rows[0][0].Website + os.EOL, null, 'utf8', function () {
                                                        fs.close(id, function () {
                                                            //console.log('line is updated'+line);
                                                        });
                                                    });
                                                    results = JSON.stringify(rows);
                                                    res.end(results);
                                                }
                                            });
                                            count = count + 1;
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        results = JSON.stringify(rows);
                        res.end(results);
                    }
                }
                 else if(req.body.mode=='loadtemples'){
                     rows[0].forEach(function(row){
                         console.log (row.UserCode);
                         row.UserCode=crypto.DecryptString(row.UserCode);
                         row.PassCode=crypto.DecryptString(row.PassCode);
                         row.HashKey=crypto.DecryptString(row.HashKey);
                     });
                     console.log('up..');
                     results= JSON.stringify(rows);
                     res.end(results);
                 }else{
                    results= JSON.stringify(rows);
                    res.end(results);
                }
            }
        });
    });
};
//DV-34
exports.generalcall=function(req,res)
{
    var method ='generalcall';
    db.getConnection(function(err, con){
        var sql="Call generalcall('"+req.body.xmldata+"');";
        console.log('Query :'+sql+"  "+method);
        con.query(sql,function(err,rows)
        {
            con.release();
            var results;
            if (err)
            {
                console.log("error undefined"+err)
                logger.log('error',+err);
                res.end( '[]');
                return;
            }
            if (rows.length == 0)
            {
                console.log();
                console.log('ttt1..');
                logger.log('warn','No match found for query',req.session.sessionID,file+'/'+method);
                res.end( '[]');
            }
            else
            {

                    results= JSON.stringify(rows);
                    res.end(results);
console.log('ttt..');
                logger.log('data',results,req.session.sessionID,file+'/'+method);
            }
        });
    });

};














