/*
* Compressor for JeWAR
* Require:
* node.js
* node-minify (https://github.com/srod/node-minify)
* */
var fs = require('fs')
var compressor = require('../../../node_modules/node-minify');
var project_root=__dirname;
var project_path=project_root.substring(0,project_root.lastIndexOf('server')-1);
var base_path = project_path+'/client/scripts/js/';
var  js_min_path = project_path+'/client/scripts/min/';
console.log('project_root :'+project_path);
console.log('base_path :'+base_path);
console.log('js_min_path :'+js_min_path);
var minPath=[{path:js_min_path+'DevaayanamAdminControllers.min.js'},{path:js_min_path+'DevaayanamAdminServices.min.js'}];

minPath.forEach(function(row){
    var check_service_min=fs.existsSync(row.path);
    if(check_service_min==true){
      fs.unlinkSync(row.path)
      console.log('file deleted '+row.path);
    }
});



var Controllers=[
    base_path+'controllers/Admin_CalendarController.js',
    base_path+'controllers/Admin_DeityController.js',
    base_path+'controllers/Admin_DonationController.js',
    base_path+'controllers/Admin_NewUserController.js',
    base_path+'controllers/Admin_PujaController.js',
    base_path+'controllers/Admin_TaskController.js',
    base_path+'controllers/Admin_TempleContactController.js',
    base_path+'controllers/Admin_TempleHistoryController.js',
    base_path+'controllers/Admin_TempleSettingsController.js',
    base_path+'controllers/DashboardController.js',
    base_path+'controllers/loginController.js'
];

var Services=[
    base_path+'services/Admin_CalendarService.js',
    base_path+'services/Admin_DeityService.js',
    base_path+'services/Admin_DonationService.js',
    base_path+'services/Admin_NewUserService.js',
    base_path+'services/Admin_PujaRuleService.js',
    base_path+'services/Admin_PujaService.js',
    base_path+'services/Admin_TaskService.js',
    base_path+'services/Admin_TempleContactService.js',
    base_path+'services/Admin_TempleHistoryService.js',
    base_path+'services/Admin_TempleSettingsService.js',
    base_path+'services/DashboardService.js',
    base_path+'services/loginService.js'
];
console.log('Controllers : Total :'+Controllers.length+' nos')
console.log(Controllers)
console.log('Services : Total :'+Services.length+' nos')
console.log(Services)

new compressor.minify({
    type: 'no-compress',
    fileIn: Services,
    fileOut: js_min_path+'services.compress.js',
    callback: function(err, min){
        if (err) console.log(err); else {console.log("Services Minified")
            new compressor.minify({
                type: 'uglifyjs',
                fileIn: js_min_path+'services.compress.js',
                fileOut: minPath[1].path,
                callback: function(err, min){
                    fs.unlinkSync(js_min_path+'services.compress.js');
                    if (err) console.log(err); else console.log("Devaayanam Services uglified");
                }
            });};
    }
});


new compressor.minify({
    type: 'no-compress',
    fileIn: Controllers,
    fileOut: js_min_path+'controllers.compress.js',
    callback: function(err, min){
        if (err) console.log(err); else{
            console.log("Controllers Minified");
            new compressor.minify({
                type: 'uglifyjs',
                fileIn: js_min_path+'controllers.compress.js',
                fileOut: minPath[0].path,
                callback: function(err, min){
                    fs.unlinkSync(js_min_path+'controllers.compress.js');
                    if (err) console.log(err);else console.log("Controllers Uglified");
                }
            });}
    }
});




//new compressor.minify({
//    type: 'yui-css',
//    fileIn: css_path+'metro-bootstrap.css',
//    fileOut: css_compile_path+'metro-bootstrap.min.css',
//    callback: function(err, min){
//        if (err) console.log(err); else console.log("main css compiled");
//    }
//});
//
//new compressor.minify({
//    type: 'yui-css',
//    fileIn: css_path+'metro-bootstrap-responsive.css',
//    fileOut: css_compile_path+'metro-bootstrap-responsive.min.css',
//    callback: function(err, min){
//        if (err) console.log(err); else console.log("responsive css compiled");
//    }
//});
//
//new compressor.minify({
//    type: 'yui-css',
//    fileIn: css_path+'iconFont.css',
//    fileOut: css_compile_path+'iconFont.min.css',
//    callback: function(err, min){
//        if (err) console.log(err); else console.log("icon font css compiled");
//    }
//});




