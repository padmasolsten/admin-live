'use strict';

var db = require('../db').pool;
var logger=require('./utilities/logger.js');
var morgan  = require('morgan');
var jwt  = require('jsonwebtoken');
var cors = require('cors');
var url = require('url');
var file='admin.js';
var secret='DVNMTOCKENSECURITYDEVEBYSAIFOCT';

var cryptojs=require('./utilities/cryptography.js');


var nodemailer = require("nodemailer");
var emailtemplate=require('./emailtemplate.js');
var prevDirName=__dirname;
prevDirName=prevDirName.substring(0,prevDirName.lastIndexOf('rest'));
var dbConfigFile = prevDirName + '/database.json';
var fs = require('fs');
var env='configuration';
var data = fs.readFileSync(dbConfigFile, 'utf8');
var dbConfig = JSON.parse(data)[env];

var email_provider=dbConfig.emailprovider;
var email_username=dbConfig.emailusername;
var email_password=cryptojs.DecryptString(dbConfig.emailpassword);

exports.validate_request = function(req, res,next){
    var method='validate api request';

    console.log('API REQUEST RECIEVED :)');
    console.log('1- GET READY FOR THE SECURITY CHECKUP ');

    var token = req.headers['devayanam-access-token'];


    // decode token
    if (token) {
        console.log('2- WOW, HEY ,FOUND TOKEN FOR YOUR REQUEST, ');

        // verifies secret and checks exp
        jwt.verify(token,secret,function (err, decoded) {
            if (err) {
                console.log('3- OOPS,SORRY , IT SEEMS YOUR TOKEN IS INVALID :( , FAILED TO AUTHENTICATE THE TOKEN');
                logger.log('info','FAILED TO AUTHENTICATE THE TOKEN',1,file+'/'+method);
                return res.json( [{result:{message:'Failed to authenticate token.'},status:false,token_auth:false,message:'Failed to authenticate token.'}]);
            } else {
                // if everything is good, save to request for use in other routes
                console.log('3- WOW , CONGRATS ,FOUND EVERYTHING IS GOOD, PLEASE WAIT LET ME OPEN YOUR DOOR ');
                req.decoded = decoded;

                if(decoded){
                    req.session.LoginID = decoded.session[0].LoginID;
                    req.session.CustomerName=decoded.session[0].FirstName;
                    req.session.UserID=decoded.session[0].UserID;
                    req.session.TempleID=decoded.session[0].TempleID;
                    req.session.Category=decoded.session[0].Category;
                    req.session.TempleName=decoded.session[0].TempleName;
                    req.session.TempleTypeID= decoded.session[0].TypeID;
                    req.session.AccessTypeID= decoded.session[0].AccessTypeID;
                    req.session.TempleEmail= decoded.session[0].TempleEmail;
                    req.session.TempleIcon= decoded.session[0].TempleIcon;
                    next();

                }
                else {
                    next();
                }

            }
        });

    }
    else {

        // if there is no token
        console.log('2- NO TOKEN PROVIDED');
        console.log('XXXXXXXXXXXXXXXXXXXXXXX  YOU DONT HAVE THE ACCESS FOR THE REQUESTED RESOURCE XXXXXXXXXXXXXXXXXXXXXXX ');
        console.log('WHAT THE HECK...,GET OUT MY  HOUSE AND YOU WILL NOT SEE ANY MINUTES OF TODAY, IRANGI PODEY :D');
        // return an error

        return res.status(403).send([{result:{message:'No token provided.'},status:false,token_auth:false,message:'No token provided.'}]);

    }


};

exports.logout = function(req, res){
    var method='logout';
    var data = req.session.LoginID;
    req.session.LoginID = null;
    req.session.AuthenticatedCID=null;
    req.session.AuthenticatedCustomerID=null;
    req.session.TempleID=null;
    req.session.TempleName = null;
    req.session.AccessTypeID=null;
    res.end('[{"User":"api user logout"}]');
    logger.log('info','logout success',req.session.sessionID,file+'/'+method);
};

exports.Login=function(req,res){
    var method='Login';
    var url_parts={hostname:''};
    if(req.body.PageURL){url_parts=url.parse(req.body.PageURL,true);}
    var host=url_parts.hostname;
    var wwwcheck=host.substring(0,2);
    if(wwwcheck.toUpperCase()=='WWW'){host=host.substring(4,host.length)}
    var loginPageHost=host;
        loginPageHost="admin."+loginPageHost; //remove this comment to run in local
	var login_password=cryptojs.EncryptString(req.body.pass);
    var sql="Select  useradmin.TempleID,  temple.TempleName,temple.Category,  useradmin.UserID,  useradmin.LoginID,  useradmin.FirstName,  temple.WebSite,  (Select    IfNull((Select      templeconfig.Value    From      templeconfig    Where      templeconfig.TempleID = useradmin.TempleID And      templeconfig.ConfigID = 15), 0)) As TypeID,  useradmin.LanguageID,  temple.CommunicationMailID As TempleEmail,IfNull((Select    templeimages.Url  From    templeimages  Where    templeimages.TempleID = temple.TempleID And    templeimages.TypeID = 3 and templeimages.LanguageID = 1  Limit 1), '/images/temple/uploads/thumbnail.jpg') As TempleIcon From  useradmin Inner Join  temple On useradmin.TempleID =    temple.TempleID And useradmin.LanguageID =    temple.LanguageID Where  useradmin.LoginID = '"+req.body.mail+"' And  useradmin.LoginPassword = '"+login_password+"' And  useradmin.LanguageID = 1";
    logger.log('info','query :'+sql,0,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:false,message:msg}]));
                logger.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                var msg='Please Check your username or Password';
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:false,message:msg}]));
                logger.log('warn','',0,file+'/'+method);
            }
            else
            {
                console.log('login_result : '+JSON.stringify(rows));
                var templeHost="admin."+rows[0].WebSite;
                console.log('RequestedHost : '+loginPageHost);
                console.log('templeHost : '+templeHost);
                req.session.LoginID = rows[0].LoginID;
                req.session.CustomerName=rows[0].FirstName;
                req.session.UserID=rows[0].UserID;
                req.session.TempleID=rows[0].TempleID;
                req.session.Category=rows[0].Category;
                req.session.TempleName=rows[0].TempleName;
                req.session.TempleTypeID= rows[0].TypeID;
                req.session.AccessTypeID= rows[0].AccessTypeID;
                req.session.TempleEmail= rows[0].TempleEmail;
                req.session.TempleIcon= rows[0].TempleIcon;
                results= JSON.stringify(rows);

                var payload = {
                    check:  true,
                    LoginID : rows[0].LoginID,
                    CustomerName:rows[0].FirstName,
                    UserID:rows[0].UserID,
                    TempleID:rows[0].TempleID,
                    Category:rows[0].Category,
                    TempleName:rows[0].TempleName,
                    TempleTypeID: rows[0].TypeID,
                    AccessTypeID: rows[0].AccessTypeID,
                    TempleEmail: rows[0].TempleEmail,
                    TempleIcon: rows[0].TempleIcon,
                    session:rows,
                    last_login:new Date().getDate(),
                };

                console.log('payload :-  '+payload);

                //var token = '';
                var token = jwt.sign(payload, secret, { expiresIn: 31556952000 / 12});
                console.log('token :-  '+token);

                // return the informations to the client
                var msg='Authentication Completed';
                var resp={ "data" : rows,token:token,message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                logger.log('info',results,0,file+'/'+method);
            }
        });

    });

};

exports.loadDashboard=function(req,res){

    var method='API loadDashboard';
	var sql="call admin_load_dashboard('"+req.session.TempleID+"')";	
    logger.log('info','API Service Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                var msg='No dashboard found for the Temple' + req.session.TempleID;
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
                var msg='Dashboard data Generated';
                var resp={ "data" : rows[0],message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);		
            }
        });
    });

};

exports.get_puja_report=function(req,res){

    var method='API GetPujaReport';
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
    var sql="call admin_puja_report('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+from+"','"+to+"')"
    console.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows[0].length == 0)
            {
                var msg='No puja booking found for the selected dates';
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
                var msg='Puja Booking Report Generated';
                var resp={ "data" : rows[0],message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);
            }
        });
    });

};
exports.getBookingReport=function(req,res){
    var method='API getBookingReport';
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
	var txnID = req.body.txnID;	
	if (req.body.txnID == undefined || req.body.txnID == null || req.body.txnID =='')
	{ txnID = 0; }
	else
	{ txnID = req.body.txnID; }
	
    //var sql="call admin_booking_report('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+to+"','"+to+"',"+txnID+")" // show booking report only for current date, which happens to be the end date, ignore from date
var sql="call admin_booking_report_app('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+to+"','"+to+"',"+txnID+")" // show booking report only for current date, which happens to be the end date, ignore from date
	
    console.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results = [];
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows[0].length == 0)
            {
                var msg='No booking found for the selected dates';
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
				rows[0].forEach(function(puja){
					var details = [];							
					rows[1].forEach(function(pujadetails){
						if (puja.TrackingNo == pujadetails.TrackingNo)
						{
							details.push(pujadetails);									
						}									
					});
					console.log('Got puja details for Trackingno..'+puja.TrackingNo+ JSON.stringify(details));																
					puja.pujadetails = details;
					results.push(puja);
					//console.log('Results..'+ JSON.stringify(results));				
				});
                var msg='Booking Report Generated';
                var resp={ "data" : rows[0],message:msg};
                console.log('info',resp,0,file+'/'+method);
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
            }
        });
    });
	
};

exports.getApprovalList=function(req,res){
    var method='API getApprovalList';
    var from=req.body.from.substring(0,10);
    var to=req.body.to.substring(0,10);
	var sql="call admin_puja_approval_list('"+req.session.TempleID+"','"+req.body.LanguageID+"','"+req.session.UserID+"','"+from+"','"+to+"')";
    console.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows[0].length == 0)
            {
                var msg='No approval request found for the selected dates';
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
				// Send in the valid values of the new approval status that the request can be updated to, to the app, 
				// these value will come in back as approval action
				rows[1].forEach(function(requestPuja){ 
					var approvalChoice;												
					var approvalChoice = {1: "APPROVED", 0: "CANCELLED"};					
					requestPuja.statusChoice = approvalChoice;
				});

                var msg='Approval Request List Generated';
                var resp={ "data" : rows[1],message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);
            }
        });
    });
	
};
    
exports.getStatusUpdateList=function(req,res){
    var method='API getStatusUpdateList';
	var sql="call admin_puja_pending_list('"+req.session.TempleID+"','"+req.body.LanguageID+"',"+req.body.txnID+")";
    console.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows[0].length == 0)
            {
                var msg='No puja pending';
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
				// Send in the valid values of the new puja status that the puja can be updated to, to the app, 
				// these value will come in back as status update api
				rows[0].forEach(function(pujaRecord){ 
					var statusChoiceList;							
					//statusChoiceList = {2: "BOOKING RECEIVED", 1: "PUJA COMPLETED", 16: "PUJA DELAYED", 3: "PUJA CANCELLED"};
					statusChoiceList = {1: "PUJA COMPLETED", 16: "PUJA DELAYED", 3: "PUJA CANCELLED"};
					if (rows[1][0].isAssisted == '1') {
						statusChoiceList["17"] = "BOOKING COMPLETE";
					}
					//if (pujaRecord.PostalStatus == '0') { 07/02/2020 - Postal statuses where not getting show for cases of free shipping 
					if (pujaRecord.ShippingID != '0' && pujaRecord.ShippingID != '91' && pujaRecord.ShippingID != '92')
					{
						statusChoiceList["6"] = "PUJA COMPLETE POSTAGE PENDING";
						statusChoiceList["7"] = "PUJA COMPLETE POSTAGE COMPLETE";
					}
					pujaRecord.statusChoice = statusChoiceList;
				});
				
                var msg='Pending Puja List Generated';
                var resp={ "data" : rows[0],message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);
            }
        });
    });
	
};

function jsUcfirst(p_string) 
{
	return p_string.charAt(0).toUpperCase() + p_string.slice(1);
};

exports.pujaApprovalAction=function(req,res) {
	var method='API pujaApprovalAction';
	console.log('request details..'+JSON.stringify(req.body));
    var sql="call approvalaction('"+req.body.RequestID+"','"+req.body.ApprovalMessage+"','"+req.body.ApprovalStatus+"','"+req.session.TempleID+"')";
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "Response" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            else if (rows.length == 0)
            {
                var msg='No match for puja data';
                var resp={ "Response" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else {
                console.log('cost'+req.body.itemCost);
                if(req.body.itemCost){
                    if(req.body.itemCost>0){
                        db.getConnection(function(err, con) {
                            var sql2="call admin_add_temple_shippingcharge('"+req.session.TempleID+"','"+req.body.itemCost+"','','"+req.body.PujaID+"')";
                            console.log(sql2);
                            con.query(sql2, function (err, rows, fields) {
                                con.release();
                                var results;
                                if (err) {
                                    console.log('itemCost error'+err);
                                    return;
                                }
                            });
                        });

                    }
                }
                console.log('ok');
                if (rows.affectedRows != 0) {
                    var TempleAddress=rows[1][0];
                    var TempleName=req.session.TempleName;
                    var TempleHouseName=TempleAddress.HouseName;
                    var TempleStreetName=TempleAddress.StreetName;
                    var TemplePostOffice=TempleAddress.PostOffice;
                    var TempleDistrict=TempleAddress.District;
                    var TempleState=TempleAddress.State;
                    var TemplePostalCode=TempleAddress.PostalCode;
                    var TemplePhoneMobile=TempleAddress.PhoneMobile;
                    var TemplePhoneWired=TempleAddress.PhoneWired;
                    var TempleCommunicationMailID=TempleAddress.Email;
                    if (rows[0][0].Status == 1) {
                        var Message = ' ';
                        var TemplateApproved;
                        if (req.body.ApprovalMessage != null && req.body.ApprovalMessage != undefined) {
                            Message = req.body.ApprovalMessage;
                        }
                        //new template
                        var message = 'puja booking received';
                        var subject = "";
                        var content = '';
                        var ItemsContent = '';
                        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        console.log('Puja Booking Approval..' + JSON.stringify(rows[0][0].CustomerID));
                        var customer_name = rows[0][0].FirstName + ' ' + rows[0][0].LastName;
						var customer_mobile = rows[0][0].PhoneMobile;
                        var enCustomerId = cryptojs.EncryptString('' + rows[0][0].CustomerID);
                        enCustomerId = enCustomerId.replace(/\//g, "110111");
                        var enRequestId = cryptojs.EncryptString('' + rows[0][0].RequestID);
                        enRequestId = enRequestId.replace(/\//g, "110111");
                        if (req.body.category == 'priest') {
                            content += '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                            content += TempleName;
                            content += '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>PUJA REQUEST APPROVED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">RequestID:<span style="font-weight:700; color:#000;">';
                            content += rows[0][0].RequestID;
                            content += '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                            content += 'Dear ' + customer_name + ' ';
                            content += ',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"> Congratulations! Following puja requested by you is approved by the priest '+TempleName.toUpperCase()+'.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                            '</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"> ' + Message + '</p><table border="3px " bordercolor="#7b0b0b" style="width:98%;margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="3" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Request ID : ';
                            content += rows[0][0].RequestID + ' ';
                            content += '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Puja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Location</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td></tr>';
                            var shipment_stats_for_booking = 0;
                            var Shipment = '-';
                            var d = new Date(rows[0][0].PujaDate);
                            var PujaDate = d.getDate() + '-' + monthNames[d.getMonth()] + '-' + d.getFullYear();
                            if (rows[0][0].PostalStatus == '0') {
                                Shipment = 'included';
                                shipment_stats_for_booking = 1
                            }
                            ItemsContent += '<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].PujaName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].DevoteeName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + PujaDate + '</td></tr>';
                            content += ItemsContent;
                            content += '</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">You can track the status of your booking in your user profile.</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"> <span style="color:#b74b03;">';
                            content += '</span></p>' +
                            //'<p style="font-weight:400; font-size:14px; color:#2d2d2d;  padding-left:70px; padding-right:25px; line-height:20px;">Choose Book with puja items if you want Pandit to arrange for the puja items.</p>' +
                            '</td></tr>' +
                            '    <tr style="text-align:center;padding-bottom: 20px;"><td><a href="http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + '/1"><button style=" margin-right:5px;background-color: #4CAF50; /* Green */border: none;color: white;padding: 9px 12px;text-align: center;text-decoration: none;display: inline-block;margin-bottom: 10px;font-size: 16px;">Book with puja items</button></a><a href="http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + '/0"><button style=" background-color: #4CAF50; /* Green */border: none;color: white;padding: 9px 12px;text-align: center;text-decoration: none;display: inline-block;margin-bottom: 10px;font-size: 16px;">Book without puja items</button></a></td></tr>';
                            content +=' <tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>';
                            content += '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                            content += TempleName.toUpperCase() + ',<br>' + TempleHouseName.toLowerCase() + ',' + TempleStreetName.toLowerCase() + ',<br>' + TemplePostOffice.toLowerCase() + ',<br>'
                            content += TempleDistrict.toLowerCase() + ',<br>' + TempleState.toLowerCase() + ',<br>' + TemplePostalCode + ',<br>' + TemplePhoneMobile + ',' + TemplePhoneWired + '.<br>' + TempleCommunicationMailID.toLowerCase();
                            content += '</h6></td></tr>' +
                            '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                        } else {
							//DV-117 - Template changes
							//The link to pay for puja should be prominent (bigger font, separate line)
							//24 hours time line message should be prominent
                            content += '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                            content += TempleName;
                            content += '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>PUJA REQUEST APPROVED <hr style="width:10%; border:solid #C00 1px; margin-top:10px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:250px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">RequestID:<span style="font-weight:700; color:#000;">';
                            content += rows[0][0].RequestID;
                            content += '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                            content += 'Dear ' + customer_name + ',';
                            content += '</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">Congratulations !! Following Puja requested by you is approved.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:4px;padding-left:70px;padding-right:2px;line-height:20px;">' +
                            'For complete your booking please proceed with your payment by <br> <span style="font-weight:800;font-size:18px;"><a href="http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + '">clicking here</a>.</p>';
							content += '<p style="font-weight:800;font-size:14px;color:#2d2d2d;padding-top:4px;padding-left:70px;padding-right:2px;line-height:12px;">Please make the payment within 2 days post which we will release the booking.</p>' +
										'<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"> </p>' + '<table border="3px " bordercolor="#7b0b0b" style="margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="4" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Request ID : ';
                            content += rows[0][0].RequestID + ' ';
                            content += '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Puja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Beneficiary</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td><td class="shipment" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Shipment</td></tr>';

                            var shipment_stats_for_booking = 0;
                            var Shipment = '-';
                            var d = new Date(rows[0][0].PujaDate);
                            var PujaDate = d.getDate() + '-' + monthNames[d.getMonth()] + '-' + d.getFullYear();

                            if (rows[0][0].PostalStatus == '0') {
                                Shipment = 'included';
                                shipment_stats_for_booking = 1
                            }
                            ItemsContent += '<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].PujaName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].DevoteeName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + PujaDate + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + Shipment + '</td></tr>';


                            content += ItemsContent;
                            content += '</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">You can track the status of your booking in your “user area” on temple website.</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">May <span style="color:#b74b03;">';
                            content += TempleName;
                            content += '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">';
                            content += TempleName.toUpperCase() + ' </span> Temple management</p></td></tr>';
                            content += '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is an online platform for temples. For complete list of temples in Devaayanam visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' ;
                            // content+='<tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                            content += '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                            content += TempleName.toUpperCase() + ',<br>' + jsUcfirst(TempleHouseName) + ',' + jsUcfirst(TempleStreetName) + ',<br>' + jsUcfirst(TemplePostOffice) + ',<br>'
                            content += jsUcfirst(TempleDistrict) + ',<br>' + jsUcfirst(TempleState) + ',<br>' + TemplePostalCode + ',<br>' + TemplePhoneMobile + ',' + TemplePhoneWired + '.<br>' + TempleCommunicationMailID.toLowerCase();
                            content += '</h6></td></tr>' +
                            '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                        }
                        console.log(content);
                        //ene template
                        console.log(req.body.filepath);
                        if (rows[0][0].Status == 1 && req.body.filepath != null && req.body.filepath != undefined && req.body.filepath != '') {
                            fs.readFile(__dirname + '/../../../ordering/client/' + req.body.filepath, function (err, data) {
                                console.log(err);
                                if (err) {
                                    console.log(err);
                                } else {
                                    var mailer = require('nodemailernew');
                                    var smtpTransport = mailer.createTransport({
                                        service: 'gmail',
                                        auth: {
                                            user: email_username,
                                            pass: email_password
                                        }
                                    });
									var mailto;
									mailto = rows[0][0].LoginID+ ', ' + rows[1][0].Email;
									console.log ('Copy temple on puja action - ' + mailto);
                                    var mailOptions = {
                                        from: email_username, // sender address
                                        to: rows[0][0].LoginID+ ', ' + rows[1][0].Email, // list of receivers // DV191 copy temple on puja approval/rejects
                                        subject: 'Puja request approved', // Subject line
                                        attachments: [{'filename': 'attachement1.jpg', 'content': data}],
                                        html: content // html body              }
                                    }
                                    console.log(mailOptions);
                                    smtpTransport.sendMail(mailOptions, function (error, response) {
                                        if (error) {
                                            console.log('e..' + error);
                                            logger.log('error', 'Result:' + error, req.session.UserID, file + '/' + method + '/Email');
                                        }
                                        else {
                                            console.log('s..' + error + response.message);
                                            logger.log('data', 'Result:' + response.message, req.session.UserID, file + '/' + method + '/Email');
                                        }
                                    });
									// DV154 - SMS to Devotee on all approval action -- start ------- approved -------
									console.log('SMS for Puja Booking Approval..' + customer_name);
				                    var smsinfo={
										mobileno:customer_mobile,
										message:'Dear ' + customer_name + ', Congratulations !! Puja requested by you at ' + TempleName + ' is approved. ' +
											' To complete your booking please proceed with your payment by ' + 
											'http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + ' clicking here.' +
											'. Please make the payment within 2 days post which we will release the booking. Thank You'									
									};
									emailtemplate.sendsms(smsinfo, function(res){});
									// DV154 - SMS to Devotee on all approval action -- start ------- approved -------									
                                }
                            });
                        } else {
                            var mailer = require('nodemailernew');
                            var smtpTransport = mailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: email_username,
                                    pass: email_password
                                }
                            });
                            var mailOptions = {
                                from: email_username, // sender address
                                to: rows[0][0].LoginID+ ', ' + rows[1][0].Email, // list of receivers // DV191 copy temple on puja approval/rejects
                                subject: 'Puja request approved', // Subject line
                                // text: Message, // plaintext body
                                html: content // html body              }
                            }
                            console.log(mailOptions); 
                            smtpTransport.sendMail(mailOptions, function (error, response) {
                                if (error) {
                                    console.log('e..' + error);
                                    logger.log('error', 'Result:' + error, req.session.UserID, file + '/' + method + '/Email');
                                }
                                else {
                                    console.log('s..' + error + response.message);
                                    logger.log('data', 'Result:' + response.message, req.session.UserID, file + '/' + method + '/Email');
                                }
                            });
							// DV154 - SMS to Devotee on all approval action -- start ------- approved -------
							console.log('SMS for Puja Booking Approval (last else)..' + customer_name);
							var smsinfo={
								mobileno:customer_mobile, 
								message:'Dear ' + customer_name + ', Congratulations !! Puja requested by you at ' + TempleName + ' is approved. ' +
										' To complete your booking please proceed with your payment by ' + 
										'http://' + rows[1][0].WebSite + '/api/payonline/' + enCustomerId + '/' + enRequestId + ' clicking here.' +
										' Please make the payment within 2 days post which we will release the booking. Thank You'
							};
							emailtemplate.sendsms(smsinfo, function(res){});
							// DV154 - SMS to Devotee on all approval action -- start ------- approved -------									
                        }
                    } else {
                        var Message=' ';
                        if(req.body.ApprovalMessage!=null && req.body.ApprovalMessage!=undefined) {
                            Message = req.body.ApprovalMessage;
                        }
                        //new template
                        var message='request puja approved';
                        var subject="";
                        var content='';
                        var ItemsContent='';
                        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        if(req.body.category=='priest') {
                            var customer_name = rows[0][0].FirstName + ' ' + rows[0][0].LastName;
                            content += '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                            content += TempleName;
                            content += '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>PUJA REQUEST NOT APPROVED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">RequestID:<span style="font-weight:700; color:#000;">';
                            content += rows[0][0].RequestID;
                            content += '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                            content += 'Dear ' + customer_name + ' ';
                            content += ',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">We regret to inform you that following Puja request made by you could not be approved.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;">   ' + Message + '  </p><table border="3px " bordercolor="#7b0b0b" style="margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="4" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Request ID : ';
                            content += rows[0][0].RequestID + ' ';
                            content += '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Puja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Beneficiary</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td><td class="shipment" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Shipment</td></tr>';

                            var shipment_stats_for_booking = 0;
                            var Shipment = '-';
                            var d = new Date(rows[0][0].PujaDate);
                            var PujaDate = d.getDate() + '-' + monthNames[d.getMonth()] + '-' + d.getFullYear();

                            if (rows[0][0].PostalStatus == '0') {
                                Shipment = 'included';
                                shipment_stats_for_booking = 1
                            }
                            ItemsContent += '<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].PujaName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + rows[0][0].DevoteeName + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + PujaDate + '</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">' + Shipment + '</td></tr>';


                            content += ItemsContent;
                            content += '</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">';
                            //content += TempleName;
                            content += '</p><p style="font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:20px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">';
                            content +='If you want Devaayanam to help arrange for a priest, please give your details <a href="http://priests.devaayanam.in/devaayanam">here</a> and we will reach out to you</span></p></td></tr>';
                            content +='<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>';
                            // content+=<tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                            content += '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                            content += TempleName.toUpperCase() + ',<br>' + TempleHouseName.toLowerCase() + ',' + TempleStreetName.toLowerCase() + ',<br>' + TemplePostOffice.toLowerCase() + ',<br>'
                            content += TempleDistrict.toLowerCase() + ',<br>' + TempleState.toLowerCase() + ',<br>' + TemplePostalCode + ',<br>' + TemplePhoneMobile + ',' + TemplePhoneWired + '.<br>' + TempleCommunicationMailID.toLowerCase();
                            content += '</h6></td></tr>' +
                            '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                        } else {
                            var customer_name=rows[0][0].FirstName+' '+rows[0][0].LastName;
							var customer_mobile =rows[0][0].PhoneMobile;
                            content+= '<!doctype html><html><head><meta charset="utf-8"><title>devaayanam booking</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">';
                            content+= TempleName;
                            content+= '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>PUJA REQUEST NOT APPROVED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">RequestID:<span style="font-weight:700; color:#000;">';
                            content+= rows[0][0].RequestID ;
                            content+= '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">';
                            content+= 'Dear '+customer_name+' ';
                            content+= ',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">We regret to inform you that following Puja request made by you could not be approved.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;">   '+Message+'  </p><table border="3px " bordercolor="#7b0b0b" style="margin-left:10px; margin-right:4px; border:1px ; border-collapse:collapse; border-style:solid; color:#7b0b0b; margin-bottom:25px; margin-top:20px;"><tr><td colspan="4" style="text-align:center;padding-top:10px;padding-bottom:10px;font-size:18px;font-weight:600;">Request ID : ';
                            content+= rows[0][0].RequestID  +' ';
                            content+= '</td></tr><tr><td class="pooja" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Puja</td><td class="Beneficiary" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px;  font-weight:600;">Beneficiary</td><td class="Date" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Date</td><td class="shipment" style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:16px; font-weight:600;">Puja Items</td></tr>';

                            var  shipment_stats_for_booking=0;
                            var Shipment='-';
                            var d = new Date(rows[0][0].PujaDate);
                            var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                            if(rows[0][0].PostalStatus=='0'){Shipment='included';  shipment_stats_for_booking=1}
                            ItemsContent+='<tr><td c style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+rows[0][0].PujaName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+rows[0][0].DevoteeName+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+PujaDate+'</td><td style="width:146px;text-align:center;padding-top:10px;padding-bottom:10px;padding-left:5px;padding-right:5px;font-size:14px;font-weight:400;color:#333;">'+Shipment+'</td></tr>';
                            content+=ItemsContent;

                            content+='</table><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"></p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">May <span style="color:#b74b03;">';
                            content+= TempleName;
                            content+='</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">';
                            content+= TempleName.toUpperCase() +' </span> Temple management</p></td></tr>';
                            content+='<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' ;
                            // content+=<tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_akshaya.jpg"/><img src="'+imageurl+'email_ad_tesori.jpg" style="margin-left:60PX;"/></td></tr><tr style="width:650px; height:auto;text-align:left;"><td><img src="'+imageurl+'email_ad_tesori.jpg"/><img src="'+imageurl+'email_ad_akshaya.jpg" style="margin-left:60PX;"/></td></tr ><tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                            content+='<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">';
                            content+=TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+TempleStreetName.toLowerCase()+',<br>'+TemplePostOffice.toLowerCase()+',<br>'
                            content+=TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase();
                            content+='</h6></td></tr>' +
                            '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';
                        }
                        console.log(content);
                        console.log(TemplateApproved);
                        //ene template

                        var mailer = require('nodemailernew');
                        var smtpTransport = mailer.createTransport({
                            service: 'gmail',
                            auth: {
                                user: email_username,
                                pass: email_password
                            }
                        });
                        var mailOptions = {
                            from: email_username, // sender address
                            to: rows[0][0].LoginID + ', ' + rows[1][0].Email, // list of receivers //DV191 copy temple on puja approval/rejects
                            subject: 'Puja request not approved', // Subject line
                            // text: Message, // plaintext body
                            html: content // html body              }
                        }
                        console.log(mailOptions);
                        smtpTransport.sendMail(mailOptions, function (error, response) {
                            if (error) {
                                logger.log('error', 'Result:' + error, req.session.UserID, file + '/' + method + '/Email');
                            }
                            else {
                                logger.log('data', 'Result:' + response.message, req.session.UserID, file + '/' + method + '/Email');
                            }
                        });
						// DV154 - SMS to Devotee on all approval action -- start ------- Rejected/Cancelled -------
						console.log('SMS for Puja Booking Rejected/Cancelled..' + customer_name);
				        var smsinfo={
							mobileno:customer_mobile,
							message:'Dear ' + customer_name +', We regret to inform you that Puja request made by you at ' + TempleName + 
									' could not be approved. Request ID-' + rows[0][0].RequestID +
									'. ' + Message								
						};
						emailtemplate.sendsms(smsinfo, function(res){});
						// DV154 - SMS to Devotee on all approval action -- end ------- Rejected/Cancelled -------									
                    }
                }
                //res.end( '[{ "RESULT" : "1"}]');
                //logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
                var msg='Puja request updated';
                var resp={ "Response" : "1",message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);
            }
        });
    });

};


exports.getAnnouncements=function(req,res)
{
	var method='API getAnnouncements';
	var sql="call admin_get_announcements('"+req.session.TempleID+"','"+req.body.LanguageID+"')";
    console.log('info','API Service Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify({result:resp,status:false,token_auth:true,message:msg}));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                var msg='No announcements found for the Temple' + req.session.TempleID;
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify({result:resp,status:false,token_auth:true,message:msg}));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
                var msg='Announcements data Generated';
                var resp={ "data" : rows[0],message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                //res.end(JSON.stringify({result:resp,status:true,token_auth:true,message:msg}));
                console.log('info',results,0,file+'/'+method);
            }
        });
    });
};

exports.postAnnouncement=function(req,res)
{
	var method='API postAnnouncement';
	var tp=new Date(req.body.AlertEndDate);
	var AlertEndDate=tp.getFullYear() +"-"+(+tp.getMonth()+1)+"-"+tp.getDate();
	var tps=new Date(req.body.AlertStartDate);
	var AlertStartDate=tps.getFullYear() +"-"+(+tps.getMonth()+1)+"-"+tps.getDate();
	var sql="Call admin_add_announcement('"+req.session.TempleID+"','"+req.body.AlertName+"','"+req.body.AlertMessage+"','"+AlertStartDate+"','"+AlertEndDate+"')";
    console.log('info','API Service Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                var msg='Unable to add announcement for the Temple' + req.session.TempleID;
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
                var msg='Announcement added to temple ' + req.session.TempleID;
                var resp={ "data" : rows,message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);		
            }
        });
    });
};

exports.deleteAnnouncement=function(req,res)
{
    var method='API deleteAnnouncement';
    var tp=new Date(req.body.EndDate);
    var date=tp.getFullYear() +"-"+(+tp.getMonth()+1)+"-"+tp.getDate();
    var sql="call admin_update_announcement('"+req.session.TempleID+"','"+req.body.ContentID+"','"+
	req.body.LanguageID+"','"+req.body.Mode+"','"+date+"','"+req.body.MessageHeader+"','"+req.body.MessageBody+"')"
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
				if (req.body.Mode == 0) {
					var msg='Unable to delete announcement for the Temple' + req.session.TempleID;
				}
				else if (req.body.Mode == 1) {
					var msg='Unable to update announcement for the Temple' + req.session.TempleID;
				}

                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
				if (req.body.Mode == 0) {
					var msg='Announcement deleted in temple ' + req.session.TempleID;
				}
				else if (req.body.Mode == 1) {
					var msg='Announcement updated in temple ' + req.session.TempleID;				
				}
                var resp={ "data" : rows,message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);		
            }
        });
    });
};

exports.bookingNotificationCount=function(req,res){

    var method='API bookingNotificationCount';
	var sql="call admin_booking_notification_count('"+req.session.TempleID+"')";	
    logger.log('info','API Service Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                var msg='No new bookings found for the Temple' + req.session.TempleID;
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
                var msg='Booking Notification count generated';
                var resp={ "data" : rows[0],message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);		
            }
        });
    });

};

exports.approvalCount=function(req,res){

    var method='API approvalCount';
	var sql="call admin_approval_notification_count('"+req.session.TempleID+"')";	
    logger.log('info','API Service Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                var msg='No Puja requests found for the Temple' + req.session.TempleID;
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
                var msg='Puja requests pending count generated';
                var resp={ "data" : rows[0],message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);		
            }
        });
    });

};


exports.pujaPendingCount=function(req,res){

    var method='API pujaPendingCount';
	var sql="call admin_puja_pending_count('"+req.session.TempleID+"')";	
    logger.log('info','API Service Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                var msg='Error in db connection';
                var resp={ "data" : msg,message:err};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('error',err,0,file+'/'+method);
                return;
            }
            if (rows.length == 0)
            {
                var msg='No Puja pending for update found for the Temple' + req.session.TempleID;
                var resp={ "data" : msg,message:msg};
                res.end(JSON.stringify([{result:resp,status:false,token_auth:true,message:msg}]));
                console.log('warn','',0,file+'/'+method);
            }
            else
            {
                var msg='Puja pending for status update - count generated';
                var resp={ "data" : rows[0],message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);		
            }
        });
    });

};

exports.updatePujaStatus=function(req,res)
{
    var method='API admin_order_puja_status_change';
    var sql="call admin_order_puja_status_change('"+req.body.PujaStatus+"','"+req.body.TrackingNo+"','"+req.body.PujaID+"','"+req.body.PujaDate+"','"+req.body.DevoteeName+"','"+req.session.TempleID+"')"
console.log(sql);
    logger.log('info','Query :'+sql,req.session.UserID,file+'/'+method);
    db.getConnection(function(err, con){
        con.query(sql,function(err, rows, fields) {
            con.release();
            var results;
            if (err)
            {
                logger.log('error','Result :'+err,req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            else if (rows.length == 0)
            {
                logger.log('warn','Result: Updating Pujsa Status',req.session.UserID,file+'/'+method);
                res.end( '[{ "RESULT" : "0"}]');
                return;
            }
            else
            {
                results = JSON.stringify(rows);
                var TempleAddress=rows[0][0];
                var email_subject,email_text,email_message;

//    get booking details from body

                email_text=req.body.PujaStatusMessage;
                var DevoteeName=req.body.DevoteeName;
                var PujaName=req.body.PujaName;
                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                var d = new Date(req.body.PujaDate);
                var PujaDate= d.getDate()+'-'+monthNames[d.getMonth()] +'-'+d.getFullYear();

                var TrackingNo=req.body.TrackingNo;
//    end

//    get temple details from session

                var TempleName=req.session.TempleName;
                var TempleHouseName=TempleAddress.HouseName;
                var TempleStreetName=TempleAddress.StreetName;
                var TemplePostOffice=TempleAddress.PostOffice;
                var TempleDistrict=TempleAddress.District;
                var TempleState=TempleAddress.State;
                var TemplePostalCode=TempleAddress.PostalCode;
                var TemplePhoneMobile=TempleAddress.PhoneMobile;
                var TemplePhoneWired=TempleAddress.PhoneWired;
                var TempleCommunicationMailID=TempleAddress.Email;

//    end



                if(req.body.PujaStatus=='6')
                {
                    //DV-49
                    email_subject='Your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+' has been completed and postal delivery is pending' ;
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
//      puja completed and postal delivery is pending
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>postal delivery Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                        TempleName.toUpperCase()+
                        '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                        'PRASADAM COMPLETED POSTAGE PENDING <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                        TrackingNo+
                        '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                        'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Your puja '+PujaName+' for date '+PujaDate+' has been completed and postal delivery is pending.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p>' +
                        '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                        '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                        TempleName.toUpperCase() +' </span> Temple management</p></td></tr>'+
                    '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +
                        '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +

                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                        '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                        TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+'<br>' +
                        TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                        +'</h6></td></tr>' +
                        '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';

                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Pooja completed and  Postage Pending</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}    </style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2>    <tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td>    <table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr>    </table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Pooja completed and  Postage Pending</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>Your puja '+PujaName+' for date '+PujaDate+' has been completed and postal delivery is pending.</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic">you have opted for prasadam shipping by postal delivery,normally it takes up to "one week" from the date of puja.We will notify you about the shipping status by mail.</p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic">You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr>    </table>    </td></tr><tr><td>    <table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr>    </table></td></tr></table></td></tr>    <tr><td height="20"></td>    </tr>    <tr>    <td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;">    <p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a>    </p></td></tr><tr><td style="font-style: italic">    <ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li>    </ul></td></tr></table></td>    </tr></table> </div></body></html>'

                }
                else if(req.body.PujaStatus=='7')
                {
//      shipping completed
                    email_subject='postal delivery has been completed for your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+'' ;
                    //DV-49
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>postal delivery Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                        TempleName.toUpperCase()+
                        '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                        'PRASADAM SHIPMENT COMPLETED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                        TrackingNo+
                        '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                        'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Prasadam shipment completed for the  puja '+PujaName+' for date '+PujaDate+' booked by you.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p>' +
                        '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                        '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                        TempleName.toUpperCase() +' </span> Temple management</p></td></tr>'+
                        '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +

                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                        '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                        TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+'<br>' +
                        TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                        +'</h6></td></tr>' +
                        '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +
                        '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>'
                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Shipping Completed</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}</style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Shipping Completed</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>Prasadam shipment completed for the  puja '+PujaName+' for date '+PujaDate+' booked by you.</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic"></p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic">You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr></table></td></tr><tr><td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr></table></td></tr></table></td></tr><tr><td height="20"></td></tr><tr><td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;"><p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a></p></td></tr><tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li></ul></td></tr></table></td></tr></table> </div></body></html>'

                }
                else if(req.body.PujaStatus=='1')
                {
//      puja and if postal that also completed
                    email_subject='Your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+' has been completed ' ;
                    //DV-49
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>Puja Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                        TempleName.toUpperCase()+
                        '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                        'PUJA COMPLETED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                        TrackingNo+
                        '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                        'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Your puja '+PujaName+' for date '+PujaDate+' has been completed .</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'If you have opted for prasadam shipping by postal delivery,<br>normally it takes up to "one week" from the today onwards .<br>We will notify you about the shipping status by mail.</p>' +
                        '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                        '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                        TempleName.toUpperCase() +' </span> Temple management</p></td></tr>' +
                    '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +
                        '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +
                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                        '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                        TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+'<br>' +
                        TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                        +'</h6></td></tr>' +

                        '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>';



                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Pooja completed</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}    </style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2>    <tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td>    <table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr>    </table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Pooja completed</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p>Your puja '+PujaName+' for date '+PujaDate+' has been completed .</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic">if you have opted for prasadam shipping by postal delivery,normally it takes up to "one week" from the today onwards .We will notify you about the shipping status by mail.</p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic">You can also see the status of this booking in your “user area” on the <a href="http://www.devaayanam.com"> website</a> .</p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr>    </table>    </td></tr><tr><td>    <table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr>    </table></td></tr></table></td></tr>    <tr><td height="20"></td>    </tr>    <tr>    <td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;">    <p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a>    </p></td></tr><tr><td style="font-style: italic">    <ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li>    </ul></td></tr></table></td>    </tr></table> </div></body></html>'

                }
                else if(req.body.PujaStatus=='16')
                {
//      puja delayed
                    email_subject='Your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+' has been delayed ' ;
                    //DV-49
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!doctype html><html><head><meta charset="utf-8"><title>postal delivery Completed</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:700,300,600,400" rel="stylesheet" type="text/css"><link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"></head><body style="font-family: "Open Sans", sans-serif;"><section class="header" style="width:100%; height:auto;"><table style="margin:0px auto; border-collapse:collapse;"><tr style="width:650px; height:auto;  text-align:center;"><td><img src="http://www.devaayanam.in/images/background/header-img.jpg" style="width:667px;"/></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:left;"><td><h5 style="font-weight:600;font-size:22px;color:#6f1313;padding-left:25px;text-align:center; margin:0px; margin-top:10px; margin-bottom:8px;">'+
                        TempleName.toUpperCase()+
                        '</h5></td></tr><tr style="width:650px; height:auto;background-color:#eee5cb;  text-align:center;"><td></td></tr><tr style="width:650px; height:auto;background-image:url(http://devaayanam.in/images/background/emailbackground.jpg);  text-align:left;"><td><h4 style="font-weight:700;font-size:22px;color:#5dab6d;padding-left:25px;text-align:center; margin:0px; margin-top:-10px "><br/>' +
                        'PUJA DELAYED <hr style="width:10%; border:solid #C00 1px; margin-top:-.5px;"></h4><div class="button" style="border:2px solid #C00; width:200px; height:50px; text-align:center; margin-left:240px; margin-top:30px;"><p style="font-weight:600;font-size:16px;color:#2d2d2d;padding-top:13px;padding-left:5px;padding-right:5px; margin:0px;">Tracking No:<span style="font-weight:700; color:#000;">'+
                        TrackingNo+
                        '</span></p></div><h2 style="font-weight:600;font-size:18px;color:#b74b03;padding-left:25px;padding-bottom:0px;text-align:left;padding-top:10px;">'+
                        'Dear '+DevoteeName+',</h2><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'We regret to inform you that following Puja booked by you had to be delayed due to unforeseen reasons.</p><p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:8px;padding-left:70px;padding-right:25px;line-height:25px;"></p><table>' + '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        '<br><b>Puja :'+PujaName+' <br> Puja Date :'+PujaDate+'</b><br>  We can do the same puja for another day. For that please send the details (beneficiary name, star, puja date)<br> to this email id Please also mention this booking and puja id in that mail.<br>We will notify you by mail when the puja is completed .</p>' +
                        '<p style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'Please provide your tracking number for any future correspondences.</p>' + '<p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;">' +
                        'May <span style="color:#b74b03;">'+TempleName.toUpperCase()+
                        '</span> showers blessings in your life,</p><p style="font-weight:600;font-size:14px;color:#2d2d2d;padding-top:10px;padding-left:70px;padding-right:25px;line-height:25px;"><span style="color:#b74b03;">'+
                        TempleName.toUpperCase() +' </span> Temple management</p></td></tr>'+
                        '<tr style="width:100vw; height:auto;text-align:left;"><td style="width:650px; height:1vh;"></td></tr>' +

                        // '<tr style="width:650px; height:auto;text-align:left;"><td><img src="http://www.devaayanam.in/images/background/banner..jpg"/></td></tr>' +
                        '<tr style="width:650px;background-color:#f9f9f7;text-align:center;height:auto;"><td><hr style="margin:0px;"><h3 style="font-weight:600;font-size:16px;color:#000;padding-right:25px;line-height:25px; padding-bottom:0px; margin:0px;">CONTACT DETAILS:</h3><h6 style="font-weight:400;font-size:14px;color:#2d2d2d;padding-top:0px;padding-right:25px;line-height:25px; margin-top:-10px; margin:0px;">'+
                        TempleName.toUpperCase()+',<br>'+TempleHouseName.toLowerCase()+','+'<br>' +
                        TempleDistrict.toLowerCase()+',<br>'+TempleState.toLowerCase()+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID.toLowerCase()
                        +'</h6></td></tr>' +
                        '<tr style="font-size: 14px;text-align: center"><td>Devaayanam is a platform for temple website. For complete list visit <a href="www.devaayanam.in/list"><b>www.devaayanam.in/list</b></a> </td></tr>' +
                        '<tr style="width:650px;background-color:#CCC;text-align:center;height:auto;"><td><hr style="margin-top: 2px;"/><h6 style="font-size:14px;padding-top:10px;padding-bottom:10px;font-weight:300; color:#000;"><a href="www.facebook.com/devaayanam"><img style="width: 15px;height: 15px;" src="https://en.facebookbrand.com/wp-content/uploads/2016/05/FB-fLogo-Blue-broadcast-2.png"></a>&nbsp;<a href="devaayanamblog.blogspot.in"><img style="width: 15px;height: 15px;" src="http://www.devaayanam.in/images/background/blogger.png"></a> <br><a href="#" style="color:#000;">Terms and Conditions</a> |<a href="#" style="color:#000;">Privacy Policy</a> | <a href="#" style="color:#000;">Contact Us</a></h6></td></tr></table></div></section></body></html>'


                    // email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Pooja Delayed</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}</style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Pooja Delayed</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p> We regret to inform you that following Puja booked by you had to be delayed due to unforeseen reasons.<br><b>Puja :'+PujaName+' <br> Puja Date :'+PujaDate+'</b><br>  We can do the same puja for another day. For that please send the details (beneficiary name, star, puja date) to this email id Please also mention this booking and puja id in that mail.</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p> We will notify you by mail when the puja is completed </p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic"></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr></table></td></tr><tr><td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr></table></td></tr></table></td></tr><tr><td height="20"></td></tr><tr><td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;"><p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a></p></td></tr><tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li></ul></td></tr></table></td></tr></table> </div></body></html>'

                }
                else if(req.body.PujaStatus=='3')
                {
//      puja cancelled
                    email_subject='Your puja '+PujaName+' in '+TempleName+' for date '+PujaDate+' has been cancelled ' ;
                    //DV-49
                    var data={mobileno:req.body.mobile,message:email_subject}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                    email_message='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Devaayanam-Puja Cancelled</title><style type="text/css">body, .header h1, .header h2, p{margin: 0; padding: 0;}.top-message p, .bottom-message p {color: #3f4042; font-size: 12px; font-family: Arial, Helvetica, sans-serif;}.header h1{color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 24px;}.header h2{color: #444444; font-family: Arial,Helvetica,sans-serif;font-size: 12px}.header p {color: #000000; font-family: "Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; font-size: 12px;}h3 {font-size: 28px;color: #444444;font-family: Arial,Helvetica,sans-serif}h4 {font-size: 22px;color: #4A72Af;font-family: Arial,Helvetica,sans-serif}h5 {font-size: 18px;color: #444444;font-family: Arial,Helvetica,sans-serif;line-height: 1.5;}p {font-size: 12px;color: #444444;font-family:"Lucida Grande","Lucida Sans","Lucida Sans Unicode",sans-serif; line-height: 1.5;}/*li {}*/h1,h2,h3,h4,h5,h6 {margin: 0 0 0.8em 0;}.border{background-color: #F2F2F2;border-bottom: 2px solid #444444;border-top: 2px solid #444444;border-right: 2px solid #444444;border-left: 2px solid #444444;margin-top: 5px;margin-bottom: 5px;margin-left: 5px;margin-right: 5px;/*color: white;*/min-height: 11.693in;}</style></head><body><div class="border"><table width="100%" align="center" cellpadding="0" cellspacing="0" bgcolor=F2F2F2><tr><td><table class="main" cellpadding="0" cellspacing="10" bgcolor="F2F2F2" width="100%"  bgcolor="E7EDF1" style="background-color: #F2F2F2"><tr><td><table class="header" width="100%"><tr><td width="100%" bgcolor="967A90 " align="center" HEIGHT=50><h1 style="color: #ffffff">'+TempleName +'</h1></td></tr></table></td></tr><tr><td></td></tr><tr><td>  <table width="100%" class="header"><tr><td width="100%" align="center" bgcolor="" HEIGHT=50><h1>Puja Cancelled</h1></td></tr>  </table></td></tr></table></td>  <tr><tr><td><table class="main" cellpadding="0" cellspacing="10" width="95%" style="background-color: #F8D8DF;margin-left: 2.5%;"><tr><td>   <table bgcolor="F8D8DF" width="95%" class="header"><tr><td style="padding-top: 10px; padding-bottom: 0px;padding-left: 10px"><b>Dear '+DevoteeName+',</b></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p><b>Tracking No:'+TrackingNo+'</b></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-left: 30px"><p> We regret to inform you that following Puja booked by you had to be cancelled due to unforeseen reasons.<br><b>Puja :'+PujaName+' <br> Puja Date :'+PujaDate+'</b><br>  We are able to present you two options to proceed with this booking .</p></td></tr><tr><td width="95%" style="padding-top: 0px; padding-bottom: 0px;padding-left: 30px"><p style="font-style: italic">  <ul style="font-size: 12px;color: #000000;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 30px"><li style="">We can consider your booking amount towards donation to Temple . </li> <li> We can do the same puja for another day. For that please send the details (beneficiary name, star, puja date) to this   email idPlease also mention this booking and puja id in that mail.</li></ul></p></td></tr><tr><td width="95%" style="padding-left: 30px"><p style="font-style: italic"></p></td></tr><tr><td width="95%" style="padding-top: 10px; padding-bottom: 20px;padding-left: 30px"><p>Please provide your tracking number for any future correspondences.</p></td></tr></table></td></tr><tr><td><table bgcolor="F8D8DF" width="95%" class="header"><tr><td width="95%" style="padding-top: 10px; padding-bottom: 5px;padding-left: 30px"></td></tr><tr><td width="95%" style="padding-top: 20px; padding-bottom: 5px;padding-left: 30px"><p>May <b>'+TempleName+'</b> showers blessings  in your life,</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p>'+TempleName+' Temple management</p></td></tr><tr><td width="95%" style="padding-bottom: 5px;padding-left: 30px"><p><b>Contact Details: </b></p><address style="margin-left: 8%">'+TempleName+',<br>'+TempleHouseName+','+TempleStreetName+',<br>'+TemplePostOffice+'<br>'+TempleDistrict+',<br>'+TempleState+',<br>'+TemplePostalCode+',<br>'+TemplePhoneMobile+','+TemplePhoneWired+'.<br>'+TempleCommunicationMailID+'</address></td></tr></table></td></tr></table></td></tr><tr><td height="20"></td></tr><tr><td><table class="top-message" cellpadding="0" cellspacing="0" width="100%"><tr><td><img src="http://www.devaayanam.com/images/DevaayanamCompanyName.png" width= "200"height="40px" style="margin-left: 30px"></p></td></tr></table><table class="bottom-message" cellpadding="0" cellspacing="0" width="100%" align="center" bgcolor="F2F2F2"><tr><td align="center" style="padding-top: 10px;"><p><a href="http://www.devaayanam.com"> Terms and Conditions</a> | <a href="http://www.devaayanam.com">Privacy Policy </a>| <a href="http://www.devaayanam.com">Contact Us</a></p></td></tr><tr><td style="font-style: italic"><ul style="font-size: 12px;color: #2E75B5;font-family:Lucida Grande,Lucida Sans,Lucida Sans Unicode,sans-serif;margin-left: 20px"><li>Devaayanam is continuously looking for ways to improve service for devotees. Please do share with us at feedback@devaayanam.in.</li></ul></td></tr></table></td></tr></table> </div></body></html>'
                } else if(req.body.PujaStatus=='17') {
                    //DV-49
                    console.log(req.body);
                    var data={mobileno:req.body.mobile,message:'Your puja booking has been completed, Tracking No:'+req.body.TrackingNo}
                    emailtemplate.sendsms(data,function(res){});
                    //DV-49
                }

                //res.end( '[{ "RESULT" : "1"}]');
                //logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
			

                if((req.body.PujaStatus!='0')&&(req.body.PujaStatus!='2')&&(req.body.PujaStatus!='17')) {
                    var mailer = require('nodemailernew');
                    var smtpTransport = mailer.createTransport({
                        service: email_provider,
                        auth: {
                            user: email_username,
                            pass: email_password
                        }
                    });
					var fromAddress="'\"" + TempleName +  "\"" + " <" + email_username + ">'"; //DV178 Dynamic display names for emails										
                    var mailOptions = {
                        //from: email_username, // sender address
						from: fromAddress, // sender address							
                        to: req.body.Email, // list of receivers
                        subject:email_subject, // Subject line
                        text: email_text, // plaintext body
                        html: email_message // html body

                    }
                    smtpTransport.sendMail(mailOptions, function(error, response){
                        if(error){
                            logger.log('error','Result:'+error,req.session.UserID,file+'/'+method+'Email');
                        }
                        else{
                            logger.log('data','Result:'+response.message,req.session.UserID,file+'/'+method+'Email');
                        }
                    });
                }
                else
                {
                    logger.log('data','Result: Not need to send mail to customer',req.session.UserID,file+'/'+method);
                }
                //res.end( '[{ "RESULT" : "1"}]');
                //logger.log('data','Result:'+JSON.stringify(rows),req.session.UserID,file+'/'+method);
                var msg='Puja status updated successfully';
                var resp={ "Response" : "1",message:msg};
                res.end(JSON.stringify([{result:resp,status:true,token_auth:true,message:msg}]));
                console.log('info',results,0,file+'/'+method);
            }
        });

    });
};