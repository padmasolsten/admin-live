    var method='processSettlement';
    var stage="";
	var var_apiID = '12348ca589bd785fd8e40f3cb84321';
	var var_secretKey = '4239fd6b875664686a85eadb588ae64dd317d765';
	
	var db = require('../../db').pool;
	
	var checkstartdate = new Date();
	checkstartdate.setDate(checkstartdate.getDate() - 3);
	var checkstartdate= [checkstartdate.getFullYear(), ('0' + (checkstartdate.getMonth() + 1)).slice(-2), ('0' + checkstartdate.getDate()).slice(-2)].join('-');
	console.log(checkstartdate.toString());
	
	var checkenddate = new Date();
	checkenddate.setDate(checkenddate.getDate() - 2);
	var checkenddate= [checkenddate.getFullYear(), ('0' + (checkenddate.getMonth() + 1)).slice(-2), ('0' + checkenddate.getDate()).slice(-2)].join('-');
	console.log(checkenddate.toString());

	var var_startDate = checkstartdate;
    var var_endDate = checkenddate;
	
	var request=require('request');
	var options={
		appId:var_apiID,
		secretKey:var_secretKey,
		startDate:var_startDate,
		endDate:var_endDate
	}
	stage = 'Settlement API call options -' + JSON.stringify(options)
	console.log (stage);							
	var postForm={
		url:'https://ces-api.cashfree.com/api/v1/settlements',
		form:options
	}	
	request.post(postForm, function (err, settleresponse, settlebody) {
		console.log ('Settlement response -' + JSON.stringify(JSON.parse(settlebody)));								
		if (err) {
			var msg='Error in ' + stage;
			var resp={ "Error" : msg,message:err};
			console.log('error',err,0,file+'/'+method);
			res.end(JSON.stringify([{result:resp,message:msg}]));
			return;
		}
		console.log ('Settlements List -' + JSON.parse(settlebody).settlements);	
		var settlementsArr = JSON.parse(settlebody).settlements;
		if (settlementsArr.length > 0) {
			settlementsArr.forEach(function(settlementRec){
				stage = 'Get Transactions in Settlement Id - ' + settlementRec.id; 	console.log (stage);
				var options={
					appId:var_apiID,
					secretKey:var_secretKey,
					settlementId:settlementRec.id
				}
				var postForm={
					url:'https://ces-api.cashfree.com/api/v1/settlement',
					form:options
				}	
				request.post(postForm, function (err, orderresponse, orderbody) {
					console.log ('Order response -' +  JSON.stringify(JSON.parse(orderbody)));								
					if (err) {
						var msg='Error in ' + stage;
						var resp={ "Error" : msg,message:err};
						console.log('error',err,0,file+'/'+method);
						res.end(JSON.stringify([{result:resp,message:msg}]));
						return;
					}
					//console.log ('Order List - ' + JSON.parse(settlebody).transactions);	
					var orderArr = JSON.parse(orderbody).transactions;

					if (orderArr.length > 0) {
						orderArr.forEach(function(orderRec){
							//stage = 'Update Order as settled - ' + orderRec.orderId; 	console.log (stage);
							db.getConnection(function(err, con){
							var sql = "call `updateSettlementDetails`('"+orderRec.orderId+"',"+settlementRec.id+",'"+settlementRec.settledOn+"',"+orderRec.referenceId+");"
							console.log (sql);
							con.query("call `updateSettlementDetails`('"+orderRec.orderId+"',"+settlementRec.id+",'"+settlementRec.settledOn+"',"+orderRec.referenceId+");",function(err, rows, fields) {
								con.release();
								console.log (rows);
								if(!err) {
									if(rows.affectedRows == 1) {
										console.log('data','updateSettlementDetails Success !!',1,stage);
										//res.end('[{"result":"1"}]');
									}
									else {
										console.log('warn','updateSettlementDetails Failed !!',0,stage);
										//res.end('[{"result":"0"}]');
									}
								}
								else{
									console.log('error','Invalid Query !!',0,stage);
									//res.end('[{"result":"0"}]');
								}
							});
						});
						});
					}	
				});					
			});
		}	
	});	
/*
let schedule = require('node-schedule');
let rule = new schedule.RecurrenceRule();

// your timezone
rule.tz = 'Asia/Calcutta';

// runs at 15:00:00
rule.second = 0;
rule.minute = 0;
rule.hour = 11;

// schedule
schedule.scheduleJob(rule, function () {
  console.log('Hello World!');
});
*/
/*
	const nDate = new Date().toLocaleString('en-US', {
		timeZone: 'Asia/Calcutta'
	}); 
	const nDate = new Date();
console.log (nDate);

var schedule = require('node-schedule');
let rule = new schedule.RecurrenceRule();

// your timezone
//rule.tz = 'Asia/Calcutta';

// runs at 15:00:00
//rule.second = 0;
rule.minute = 17;
//rule.hour = 11;

var sss = schedule.scheduleJob(rule, function() {
	console.log('Start Auto Settlement update.....');
	//exports.processSettlement();
	console.log('End Auto Settlement update.....');		  
});
*/