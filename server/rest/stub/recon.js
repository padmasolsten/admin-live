    var method='processSettlement';
    var stage="";

	var db = require('../../db').pool;
	var request=require('request');

	db.getConnection(function(err, con){
		stage = "call `processOrderSettlement`();"
		var sql = "call `processOrderSettlement`();"
		console.log (sql);
		con.query(sql,function(err, rows, fields) {
		con.release();
		var orderArr = rows[0];
		console.log ('To Process - ' + orderArr.length);
		if (err) {
			var msg='Error in ' + stage;
			var resp={ "Error" : msg,message:err};
			console.log('error',err,0,file+'/'+method);
		}
		if (orderArr.length > 0) {
			orderArr.forEach(function(settlementRec){
				stage = 'Get Settlement Statius for Order Id - ' + settlementRec.GatewayTransactionID; 	console.log (stage);								
				var options = {
				  'method': 'POST',
				  'url': 'https://ces-api.cashfree.com/ces/v1/authorize',
				  'headers': {
					'X-Client-Id': 'CF12348XI38PD42EPEUAMA',
					'X-Client-Secret': '3c140394c494abd5ad7f4c5fc2d85dae2a4a77aa'
				  }
				};
				request(options, function (err, response, body) {
					//console.log ('Authorise call response - ' + JSON.stringify(JSON.parse(body)));					
					if ((JSON.parse(body).status) == 'SUCCESS') {
						var tokenDetails = JSON.parse(body).data;
						console.log ('Token -' +  tokenDetails.token);							
					
						var orderURL = 'https://ces-api.cashfree.com/ces/v1/getOrderSettlementStatus/'+ settlementRec.GatewayTransactionID;
						var optionsGet = {
						  'method': 'GET',
						  'url': orderURL,
						  'headers': {
							'Authorization': 'Bearer ' + tokenDetails.token,
							'Content-Type': 'application/x-www-form-urlencoded'
						  },
						  form: {
							'Authorization': tokenDetails.token
						  }
						};
						console.log ('Status call ' + optionsGet);
						request(optionsGet, function (err, response, body) {
							console.log ('Order response -' +  JSON.stringify(JSON.parse(body).vendorTransfers));															
							if (err) {
								var msg='Error in ' + stage;
								var resp={ "Error" : msg,message:err};
								console.log('error',err,0,file+'/'+method);
							}
							else {
								var transferArr = JSON.parse(body).vendorTransfers;
								var trasnferStatus = JSON.parse(body).settlementStatus;								
								console.log ('Transfer status, details ' + trasnferStatus + ', ' + JSON.stringify(transferArr));
								
								var vendorArr = [];
								if (transferArr != undefined) {
									vendorArr = transferArr.filter(function (e) {
										return e.vendorId == settlementRec.vendorId;
									});									
									console.log ('Vendor details ' + JSON.stringify(vendorArr));
								}	
								var var_addedon = '';
								var var_utr = '0';
								if (vendorArr != undefined) {
									vendorArr.forEach(function(transferRec){
										var_addedon = transferRec.addedOn;
										var_utr = transferRec.utr;
									})	
								}
								db.getConnection(function(err, con){
									var sql = "call `updateSettlementDetails`('"+settlementRec.GatewayTransactionID+"','"+settlementRec.vendorId+"','"+
													var_addedon+"','"+var_utr+"','"+trasnferStatus+"');"
									console.log (sql);
									con.query(sql,function(err, rows, fields) {
										con.release();
										console.log (rows);
										if(!err) {
											if(rows.affectedRows == 1) {
												console.log('data','updateSettlementDetails Success !!',1,stage);
											}
											else {
												console.log('warn','updateSettlementDetails Failed !!',0,stage);
											}
										}
										else{
											console.log('error','Invalid Query !!',0,stage);
										}
									});
								});
							}
						});		
					}
				});
			});	
		}
	});	
	});
