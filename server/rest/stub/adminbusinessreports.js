try {
var db = require('../db').pool;

// exceljs package
const Excel = require('exceljs');

var fromDate = '2020-02-01'
var toDate = '2020-02-10'
var timestamp = (new Date()).getHours() + (new Date()).getMinutes() + (new Date()).getSeconds(); 

const xlfilepathname = '../../xlreports/DvnmBusiness_' + fromDate + '_to_' + toDate + '_' + timestamp + '.xlsx';
console.log ('Filename ' + xlfilepathname);

var workbook = new Excel.Workbook();

// access the excel sheet
var revenueTotalSheet = workbook.addWorksheet('Revenue_Total');	
var newUsersSheet = workbook.addWorksheet('New_Users');
var userPujaSheet = workbook.addWorksheet('User_Puja');
var pujaTotalSheet = workbook.addWorksheet('Puja_Total');
var templeUsageSheet = workbook.addWorksheet('Temple_Usage');
writeBusinessReport();

function writeBusinessReport() {
	var readRevenueTotal = workbook.getWorksheet('Revenue_Total');
	writeRevenueTotal(workbook,readRevenueTotal);
	
	var readUserStatistics = workbook.getWorksheet('New_Users');
	writeUserStatistics(workbook,readUserStatistics);

	var readUserPujaSheet = workbook.getWorksheet('User_Puja');
	writeUserPuja(workbook,readUserPujaSheet);

	var readPujaTotalSheet = workbook.getWorksheet('Puja_Total');
	writePujaTotal(workbook,readPujaTotalSheet);

	var readtempleUsageSheet = workbook.getWorksheet('Temple_Usage');
	writeTempleUsage(workbook,readtempleUsageSheet);

	res.sendFile(xlfilepathname, function(err){
              console.log('---------- error downloading file: ' + err);
    });
}

function writeRevenueTotal(workbook,worksheet){
	//console.log('addRowsToExcelSheet...' );
	db.getConnection(function(err, con){
		var getdata=" select t.TempleName as 'TempleName', sum(bd.pujaqty) as 'Pujas', Sum(pujaamount)  as 'Revenue', sum(Commission) as 'ServiceCharge' " + 
					" from paymentgatewayorder pg, temple t, booking bk, (select trackingno, sum(quantity) pujaqty, Sum(quantity * rate) pujaamount from bookingpujadetails group by TrackingNo) bd " + 	
					" where pg.TempleID=t.TempleID and t.LanguageID=1 " + 
					" and bk.bookingdate between '" + fromDate + "' and '" + toDate + "'" +
					" and PGStatus = 'S' and pg.trackingno = bk.TrackingNo " + 	
					" and bk.trackingno = bd.trackingno " +
					" group by t.TempleName";
		console.log("Write Temple Revenue..sql - "+getdata)
		con.query(getdata,function(err, rows, fields) {
			con.release();
			if (err) {
				console.log('error', err, 'writeRevenueTotal/adminbusinessreports.js');
			}
			else if (rows.length != 0) {		
				var headerrow = [];			
				headerrow[1]='TempleName';       
				headerrow[2]='#Pujas';
				headerrow[3]='Revenue';
				headerrow[4]='ServiceCharge';
				worksheet.addRow(headerrow);

				var totalPuja = 0;
				var totalRevenue = 0;
				var totalServiceCharge = 0;
				rows.forEach(function(xlrow){			
					var rowValues = [];
					rowValues[1]=xlrow.TempleName;       
					rowValues[2]=xlrow.Pujas;
					rowValues[3]=xlrow.Revenue;
					rowValues[4]=xlrow.ServiceCharge;
					totalPuja = totalPuja + xlrow.Pujas;
					totalRevenue = totalRevenue + xlrow.Revenue;
					totalServiceCharge = totalServiceCharge + xlrow.ServiceCharge;

					worksheet.addRow(rowValues);
				})
				
				var totalrow = [];			
				totalrow[1]='Total';       
				totalrow[2]=totalPuja;
				totalrow[3]=totalRevenue;
				totalrow[4]=totalServiceCharge;
				worksheet.addRow(totalrow);
				
				worksheet.getColumn(1).width = 50;
				worksheet.getColumn(2).width = 15;
				worksheet.getColumn(3).width = 15;
				worksheet.getColumn(4).width = 15;
				worksheet.getCell('A1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('B1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('C1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('D1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};

				workbook.xlsx.writeFile(xlfilepathname).then(() => {
				console.log("Write Temple Revenue Complete");
				callback(null);
				});	
			}
			});
	});
}

function writeUserStatistics(workbook,worksheet){
	//console.log('addRowsToExcelSheet...' );
	writeNewUsers (workbook,worksheet);
	writeNewGuests (workbook,worksheet);
}

function writeNewUsers(workbook,worksheet){
	//console.log('addRowsToExcelSheet...' );
	db.getConnection(function(err, con){
		var getdata='select FirstName, LastName, LoginID from customer where (Created_Datetime) between ' +
						" '" + fromDate + "' AND '" + toDate + "'";
		console.log("Write New Users..sql - " + getdata)
		con.query(getdata,function(err, rows, fields) {
			con.release();
			if (err) {
				console.log('error', err, 'writeNewUsers/adminbusinessreports.js');
			}
			else if (rows.length != 0) {		
				var headerrow = [];			
				headerrow[1]='FirstName';       
				headerrow[2]='LastName';
				headerrow[3]='LoginID/Email';
				worksheet.addRow(headerrow);
				

				var totalUsers = 0;
				rows.forEach(function(xlrow){			
					// Add a row by sparse Array (assign to columns)
					var rowValues = [];
					rowValues[1]=xlrow.FirstName;       
					rowValues[2]=xlrow.LastName;
					rowValues[3]=xlrow.LoginID;

					totalUsers = totalUsers + 1;

					worksheet.addRow(rowValues);
				})
				
				var totalrow = [];			
				totalrow[1]='Total #New Users';       
				totalrow[2]=totalUsers;
				worksheet.addRow(totalrow);
				
				worksheet.getCell('A1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('B1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('C1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};

				
				workbook.xlsx.writeFile(xlfilepathname).then(() => {
				console.log("Write New Users Complete");
				callback(null);
				});	
			}
			});
	});
}
function writeNewGuests(workbook,worksheet){
	//console.log('addRowsToExcelSheet...' );
	db.getConnection(function(err, con){
		var getdata="select addr.FirstName, addr.LastName, addr.Email, 'guest'" + 
						" from bookingaddress  addr, sessiontrackingdetails st, " +
						" (select bp.trackingno, sum(bp.quantity) qty, sum(bp.rate) pujaamount " +
						" from bookingpujadetails bp " +
						" group by bp.TrackingNo) tmp " +
						" where addr.type in ('guest') " + 
						" and addr.TrackingNo = st.sessionid " +
						" and st.trackingno in  (select trackingno from booking where bookingdate between '" + fromDate + "' AND '" + toDate + "')" + 
						" and st.trackingno = tmp.trackingno " +
						" group by addr.FirstName, addr.LastName, addr.Email";
		console.log("Write Guest Users..sql - " + getdata)
		con.query(getdata,function(err, rows, fields) {
			con.release();
			if (err) {
				console.log('error', err, 'writeNewGuests/adminbusinessreports.js');
			}
			else if (rows.length != 0) {		
				var headerrow = [];			
				headerrow[1]='FirstName';       
				headerrow[2]='LastName';
				headerrow[3]='LoginID/Email';
				worksheet.addRow(headerrow);

				var totalUsers = 0;
				rows.forEach(function(xlrow){			
					// Add a row by sparse Array (assign to columns)
					var rowValues = [];
					rowValues[1]=xlrow.FirstName;       
					rowValues[2]=xlrow.LastName;
					rowValues[3]=xlrow.Email;

					totalUsers = totalUsers + 1;

					worksheet.addRow(rowValues);
				})
				
				var totalrow = [];			
				totalrow[1]='Total #Guests';       
				totalrow[2]=totalUsers;
				worksheet.addRow(totalrow);

				worksheet.getColumn(1).width = 20;
				worksheet.getColumn(2).width = 20;
				worksheet.getColumn(3).width = 40;

				workbook.xlsx.writeFile(xlfilepathname).then(() => {
				console.log("Write Guests Complete");
				callback(null);
				});	
			}
			});
	});
}

function writeUserPuja(workbook,worksheet){
	//console.log('addRowsToExcelSheet...' );
	db.getConnection(function(err, con){
		var getdata=" select addr.FirstName, addr.LastName, addr.Email, sum(tmp.qty) countquantity, sum(tmp.pujaamount) sumamount, count(tmp.TrackingNo) countbooking" +
						" from bookingaddress  addr, sessiontrackingdetails st, " +
						" (select bp.trackingno, sum(bp.quantity) qty,  Sum(bp.rate * bp.quantity) pujaamount " +
						" from bookingpujadetails bp " +
						" group by bp.TrackingNo) tmp " +
					" where addr.type in ('customer','guest') " +
					" and addr.TrackingNo = st.sessionid " +
					" and st.trackingno in (select trackingno from booking where bookingdate between '" + fromDate + "' AND '" + toDate + "')" + 
					" and st.trackingno = tmp.trackingno " +
					" and st.trackingno in (select TrackingNo from paymentgatewayorder where pgstatus = 'S' ) " +
					" group by addr.FirstName, addr.LastName, addr.Email";
		console.log("Write User Puja Statistics..sql - "+getdata)
		con.query(getdata,function(err, rows, fields) {
			con.release();
			if (err) {
				console.log('error', err, 'writeUserPuja/adminbusinessreports.js');
			}
			else if (rows.length != 0) {		
				var headerrow = [];			
				headerrow[1]='FirstName';       
				headerrow[2]='LastNameLastName';
				headerrow[3]='Email';
				headerrow[4]='#Puja';
				headerrow[5]='Total Amount';
				headerrow[6]='# Booking';
				worksheet.addRow(headerrow);

				var countQty = 0;
				var totalRevenue = 0;
				var countBooking = 0;
				rows.forEach(function(xlrow){			
					var rowValues = [];
					rowValues[1]=xlrow.FirstName;       
					rowValues[2]=xlrow.LastName;
					rowValues[3]=xlrow.Email;
					rowValues[4]=xlrow.countquantity;
					rowValues[5]=xlrow.sumamount;
					rowValues[6]=xlrow.countbooking;

					countQty = countQty + xlrow.countquantity;
					totalRevenue = totalRevenue + xlrow.sumamount;
					countBooking = countBooking + xlrow.countbooking;

					worksheet.addRow(rowValues);
				})
				var totalrow = [];		
				totalrow[3]= "Total";  				
				totalrow[4]= countQty;  
				totalrow[5]= totalRevenue;
				totalrow[6]= countBooking; 
				worksheet.addRow(totalrow);
				
				worksheet.getColumn(1).width = 25;
				worksheet.getColumn(2).width = 25;
				worksheet.getColumn(3).width = 30;
				worksheet.getColumn(4).width = 15;
				worksheet.getColumn(5).width = 15;
				worksheet.getColumn(6).width = 15;
				worksheet.getCell('A1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('B1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('C1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('D1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('E1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('F1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				
				
				workbook.xlsx.writeFile(xlfilepathname).then(() => {
				console.log("Write User Puja Statistics Complete");
				callback(null);
				});	
			}
			});
	});
}

function writePujaTotal(workbook,worksheet){
	//console.log('addRowsToExcelSheet...' );
	db.getConnection(function(err, con){
		var getdata="select p.PujaName as PujaName, sum(b.quantity) as PujaTotal from bookingpujadetails b, puja p " +
					" where b.pujadate BETWEEN '" + fromDate + "' AND '" + toDate + "'" +
					" and b.TempleID = p.TempleID " +
					" and b.PujaID = p.PujaID " +
					" and b.PujaSubTypeID = p.PujaTypeID " +
					" group by p.PujaName ";
		console.log("Write Puja Totals..sql - " + getdata)
		con.query(getdata,function(err, rows, fields) {
			con.release();
			//console.log ('Puja total data -'+ JSON.stringify (rows));
			if (err) {
				console.log('error', err, 'writePujaTotal/adminbusinessreports.js');
			}
			else if (rows.length != 0) {		
				var headerrow = [];			
				headerrow[1]='PujaName';       
				headerrow[2]='Total';
				worksheet.addRow(headerrow);

				rows.forEach(function(xlrow){			
					// Add a row by sparse Array (assign to columns)
					var rowValues = [];
					rowValues[1]=xlrow.PujaName;       
					rowValues[2]=xlrow.PujaTotal;

					worksheet.addRow(rowValues);
				})
				
				worksheet.getColumn(1).width = 30;
				worksheet.getColumn(2).width = 15;
				worksheet.getCell('A1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('B1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				
				workbook.xlsx.writeFile(xlfilepathname).then(() => {
				console.log("Write Puja Totals Complete");
				callback(null);
				});	
			}
			});
	});
}


function writeTempleUsage(workbook,worksheet){
	//console.log('addRowsToExcelSheet...' );
	db.getConnection(function(err, con){
		var getdata="select t.templename as TempleName, b.lastbookedon as lastbookingdate, bpuja.lastpujaon as lastpujadate, pendingpuja pendingupdates" +
					" from temple t, " +
						" (select templeid, max(bookingdate) lastbookedon from booking group by templeid) b, " +
						" (select templeid, max(pujadate) lastpujaon from bookingpujadetails group by templeid) bpuja, " +
						" (Select b.templeid, count(*) pendingpuja from bookingpujadetails d, booking b " +
								" where d.PujaStatus Not In (1, 3, 7)" +
								" and d.pujadate < now() " +
								" and b.trackingno = d.trackingno " +
								" and b.Status = '0' " +
								" group by b.templeid) pendingupd " +
						" where t.languageid = 1 and t.templeid not in (0,59) and t.status = 1 " +
						" and t.templeid = b.TempleID " +
						" and t.templeid = bpuja.templeid " +
						" and t.templeid = pendingupd.templeid ";
		console.log("Write Temple Usage..sql - " + getdata)
		con.query(getdata,function(err, rows, fields) {
			con.release();
			if (err) {
				console.log('error', err, 'writeTempleUsage/adminbusinessreports.js');
			}
			else if (rows.length != 0) {		
				var headerrow = [];			
				headerrow[1]='TempleName';       
				headerrow[2]='Last Booking Date';
				headerrow[3]='Last Puja Date';
				headerrow[4]='#Pending Updates';
				worksheet.addRow(headerrow);

				rows.forEach(function(xlrow){			
					// Add a row by sparse Array (assign to columns)
					var rowValues = [];
					rowValues[1]=xlrow.TempleName;       
					rowValues[2]=xlrow.lastbookingdate;
					rowValues[3]=xlrow.lastpujadate;
					rowValues[4]=xlrow.pendingupdates;
					worksheet.addRow(rowValues);
				})
				
				worksheet.getColumn(1).width = 50;
				worksheet.getColumn(2).width = 20;
				worksheet.getColumn(3).width = 20;
				worksheet.getColumn(4).width = 20;	
				
				worksheet.getCell('A1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('B1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('C1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};
				worksheet.getCell('D1').fill = {type: 'pattern', pattern: 'solid', fgColor:{argb:'cccccc'}};

				
				workbook.xlsx.writeFile(xlfilepathname).then(() => {
				console.log("Write  Temple Usage Complete");
				callback(null);
				});	
			}
			});
	});
}
} catch(err) {
        console.log('adminbusinessreports.js this is the error: ' + err);
}