'use strict';
var session = require('../rest/Session');
var CommonFunctions = require('../rest/CommonFunctions');
var DashBoard= require('../rest/Dashboard.js');
var masterBox= require('../rest/Masterbox.js');
var onboard=require('../rest/TempleOnboarding.js');

//admin
var Admin_Calendar=require('../rest/Admin_Calendar.js');
var Admin=require('../rest/Admin');
var Admin_Donation=require('../rest/Admin_Donation');
var Admin_NewUser=require('../rest/Admin_NewUser');
var Admin_Puja=require('../rest/Admin_Puja.js');
var Admin_Deity=require('../rest/Admin_Deity.js');
var Admin_PujaRule=require('../rest/Admin_PujaRule.js');
var Admin_TempleSettings=require('../rest/Admin_TempleSettings.js');
var Admin_History=require('../rest/Admin_History.js');
var Admin_TempleContact=require('../rest/Admin_TempleContact.js');

//DV197 Business Report
var Admin_Business_Report=require('../rest/Admin_Business_Report.js');

//Cashfree payment reconciliation
var cfSettlementUpdate=require('../rest/cfSettlementReconciliation.js');

//api services
var api=require('../rest/API_Services.js');

module.exports = function(app)
{
//    generate session id function
    app.post('/create_session' , session.create_session);
    app.post('/SetSession' , session.SetSession);

    //Admin Routes Starts Here


    app.post('/admin/login', Admin.Login);
    app.get('/admin/logout', Admin.logout);



//    Admin New User Routes Starts Here
    app.post('/admin/user/check',Admin_NewUser.UsernameAvailability);
    app.post('/admin/user/add',Admin_NewUser.AddUserAdmin);
    app.post('/admin/user/types',Admin_NewUser.LoadAccessType);


    app.post('/admin/management/loadusers',Admin_TempleContact.loadusers);
    app.post('/admin/management/add',Admin_TempleContact.AddContact);
    app.post('/admin/management/designation',Admin_TempleContact.LoadDesignation);
    app.post('/admin/management/listcontact',Admin_TempleContact.loadcontacts);


//    Admin New User Ends Here
//------------------------------------------------------------------------------------------------
//    Admin Donation Routes Starts Here

    app.post('/admin/donation/load',Admin_Donation.LoadDonation);
    app.post('/admin/donation/add',Admin_Donation.AddDonation);
    app.post('/admin/donation/language',Admin_Donation.LoadDonationByLanguage);
    app.post('/admin/donation/update',Admin_Donation.DonationUpdate);

//    Admin Donation Routes Ends Here
// -------------------------------------------------------------------------------------------------
//    Admin Puja Routes Starts Here

    app.post('/admin/puja/load',Admin_Puja.LoadPuja);
    app.post('/admin/puja/language',Admin_Puja.LoadPujaByLanguage);
    app.post('/admin/puja/add',Admin_Puja.AddPuja);
    app.post('/admin/puja/update',Admin_Puja.PujaUpdate);
    app.post('/admin/puja/sub/add',Admin_Puja.AddSubPuja);
    app.post('/admin/puja/check/update',Admin_Puja.puja_check_update);
    app.post('/admin/puja/types',Admin_Puja.LoadPujaType);

    app.post('/admin/PujaPackages',Admin_Puja.manage_puja_packages);

    app.post('/admin/order/puja/pending' , Admin_Puja.LoadIncompletedPuja);
    app.post('/admin/order/puja/status' , Admin_Puja.ChangePujaStatus);

    app.post('/admin/order/puja/request' , Admin_Puja.LoadPujaApproval);
    app.post('/admin/order/puja/approve' , Admin_Puja.ApprovalAction);


//    Admin donation Routes Ends Here

    app.post('/admin/dashboard/load' , DashBoard.loadDashboard);

    app.post('/admin/order/puja/report' , DashBoard.GetPujaReport);
    app.post('/admin/order/booking/report' , DashBoard.GetBookingReport);
    app.post('/admin/order/revenue/report' , DashBoard.GetRevenueReport);
    app.post('/admin/order/payment/report' , DashBoard.GetPaymentReport);
    app.post('/admin/order/approval/report' , DashBoard.GetApprovalReport);
    app.post('/admin/order/donation/report' , DashBoard.GetDonationReport);
    app.post('/admin/order/postaladdress/report' , DashBoard.GetPostalAddressReport);
	app.post('/admin/order/postal/complete' , Admin_Puja.PostalAction); //DV161 - Postal status update
    app.post('/admin/getdevoteelist' , DashBoard.getdevoteelist);
    app.post('/admin/getreviews' , DashBoard.getreviews);
    app.post('/admin/updatereview' , DashBoard.updatereview);
    app.post('/admin/addreview' , DashBoard.addreview);
    app.post('/admin/sendnotification' , DashBoard.sendnotification);
    app.post('/admin/updatedevotee' , DashBoard.updatedevotee);
    app.post('/admin/loaddashboard' , DashBoard.loaddashboard);
//    Admin Puja Routes Ends Here
// ----------------------------------------------------------------------------------------------------
//    Admin Deity Routes Strats Here

    app.post('/admin/deity/add',Admin_Deity.AddDeity);
    app.post('/admin/deity/load',Admin_Deity.LoadDeity);
    app.post('/admin/deity/remove',Admin_Deity.RemoveDeity);
    app.post('/admin/master/addpuja',masterBox.addmasterpuja);
    app.post('/admin/master/deletepuja',masterBox.deletemasterpuja);
    app.post('/admin/master/updatepuja',masterBox.updatepuja);


//    Admin Deity Routes Ends Here
//  ---------------------------------------------------------------------------------------------------
//    Admin Calendar Routes Starts Here


    app.post('/admin/festival/add',Admin_Calendar.AddFestival);
    app.post('/admin/event/add/sub',Admin_Calendar.AddSubEvent);

    app.post('/admin/announcement/add',Admin_Calendar.AddAnnouncement);
    app.post('/admin/announcement/load',Admin_Calendar.LoadAnnouncement);
    app.post('/admin/announcement/language',Admin_Calendar.LoadAnnouncementByLanguage);
    app.post('/admin/announcement/update',Admin_Calendar.UpdateAnnouncement);


    app.post('/admin/event/add',Admin_Calendar.AddEvent);
    app.post('/admin/event/load',Admin_Calendar.LoadEvent);
    app.post('/admin/event/language',Admin_Calendar.LoadEventByLanguage);
    app.post('/admin/event/update',Admin_Calendar.UpdateEvent);
    app.post('/admin/generaltab',Admin_History.generaltab);
    //DV-41-SAIF
    //Messages for users in payment page
    app.post('/admin/manage_pg_messages',Admin_History.manage_pg_messages);

    //Admin Calendar Routes Ends Here
//  ---------------------------------------------------------------------------------------------------

    app.post('/admin/about/add',Admin_History.AddHistory);
    app.post('/admin/about/load',Admin_History.LoadTempleHistory);
    app.post('/admin/about/update',Admin_History.UpdateTempleHistory);

    app.post('/api/temple/imageupload',Admin_History.imageUpload);
    app.post('/admin/image/upload',Admin_History.imageUpload);
    app.post('/admin/image/add',Admin_History.SaveImage);
    app.post('/admin/image/load',Admin_History.LoadImage);
    app.post('/admin/image/update',Admin_History.UpdateImage);

    app.post('/admin/rule/add',Admin_History.AddTempleRule);
    app.post('/admin/rule/load',Admin_History.LoadTempleRule);
    app.post('/admin/rule/update',Admin_History.UpdateTempleRule);

    app.post('/admin/timings/add',Admin_History.AddTempleTimings);
    app.post('/admin/timings/load',Admin_History.LoadTempleTimings);
    app.post('/admin/timings/update',Admin_History.UpdateTempleTimings);

    app.post('/admin/shippingcharge/add',Admin_History.addshippingcharge);
    app.post('/admin/shippingcharge/load',Admin_History.loadshippingcharge);
    app.post('/admin/shippingcharge/update',Admin_History.updateshippingcharge);
//  ---------------------------------------------------------------------------------------------------
//Puja Rule


    app.post('/admin/puja/load',Admin_PujaRule.LoadPuja);
    app.post('/api/admin/AddPujaRule',Admin_PujaRule.AddPujaRule);
    app.post('/api/admin/GetPujaRuleDetails',Admin_PujaRule.GetPujaRuleDetails);
    app.post('/api/admin/PujaRuleUpdate',Admin_PujaRule.PujaRuleUpdate);
    app.post('/api/admin/Block',Admin_PujaRule.Block);
    app.post('/api/admin/Exclusion',Admin_PujaRule.Exclusion);
    app.post('/api/admin/ExclusionUpdate',Admin_PujaRule.ExclusionUpdate);
    app.post('/api/admin/GetExclusionDetails',Admin_PujaRule.GetExclusionDetails);

    app.post('/api/admin/AddSettings',Admin_TempleSettings.AddSettings);
    app.post('/api/admin/LoadKey',Admin_TempleSettings.LoadKey);
    app.post('/api/admin/GetKeyValue',Admin_TempleSettings.GetKeyValue);
    app.post('/api/admin/UpdateSettings',Admin_TempleSettings.UpdateSettings);



//  ---------------------------------------------------------------------------------------------------


//    CommonFunctions Routes Starts Here
    app.get('/languages',CommonFunctions.LoadLanguages);
    app.post('/stars',CommonFunctions.LoadStars);
    app.post('/admin/shipping/load',CommonFunctions.LoadShippingOptions);





// master box loading routes

    app.post('/admin/master/puja',masterBox.loadPuja);
    app.post('/admin/master/donation',masterBox.loadDonation);
    app.post('/admin/master/deity',masterBox.loadDeity);
    app.post('/admin/master/onboard',onboard.onboardtemple);
    app.post('/admin/master/generalcall',onboard.generalcall);
    app.post('/api/Temple/ListTemples',masterBox.LoadTemples);
    app.post('/api/Temple/Listtransactions',masterBox.loadtransactions);
    app.post('/api/Temple/sendsettlementdata',masterBox.sendsettlementdata);
    app.post('/api/Temple/updatesettlmentstatus',masterBox.updatesettlmentstatus);

    app.get('/api/transferAmount',masterBox.autoTransferAmount);
    app.get('/api/checkTransferStatus',masterBox.transferStatus);

    app.post('/api/Temple/GetPujaList',masterBox.getpujalist);
    app.post('/api/Temple/sendsms',masterBox.sendreminder);
    app.post('/api/qwerty/sendremaindertoalltemple',masterBox.sendremindertoall);

	// DV98 -- Temple Puja Links ----		
    app.post('/admin/general/templepujalink',Admin_History.loadTemplePujaLinks);
	app.post('/admin/general/updatetemplepujalink',Admin_History.updateTemplePujaLink);	
	app.post('/admin/general/createtemplepujalink',Admin_History.createTemplePujaLink);		
	app.post('/admin/general/listtemples',Admin_History.listTemples);	
	app.post('/admin/general/listtemplepujas',Admin_History.listTemplePujas);	

	//DV197 Business Report
	app.post('/admin/dvnmamdin/getBusinessReport',Admin_Business_Report.getBusinessReport);		
	
	app.post('/api/dvnmamdin/updateOrderSettlement',cfSettlementUpdate.processSettlement);			
	
  //App API Services Start
    app.post('/api/admin/login', api.Login);
    app.get('/api/admin/logout', api.logout);

    //JWT API Services Start
    app.all('/dvnmapi/*', api.validate_request);
    app.post('/dvnmapi/admin/pujareport', api.get_puja_report);
    app.post('/dvnmapi/admin/loaddashboard', api.loadDashboard);	
	app.post('/dvnmapi/admin/bookingreport' , api.getBookingReport);
	app.post('/dvnmapi/admin/approvallist' , api.getApprovalList);
	app.post('/dvnmapi/admin/pujaapproval' , api.pujaApprovalAction);	
	app.post('/dvnmapi/admin/statusupdatelist' , api.getStatusUpdateList);
	app.post('/dvnmapi/admin/updatepujsastatus' , api.updatePujaStatus);		
	app.post('/dvnmapi/admin/getannouncements' , api.getAnnouncements);	
	app.post('/dvnmapi/admin/postannouncement' , api.postAnnouncement);		
	app.post('/dvnmapi/admin/deleteannouncement' , api.deleteAnnouncement);			
	app.post('/dvnmapi/admin/bookingNotificationCount' , api.bookingNotificationCount);				
	app.post('/dvnmapi/admin/approvalPendingCount' , api.approvalCount);					
	app.post('/dvnmapi/admin/pujaUpdatePendingCount' , api.pujaPendingCount);						
};