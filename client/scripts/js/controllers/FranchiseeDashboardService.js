DevRoute.factory('FranchiseeDashboardService',function($http,$location,$rootScope,logger,$window) {
    return {
        groupreport: function($scope,pujadata,type){
            if (type=='1'){
                $scope.pujareportgroup = _.groupBy(pujadata, function (Element){
                    return Element.TempleName;
                });
                $scope.pujareport = pujadata;
            }
            else{
                $scope.pujareportgroup = _.groupBy(pujadata, function (Element){
                    return Element.PujaDate;
                });
                $scope.pujareport = pujadata;
            }
        },
        sendsms: function(formdata,doCallback) {
            $http.post('/api/Temple/sendsms',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        getpujalist: function ($scope,type,rptdates) {
            var data = {"templeid": "0", "fdate":rptdates.from, "tdate":rptdates.to};
            $http.post('/api/Temple/GetPujaList', data).success(function (response) {
                if (response[0].RESULT != 'NODATA' && response[0].RESULT != 'ERROR') {
                    //response.forEach(function(row){
                    //    row.PujaDate=new Date(row.PujaDate);
                    //})
                    if (type=='1'){
                        $scope.pujareportgroup = _.groupBy(response, function (Element){
                            return Element.TempleName;
                        });
                        $scope.pujareport = response;
                    }
                    else{
                        $scope.pujareportgroup = _.groupBy(response, function (Element){
                            return Element.PujaDate;
                        });
                        $scope.pujareport = response;
                    }
                }
                else{
                    $scope.pujareport='';
                    if($rootScope.TempleID==0) {
                        logger.logWarning(response[0].usermessage);
                    }
                }
            });
            if ($scope.reporttype!=1){
                this.groupreport($scope,$scope.pujareport,type);
            }
        },
        printReport: function(f,data,ReportHeader,dates) {
            var columnBody = [[]];
            for (var keys in data[0]) {
            if (keys != "$$hashKey") {
                //keys = localize.getLocalizedString(keys);
                columnBody[0].push(keys);
            }
        }
            data.forEach(function (rows, i) {
            columnBody.push([]);
            for (var rowKey in rows) {
                if (rowKey != "$$hashKey") {
                    //var rowText = localize.getLocalizedString(rows[rowKey]);
                    columnBody[i + 1].push(rows[rowKey]);
                }
            }
        });
        //ReportHeader = localize.getLocalizedString(ReportHeader);
        var docDefinition = {
            content: [
                {
                    columns: [
                        {width: '*', text: "From: " + dates.FromDate, alignment: 'left'},
                        {width: '*', text: ReportHeader, style: 'header', alignment: 'center'},
                        {width: '*', text: "Printed On: ", alignment: 'right'}
                    ]
                },
                {
                    columns: [
                        {width: '*', text: "To: " + dates.ToDate, alignment: 'left'},
                        {width: '*', text: " " + dates.Today, alignment: 'right'}]
                },
                {
                    style: 'tableExample',
                    table: {
                        body: columnBody
                    },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 2 : 1;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                        }
                    }
                }
            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 0, 0, 10]
                },
                subheader: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 10, 0, 5]
                },
                tableExample: {
                    fontSize: 10,
                    margin: [0, 5, 0, 15]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                }
            },
            defaultStyle: {
                font: 'malayalam'
            },
            pageSize: "A4"
        };
        if (dates.pageOrientation != undefined) {
            docDefinition.pageOrientation = dates.pageOrientation
        }
//            docDefinition.content[1].table.body=columnBody;
        var pdf = pdfMake.createPdf(docDefinition);
        var fileName=ReportHeader+"_"+dates.FromDate+"_TO_"+dates.ToDate;
        if(f=='print'){
            pdf.print();
        }
        else if(f=='pdf'){
            pdf.download(fileName);
        }
        else if(f=='word'){
            alasql("SELECT * INTO CSV('"+fileName+".csv', {headers:true}) FROM ?",[data]);
        }
        else if(f=='excel'){
            //alasql("SELECT * INTO XLSX('"+fileName+".xlsx',{headers:true}) FROM ?",[data]);

            alasql("SELECT * INTO CSV('"+fileName+".csv', {headers:true}) FROM ?",[data]);
        }
    },
        printpujareport: function(type,data,temple){
            this.exportPDF(type, data,temple);
        },
        exportPDF: function (f,pujadata,temple){
            var data=[];
            pujadata.forEach(function(puja,i){
                data.push({
                    SlNo:""+(i+1),
                    PujaDate:""+$rootScope.date2Format(puja.PujaDate,'dd/mm/yyyy'),
                    Puja:""+puja.PujaName,
                    Deity:""+puja.Deity,
                    Count:""+puja.PujaCount
                });
            });
            this.printReport(f,data,'Puja List - ' + temple,{FromDate:$rootScope.date2Format(new Date(),'dd/mm/yyyy'),ToDate:$rootScope.date2Format(new Date().setDate(new Date().getDate()+10),'dd/mm/yyyy'),Today:$rootScope.date2Format(new Date(),'dd/mm/yyyy')});
        }
    }
});