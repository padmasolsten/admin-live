// <copyright file="Admin_PujaController.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Admin_Puja Functions </summary>

'use strict';

DevRoute.controller('Admin_TaskController',['$scope','$rootScope','Admin_TaskService','logger','$modal','$http', function ($scope,$rootScope,Admin_TaskService,logger,$modal,$http) {
var Dates={
        LanguageID:''
}
    var fromdate=new Date().setDate(new Date().getDate()-parseInt(30));
    $rootScope.reportDates={from:new Date(fromdate),to:new Date(),LanguageID:$rootScope.SessionLanguageID};
        $scope.loadApprovalPuja=function(Dates){
                $rootScope.approvaldates=Dates;
                Admin_TaskService.approvalpuja(Dates,function (data) {
                        if(data[0].length==0)
                        {
                                $scope.NoPujaApprovalListText='No puja to list for approval  !';
                                $scope.NoPujaApprovalListTextHide=false;
                                $scope.PujaApprovalCount=0;
                                $scope.PujaApprovalOrders='';
                                $scope.PujaApprovalList='';
                        }
                        else
                        {
                                $scope.ApprovalStatus=[];
                                $scope.NoPujaApprovalListTextHide=true;
                                $scope.PujaApprovalOrders=data[0];
                                $scope.PujaApprovalList=data[1];
                                $scope.PujaApprovalCount=$scope.PujaApprovalList.length;
                                $scope.PujaApprovalList.forEach(function(puja){
                                        puja.ApprovalStatus='1';
                                });
                                logger.log($scope.PujaApprovalCount+' Puja waiting for your approval');
                        }
                });
                //Admin_TaskService.ApprovalPuja($scope);
        };
    $scope.loadApprovalPuja($rootScope.reportDates);
    $rootScope.loadIncompletedPuja=function(){
        Admin_TaskService.incompletedpuja(function (data) {
            if(data[0].length !=0)
            {
                $scope.NoIncompletedPujaTextHide=true;
                $scope.IncompletedPujaList=data[0];
                $scope.IncompletedPujaListCount=$scope.IncompletedPujaList.length
            }
            else
            {
                $scope.NoIncompletedPujaText='No puja to list !';
                $scope.IncompletedPujaList='';
                $scope.NoIncompletedPujaTextHide=false;
                $scope.IncompletedPujaListCount=0;
            }
                $scope.isassisted=data[1][0].isAssisted;
        });
    };
        $rootScope.taskPageLoad=function(){
            $rootScope.loadIncompletedPuja();
            $scope.loadApprovalPuja();
        };
    $scope.itemImage='';
    $scope.setFile=function(file) {
        $scope.itemImage=file[0];
    }
    $scope.itemCost=0;
        $scope.ApprovalAction=function(puja, message) {
            puja.ApprovalMessage = message;
            puja.ItemCost = $scope.itemCost;
            puja.TempleName = $rootScope.TempleName;
            puja.category = $rootScope.Category;
         if($scope.itemImage){
            var file = $scope.itemImage;
            var uploadUrl = "/uploadfiles";
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (res)
            {
                $scope.itemImage='';
                if (res.path != undefined && res.path != '') {
                    $scope.path = res.path;
                    $scope.path = $scope.path.replace(/\W+/g, '/');
                    $scope.path = $scope.path.substring(16, $scope.path.length)
                    $scope.path = $scope.path.substring(0, $scope.path.lastIndexOf('/')) + '.' + $scope.path.substring($scope.path.lastIndexOf('/') + 1, $scope.path.length);
                    $scope.path = $scope.path.substring(1, $scope.path.length);
                    puja.filepath = $scope.path;
                    Admin_TaskService.approvalaction(puja, function (data) {
                        if (data[0].RESULT == '0') {
                            logger.logError('Puja approval action failed, Please try again ');
                        }
                        else {
                            logger.logSuccess('Booking updated successfully');
                            $scope.loadApprovalPuja($rootScope.approvaldates);
                        }
                    });
                }
            });
        } else {
             Admin_TaskService.approvalaction(puja, function (data) {
                 if (data[0].RESULT == '0') {
                     logger.logError('Puja approval action failed, Please try again ');
                 }
                 else {
                     logger.logSuccess('Booking updated successfully');
                     $scope.loadApprovalPuja($rootScope.approvaldates);
                 }
             });
         }
        };

        $scope.ChangePriestPujaStatus=function(puja){
                var PujaDate=new Date(puja.PujaDate);
                PujaDate = PujaDate.getFullYear() +"-"+(+PujaDate.getMonth()+1)+"-"+PujaDate.getDate();
                var formdata={
                        'CustomerID': puja.CustomerID,
                        'TrackingNo': puja.TrackingNo,
                        'TempleID': puja.TempleID,
                        'ReceiptNo': puja.ReceiptNo,
                        'BillAmount': puja.BillAmount,
                        'Email': puja.CustomerEmail,
                        'mobile': puja.mobile,
                        'FirstName': puja.FirstName,
                        'LastName': puja.LastName,
                        'PujaStatus': puja.PujaStatus,
                        'PujaName':puja.PujaName,
                        'PujaID':puja.PujaID,
                        'PujaDate':PujaDate,
                        'DevoteeName':puja.DevoteeName,
                        'PujaStatusMessage': puja.PujaStatusMessage
                }
            if(puja.PujaStatus!=17) {
                Admin_TaskService.changepujastatus(formdata, function (data) {
                    if (data[0].RESULT == '0') {
                        logger.logError('Puja status change failed, Please try again ');
                    }
                    else {
                        logger.logSuccess('Puja status changed successfully');
                        $rootScope.loadIncompletedPuja();
                    }
                });
            } else {
                if(puja.PujaStatus==17) {
                    $rootScope.pujadetails=puja;
                    $rootScope.modalInstance = $modal.open({
                        templateUrl: "sendreceipt.html",
                        controller: 'admincontroller'
                    });
                }
            }
        };
    $scope.ChangePujaStatus=function(puja){
        var PujaDate=new Date(puja.PujaDate);
        PujaDate = PujaDate.getFullYear() +"-"+(+PujaDate.getMonth()+1)+"-"+PujaDate.getDate();
        var formdata={
            'CustomerID': puja.CustomerID,
            'TrackingNo': puja.TrackingNo,
            'TempleID': puja.TempleID,
            'ReceiptNo': puja.ReceiptNo,
            'BillAmount': puja.BillAmount,
            'Email': puja.CustomerEmail,
            'mobile': puja.mobile,
            'FirstName': puja.FirstName,
            'LastName': puja.LastName,
            'PujaStatus': puja.PujaStatus,
            'PujaName':puja.PujaName,
            'PujaID':puja.PujaID,
            'PujaDate':PujaDate,
            'DevoteeName':puja.DevoteeName,
            'PujaStatusMessage': puja.PujaStatusMessage
        }
        if(puja.PujaStatus!=17) {
            Admin_TaskService.changepujastatus(formdata, function (data) {
                if (data[0].RESULT == '0') {
                    logger.logError('Puja status change failed, Please try again ');
                }
                else {
                    logger.logSuccess('Puja status changed successfully');
                    $rootScope.loadIncompletedPuja();
                }
            });
        } else {
            if(puja.PujaStatus==17) {
                $rootScope.pujadetails=puja;
                $rootScope.modalInstance = $modal.open({
                    templateUrl: "sendreceipt.html",
                    controller: 'admincontroller'
                });
            }
        }
    };
    $scope.populateDescription=function(puja) {
        puja.PujaStatusMessage= $scope.StatusDesc[puja.PujaStatus]
    };

        $scope.StatusDesc=new Array();
        $scope.StatusDesc[0]='';
        $scope.StatusDesc[2]='new pooja booking received';
        $scope.StatusDesc[6]='Your pooja has been completed and  postage is pending ';
        $scope.StatusDesc[7]='Your pooja and postage  has been completed';
        $scope.StatusDesc[1]='Your puja booking completed';
        $scope.StatusDesc[3]='Your puja booking has been cancelled';
        $scope.StatusDesc[16]='Your puja booking has been delayed';
        $scope.StatusDesc[17]='Your puja booking has been completed';

}]).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])

