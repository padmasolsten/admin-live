// <copyright file="LoginController.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Login Functions </summary>

'use strict';

DevRoute.controller('loginController', ['$http','$scope','$rootScope','loginService','$window','logger','$location','$cookies','base64', function ($http,$scope,$rootScope,loginService,$window,logger,$location,$cookies,base64) {
    $scope.MyProfileDivHide=true;
  $scope.headerhide=function(){
      $rootScope.signin=true;
  }
    loginService.loadlanguages(function (data) {
        $rootScope.allLanguages=data;
    });
    $rootScope.setLanguage=function(lang){
        localStorage.removeItem("SessionLanguage");
        localStorage.setItem("SessionLanguage",JSON.stringify(lang));
        $rootScope.SessionLanguage=lang;
        $rootScope.SessionLanguageID=$rootScope.SessionLanguage.languages_id;
        logger.logSuccess(lang.languages_name+' selected');
        $rootScope.setLanguageInScript(lang.languages_name);
        //$window.location.reload();
        $rootScope.loadByLanguage();
    };
    $scope.login=function(user){
        var url=$window.location.href;
        var formdata = { "mail":user.mail, "pass":user.pass ,"PageURL":url};
        loginService.login(formdata,function (response) {

            if(response[0].RESULT==undefined){
                user.pass='';
                $rootScope.CustomerName=response[0].FirstName;
                $rootScope.TempleName=$rootScope.TempleNameHeading=response[0].TempleName;
                $rootScope.TempleID=response[0].TempleID;
                $rootScope.TempleTypeID=response[0].TypeID;
                $rootScope.Category=response[0].Category;
                $rootScope.LoginID = response[0].LoginID;
                $rootScope.UserID=response[0].UserID;
                $rootScope.AccessTypeID= response[0].AccessTypeID;
                $rootScope.TempleEmail= response[0].TempleEmail;
                $rootScope.TempleIcon= response[0].TempleIcon;
                $rootScope.LoginDivHide=true;
				$rootScope.registerNo = response[0].registerno;
                if(user.TypeID=='0'){
                    $location.path('/');
                }
                else{

                    $location.path('/allreports');
                }
                var date= new Date();
                if(user.remember){
                    //date.setTime(date.getTime()+(30*24*60*60*1000));
                    //document.cookie='devaayanam_admin_session='+escape(base64.encode(JSON.stringify(results[0])))+';expires='+date.toGMTString();
                    document.cookie='devaayanam_admin_session='+escape(base64.encode(JSON.stringify(response[0])))+';';

                }
                else
                {
                    document.cookie='devaayanam_admin_session='+escape(base64.encode(JSON.stringify(response[0])))+';';
                };

                $rootScope.loadShipping(response[0].TempleID);
                //$window.location.reload();
            }
            else {
                logger.logError('Please Check Your Email or Password');
                $rootScope.LoginstatusText="Please Check Your Email or Password";
                $scope.user.pass='';
            }
        });
    };
    $scope.logout=function(){
        delete $cookies['devaayanam_admin_session'];
        loginService.logout(function (data) {
            $rootScope.CustomerName=null;
            $rootScope.TempleName=$rootScope.TempleNameHeading=null;
            $rootScope.TempleID=null;
            $rootScope.TempleTypeID=null;
            $rootScope.LoginID=null;
            $rootScope.UserID=null;
            $rootScope.AccessTypeID=null;
            $rootScope.TempleEmail=null;
            $rootScope.TempleIcon= null;
            $rootScope.LoginDivHide=false;
            $location.path('/signin');
        });
    };
    $rootScope.loadByLanguage=function(){
        if($rootScope.onPujaPageLoad!=undefined){
            $rootScope.onPujaPageLoad();
        }
        if($rootScope.LoadDeity!=undefined){
            $rootScope.LoadDeity();
        }
        if($rootScope.announcementPageLoad!=undefined){
            $rootScope.announcementPageLoad();
        }
        if($rootScope.eventPageLoad!=undefined){
            $rootScope.eventPageLoad();
        }
        if($rootScope.onDonationPageLoad!=undefined){
            $rootScope.onDonationPageLoad();
        }
        if($rootScope.taskPageLoad!=undefined){
            $rootScope.taskPageLoad();
        }
        if($rootScope.loadAboutPage!=undefined){
            $rootScope.loadAboutPage();
        }
    };
}]);