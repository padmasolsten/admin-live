// <copyright file="Admin_DonationController.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Admin_Donation Functions </summary>

'use strict';

DevRoute.controller('Admin_DonationController',['$http','$location','$scope','$rootScope','Admin_DonationService','logger', function ($http,$location,$scope,$rootScope,Admin_DonationService,logger) {

    $scope.loadMasterDonation=function(){
        Admin_DonationService.loadmasterdonation(function (data) {
            $rootScope.masterDonationList= data;
        });
    };
    $scope.loadDonation=function(){
        Admin_DonationService.loaddonation(function (data) {
            $rootScope.DonationList=data;
        });
    };
    $rootScope.onDonationPageLoad=function(){
        $scope.loadMasterDonation();
        $scope.loadDonation();
    };

    $scope.Today=new Date();
    $scope.StartFromDate=new Date();
    $scope.EndOnDate=new Date();
    $scope.EndOnDate.setDate(new Date().getDate()+1);
    $scope.donationDesc='';
    $scope.open = function($event,source) {
        $event.preventDefault();
        $event.stopPropagation();
        if(source=="Start"){
            $scope.EndOpened = false;
            return $scope.StartOpened = true;
        }
        else{
            $scope.StartOpened = false;
            return $scope.EndOpened = true;
        }

    };
    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.getDonationDataByLanguage=function(){
        if($scope.donationUpdateData!=undefined){
            var formData = {
                'DonationID' : $scope.donationUpdateData.DonationID,
                'LanguageID' : $scope.donationUpdateData.LanguageID
            }; Admin_DonationService.getdonationdatabylanguage(formData,function (data) {

                if(data[0].RESULT!=undefined){
                    logger.logWarning('no description found for this language');
                }
                else
                {
                    $scope.donationUpdateData.Desc=data[0].Desc;
                    if(data[0].languages_code!="en") {
                        //google_transliteration.makeTransliteratable(['multi_lang_input_donation_desc']);
                        //google_transliteration.setLanguagePair(google.elements.transliteration.LanguageCode.ENGLISH, response[0].languages_code);
                    }
                }
            });
        }
        else
        {
            logger.logWarning('Please select a donation')
        }

    };

    $scope.AddDonation=function(){
        var a=confirm('Do you want to proceed ?');
        if(a==true){
            var formData={
                'DonationMasterID' : $scope.donationData.DonationID ,
                'Amount' : $scope.donationAmount ,
                'Desc' :$scope.donationDesc,
                'StartFromDate':$scope.StartFromDate,
                'EndOnDate':$scope.EndOnDate
            };
            Admin_DonationService.adddonation(formData,function (data) {
                logger.log(data.Result);
                $scope.loadDonation();
                $scope.StartFromDate=new Date();
                $scope.EndOnDate=new Date();
                $scope.EndOnDate.setDate(new Date().getDate()+1);
                $scope.donationData='';
                $scope.donationAmount='';
                $scope.donationDesc='';
            });
        }
        else{

        }
    }
    $scope.updateDonation=function() {
        Admin_DonationService.updatedonation($scope.donationUpdateData,function (data) {
            logger.log(data.Result);
            $scope.loadDonation();
            $scope.donationUpdateData={};
        });
    };
    $scope.ChangeDonationStatus=function(donation,newStatus) {
        var formData = {
            'LanguageID':donation.LanguageID,
            'Desc' : donation.Desc,
            'Amount' : donation.Amount,
            'DonationID':donation.DonationID,
            'Status':newStatus
        };
        Admin_DonationService.changedonationstatus(formData,function (data) {
            logger.log(data.Result);
            $scope.loadDonation();
        });
    };
}]);

