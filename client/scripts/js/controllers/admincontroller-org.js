/**
 * Created by saneesh on 2/22/2017.
 */
// <copyright file="LoginController.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Login Functions </summary>

'use strict';

DevRoute.controller('admincontroller', ['$scope','$rootScope','adminservice','logger','$modal','$http','$sce', function ($scope,$rootScope,adminservice,logger,$modal,$http,$sce) {
    $rootScope.reportDates={from:new Date(),to:new Date(),LanguageID:''};
    $scope.reportDates.from=new Date();
    $scope.reportDates.to=new Date();
    $scope.davaayanamadmin = false;
    $scope.franchiseehide = true;
    $scope.settlementhide = true;
    $scope.$watch('TempleID',function(){
        if( $rootScope.TempleID==0){
            $scope.davaayanamadmin = true;
            $scope.franchiseehide = false;
            $scope.settlementhide = true;
            $scope.reportDates.from=new Date(new Date().getTime()-15*24*60*60*1000);
            $scope.reportDates.to=new Date();
        }
    });
    $scope.currentdate=new Date();
    if($rootScope.TempleID==0) {
        $scope.reportDates.from=new Date(new Date().getTime()-15*24*60*60*1000);
        $scope.reportDates.to=new Date();
        $scope.davaayanamadmin = true;
        $scope.franchiseehide = false;
        $scope.settlementhide = true;
    }else{
        $scope.davaayanamadmin = false;
		$scope.templeRegistered = false;
		// DV134 - Registration page changes - check if temple is already registered, if not then show registration button on left
		/*----------- testing stub -----------
		if ($scope.templeRegistered == true) 
			{ $scope.varClassRegistration = 'col-md-11 box disableRegister';}
		else { $scope.varClassRegistration = 'col-md-11 box blinking';}
		*/
		var ctrlData = { TempleName : $rootScope.TempleName};
		/* refer temple.registerno column to see if temple is registered (templeregistationdetails) -- populated in logincontroller now
		adminservice.checkTempleRegistered(ctrlData, function (data) {
			//console.log ('Got Registered temples data - ' + JSON.stringify(data));
			 if(data[0].length!=0){				
				$scope.templeRegistered = true;
				$scope.varClassRegistration = 'col-md-11 box disableRegister';
				}
			 else {
				$scope.templeRegistered = false;	
				$scope.varClassRegistration = 'col-md-11 box blinking';
			 }
		});					
		*/
		$scope.templeRegistered = true;
		$scope.varClassRegistration = 'col-md-11 box disableRegister';
    }
    $rootScope.signin=false;
    $rootScope.pujaReportSearchDate=new Date();
    $scope.getPujaReport=function(dates,log){
        dates.LanguageID=$rootScope.SessionLanguageID;
        $rootScope.reportDates=dates;
        adminservice.getpujareport(dates,function (data) {
            if(data[0].length!=0){
                $scope.searchpuja=true;
                if(dates.search==undefined){
                    $rootScope.PujaReport=data[0];
                }
                else
                {
                    $rootScope.pujaReportSearchResult=data[0];
                }
            }
            else{
                $scope.searchpuja=false;

                if(dates.search==undefined){
                    $rootScope.PujaReport='';
                }
                else
                {
                    $rootScope.pujaReportSearchResult='';

                }
                if(log==1){
                    logger.logWarning('No puja available for the selected dates')
                }

            }
        });
    };
    $scope.getBookingReport=function(dates,log){
        dates.LanguageID=$rootScope.SessionLanguageID;
        adminservice.getbookingreport(dates,function (data){
            if(data[0].length!=0){
                $rootScope.BookingReport=data[0];
                $scope.AllBookingDetails=data[1];
                $scope.isCollapsed=true;
                $rootScope.BookingReport.forEach(function(booking){
                    console.log ('booking row : ' + booking)
                    booking.bookingdetails=[];
                    $scope.AllBookingDetails.forEach(function(row){
                        if(booking.TrackingNo==row.TrackingNo){
                            booking.bookingdetails.push(row)
                        }
                    });
                });
            }
            else{
                $rootScope.BookingReport='';
                $rootScope.BookingDetails='';
                if(log==1){
                    logger.logWarning('No Booking available for the selected dates')
                }
            }
        });
    };
    $scope.getPostalAddressReport=function(dates,log){
        dates.LanguageID=$rootScope.SessionLanguageID;
        $rootScope.PostalAddressReport='';
        adminservice.getpostaladdressreport(dates,function (data){
            if(data[0].length!=0){
                return  $rootScope.PostalAddressReport=data[0];
            }
            else{
                if(log==1){
                    logger.log('No shipment for the selected dates');
                }
            }

        });
    };
	//DV161 - Postal status update ---------- start -----------
	$scope.PostalAction=function(puja, message) {
		puja.ApprovalMessage = message;
		puja.PujaStatus = puja.UpdateStatus; // Will have the new status 7 - PUJA_COMPLETE_POSTAGE_COMPLETE
		console.log ('In Postal Action -' + puja);
		adminservice.postalAction(puja, function (data) {
			 if (data[0].RESULT == '0') {
				logger.log('Postal approval action failed, Please try again.');
			 }
			 else {
				logger.log('Postal staus updated successfully.');
				//$scope.loadApprovalPuja($rootScope.approvaldates);
				$scope.getPaymentReport( $rootScope.reportDates,1);
			 }
		});
	};
	//DV161 - Postal status update ---------- end -----------	
    $scope.getPaymentReport=function(dates,log){
        dates.LanguageID=$rootScope.SessionLanguageID;
        adminservice.getpaymentreport(dates,function (data){
            if(data[0].length!=0){
                $rootScope.pujaPaymentReport=data[0];
                $rootScope.showPaymentReport=true;
            }
            else{
                $rootScope.showPaymentReport='';
                if(log==1){
                    logger.log('No payment received for the selected dates');
                }
            }

        });
    };
    $scope.getDonationReport=function(dates,log){
        dates.LanguageID=$rootScope.SessionLanguageID;
        adminservice.getdonationreport(dates,function (data){
            if(data[0].length!=0){
                return  $rootScope.DonationReport=data[0];
            }
            else{
                if(log==1){
                    logger.log('No donation received for the selected dates');
                }
            }
        });
    };
    $scope.searchPujaReport=function(date,log){
        dates.LanguageID=$rootScope.SessionLanguageID;
        var dates={from:date,to:date,search:log};
        adminservice.getpujareport(dates,function (data){
        });
    };
//devotee lists
    $scope.messagebox=true;
    $scope.sendsms=false;
    $scope.sendmail=false;
    $scope.editmode=false;
    $rootScope.userdata={
        LoginID:'',
        FirstName:'',
        PhoneMobile:'',
        option:'update'
    };
    $scope.editdevotees=function(data){
        $rootScope.userdata.LoginID=data.LoginID;
        $rootScope.userdata.PhoneMobile=data.PhoneMobile;
        $rootScope.userdata.FirstName=data.FirstName;
    }
    //DV-10
    $scope.loadContent=function(type){
        $scope.imageurl=$rootScope.baseUrl+'/';
        adminservice.loadreviews({TempleID:$rootScope.TempleID,type:type},function (data) {
            if(data[0].type=='review') {
                $scope.reviews =data;
            } else if(data[0].type=='downloads') {
                $scope.downloads =data;
            }
        });
    }
    //DV-10
    //DV-59
    $scope.updateContent=function(data,type,mode) {
        data.type=type;
        data.mode=mode;
        adminservice.updatereview(data,function (data) {
            logger.logSuccess('Done');
            $scope.loadContent(type);
        });
    }
    //DV-59
    $scope.customerdata={
        review:'',
        TempleID:$rootScope.TempleID,
        // CustomerID:$rootScope.CustomerID,
        LanguageID:$rootScope.SessionLanguageID,
        customername:'',
        customerimagepath:''
    }
    //DV-59
    $scope.addreview=function(filein){
        var file = filein;
        if(file==''||file==undefined) {
            logger.log('Choose photo');
            return;
        }
        if($scope.customerdata.customername==''||$scope.customerdata.customername==undefined) {
            logger.log('Enter priest Name');
            return;
        }
        if($scope.customerdata.review==''||$scope.customerdata.review==undefined) {
            logger.log('Enter priest Description');
            return;
        }
        if($scope.templedata.website==''||$scope.templedata.website==undefined) {
            logger.log('Enter priest website');
            return;
        }
        else if($scope.templedata.templeadmin=='') {
            logger.log('Enter priest Admin Username');
            return;
        } else if($scope.templedata.templeadminpassword=='') {
            logger.log('Enter priest Admin Password');
            return;
        }
        if($scope.templedata.templeadminpassword!=$scope.templedata.confirmpassword){
            logger.log('Missmatch passwords');
            return;
        }

        var uploadUrl = "/uploadgallery";
        var fd = new FormData();
        fd.append('file', file);
        if ($scope.ValidateSize(file)) {
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (res) {
                if (res.path == undefined || res.path == '') {
                    $scope.path = '/images/temple/uploads/personfaceapng1507265571000.jpg';
                } else {
                    $scope.path = res.path;
                    $scope.path = $scope.path.replace(/\W+/g, '/');
                    $scope.path = $scope.path.substring(16, $scope.path.length)
                    $scope.path = $scope.path.substring(0, $scope.path.lastIndexOf('/')) + '.' + $scope.path.substring($scope.path.lastIndexOf('/') + 1, $scope.path.length);
                    $scope.path = $scope.path.substring(1, $scope.path.length);
                }
                $scope.customerdata.customerimagepath = $scope.path;
                $scope.templedata.description=$scope.customerdata.review;
                $scope.templedata.templename=$scope.customerdata.customername;
                $scope.templedata.displayname=$scope.customerdata.customername;
                $scope.templedata.imagepath=$scope.path;
                $scope.templedata.category='priest';
                $scope.onboardtemple('createonboard');
            })
        }
    }
    //DV-59
    $scope.loaddevotees=function(){
        adminservice.loaddevotees({TempleID:$rootScope.TempleID},function (data) {
            $scope.imageurl=$rootScope.baseUrl+'/';
            $scope.devoteelist =data;
        });
    }

//DV-56-A provision in Devaayanam admin to download user mailids for newsletters.
//start-DV-56

    $scope.exportContacts=function(data,field,format){
        console.log(JSON.stringify($scope.devoteelist))

        var customerList=[];
        var title="Customer__Report";
        var file_name=title+"_"+field+"";



        $scope.devoteelist.forEach(function(row,i){
            customerList.push({
                "Name": ""+row.FirstName+""+row.LastName,
                "Email": ""+row.LoginID,
                "Mobile": ""+row.PhoneMobile
            });




            if($scope.devoteelist.length==i+1){
                if(format=="XLS"){
                    file_name=file_name+".xlsx";
                    alasql('SELECT * INTO XLSX("'+file_name+'",{headers:true}) FROM ?',[customerList]);
                }
                else{
                    file_name=file_name+".csv";
                    alasql('SELECT * INTO CSV("'+file_name+'",{headers:true}) FROM ?',[customerList]);
                }

            }
        });
    };


//end -DV-56
    var modalInstance;
    $rootScope.notificationdata={
        from:'',
        to:'',
        message:'',
        emailprovider:'',
        emailusername:'',
        emailpassword:'',
        option:''
    };
    $scope.opentemplate =function(data,option,mode){
        $rootScope.maildata={
            from:'',
            to:'',
            message:'',
            emailprovider:'',
            emailusername:'',
            emailpassword:'',
            option:'',
            mode:'',
            todata:[]
        };
        $rootScope.maildata.mode='one';
        if(mode=="all" && option == 'mail'){
            $rootScope.maildata.mode='all';
            $scope.devoteelist.forEach(function(row){
                $rootScope.maildata.todata.push(row.LoginID);
            })

            modalInstance = $modal.open({
                templateUrl: "allmail.html",
                controller: 'admincontroller'

            });

        }


        if(mode=="all" && option == 'sms'){
            $rootScope.maildata.mode='all';
            $scope.devoteelist.forEach(function(row){
                $rootScope.maildata.todata.push(row.PhoneMobile);
            })

            modalInstance = $modal.open({
                templateUrl: "allsms.html",
                controller: 'admincontroller'

            });

        }
        if(option=="mail"&& mode=='one'){
            $rootScope.maildata.option="mail";
            $rootScope.maildata.from="";
            $rootScope.maildata.to=data.LoginID;
            modalInstance = $modal.open({
                templateUrl: "emailtemplate.html",
                controller: 'admincontroller'

            });
        }else if(option=="sms"&& mode=='one') {
            $rootScope.maildata.option="sms";
            $rootScope.maildata.from="";
            $rootScope.maildata.to=data.PhoneMobile;
            modalInstance = $modal.open({
                templateUrl: "smstemplate.html",
                controller: 'admincontroller'

            });
        }

    };

    $scope.cancel = function(modalInste){
        $scope.messagebox=false;
    };
    $scope.sendmail=function(option,mode){
        if(mode=='other'){

            $rootScope.maildata=$rootScope.notificationdata;
            $rootScope.maildata.mode='one';
        }
        $rootScope.maildata.option=option;
        adminservice.sendmail($rootScope.maildata,function (data) {
            if(data=="success"){
                if($rootScope.maildata.option=="sms"){
                    logger.log("Email send successfully");
                }else {
                    logger.log("Message send successfully");
                }
            }else{
                logger.log("Something went wrong");
            }
        });
    }
    $scope.updatedevotee=function(){
        adminservice.updatedevotee($rootScope.userdata,function (data) {
            if(data.affectedRows!=0){
                logger.log("Updated successfully");
            }else{
                logger.log("Something went wrong");
            }
            if(data.affectedRows>1){
                logger.log("Please contact to Devaayanam");
            }

        });
    }

    //end

    //settlement
    $scope.settlementdatahide=true;
    $scope.updatestatus={
        new:'',
        TrackingNo:''
    };
    $scope.filterstaus='S';
    $scope.reportDates={
        from:'',
        to:'',
        TempleID:'',
        Mode:''
    }
    var data={
        "LanguageID": $rootScope.SessionLanguageID,
        "Category":undefined
    };
    $scope.devaayanamadmin=function(){
        adminservice.LoadTemples(data, function (data) {
            $rootScope.templelist = data[0];
        });
        $scope.reportDates.from=new Date(new Date().getTime()-15*24*60*60*1000);
        $scope.reportDates.to=new Date();
        $scope.reportDates.TempleID='all';
        $scope.loadsettlements();
    }
    $scope.reportDates.from=new Date();
    $scope.loadsettlements=function(){
        var initdatefrom, initdateto;
        initdatefrom=$rootScope.date2Format($scope.reportDates.from);
        initdateto=$rootScope.date2Format($scope.reportDates.to);
        $scope.reportDates.from=initdatefrom;
        $scope.reportDates.to=initdateto;
        adminservice.loadsettlements($scope.reportDates, function (data) {
            if(data.length!=0){
                $scope.settlementdatahide=false;
                $scope.transactions=data[0];
                //$scope.transactions[0].TransactionStatus='F';
            }
        });
    }
    $scope.mailpreview =function(){
        var modalInstance;
        modalInstance = $modal.open({
            templateUrl: "mailpreview.html",
            controller: 'admincontroller',
            windowClass: 'app-modal-window',
            overflow: 'scroll'
        });
    };
    $scope.settlementtrackingno=new Array();
    $scope.checkBankName=function(data) {
        if(data.BankName=='fonepaisa_temple') {
            return {'background-color':'#ff88a0'};
        } else {
            return {};
        }
    }
    $scope.generatesettlementfile=function(mode){
        if($scope.settlementtrackingno.length!=0) {
            adminservice.generatesettlementfile({settlementtrackingno:$scope.settlementtrackingno,mode:mode,to:$scope.toaddress}, function (data) {
                if(mode=='previewmail') {
                    $rootScope.maildata=$sce.trustAsHtml(data);
                    $scope.mailpreview();
                }
                if(mode=='createfile') {

                    $rootScope.xlxdata=data;

                    var date=new Date(data[0].TransactionDate);
                    var month =(date.getMonth()+1);
                    var day =(date.getDate());
                    if((date.getMonth()+1)<10) {
                        month ='0'+(date.getMonth()+1);
                    }
                    if((date.getDate()+1)<10) {
                        day ='0'+(date.getDate());
                    }

                    alasql('SELECT * INTO XLSX("settlementfile '+data[0].TransactionDate+'.xlsx",{headers:true}) FROM ?',[$rootScope.xlxdata]);
                }
                if (mode=='sendmail'||mode=='transferAmount') {
                    logger.log(data);
                }
            });
        } else {
            logger.logError("Select transactions from list");
        }
    }
    $scope.updatesettlementstatus=function(index,updaterow){
        $scope.updatestatus.TrackingNo=updaterow.TrackingNo;
        adminservice.updatesettlementstatus(updaterow, function (data) {
            if(data.length!=0){
                logger.logSuccess(data);
                $scope.loadsettlements();
            }
        });
    }
    $scope.addlistforsettlement=function(data,settlementdata){
        if(data==true){
            if(settlementdata.TransactionStatus=='S') {
                var allreadyadded=false;
                $scope.settlementtrackingno.forEach(function(row){
                    if(row==settlementdata.TrackingNo){
                        var allreadyadded=true;
                    }
                })
                if(allreadyadded==false){
                    $scope.settlementtrackingno.push(settlementdata.TrackingNo);
                }
            }
        }
        if(data==false){
            var index=0;
            var newindex;

            $scope.settlementtrackingno.forEach(function(row){
                if(row==settlementdata.TrackingNo){
                    newindex=index;
                }
                index++;
            })
            $scope.settlementtrackingno.splice(newindex,1);

        }
    }
    $scope.getallpujareport=function(type){

        //var originalContents, popupWin, printContents;
        //printContents = document.getElementById('allpujareport').innerHTML;
        //originalContents = document.body.innerHTML;
        //
        //if(type=='print'){
        //    popupWin = window.open();
        //    popupWin.document.open();
        //    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="styles/main.css" /></head><body onload="window.print()"><h1 style="text-align: center"><img src="http://devaayanam.in/images/DevaayanamCompanyName.png" width="200px"height="40px"></h1> <hr>' + printContents + '</html>');
        //    return popupWin.document.close();
        //}
        //else if(type=='pdf')
        //{
        $scope.exportPDF(type);

        //}
    };
    var dt=new Date();
    $scope.printReport=function(f,data,ReportHeader,dates) {

        var columnBody = [[]];
        for (var keys in data[0]) {
            if (keys != "$$hashKey") {
                //keys = localize.getLocalizedString(keys);
                columnBody[0].push(keys);
            }
        }
        data.forEach(function (rows, i) {
            columnBody.push([]);
            for (var rowKey in rows) {
                if (rowKey != "$$hashKey") {
                    //var rowText = localize.getLocalizedString(rows[rowKey]);
                    columnBody[i + 1].push(rows[rowKey]);
                }
            }
        });
        //ReportHeader = localize.getLocalizedString(ReportHeader);
        var docDefinition = {
            content: [
                {
                    columns: [
                        {width: '*', text: "From: " + dates.FromDate, alignment: 'left'},
                        {width: '*', text: ReportHeader, style: 'header', alignment: 'center'},
                        {width: '*', text: "Printed On: ", alignment: 'right'}
                    ]
                },
                {
                    columns: [
                        {width: '*', text: "To: " + dates.ToDate, alignment: 'left'},
                        {width: '*', text: " " + dates.Today, alignment: 'right'}]
                },
                {
                    style: 'tableExample',
                    table: {
                        body: columnBody
                    },
                    layout: {
                        hLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 2 : 1;
                        },
                        vLineWidth: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                        },
                        hLineColor: function (i, node) {
                            return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                        },
                        vLineColor: function (i, node) {
                            return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                        }
                    }
                }
            ],
            styles: {
                header: {
                    fontSize: 18,
                    bold: true,
                    margin: [0, 0, 0, 10]
                },
                subheader: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 10, 0, 5]
                },
                tableExample: {
                    fontSize: 10,
                    margin: [0, 5, 0, 15]
                },
                tableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                }
            },
            defaultStyle: {
                font: 'malayalam'
            },
            pageSize: "A4"
        };
        if (dates.pageOrientation != undefined) {
            docDefinition.pageOrientation = dates.pageOrientation
        }
//            docDefinition.content[1].table.body=columnBody;

        //docDefinition={ content:'This is a test document'}
        var pdf = pdfMake.createPdf(docDefinition);
        var fileName=ReportHeader+"_"+dates.FromDate+"_TO_"+dates.ToDate;
        /*
		if(f=='print'){
            pdf.print();
        }*/
        if(f=='print'){
			pdf.getBuffer(function (buffer) {
				var file = new Blob([buffer], {type: 'application/pdf'});
				var fileURL = URL.createObjectURL(file);
				window.location.href = fileURL; 
			}); // {autoPrint: true}); 			
        }
		
        else if(f=='pdf'){
            pdf.download(fileName);
        }
        else if(f=='word'){
            alasql("SELECT * INTO CSV('"+fileName+".csv', {headers:true}) FROM ?",[data]);
        }
        else if(f=='excel'){
            //alasql("SELECT * INTO XLSX('"+fileName+".xlsx',{headers:true}) FROM ?",[data]);
            data=[];
            var index=0;
            $rootScope.BookingReport.forEach(function (puja, i) {
                   var BookingDate= "" + $rootScope.date2Format(puja.BookingDate, 'dd/mm/yyyy');
                   var ReceiptNo= "" + puja.ReceiptNo;
                   var TrackinNo= "" + puja.TrackingNo;
                puja.bookingdetails.forEach(function(row,j){
                        index=index+1;
                        data.push({
                            SlNo: "" + index,
                            BookingDate: "" + BookingDate,
                            ReceiptNo: "" + ReceiptNo,
                            TrackinNo: "" + TrackinNo,
                            Type:row.servicetype,
                            'Puja/Donation Date': $rootScope.date2Format(row.servicedate, 'dd/mm/yyyy'),
                            'Puja/Donation': row.servicename,
                            Deity: row.dietyname,
                            BeneficiaryName: row.devoteename,
                            BeneficiaryStar: row.devoteestar,
                            Rate: "" + row.servicerate,
                            Status: "" + row.servicestatus
                        });
                });
            });
            var a = document.createElement("a");
            var csv = Papa.unparse(data);
            a.href = 'data:attachment/csv;charset=utf-8,' + encodeURI(csv);
            a.target = '_blank';
            a.download = fileName+'.csv';
            document.body.appendChild(a);
            a.click();
            //alasql("SELECT * INTO CSV('"+fileName+".csv', {headers:true}) FROM ?",[data]);
        }


    };
    $scope.exportPDF=function(f,type){
        var data=[];
        var reportheader="";
        if(type=='puja') {
            reportheader="Puja Report";
            $rootScope.PujaReport.forEach(function (puja, i) {
                data.push({
                    SlNo: "" + (i + 1),
                    PujaDate: "" + $rootScope.date2Format(puja.PujaDate, 'dd/mm/yyyy'),
                    Deity: "" + puja.DeityName,
                    Puja: "" + puja.PujaName,
                    Devotee: "" + puja.DevoteeName,
                    Star: "" + puja.StarName,
                    Shipping: "" + $rootScope.shippingOptions[puja.ShippingID],
                    Status: "" + puja.StatusDesc

                });
            });
        }else if(type=='booking')
        {
            reportheader="Booking Report";
            $rootScope.BookingReport.forEach(function (puja, i) {
                data.push({
                    SlNo: "" + (i + 1),
                    BookingDate: "" + $rootScope.date2Format(puja.BookingDate, 'dd/mm/yyyy'),
                    ReceiptNo: "" + puja.ReceiptNo,
                    TrackinNo: "" + puja.TrackingNo,
                    Customer: "" + puja.FirstName+" "+puja.LastName,
                    Address: "" + puja.HouseName+","+puja.StreetName+", "+puja.District+", "+puja.State+",contact:"+puja.PhoneMobile,
                    Amount: "" + puja.BillAmount,
                    Status: "" + puja.StatusDesc
                });
            });
        }else if(type=='shipping'){
            reportheader="Shipment Report";
            $rootScope.PostalAddressReport.forEach(function (puja, i) {
                data.push({					
                    SlNo: "" + (i + 1),					
                    TrackingNo: "" + puja.TrackingNo,
                    Customer: "" + puja.FirstName+" "+puja.LastName,
                    PujaName: "" + puja.PujaName,
                    PujaDate: "" +  $rootScope.date2Format(puja.PujaDate, 'dd/mm/yyyy'),
                    Address: "" + puja.HouseName+","+puja.StreetName+", "+puja.District+", "+puja.State+"",
                    Contact: "" + puja.PhoneMobile+","+puja.PhoneWired,
                    Shipping: "" + $rootScope.shippingOptions[puja.ShippingID],
                    Status: "" + puja.StatusDesc					
                });
            });
        }
        else if(type=='payment'){
            reportheader="Payment Report";
            $rootScope.pujaPaymentReport.forEach(function (puja, i) {
                data.push({
                    SlNo: "" + (i + 1),
                    BookingDate: "" + $rootScope.date2Format(puja.BookingDate, 'dd/mm/yyyy'),
                    ReceiptNo: "" + puja.ReceiptNo,
                    Customer: "" + puja.FirstName+" "+puja.LastName,
                    Address: "" + puja.HouseName+","+puja.StreetName+", "+puja.District+", "+puja.State+"",
                    Amount: "" + puja.BillAmount,
                    Payment_Ref_No: ""+ puja.GatewayTransactionID,
                    Contact: "" + puja.PhoneMobile+","+puja.PhoneWired
                });
            });
        }
        else if(type=='donation'){
            reportheader="Donation Report";
            $rootScope.DonationReport.forEach(function (puja, i) {
                data.push({
                    SlNo: "" + (i + 1),
                    DonationDate: "" + $rootScope.date2Format(puja.DonationDate, 'dd/mm/yyyy'),
                    ReceiptNo: "" + puja.DonationName,
                    Customer: "" + puja.Devotee+" ",
                    Address: "" + puja.HouseName+","+puja.StreetName+", "+puja.District+", "+puja.State+"",
                    Contact: "" + puja.PhoneMobile+","+puja.PhoneWired
                });
            });
        }

        $scope.printReport(f,data,reportheader,{FromDate:$rootScope.date2Format($rootScope.reportDates.from,'dd/mm/yyyy'),ToDate:$rootScope.date2Format($rootScope.reportDates.to,'dd/mm/yyyy'),Today:$rootScope.date2Format(dt,'dd/mm/yyyy')});


        //alasql("SELECT * INTO PDF('"+fileName+".pdf', {headers:true}) FROM ?",[ $rootScope.hethi_role_task_list]);

        //var pdf = new jsPDF('p', 'pt', 'letter');
        // source can be HTML-formatted string, or a reference
        // to an actual DOM element from which the text will be scraped.
        //var source  = $('#allpujareportPDF')[0];

        // we support special element handlers. Register them with jQuery-style
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors
        // (class, of compound) at this time.


        //var specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        //'#bypassme': function (element, renderer) {
        // true = "handled elsewhere, bypass text extraction"
        //return true
        //}
        //};
        //var margins = {
        //    top: 80,
        //    bottom: 60,
        //    left: 40,
        //    width: 522
        //};
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        //pdf.fromHTML(
        //    source, // HTML string or DOM elem ref.
        //    margins.left, // x coord
        //    margins.top, { // y coord
        //        'width': margins.width, // max width of content on PDF
        //        'elementHandlers': specialElementHandlers
        //    },

        //function (dispose) {
        // dispose: object with X, Y of the last line add to the PDF
        //          this allow the insertion of new lines after html
        //pdf.save(fileName+'.pdf');
        //}, margins);
    };
    //settlement end
    //dashboard
    $scope.loaddashboard=function () {
        adminservice.loaddashboard(function (data) {
            $scope.dashboarddata=data;
            $scope.customers=data[0];
            $scope.bookings=data[1];
            $scope.pujas=data[2];
            $scope.billvalues=data[3];
        });
    }
    //upload receipt
    $scope.uploadFile = function(myFile){
        var file = myFile;
        var uploadUrl = "/uploadreceipt";
        var fd = new FormData();
        fd.append('file', file);
        fd.append('pujadetails',JSON.stringify($rootScope.pujadetails));
        if(file!=undefined) {
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function () {
                    logger.log('Message sent');
                    logger.log('Status updated');
                    $rootScope.loadIncompletedPuja();
                    $rootScope.modalInstance.close();
                })
                .error(function () {
                    console.log("error!!");
                });
        } else {
            logger.log('Choose Receipt Image');
        }
    };

    //masterpujaload
    $scope.masterpuja={
        pujaname:'',
        description:''
    };
    $scope.onmasterpujaload=function(){
        adminservice.loadmasterpuja(function (data) {
            $rootScope.masterPujaList=data;
        });
        adminservice.loadmasterdeity(function (data) {
            $rootScope.masterdeitylist=data;
        });
    };
    $scope.add_puja=function(type){
        $scope.masterpuja.type=type;
		$scope.masterpuja.templeid=0;		//DV203 Puja tags. Common puja tags are added thru Dvnm Admin will go with templeid 0, added via Temple admin will go the templeid
        if($scope.masterpuja.pujaname!='') {
            adminservice.addpuja($scope.masterpuja, function (data) {
                if (data.affectedRows != 0) {
                    if (type == 'puja') {
                        logger.log('puja added successfully');
                        $scope.onmasterpujaload();
                        $scope.masterpuja.pujaname = '';
                        $scope.masterpuja.description = '';
                    } else if (type == 'deity') {
                        logger.log('deity added successfully');
                        $scope.onmasterpujaload();
                        $scope.masterpuja.pujaname = '';
                        $scope.masterpuja.description = '';
                    } else if (type == 'pujatype') {
                        logger.log('puja type added successfully');
                        $scope.onmasterpujaload();
                        $scope.masterpuja.pujaname = '';
                        $scope.masterpuja.description = '';
                    }
                } else {
                    logger.log('Something went wrong');
                }
            });
        }
    };
    $scope.deletemasterpuja=function (data,type) {
        data.type=type;
        adminservice.deletemasterpuja(data,function (data) {
            if(data.affectedRows>0) {
                if(type=='puja') {
                    logger.log('puja deleted');
                }
                else if(type=='deity'){
                    logger.log('deity deleted');
                }
                $scope.onmasterpujaload();
            }else{
                logger.log('Something went wrong');
            }
        })
    }
    $scope.updatemasterpuja=function (data,type) {
        data.type=type;
        adminservice.updatemasterpuja(data,function (data) {
            if(data.affectedRows>0) {
                if(type=='puja') {
                    logger.log('puja updated');
                }
                else if(type=='deity'){
                    logger.log('deity updated');
                }
                $scope.onmasterpujaload();
            }else{
                logger.log('Something went wrong');
            }
        })
    }

    //masterpujaend
    //dv-83-start
    $scope.ValidateSize=function(file) {
        var FileSize = file.size / 1024 ; // in Kb
        if (FileSize > 200) {
            alert('File size exceeds 200 KB');
            return false;
        } else {
            return true;
        }
    }
    $scope.uploadImage = function() {
        var file = $scope.myFile;
        if($scope.myFile==''||$scope.myFile==undefined) {
            logger.log('Choose photo');
            return;
        }
        var uploadUrl = "/uploadgallery";
        var fd = new FormData();
        fd.append('file', file);
        if ($scope.TypeID != 4) {
            if ($scope.ValidateSize($scope.myFile)) {
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).success(function (res) {
                    $scope.path = res.path;
                    $scope.path = $scope.path.replace(/\W+/g, '/');
                    $scope.path = $scope.path.substring(16, $scope.path.length)
                    $scope.path = $scope.path.substring(0, $scope.path.lastIndexOf('/')) + '.' + $scope.path.substring($scope.path.lastIndexOf('/') + 1, $scope.path.length);
                    $scope.templedata.imagepath=$scope.path;
                    $scope.onboardtemple('createonboard');
                });
            }
        }
    }
    //dv-83-end
    //onboard temple
//DV-34
    $scope.createTempleData= function() {
    $scope.templedata = {
        selectedtemple: -2,
            selectedmode: '',
            templename: '',
            displayname: '',
        devasom: '',
        website:'',
        emailid: '',

        category:'',
        imagepath:$scope.path,
        description:'',

        housename : '',
        streetname : '',
        postoffice : '',
        postalcode : '',
        state : '',
        district : '',
        country : 'India',
        phonemobile : '',
        phonewired : '',


    templeadmin: '',
    templeadminpassword: '',
    confirmpassword: '',

    usercode: '',
    passcode: '',
    hashkey: '',

    name: '',
    ifsc: '',
    bankname: '',
    branchname: '',
    acnumber: '',
    actype: '',
    mapurl: "",
        isAssisted:0,
        pgname:'',
        internetBanking:1,

        schedulerStatus:0,
        maxPujaPerTransaction:0,
        chargePerPuja:0,
        chargePerTransaction:0
    }
},
    $scope.createTempleData();
    $scope.changeGateway=function(data) {
                $scope.templedata.usercode= '';
                $scope.templedata.passcode= '';
                $scope.templedata.hashkey= '';
        if (data=='fed_devaayanam') {
                $scope.templedata.usercode= 'DEVAATST';
                $scope.templedata.passcode= 'DEVAA123#';
                $scope.templedata.hashkey= 'ASDADASDSA768768';
        }
        if(data==$scope.selectedTemple.BankName) {
            $scope.templedata.usercode= $scope.selectedTemple.UserCode;
            $scope.templedata.passcode= $scope.selectedTemple.PassCode;
            $scope.templedata.hashkey= $scope.selectedTemple.HashKey;
        }
    }
    $scope.onboardtemple=function(mode){
        if(mode=='createonboard'&&$scope.templedata.category=='temple') {
            if($scope.templedata.templename=='') {
                logger.log('Enter temple name');
                return;
            } else if($scope.templedata.displayname=='') {
                logger.log('Enter Display name');
                return;
            } else if($scope.templedata.devasom=='') {
                logger.log('Enter Devasom');
                return;
            } else if($scope.templedata.website=='') {
                logger.log('Enter Temple Website');
                return;
            } else if($scope.templedata.emailid=='') {
                //logger.log('Enter Temple E-mail');
                $scope.templedata.emailid=='support@devaayanam.in';
                //return;
            } else if($scope.templedata.templeadmin=='') {
                logger.log('Enter Temple Admin Username');
                return;
            } else if($scope.templedata.templeadminpassword=='') {
                logger.log('Enter Temple Admin Password');
                return;
            }
            if($scope.templedata.templeadminpassword!=$scope.templedata.confirmpassword){
                logger.log('Missmatch passwords');
                return;
            }
        } else if(mode=='createonboard'&&$scope.templedata.category=='priest')
        {
            $scope.templedata.displayname=$scope.templedata.templename;
            if($scope.templedata.templename=='') {
                logger.log('Enter priest name');
                return;
            } else if($scope.templedata.description=='') {
                logger.log('Enter description');
                return;
            } else if($scope.housename=='') {
                logger.log('Enter house name');
                return;
            } else if($scope.streetname=='') {
                logger.log('Enter street name');
                return;
            } else if($scope.postoffice=='') {
                logger.log('Enter postoffice');
                return;
            } else if($scope.postalcode=='') {
                logger.log('Enter postal code');
                return;
            } else if($scope.district=='') {
                logger.log('Enter district');
                return;
            } else if($scope.state=='') {
                logger.log('Enter state');
                return;
            } else if($scope.phonemobile=='') {
                logger.log('Enter Phone Mobile');
                return;
            } else if($scope.templedata.website=='') {
                logger.log('Enter priest Website');
                return;
            } else if($scope.templedata.emailid=='') {
                //logger.log('Enter Temple E-mail');
                $scope.templedata.emailid=='support@devaayanam.in';
                //return;
            } else if($scope.templedata.templeadmin=='') {
                logger.log('Enter priest Admin Username');
                return;
            } else if($scope.templedata.templeadminpassword=='') {
                logger.log('Enter priest Admin Password');
                return;
            }
            if($scope.templedata.templeadminpassword!=$scope.templedata.confirmpassword){
                logger.log('Missmatch passwords');
                return;
            }
        }
        if(mode=='updatetemple') {
            if($scope.templedata.schedulerStatus==true||$scope.templedata.schedulerStatus==1){
                $scope.templedata.schedulerStatus=1;
            }else{
                $scope.templedata.schedulerStatus=0;
            }
            if($scope.templedata.pgname=='fed_devaayanam'||$scope.templedata.pgname=='fonepaisa_devaayanam') {
                $scope.templedata.isAssisted=1;
            } else {
                $scope.templedata.isAssisted=0;
            }
            if($scope.templedata.pgname=='no_pg') {
                $scope.templedata.internetBanking=0;
            } else {
                $scope.templedata.internetBanking=1;
            }
            var a=confirm('Do you want to update temple '+$scope.templedata.templename+' ?');
            if(a==false)
            return;
            $scope.selectedTemple.BankName=$scope.templedata.pgname;
            $scope.selectedTemple.UserCode=$scope.templedata.usercode;
            $scope.selectedTemple.PassCode=$scope.templedata.passcode;
            $scope.selectedTemple.HashKey=$scope.templedata.hashkey;
            if($scope.templedata.mapurl) {
                if($scope.templedata.mapurl.length>37) {
                    $scope.templedata.mapurl = $scope.templedata.mapurl.substring(37);
                }
            }
        }
        if(mode=='changeStatus') {
            if($scope.templedata.status==1) {
                $scope.templedata.status = 0;
            } else if($scope.templedata.status==0) {
                $scope.templedata.status = 1;
            }
            if($scope.templedata.status!=1&&$scope.templedata.status!=0) {
                return ;
            }
        }
        $scope.templedata.selectedmode=mode;
        var x2js = new X2JS();
        var data = {data: $scope.templedata};
        var registerdata = "<root>" + x2js.json2xml_str(data) + "</root>";
        var data={
            xmldata:registerdata,
            usercode:$scope.templedata.usercode,
            passcode:$scope.templedata.passcode,
            hashkey:$scope.templedata.hashkey,
            mode:$scope.templedata.selectedmode
        }
        adminservice.onboardtemple(data,function(data){
            if(mode=='loadtemples') {
                $scope.templelists=data[0];
                if($scope.templedata.mapurl.length>2) {
                    $scope.templedata.mapurl='https://www.google.com/maps/embed?pb='+$scope.templedata.mapurl;
                }
            }
            if(mode=='createonboard') {
                logger.log(data[0][0].response);
if(data[0][0].status==1) {
    $scope.customerdata.website=data[0][0].Website;
    adminservice.addreview($scope.customerdata, function (data) {
        if (data.affectedRows != 0) {
            //logger.logSuccess('Temple priest added');
            $scope.customerdata = new Array();
            //$scope.loadreviews();
            window.location.reload();
        } else {
            logger.logError('Something went Wrong!');
        }
    });
}
                if(data[0][0].status==1) {
                    $scope.onboardtemple('writetofile');
                    $scope.createTempleData();
                }
            }
            if(mode=='updatetemple') {
                if($scope.templedata.schedulerStatus==true||$scope.templedata.schedulerStatus==1){
                    $scope.templedata.schedulerStatus=true;
                }else{
                    $scope.templedata.schedulerStatus=false;
                }
                $scope.onboardtemple('loadtemples');
                logger.log(data[0][0].response);
            }
            if(mode=='changeStatus') {
                $scope.onboardtemple('loadtemples');
                logger.log(data[0][0].response);
            }
        })

    }
    $scope.onchangetemplename=function(selected){
        $scope.selectedTemple=selected;
        $scope.templedata.selectedtemple=selected.TempleID;
		//DV149 - Deactivate temples - On refresh or loading again, showing as active -- caps trouble in scope variable
		// OLD CODE -- $scope.templedata.status=selected.Status;
		$scope.templedata.status=selected.status;        
        $scope.templedata.templename=selected.TempleName;
        $scope.templedata.usercode=selected.UserCode;
        $scope.templedata.passcode=selected.PassCode;
        $scope.templedata.hashkey=selected.HashKey;
        $scope.templedata.pgname=selected.BankName;
        $scope.templedata.name=selected.BenficiaryName;
        $scope.templedata.bankname=selected.BenficiaryBankName;
        $scope.templedata.branchname=selected.BenficiaryBankBranch;
        $scope.templedata.acnumber=selected.BenficiaryBankACNumber;
        $scope.templedata.actype=selected.BenficiaryBankACType;
        $scope.templedata.ifsc=selected.BenficiaryBankIFSC;
        $scope.templedata.mapurl=selected.Value;
        $scope.templedata.website=selected.WebSite;

        $scope.templedata.schedulerStatus=selected.schedulerStatus;
        if($scope.templedata.schedulerStatus==true||$scope.templedata.schedulerStatus==1){
            $scope.templedata.schedulerStatus=true;
        }else{
            $scope.templedata.schedulerStatus=false;
        }
        $scope.templedata.maxPujaPerTransaction=selected.maxPujaPerTransaction;
        $scope.templedata.chargePerPuja=selected.chargePerPuja;
        $scope.templedata.chargePerTransaction=selected.chargePerTransaction;
        if($scope.templedata.mapurl) {
            if($scope.templedata.mapurl.length>2) {
                $scope.templedata.mapurl = 'https://www.google.com/maps/embed?pb=' + selected.Value;
            }
        }
    }
    <!--DV-34-->
    $scope.generalparms={
        selectedmode:'',
        fromdate:new Date(),
        todate:new Date(),
        selectedtemple:-2,
        header:'',
        desc:'',
        filepath:'',
        enddate:''
    }
    $scope.showreport=false;
    $scope.generalcall=function(mode){
        if(mode=='adddownload'){
            var file = $scope.generalparms.filepath;
            var uploadUrl = "/uploadfiles";
            var fd = new FormData();
            fd.append('file', file);
			
			// DV182-Download feature not proper ------------ Start -----------------
			// Check file size is less than 5mb and upload
			const fi = document.getElementById('uploadFile'); 
			// Check if any file is selected. 
			if (fi.files.length > 0) { 
				for (const i = 0; i <= fi.files.length - 1; i++) { 
					const fsize = fi.files.item(i).size; 
					const file = Math.round((fsize / 1024)); 
					// The size of the file. 
					if (file >= 5120) { 
						alert( 
						  "File too Big, please select a file less than 5mb"); 
					} else {
			            $http.post(uploadUrl,fd, {
							transformRequest: angular.identity,
							headers: {'Content-Type': undefined}
							}).success(function(res) {
							if(res.path!=undefined&&res.path!=''){
								$scope.path = res.path;
								$scope.path = $scope.path.replace(/\W+/g, '/');
								$scope.path = $scope.path.substring(16, $scope.path.length)
								$scope.path = $scope.path.substring(0, $scope.path.lastIndexOf('/')) + '.' + $scope.path.substring($scope.path.lastIndexOf('/') + 1, $scope.path.length);
								$scope.path = $scope.path.substring(1, $scope.path.length);
								$scope.generalparms.filepath=$scope.path;
								$scope.generalparms.selectedtemple=$rootScope.TempleID;
								$scope.generalcall('adddownloads');
								}
							});
					}
				}
			}
			// DV182-Download feature not proper ------------ end -----------------			
            return;
        }else{
        $scope.generalparms.selectedmode=mode;
        var x2js = new X2JS();
        var data = {data: $scope.generalparms};
        var registerdata = "<root>" + x2js.json2xml_str(data) + "</root>";
        var data={
            xmldata:registerdata
        }
        adminservice.generalcall(data,function(data) {
            if(data[0][0].mode=='loaddata'){
				/* DV197 - old code
				$scope.showreport=true;
                $scope.reportdata=data;
				window.location.href = '/Download?id=' + test + '&format=pdf';
				-- old code end */ 
				var reportOptions = {
					fromdate:  $rootScope.date2Format($scope.generalparms.fromdate, 'yyyy-mm-dd'),
					todate: $rootScope.date2Format($scope.generalparms.todate, 'yyyy-mm-dd')
				};
				$http.post('/admin/dvnmamdin/getBusinessReport', reportOptions).then(function(response){					
					console.log("Results",response);
					// vm.exportMessage = "Found " + vm.results.length + " registrations.";
					$scope.resultBusinessReport = response.data;
					});
            }
            if(data[0][0].mode=='adddownloads'){
                $scope.generalparms.filepath='';
                $scope.generalparms.desc='';
                $scope.generalparms.header='';
               logger.logSuccess('New file added to downloads');
            }
        });
        }
    }
    $scope.setEditMode = function(data) {
        if(data.isCollapsed==true) {
            data.isCollapsed=false;
        } else if(data.isCollapsed==false) {
            data.isCollapsed=true;
        }
    }
}]).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]).filter('filterWithOr', function ($filter) {
    var comparator = function (actual, expected) {
        if (angular.isUndefined(actual)) {
            // No substring matching against `undefined`
            return false;
        }
        if ((actual === null) || (expected === null)) {
            // No substring matching against `null`; only match against `null`
            return actual === expected;
        }
        if ((angular.isObject(expected) && !angular.isArray(expected)) || (angular.isObject(actual) && !hasCustomToString(actual))) {
            // Should not compare primitives against objects, unless they have custom `toString` method
            return false;
        }
        console.log('ACTUAL EXPECTED')
        console.log(actual)
        console.log(expected)

        actual = angular.lowercase('' + actual);
        if (angular.isArray(expected)) {
            var match = false;
            expected.forEach(function (e) {
                console.log('forEach')
                console.log(e)
                e = angular.lowercase('' + e);
                if (actual.indexOf(e) !== -1) {
                    match = true;
                }
            });
            return match;
        } else {
            expected = angular.lowercase('' + expected);
            return actual.indexOf(expected) !== -1;
        }
    };
    return function (array, expression) {
        return $filter('filter')(array, expression, comparator);
    };
});