// <copyright file="Admin_CalendarController.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Calendar Functions </summary>

'use strict';

DevRoute.controller('Admin_CalendarController',['$scope','$rootScope','Admin_CalendarService','logger', function ($scope,$rootScope,Admin_CalendarService,logger) {


    $scope.$watch('TempleID',function(){
        if( $rootScope.TempleID==0){
            $scope.davaayanamadmin = true;
            $scope.franchiseehide = false;
            $scope.settlementhide = true;
        }
    });
    if($rootScope.TempleID==0) {
        $scope.davaayanamadmin = true;
        $scope.franchiseehide = false;
        $scope.settlementhide = true;
    }else{
        $scope.davaayanamadmin = false;
    }


    $scope.AlertStartDate=new Date();
    $scope.AlertEndDate=$scope.AlertStartDate
    $scope.AlertEndDate.setDate(new Date().getDate()+1);
    $scope.EndDate=$scope.AlertEndDate;
    $scope.StartDate=$scope.AlertStartDate;
    $scope.open = function($event,mode) {
        $event.preventDefault();
        $event.stopPropagation();
        if(mode=='start'){
            $scope.opened2 = false;
            return $scope.opened1 = true;
        }
        else{
            $scope.opened1 = false;
            return $scope.opened2 = true;
        }

    };
    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.LoadAnnouncement=function(){
        var formdata={LanguageID: $rootScope.SessionLanguageID}
        Admin_CalendarService.loadannouncement(formdata,function (data) {
            if(data.length!=0){
                $rootScope.AnnouncementList=data;
                $scope.ShowAnnouncementEditCollapse=false;
            }
            else{
                $rootScope.AnnouncementList=null;
                $scope.ShowAnnouncementEditCollapse=true;
            }
        });


    };
    $rootScope.announcementPageLoad=function(){
        $scope.LoadAnnouncement();
    };
    $scope.getAnnouncementDataByLanguage=function(){
        if($scope.UpdateAnnouncementData!=undefined){
            Admin_CalendarService.loadannouncement($scope.UpdateAnnouncementData,function (data) {
                if(data.length!=0){
                    $scope.UpdateAnnouncementData.AlertName=data[0].AlertName;
                    $scope.UpdateAnnouncementData.Message=data[0].Message;
                    if(data[0].languages_code!="en") {
                        //google_transliteration.makeTransliteratable(['multi_lang_input_announcement_title','multi_lang_input_announcement_desc']);
                        //google_transliteration.setLanguagePair(google.elements.transliteration.LanguageCode.ENGLISH, response[0].languages_code);
                    }
                }
                else
                {
                    logger.logError('description in this language not available');
                }
            });
        }
        else
        {
            logger.logError('Please select a announcement');
        }
    };
    $scope.AddAlert=function(){
        var a=confirm('Do you want to proceed ?');
        if(a==true){
            var formData={
                'AlertName' : $scope.AlertName,
                'AlertMessage':$scope.AlertMessage,
                'AlertStartDate':$scope.AlertStartDate,
                'AlertEndDate':$scope.AlertEndDate
            };
            Admin_CalendarService.addannouncement(formData,function (data) {
                if(data[0].RESULT !=0)
                {
                    $scope.LoadAnnouncement();
                    logger.log('Announcement added successfully');
                    $scope.AlertStartDate=new Date();
                    $scope.AlertEndDate =new Date();
                    $scope.AlertName='' ;
                    $scope.AlertMessage='' ;

                }
                else
                {
                    logger.logError('Something  went wrong !! Please try again');

                }
            });
        }
        else{
            $scope.AlertStartDate=new Date();
            $scope.AlertEndDate=new Date();
            $scope.AlertEndDate.setDate(new Date().getDate()+1);
            $scope.AlertName='' ;
            $scope.AlertMessage='' ;
        }

    };
    $scope.updateAnnouncement=function(index,mode){
        if(index!=undefined){
            $scope.UpdateAnnouncementData=$rootScope.AnnouncementList[index];
            $scope.UpdateAnnouncementData.Mode=mode;
        }else{
            $scope.UpdateAnnouncementData.Mode=mode;
        }
        Admin_CalendarService.updateannouncement($scope.UpdateAnnouncementData,function (data) {
            if(data.affectedRows!=0){
                if(mode=='0'){
                    logger.log('Announcement deleted successfully');
                }
                else{
                    logger.log('Announcement updated successfully');
                }
                $scope.LoadAnnouncement();
                $scope.UpdateAnnouncementName='';
                $scope.UpdateAnnouncementDetail='';
                $scope.UpdateAnnouncementMessage='';
                $scope.UpdateAnnouncementEndDate='';
            }
            else
            {
                logger.logError('Unable to update announcement ! Please try again');
            }
        });
    };


    $scope.loadEvents=function(){
      var formdata = {LanguageID: $rootScope.SessionLanguageID}
        Admin_CalendarService.loadevent(formdata,function (data) {

            if(data.length!=0){
                $rootScope.EventList=data;
                $rootScope.EventListShow=true;
            }
            else{
                $rootScope.EventList='';
                $rootScope.EventListShow=false;
            }

        });
    };
    $rootScope.eventPageLoad=function(){
        $scope.loadEvents();
    };
    $scope.addNewEvent=function(){
        //var a=confirm('Do you want to proceed ?');
		/*
		if($scope.EventName == null || $scope.EventName == undefined || $scope.EventName == "" ){
			logger.log('Please enter Event Name');
		}
		if($scope.Desc == null || $scope.Desc == undefined || $scope.Desc == "" ){
			logger.log('Please enter Description');
		}
		if($scope.StartDate == null || $scope.StartDate == undefined || $scope.StartDate == "" ){
			logger.log('Please enter Start Date');
		}
		if($scope.EndDate == null || $scope.EndDate == undefined || $scope.EndDate == "" ){
			logger.log('Please enter End Date');
		}
		*/
        //if(a==true){

            var startTime=0;
            var EndTime=0;
            if($scope.StartTime!=undefined){startTime=$scope.StartTime};
            if($scope.EndTime!=undefined){EndTime=$scope.EndTime};
            var formData={

                'EventName' : $scope.EventName ,
                'Desc' : $scope.Desc ,
                'StartDate' :$scope.StartDate,
                'EndDate':$scope.EndDate,
                'StartTime':startTime,
                'EndTime':EndTime
            };

            Admin_CalendarService.addevent(formData,function (data) {
                if(data.length!=0){
                    logger.log('Event added successfully');
                    $scope.loadEvents();
				$scope.EventName ='';
				$scope.Desc='' ;
				$scope.StartDate='';
				$scope.EndDate='';
				$scope.StartTime='';
				$scope.EndTime='';

                }
                else
                {
                    logger.logError('Something  went wrong !! Please try again');
                }
            });
		/*
        }
        else{

        }
		*/
    };
    $scope.getEventDataByLanguage=function(){
        if($scope.UpdateEventData!=undefined){
            Admin_CalendarService.geteventdatabylanguage($scope.UpdateEventData,function (data) {
                if(data.length!=0){
                    $scope.UpdateEventData.EventName=response[0].EventName;
                    $scope.UpdateEventData.Desc=response[0].Desc;
                    if(data[0].languages_code!="en") {
                        //google_transliteration.makeTransliteratable(['multi_lang_input_event_title','multi_lang_input_event_desc']);
                        //google_transliteration.setLanguagePair(google.elements.transliteration.LanguageCode.ENGLISH, response[0].languages_code);
                    }
                }
                else
                {
                    logger.logError('event details not available in this language');
                }
            });
        }
        else
        {
            logger.logError('Please select a event');
        }
    };
    $scope.updateEvent=function(index,mode){
        if(index!=undefined){
            $scope.UpdateEventData=$rootScope.EventList[index];
            $scope.UpdateEventData.Mode=mode;
        }else{
            $scope.UpdateEventData.Mode=mode;
        }
        Admin_CalendarService.updateevent($scope.UpdateEventData,function (data) {
            if(data.affectedRows!=0){
                if(mode=='0'){
                    logger.log('Event deleted successfully');
                }
                else{
                    logger.log('Event updated successfully');
                }
                $scope.loadEvents();
                $scope.UpdateEventData='';
            }
            else
            {
                logger.logError('Unable to update event ! Please try again');
            }
        });
    };



    $scope.AddFestival=function(Festival){

        var Data={
            'FestivalName':Festival.Name,
            'FestivalDate':Festival.Date,
            'FestivalDesc':Festival.Desc
        };


        Admin_CalendarService.addfestival(Data,function (data) {

            if(data[0].RESULT !=undefined)
            {
                logger.log('Festival added successfully');

            }
            else
            {
                logger.logError('Something  went wrong !! Please try again');

            }

            Festival.Name ='';
            Festival.Date='' ;
            Festival.Desc='' ;

        });

    };
    $scope.AddSubEvent=function(){

        var formData={

            'EventID':$scope.Event,
            'SubEvent' : $scope.SubEvent,
            'Desc':$scope.Desc,
            'StartTime':$scope.StartTime,
            'EndTime':$scope.EndTime
        };

        $scope.EventID ='';
        $scope.SubEvent='' ;
        $scope.Desc='' ;
        $scope.StartTime ='';
        $scope.EndTime='' ;

        Admin_CalendarService.addsubevent(formData,function (data) {
            if(data[0].RESULT !='0')
            {
                logger.log('Sub Event added successfully');

            }
            else
            {
                logger.logError('Something  went wrong !! Please try again');

            }
        });

    };
    //Admin_PujaService.changepujastatus(formData,function (data) {
    //});
}]);

