
DevRoute.controller('Admin_PujaController',['$http','$location','$scope','$rootScope','Admin_PujaService','Admin_PujaRuleService','adminservice','$filter','$q','logger', function ($http,$location,$scope,$rootScope,Admin_PujaService,Admin_PujaRuleService,adminservice,$filter,$q,logger) {

	//DV203 Puja tag changes
	$scope.dropdownSetting = {
        scrollable: true,
        enableSearch: true,
        scrollableHeight : '400px',
        closeOnBlur: true,
        showCheckAll: false,
        showUncheckAll: false,		
        checkBoxes: false,
        displayProp: 'label', // se colocar id funciona 
        smartButtonMaxItems: 5	
    }
    //fetch data from database for show in multiselect dropdown
    /*$http.get('/home/getcategories').then(function (data) {
        angular.forEach(data.data, function (value, index) {
            $scope.Categories.push({ id: value.CategoryID, label: value.CategoryName });
        });
    })*/

	$scope.pujaTagsModel = [];
	/*
	$scope.tagsList = [
		{"id": 1, "label": "Health" }, 
		{"id": 2, "label": "Wealth" },
		{"id": 3, "label": "Education" },
		{"id": 4, "label": "Lamps" },
		{"id": 5, "label": "Homam" },
		{"id": 6, "label": "Abishekams" },
		{"id": 7, "label": "Thulabaram" },
		{"id": 8, "label": "Archana" }
		];	
	*/

    //masterpujaload
    $scope.masterpuja={
		pujaname:'',
        description:'',	
		type:'pujatype',
		templeid:$rootScope.TempleID
    };
    $scope.addPujaType=function(){
		//DV203 Puja tags. Common puja tags are added thru Dvnm Admin will go with templeid 0, added via Temple admin will go the templeid
        if($scope.pujatypedesc!='') {
			$scope.masterpuja.pujaname = $scope.pujatypedesc;
            adminservice.addpuja($scope.masterpuja, function (data) {
				logger.log('Puja Type added successfully');
				/*
                if (data.affectedRows != 0) { // not sure why this is not having the count
					logger.log('Puja Type added successfully');
					$scope.masterpuja.pujaname = '';
					$scope.masterpuja.description = '';
		
                } else {
                    logger.log('Something went wrong');
                }
				*/
            });
        }
    };
	$scope.populatePujaData = function () {
		//alert ('in here');
		//DV203 puja tags		
		$scope.updatetagsList = [];	
		$scope.updatepujaTagsModel = [];
		$scope.updatetagsList.length = 0;
		$scope.updatepujaTagsModel.length = 0;
		var temptagsList = { id: '', label: '' };
		var temptagid = { id: '' };
		
		//$scope.filter = function($scope.pujaUpdateData) {return ($rootScope.PujaList.indexOf($scope.pujaUpdateData.PujaID) !== -1)};
		
		var str = $scope.pujaUpdateData.currentpujatags;
		$rootScope.PujaTypeValues.forEach(function(pt){					
			temptagsList.id = pt.PujaTypeID;
			temptagsList.label = pt.Type;
			$scope.updatetagsList.push(angular.copy(temptagsList));
			var type = pt.PujaTypeID;
			var check = -1;
			if (str != 0 && str != undefined && str!= 'null' && str != null) // when puja is not assigned to any tag other than 
			{
				check = str.split(',').indexOf(type.toString());
			}			
			if (check >= 0)
			{
				temptagid.id = pt.PujaTypeID;
				$scope.updatepujaTagsModel.push (angular.copy(temptagid));
			}
		})								
	}
	//DV203 - Puja tags
	
	
    $scope.loadMasterPuja=function(){

        Admin_PujaService.loadmasterpuja(function (data) {
            $rootScope.masterPujaList=data;
        });

    };
    $scope.loadMasterDeity=function(){
        $http.post('/admin/master/deity',{LanguageID: $rootScope.SessionLanguageID}).success(function(response) {
            $rootScope.masterDeityList=response;
        });
    };
    $rootScope.LoadDeity=function() {
        Admin_PujaService.loaddeity(function (data) {
            $rootScope.allDeityList=data;
        });
    };
    $rootScope.LoadDeity();
    $scope.loadMasterDeity();
    $scope.loadPuja=function(){
        Admin_PujaService.loadpuja(function (data) {
            $rootScope.PujaList=data;
        });

    };
    $scope.LoadPujaType=function(){
        Admin_PujaService.loadpujatypes(function (data) {
            $rootScope.PujaTypeValues=data;
			//DV203 puja tags
			$scope.tagsList = [];	
			var temptagsList = { id: '', label: '' };	
			$rootScope.PujaTypeValues.forEach(function(pt){
				temptagsList.id = pt.PujaTypeID;
				temptagsList.label = pt.Type;
				$scope.tagsList.push(angular.copy(temptagsList));
			})					
        });
    };
    $scope.LoadStars=function(){
        Admin_PujaService.loadstars(function (data) {
            $rootScope.Stars=data;
        });
    };
    $rootScope.onPujaPageLoad=function(){
        $scope.loadPuja();
        $scope.LoadPujaType();
        $scope.loadMasterPuja();
        $scope.loadMasterDeity();
        $scope.LoadStars();
    };


    $scope.getPujaDataByLanguage=function(){
        var formData= {LanguageID:$scope.pujaUpdateData.LanguageID,PujaID:$scope.pujaUpdateData.PujaID}
        Admin_PujaService.getpujadatebylanguage(formData,function (data) {
            if(data[0].RESULT!=undefined){
                logger.logWarning('no description found for this language');
                //$scope.pujaUpdateData.Desc='';
            }
            else
            {		
				$scope.pujaUpdateData.Desc=data[0].Desc;
                if(data[0].languages_code!="en"){
                    //google_transliteration.makeTransliteratable(['multi_lang_input_puja_desc']);
                    //google_transliteration.setLanguagePair(google.elements.transliteration.LanguageCode.ENGLISH, response[0].languages_code);
                }
            }
        });
    };
    $scope.newPujaId='';

    $scope.GetPujaRuleDetails=function(){
        Admin_PujaRuleService.GetPujaRuleDetails($scope);
    };
    $scope.add_puja=function(){

		var tempPujaTagsString = Array.prototype.map.call($scope.pujaTagsModel, function(item) { return item.id; }).join(",");
        var formData={
            'DeityID'     :$scope.DeityValue.DeityID,
            'MasterPujaID':$scope.pujaData.PujaID,
            'PujaTypeID'    :$scope.PujaType,
            'Amount'      :$scope.Amount,
            'ListIndex'   :$scope.ListIndex,
            'Status'      :$scope.Status,
            'EnableCount'      :$scope.EnableCount,
			'PujaTags':tempPujaTagsString
        };
        Admin_PujaService.addpuja(formData,function (data) {
            logger.log(data[0].RESULT);
            logger.log('puja added successfully');
            if(data[0].RESULT=='New puja added'&&$rootScope.isPriest) {
               $scope.newPujaId=data[1][1][0].PujaID;
                $scope.generltab('updatepujarulePriest');
            };
            $scope.loadPuja();
            $scope.DeityValue='';
            $scope.pujaData='';
            $scope.PujaType ='';
            $scope.Amount ='';
            $scope.ListIndex ='';
            $scope.Status=1;
            $scope.EnableCount=0;
        });
    };
    $scope.generltab=function(mode) {
        var priestMode=false;
        if(mode=='updatepujarulePriest'){
            priestMode=true;
            mode='updatepujarule';
        }
        $scope.generaldata = {
            selectedmode: mode,
            facebookpage: '',
            generaldata: '',
            selectedtemplate: '',
            pujaid: $scope.newPujaId,
            bookingfromdate: '',
            bookingtodate: '',
            onlinebookingstatus: '',
            titleimagepath:'',
            imagetitle:'',
            imagedesc:'',
            imagetype:'1',
            prayer:'',
            addtype:'',
            addimage:'',
            addimageformail:'',
            addimageformailpath:'',
            addimagepath:'',
            addname:'',
            addlink:'',
            selectedtemple:-2,
            selectedcontentid:-2,
            addexpirydate:'',
            selectedkey:'',
            templeconfigvalue:"''",
            enablereminder:"",
            enableautosettlement:"",
            enablestatusupdate:""
        }
        if(priestMode==true){
            $scope.generaldata.onlinebookingstatus=1;
        }
        $scope.formdata=[];
        $scope.formdata.push($scope.generaldata);
        var data = {data: $scope.formdata};
        var x2js = new X2JS();
        var registerdata = "<root>" + x2js.json2xml_str(data) + "</root>";
        $scope.generaldata.generaldata=registerdata;
        Admin_PujaService.generaltab($scope.generaldata,function(data) {
            if (data.length != 0) {
                if (mode == 'updatefacebook') {
                    logger.log('Facebook page updated');
                    $scope.generaltab('loadfacebook');
                }
            }
        });
    }
    $scope.AddPujaRule=function(){
        Admin_PujaRuleService.AddPujaRule($scope);
    };

    $scope.LoadPujaDetails=function(){
        Admin_PujaService. LoadPujaDetails($scope);
    };
    $scope.AddSubPuja=function(){

        var formData={
            'PujaID':$scope.PujaID,
            'SubTypeName' : $scope.SubTypeName
        };
        $scope.PujaID ='';
        $scope.SubTypeName='' ;
        Admin_PujaService.changepujastatus(formData,function (data) {
        });
    };
    $scope.puja_update=function(){
		var tempPujaTagsString = Array.prototype.map.call($scope.updatepujaTagsModel, function(item) { return item.id; }).join(",");
		$scope.pujaUpdateData.PujaTags = tempPujaTagsString;
        Admin_PujaService.pujaupdate($scope.pujaUpdateData,function (data) {
            logger.log(data[0].RESULT);
            $scope.loadPuja();
			// DV199 - 16-Mar-2020 clear update scope details after save, otherwise we select another puja, data is not getting populated 
			$scope.pujaUpdateData = null;
        });
    };
    $scope.PujaRuleUpdate=function(){
        Admin_PujaRuleService.PujaRuleUpdate($scope);
    };
    $scope.Block=function(){
        Admin_PujaRuleService.Block($scope);
    };
    $scope.Exclusion=function(){
        Admin_PujaRuleService.Exclusion($scope);
    };
    $scope.ExclusionUpdate=function(){
        Admin_PujaRuleService.ExclusionUpdate($scope);
    };
    $scope.GetExclusionDetails=function(){
        Admin_PujaRuleService.GetExclusionDetails($scope);
    };
    $scope.ListPuja=function(){
        Admin_PujaService.ListPuja($scope);
    };

    $scope.ChangePujaStatus=function(puja,newStatus){
        var formData = {
            'PujaID':puja.PujaID,
            'DeityID':puja.DeityID,
            'LanguageID':puja.LanguageID,
            'Desc' : puja.Desc,
            'Amount' : puja.Amount,
            'Status':newStatus
        };
        Admin_PujaService.changepujastatus(formData,function (data) {
            logger.log(data[0].RESULT);
            $scope.loadPuja();

        });
    };



    $scope.load_packages=function(){
        var x2js=new X2JS();
        var t='load_active_puja_packages';
        var temp={type:t,data:{TempleID:$rootScope.TempleID}};
        var search_data_xml="<root>"+x2js.json2xml_str(temp)+"</root>";
        var formdata={type:t,data:search_data_xml};
        Admin_PujaService.load_packages(formdata, function (response) {

            $rootScope.puja_package_list=response[0];
            if(response[0].length==0){
                $rootScope.puja_package_list=[]
            }
        });

    };

    $scope.load_package_details_by_id=function(p){
        var x2js=new X2JS();
        var t ='load_package_details_by_id';
        var temp={type:t,data:{PackageID: p.PackageID}};
        var search_data_xml="<root>"+x2js.json2xml_str(temp)+"</root>";
        var formdata={type:t,data:search_data_xml};
        Admin_PujaService.load_packages(formdata, function (response) {
            //$rootScope.selected_package=response[0][0];
            p.pujas=response[1];
            p.open=!p.open;

        });

    };

    $scope.generate_puja_package_booking_report=function(reports){
        reports.from=$rootScope.date2Format(reports.from);
        reports.to=$rootScope.date2Format(reports.to);
        var x2js=new X2JS();
        var t ='puja_package_booking_report';
        var temp={type:t,data:reports};
        var search_data_xml="<root>"+x2js.json2xml_str(temp)+"</root>";
        var formdata={type:t,data:search_data_xml};
        Admin_PujaService.load_packages(formdata, function (response) {
            if(response[0].length==0){
                $rootScope.puja_package_report=[]
            }
            else
            {
                $rootScope.puja_package_report=response[0];
                $rootScope.puja_package_report_pujas=response[1];
                $rootScope.puja_package_report.forEach(function(pk){
                    pk.pujas=[];
                    $rootScope.puja_package_report_pujas.forEach(function(p){
                        if(pk.TrackingNo== p.TrackingNo){
                            pk.pujas.push(angular.copy(p));
                        }
                    })
                });

            }
        });


    };

    $rootScope.get_sum_of_key_for_array=function(data,key){
        var total = 0;
        var count = 0;

        for(count=0;count<data.length;count++){
            total += data[count][key];
        }
        return total;
    };
    $rootScope.show_in_amount=function(amount){
        return parseFloat(amount).toFixed(2)
    };


}]);

DevRoute.controller('Admin_PujaPackageController',['$http','$location','$scope','$rootScope','Admin_PujaService','Admin_PujaRuleService','$filter','$q','logger', function ($http,$location,$scope,$rootScope,Admin_PujaService,Admin_PujaRuleService,$filter,$q,logger) {


}]);