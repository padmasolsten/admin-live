// <copyright file="Admin_DeityController.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Deity Functions </summary>

'use strict';

DevRoute.controller('Admin_DeityController',['$http','$rootScope','$scope','Admin_DeityService','logger', function ($http,$rootScope,$scope,Admin_DeityService,logger) {
    $scope.loadMasterDeity=function(){
        Admin_DeityService.loadmasterdeity(function (data) {
            $rootScope.masterDeityList = data;
        });
    };
    $scope.removedeity=function(puja,newstatus){
        var formData = {
            'DeityID':puja.DeityID,
            'TempleID':puja.TempleID,
            'NewStatus':newstatus
        };
        Admin_DeityService.removedeity(formData,function (data) {
            if (data.RESULT="1"){
                $scope.LoadDeity();
                logger.log("deity removed successfully");
            }
            else
            {
                logger.log("user operation failed");
            }
        });
    };
    $scope.AddDeity=function() {
        $scope.newdeity=true;
        if($scope.Category==undefined || $scope.Category==""){
            logger.logError("Select a category from list");
        }else {
            $rootScope.allDeityList.forEach(function(row){
                if(row.DeityName==$scope.DeityData.DeityName){
                    $scope.newdeity=false;
                }
            })
            if($scope.newdeity==true) {
                var formData={
                    'DeityMasterID':$scope.DeityData.DeityID,
                    'Category' : $scope.Category
                };
                $scope.DeityData ='';
                $scope.Category='' ;
                Admin_DeityService.adddeity(formData,function (data) {

                    if(data.affectedRows==undefined){
                        logger.logSuccess("failed to add new deity");
                    }
                    else
                    {
                        logger.logSuccess("successfully added new deity");
                        $rootScope.LoadDeity();
                    }
                });
            }else{
                logger.logError("Deity already exists");
            }
        }
    };
    $rootScope.LoadDeity=function() {
        Admin_DeityService.loaddeity(function (data) {
            $rootScope.allDeityList=data;
        });
    };
    $rootScope.LoadDeity();
}]);

