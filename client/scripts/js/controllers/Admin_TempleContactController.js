// <copyright file="Admin_TempleContactController.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Admin_TempleContact Functions </summary>

'use strict';

DevRoute.controller('Admin_TempleContactController',['$scope','Admin_TempleContactService','logger', function ($scope,Admin_TempleContactService,logger) {
    Admin_TempleContactService.loaddesignation(function (data) {
        $scope.DesignationValues=data;
    });
    $scope.AddContact=function(){
        var formData = {

            'TempleID':$scope.TempleID,
            'TitleID':$scope.TitleID,
            'ContactName' : $scope.ContactName,
            'HouseNumber' : $scope.HouseNumber,
            'HouseName' : $scope.HouseName ,
            'StreetName' :$scope.StreetName ,
            'PostOffice' : $scope.PostOffice ,
            'Email' : $scope.Email ,
            'State' : $scope.State,
            'District' : $scope.District,
            'PIN':$scope.PIN,
            'PhoneWired':$scope.PhoneWired,
            'PhoneMobile':$scope.PhoneMobile,
            'Status' : 1
        };

        Admin_TempleContactService.addcontact(formData,function (data) {
            if(data[0].RESULT !=0)
            {
                logger.logSuccess('New temple contact  added successfully');
                $scope.Title ='';
                $scope.ContactName ='';
                $scope.HouseNumber='' ;
                $scope.HouseName='' ;
                $scope.StreetName='' ;
                $scope.PostOffice='' ;
                $scope.Email='' ;
                $scope.State='' ;
                $scope.District='' ;
                $scope.PIN='' ;
                $scope.PhoneWired='' ;
                $scope.PhoneMobile='' ;
                $scope.Status='' ;

				//DV-115 - Refresh contacts grid after Add contact
                $scope.listcontacts({mode:''},'load');
            }
            else
            {
                logger.logError('Something  went wrong !! Please try again');

            }

        });
    }

    $scope.listcontacts=function(data,mode){
        data.mode=mode;
        Admin_TempleContactService.listcontact(data,function (data) {
            $scope.templecontacts=data;
            if(mode=='delete'){
                logger.log('Contact Deleted');
                $scope.listcontacts({mode:''},'load');
            }
            $scope.templecontacts.forEach(function(user){
                $scope.DesignationValues.forEach(function(row){
                    if(row.TitleID==user.Title){
                        user.designation=row.Title;
                    }
                });

            })
        });
    }

    $scope.listcontacts({mode:''},'load');
}]);





