DevRoute.controller('FranchiseeDashboardController', ['$http','$scope','FranchiseeDashboardService','$location','$rootScope','$modal','logger', function ($http,$scope,FranchiseeDashboardService,$location,$rootScope,$modal,logger){
    $scope.reportDates={from:'',to:'',three:''};
    $scope.reportDates.to=new Date(new Date().getTime()+3*24*60*60*1000);
    $scope.reportDates.from=new Date(new Date());

    $scope.open = function($event,index) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened=new Array();
        return $scope.opened[index] = true;
    };
    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1
    };
    $scope.printpujareport=function(type,value,temple){
        FranchiseeDashboardService.printpujareport(type,value,temple);
    };
    $scope.smsstatus="Send SMS";
    $scope.smsresendstatus="Resend SMS";
    $scope.currentdate=new Date();
    $scope.previousdate=new Date();
    $scope.currentdate=$scope.currentdate+"";
    $scope.previousdate.setDate(new Date().getDate()+1);
    $scope.previousdate=$scope.previousdate+"";
    $scope.currentdate=$rootScope.date2Format($scope.currentdate);
    $scope.previousdate=$rootScope.date2Format($scope.previousdate);

    $scope.sendsms=function(puja,type){
        puja.smsstatus="Sending";
        puja.smsresendstatus="Sending";
        puja.PujaDate=$rootScope.date2Format(puja.PujaDate);
        FranchiseeDashboardService.sendsms(puja,function(data){
            logger.log(data);
            $scope.Showfranchiseereport(type,$scope.reportDates);
        });
    }
    $scope.Showfranchiseereport=function(type,reportdates){
        $scope.reporttype=type;
        var initdatefrom, initdateto;
        initdatefrom=$rootScope.date2Format($scope.reportDates.from);
        initdateto=$rootScope.date2Format($scope.reportDates.to);
        $scope.reportDates.from=initdatefrom;
        $scope.reportDates.to=initdateto;
        FranchiseeDashboardService.getpujalist($scope,type,reportdates);
    };
    $scope.Showfranchiseereport(1,$scope.reportDates);
    var initdatefrom, initdateto;
    initdatefrom=new Date();
    initdateto=new Date();
    initdateto.setDate(initdateto.getDate()+10);
    initdatefrom=$rootScope.date2Format(initdatefrom);
    initdateto=$rootScope.date2Format(initdateto);
    //$scope.reportDates.from=initdatefrom;
    //$scope.reportDates.to=initdateto;
    $scope.Showfranchiseereport(1,$scope.reportDates);
}]);