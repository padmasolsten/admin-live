// <copyright file="Registration_TempleHistoryController.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Registration Temple History Functions </summary>

'use strict';

DevRoute.controller('Admin_TempleHistoryController',['$http','$scope','$rootScope','Admin_TempleHistoryService','logger', function ($http,$scope,$rootScope,Admin_TempleHistoryService,logger) {
    $scope.historydata={
        "CategoryName":'',
        "DisplaySequence":'',
        "Description" : ''
    };
    var formData={
        "CategoryName":$scope.about_CategoryName,
        "DisplaySequence":$scope.about_DisplaySequence,
        "Description" : $scope.about_Description
    };
    $scope.loadHistory=function(){
        Admin_TempleHistoryService.loadtemplehistory(function (data) {
            if(data.length!=0){
                $scope.TempleAbout=data;
                $scope.TempleAboutCategories = _.groupBy(data,function(Element){
                    return Element.CategoryName
                });
            }
            else{
                $scope.TempleAboutCategories='';
            }
        });
    };
    $scope.loadTempleRule=function() {
        Admin_TempleHistoryService.loadtemplerule(function (data) {
            if (data.length != 0) {
                $scope.templeRules = data;
            }
            else {
                $scope.templeRules = '';
            }
        });
    };
    $scope.loadshippingcharge=function(){

        Admin_TempleHistoryService.loadshippingcharge(function (data) {
            if(data.length!=0){
                $scope.templeshippingcharges=data;
            }
            else
            {
                $scope.templeshippingcharges='';
            }

        });
    };
    $scope.setpujadata=function(data){
        $scope.generaldata.pujaid=data.PujaID;
    }
    $scope.creategeneraldata=function() {
        $scope.generaldata = {
            selectedmode: '',
            facebookpage: '',
            generaldata: '',
            selectedtemplate: '',
            pujaid: '',
            bookingfromdate: '',
            bookingtodate: '',
            onlinebookingstatus: 0,
            titleimagepath:'',
            imagetitle:'',
            imagedesc:'',
            imagetype:'1',
            prayer:'',
            addtype:'',
            addimage:'',
            addimageformail:'',
            addimageformailpath:'',
            addimagepath:'',
            addname:'',
            addlink:'',
            selectedtemple:-2,
            selectedcontentid:-2,
            addexpirydate:'',
            selectedkey:'',
            templeconfigvalue:"''",
            enablereminder:"",
            enableautosettlement:"",
            enablestatusupdate:""
        }
    }
    //$scope.addimage='';
    //$scope.addimageformail='';
    $scope.creategeneraldata();
    $scope.onchangetemplename=function(selected){
        $scope.generaldata.selectedtemple = selected.TempleID;
    }
    $scope.onchangelocalinfoplace=function(selected){
        $scope.generaldata.selectedkey=selected.Key;
        try {
            $scope.businesses = JSON.parse(selected.Value);
        }catch(err) {
            $scope.businesses=[];
        }
        $scope.selectedbusinessid=$scope.businesses.length;
        $scope.businesses[$scope.selectedbusinessid]={
            "group":"",
            "lob":"",
            "businessname":"",
            "address":"",
            "phone":'',
            "email":"",
            "contact":"",
            "tagline":"",
            "website":"",
            "qualifier":"standard",
            "status":"trial",
            "services1":"",
            "services2":"",
            "services3":""
        }
    }
    $scope.onchangebusinessname=function(selected){
        //alert(JSON.stringify($scope.businesses))
        var count=0;
        $scope.selectedbusinessid=$scope.businesses.length-1;
        $scope.businesses.forEach(function(row){
            if(selected.businessname==row.businessname && selected.phone==row.phone)
            {
                $scope.selectedbusinessid= count;
            }
            count++;
        });
        if( $scope.selectedbusinessid==$scope.businesses.length-1){
            $scope.businesses[$scope.businesses.length-1].businessname=selected;
        }
    }
    $scope.onchangeaddname=function(selected){
        $scope.generaldata.selectedcontentid=selected.ContentID;
    }
    $scope.templelists =[];
    $scope.categorylist =[{name:'Temple',value:'temple'},{name:'Priest',value:'priest'}];
    $scope.advertisements =[];
    $scope.editadds=function(){
        $scope.editadd=!$scope.editadd;
    }
    $scope.editadd=false;
    $scope.advertisementsassigned =[];
        $scope.generaltab=function(mode){
            if(mode=='updatelocalinfo'){
                if($scope.generaldata.selectedkey==""){
                    logger.logError('Select Place and Business again');
                    return ;
                }
                if($scope.businesses[$scope.businesses.length-1].group==""||$scope.businesses[$scope.businesses.length-1].lob=="")
                {
                    $scope.businesses.pop();
                }else {
                    if ($scope.businesses[$scope.businesses.length - 1].businessname==""){
                        logger.logError('Enter business name');
                        return ;
                    }
                }
                $scope.generaldata.templeconfigvalue=JSON.stringify($scope.businesses);
            }
        var validationstatus=true;
        if(mode=='uploadaddimage'){
             validationstatus=false;
            var file = $scope.addimage;
            var uploadUrl = "/uploadgallery";
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl,fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function(res) {
                $scope.path = res.path;
                $scope.path = $scope.path.replace(/\W+/g, '/');
                $scope.path = $scope.path.substring(16, $scope.path.length)
                $scope.path = $scope.path.substring(0, $scope.path.lastIndexOf('/')) + '.' + $scope.path.substring($scope.path.lastIndexOf('/') + 1, $scope.path.length);
                $scope.path = $scope.path.substring(1, $scope.path.length)
                $scope.generaldata.addimagepath=$scope.path;
                $scope.generaltab('uploadmailimage');
            });
            }
        if(mode=='uploadmailimage'){
            validationstatus=false;
            var file = $scope.addimageformail;
            var uploadUrl = "/uploadgallery";
            var fd = new FormData();
            fd.append('file', file);
            $http.post(uploadUrl,fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function(res) {
                $scope.path = res.path;
                $scope.path = $scope.path.replace(/\W+/g, '/');
                $scope.path = $scope.path.substring(16, $scope.path.length)
                $scope.path = $scope.path.substring(0, $scope.path.lastIndexOf('/')) + '.' + $scope.path.substring($scope.path.lastIndexOf('/') + 1, $scope.path.length);
                $scope.path = $scope.path.substring(1, $scope.path.length)
                $scope.generaldata.addimageformailpath=$scope.path;
                $scope.formdata=[];
                $scope.formdata.push($scope.generaldata);
                var data = {data: $scope.formdata};
                var x2js = new X2JS();
                var registerdata = "<root>" + x2js.json2xml_str(data) + "</root>";
                $scope.generaldata.generaldata=registerdata;
                Admin_TempleHistoryService.generaltab($scope.generaldata, function (data) {
                    if (data.length != 0) {
                        logger.log('image uploaded');
                        $scope.generaltab('loadtemples');
                        window.location.reload();
                        $scope.creategeneraldata();
                    }
                });
            });
        }
        $scope.generaldata.selectedmode=mode;

        if(validationstatus) {
            $scope.formdata=[];
            $scope.formdata.push($scope.generaldata);
            var data = {data: $scope.formdata};
            var x2js = new X2JS();
            var registerdata = "<root>" + x2js.json2xml_str(data) + "</root>";
            $scope.generaldata.generaldata=registerdata;
            if(mode=='deleteadvertisements'||mode=='updateadvertisements'||mode=='assignadvertisements'){
                if($scope.generaldata.selectedtemple==-2||$scope.generaldata.selectedcontentid==-2){
                    logger.logError('Select fields again ');
                    return ;
                }
            }
            Admin_TempleHistoryService.generaltab($scope.generaldata, function (data) {
                if (data.length != 0) {
                    if (mode == 'updatefacebook') {
                        logger.log('Facebook page updated');
                        $scope.generaltab('loadfacebook');
                    }
                    if (mode == 'loadfacebook') {
                        $scope.generaldata.facebookpage = data[0][0].Description;
                    }
                    if (mode == 'loadtemplate') {
                        $scope.templatelist = data[0];

                    }
                    if (mode == 'updatetemplate') {
                        logger.log('Website template changed');
                        //$scope.generaltab('loadtemplate');
                    }
                    if (mode == 'loadaddress') {
                        $scope.generaldata.housename = data[0][0].HouseName;
                        $scope.generaldata.streetname = data[0][0].StreetName;
                        $scope.generaldata.postoffice = data[0][0].PostOffice;
                        $scope.generaldata.postalcode = data[0][0].PostalCode;
                        $scope.generaldata.state = data[0][0].State;
                        $scope.generaldata.district = data[0][0].District;
                        $scope.generaldata.country = data[0][0].Country;
                        $scope.generaldata.phonemobile = data[0][0].PhoneMobile;
                        $scope.generaldata.phonewired = data[0][0].PhoneWired;
                        $scope.generaldata.email = data[0][0].Email;

                        $scope.generaldata.airport = data[1][0].NearestAirPort;
                        $scope.generaldata.busstand = data[1][0].NearestBusStation;
                        $scope.generaldata.railwaystation = data[1][0].NearestRailwayStation;
                        $scope.generaldata.route = data[1][0].Route;
                    }
                    if (mode == 'updateaddress') {
                        logger.log('Updated Successfully');
                        $scope.generaltab('loadaddress');
                    }
                    if (mode == 'updatepujarule') {
                        logger.log('Updated Successfully');
                        $scope.creategeneraldata();
                        window.location.reload();
                    }
                    if(mode=='loadprayer'){
                        $scope.generaldata.prayer=data[0][0].MessageBody;
                    }
                    if(mode=='updateprayer'){
                        logger.log('Updated Successfully');
                        $scope.generaltab('loadprayer');
                    }

                    if(mode=='loadadvertisements'){
                        $scope.advertisements = data[0];
                        //window.location.reload();
                        $scope.generaltab('loadadvertisementsassigned');
                    }
                    if(mode=='loadadvertisementsassigned'){
                        $scope.advertisementsassigned = data[0];
                    }
                    if(mode=='updateadvertisements'){
                        logger.log('Updated')
                        $scope.generaltab('loadtemples');
                        $scope.editadd=false;
                        $scope.creategeneraldata();
                    }
                    if(mode=='deleteadvertisements'){
                        logger.log('Deleted');
                        $scope.generaltab('loadtemples');
                        $scope.creategeneraldata();
                    }
                    if(mode=='loadtemples'){
                        $scope.generaltab('loadadvertisements');
                        $scope.templelists=data[0];
                        var newlist=new Array();
                        newlist.push({'TempleID':-1,'TempleName':'All'});
                        $scope.templelists.forEach(function(row){
                           if(row.TempleID!=0){
                               newlist.push(row);
                           }
                        });
                        $scope.templelists=newlist;
                    }
                    if(mode=='assignadvertisements'){
                       logger.log('Success');
                        $scope.selectedadd='';
                        $scope.selectedtemple='';
                        $scope.generaltab('loadtemples');
                        $scope.creategeneraldata();
                    }
                    if(mode=='loadtemplesforlocalinfo'){
                        $scope.templelists=data[0];
                        var newlist=new Array();
                        //newlist.push({'TempleID':-1,'TempleName':'All'});
                        $scope.templelists.forEach(function(row){
                            if(row.TempleID!=0){
                                newlist.push(row);
                            }
                        });
                        $scope.templelists=newlist;
                        $scope.generaltab('loadlocalinfo');
                    }
                    if(mode=='loadlocalinfo'){
                        $scope.localinfolist=data[0];
                    }
                    if(mode=='addlocalinfo'){
                        if(data.affectedRows!=0) {
                            $scope.generaltab('loadlocalinfo');
                            logger.log('New place added');
                        }
                    }
                    if(mode=='updatelocalinfo'){
                        if(data.affectedRows!=0) {
                            $scope.generaldata.selectedkey="";
                            $scope.generaltab('loadlocalinfo');
                            logger.log('Local info updated');
                        }
                    }
                    if(mode=='assignlocalinfo'){
                        if(data.affectedRows!=0) {
                            $scope.generaltab('loadlocalinfo');
                            logger.log('Local info assigned');
                        }
                    }
                    if(mode=='checkreminder') {
                        if (data[0][0]) {
							if(data[0][0].Value=='true'){
								$scope.generaldata.enablereminder=true;
							}else{
								$scope.generaldata.enablereminder=false;
							}
						}
                        if (data[1][0]) {						
							if(data[1][0].Value=='true'){
								$scope.generaldata.enablestatusupdate=true;
							}else{
								$scope.generaldata.enablestatusupdate=false;
							}
						}
                    }                    if(mode=='checkautosettlement') {
                        $scope.generaldata.settlementmails=data[0][0].Description;
                        if(data[0][0].Value=='true'){
                            $scope.generaldata.enableautosettlement=true;
                        }else{
                            $scope.generaldata.enableautosettlement=false;
                        }
                    }
                    if(mode=='updatemailaddress') {
                        $scope.generaltab('checkautosettlement');
                        logger.log('Mail address updated');
                    }
                }
                else {
                    $scope.facebook.facebookpage = '';
                    logger.log('Something went wrong!');
                }

            });
        }
    };

    //DV-41-SAIF
    //Messages for users in payment page

	// DV98 -- Temple Puja Links -- start --
    $scope.loadTemplePujaLinks=function(){
		var templedata = {
			"TempleName": $scope.templeNameSearch,
			"LanguageID": '1'
		};
        Admin_TempleHistoryService.loadTemplePujaLinks(templedata, function (data) {
            if(data.length!=0){
                $scope.templepujalinks=data[0];
            }
            else{
                $scope.templepujalinks='';
            }
        });
    };	
	//$scope.loadTemplePujaLinks();
    $scope.updateTemplePujaLink=function(pujalink,currStatus){		
		var newStatus;
		if (currStatus == 0) {newStatus = 1;}
		else {newStatus = 0;}
        var formData = {
			'DeityID':pujalink.DeityID,
			'PujaTypeID':pujalink.PujaTypeID,
            'PujaID':pujalink.PujaID,
            'TempleID':pujalink.TempleID,
            'Status':newStatus
        };
        Admin_TempleHistoryService.updateTemplePujaLink(formData,function (data) {
				if(data[0].RESULT !=0) {
					logger.log('Temple Puja Link updated');					
				}
				else{
					logger.logError('Something went wrong !! Please try again');
				}	
			});
    };
    $scope.listTemples=function(){
		var templedata = {
			"LanguageID": '1'
		};
        Admin_TempleHistoryService.listTemples(templedata, function (data) {
            if(data.length!=0){
                $scope.listtemples=data[0];
            }
            else{
                $scope.listtemples='';
            }	
        });
	};	
	$scope.listTemples();
	
    $scope.setPujaLinkTempledata=function(data){
        $scope.pujalinkTempleID=data.TempleID;	
		$scope.templeSite=data.WebSite;	
		$scope.listTemplePujas();	
    };
    $scope.setPujalinkPujadata=function(data){
        $scope.pujalinkDeityID=data.DeityID;
		$scope.pujalinkPujaTypeID=data.PujaTypeID;
		$scope.pujalinkPujaID=data.PujaID;
		$scope.pujalinkPujaName=data.PujaName;
    };
	$scope.clearAddPujaLink = function() {
		$scope.pujalinkTempleID="";
		$scope.pujalinkDeityID="";
		$scope.pujalinkPujaTypeID="";
		$scope.pujalinkPujaID="";
		$scope.templeSite="";
		$scope.pujalinkPujaName="";
		$scope.pujaLinkTabTempleData="";
		$scope.pujaLinkTabPujaData="";
	};
	$scope.createTemplePujaLink=function() {
		if ($scope.pujalinkTempleID !="" || $scope.pujalinkPujaID !="")		
		{
			var pujaName = $scope.pujalinkPujaName;
			//var pujalink = $scope.templeSite + "/puja/" + pujaName.replace(" ", "_");		
			var pujalink = $scope.templeSite + "/puja/" + pujaName.replace(/ /g, "_");					
			var templedata = {			
				"LanguageID": '1',
				'TempleID':$scope.pujalinkTempleID,
				'PujaTypeID':$scope.pujalinkPujaTypeID,
				'DeityID':$scope.pujalinkDeityID,				
				'PujaID':$scope.pujalinkPujaID,
				'PujaLink':pujalink
			};
			Admin_TempleHistoryService.createTemplePujaLink(templedata, function (data) {
				if(data[0].RESULT !=0) {
					$scope.clearAddPujaLink();
					logger.log('Temple Puja Link added successfully');
				}
				else{
					$scope.clearAddPujaLink();
					logger.logError('Something went wrong !! Please try again');
				}	
			});
		}
		else {
			logger.log('Please select Temple and Puja to Link');
		}
	}	

    $scope.listTemplePujas=function(){
		var templedata = {			
			"LanguageID": '1',
            'TempleID':$scope.pujalinkTempleID		
		};
        Admin_TempleHistoryService.listTemplePujas(templedata, function (data) {
            if(data.length!=0){
                $scope.listtemplepujas=data[0];
            }
            else{
                $scope.listtemplepujas='';
            }	
        });
    };		
	// DV98 -- Temple Puja Links -- end --	

    $scope.open = function($event,mode) {
        $event.preventDefault();
        $event.stopPropagation();
        if(mode=='start'){
            $scope.opened2 = false;
            return $scope.opened1 = true;
        }
        else{
            $scope.opened1 = false;
            return $scope.opened2 = true;
        }

    };
    $scope.dateOptions = {
        'year-format': "'yy'",
        'starting-day': 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.chooseCategory=true;
    $scope.pg_msg_temple_selected = function(t) {
        if(t.TempleID!==0&&t.TempleID!==-1) {
            $scope.chooseCategory=false;
        } else {
            $scope.chooseCategory=true;
        }
        if(t.TempleID){
            $scope.pgmessages.TempleID=t.TempleID;
        }
    }
    $scope.pg_msg_category_selected = function(t) {
        if(t.value){
            $scope.pgmessages.Category=t.value;
        }
    }

    $scope.manage_pg_msg= function(mode,data) {
        
        var x2js=new X2JS();
        switch(mode) {
            case 'load_all_pg_msg':
                var temp={type:mode,data:data};
                var search_data_xml="<root>"+x2js.json2xml_str(temp)+"</root>";
                var formdata={data:search_data_xml};
                Admin_TempleHistoryService.manage_pg_messages(formdata, function (response) {
                    if(response[0].RESULT)
                    {
                        logger.logError("Something went wrong,unable to load  PG  Message,Please check your inputs !!");
                    }
                    else
                    {
                        //logger.logSuccess('  PG Message list loaded !');
                        $scope.pg_msg_list=response[0];
                    }
                });
                break;
            case 'add_pg_msg':
                data.Start_DateTime=''+$rootScope.date2Format(data.Start_DateTime,'yyyy-mm-dd');
                data.Expiry_DateTime=''+$rootScope.date2Format(data.Expiry_DateTime,'yyyy-mm-dd');
                if(data.TempleID==-1){data.TempleID=0;}
                if($scope.chooseCategory==true&&(data.Category==null||data.Category==undefined||data.Category=='')){
                    logger.log('Choose Category!');
                    return;
                }
                var temp={type:mode,data:data};
                var search_data_xml="<root>"+x2js.json2xml_str(temp)+"</root>";
                var formdata={data:search_data_xml};
                Admin_TempleHistoryService.manage_pg_messages(formdata, function (response) {
                    if(response.affectedRows)
                    {
                        logger.logSuccess(' new PG Message added !');
                        $scope.load_pg_msg_page();
                    }
                    else
                    {
                        logger.logError("Something went wrong,unable to add new PG  Message,Please check your inputs !!");
                    }
                });

                break;
            case 'edit_pg_msg':
                    $scope.pgmessages=data;
                    $scope.msg_paytm=!$scope.msg_paytm;
                  break;

            case 'update_pg_msg':
                data.Start_DateTime=''+$rootScope.date2Format(data.Start_DateTime,'yyyy-mm-dd');
                data.Expiry_DateTime=''+$rootScope.date2Format(data.Expiry_DateTime,'yyyy-mm-dd');
                if(data.TempleID==-1){data.TempleID=0;}
                var temp={type:mode,data:data};
                var search_data_xml="<root>"+x2js.json2xml_str(temp)+"</root>";
                var formdata={data:search_data_xml};
                Admin_TempleHistoryService.manage_pg_messages(formdata, function (response) {
                    if(response.affectedRows)
                    {
                        logger.logSuccess(' PG Message updated !');
                        $scope.load_pg_msg_page();
                    }
                    else
                    {
                        logger.logError("Something went wrong,unable to update new PG  Message,Please check your inputs !!");
                    }
                });

                break;
            case 'delete_pg_msg':
                data.Start_DateTime=''+$rootScope.date2Format(data.Start_DateTime,'yyyy-mm-dd');
                data.Expiry_DateTime=''+$rootScope.date2Format(data.Expiry_DateTime,'yyyy-mm-dd');
                if(data.TempleID==-1){data.TempleID=0;}
                var temp={type:mode,data:data};
                var search_data_xml="<root>"+x2js.json2xml_str(temp)+"</root>";
                var formdata={data:search_data_xml};
                Admin_TempleHistoryService.manage_pg_messages(formdata, function (response) {
                    if(response.affectedRows)
                    {
                        logger.logSuccess(' PG Message Deleted !');
                        $scope.load_pg_msg_page();
                    }
                    else
                    {
                        logger.logError("Something went wrong,unable to delete  PG  Message,Please check your inputs !!");
                    }
                });

                break;



            default:

                alert('No case found for this method.')
        }

    }

    $scope.load_pg_msg_page = function() {
        $scope.pgmessages={
            TempleID:-1,
            IndexID:1,
            LanguageID:1,
            PGID:1,
            Message:"",
            Created_DateTime:new Date(),
            Start_DateTime:new Date(),
            Expiry_DateTime:new Date(),
            Status:1
        }
        $scope.pgmessages_Temple={TempleName:'All Temples',TempleID:0};
        $scope.manage_pg_msg('load_all_pg_msg',$scope.pgmessages_Temple);
    }

    //END DV-41-SAIF



    $scope.loadPujaTime=function(){

        Admin_TempleHistoryService.loadpujatime(function (data) {
            if(data.length!=0){
                $scope.templePujaSchedule=data;
            }
            else
            {
                $scope.templePujaSchedule='';
            }

        });
    };

    $scope.loadPujaTime=function(){

        Admin_TempleHistoryService.loadpujatime(function (data) {
            if(data.length!=0){
                $scope.templePujaSchedule=data;
            }
            else
            {
                $scope.templePujaSchedule='';
            }

        });
    };
    $scope.LoadImages=function(){
        Admin_TempleHistoryService.loadimages(function (data) {
            $scope.TempleImages= data;
        });
    };
    $rootScope.loadAboutPage=function(){
        $scope.loadHistory();
        $scope.loadTempleRule();
        $scope.LoadImages();
        $scope.loadPujaTime();
        $scope.loadshippingcharge();
    };
    $scope.AddHistory=function(){
        $scope.about_Description="";
        Admin_TempleHistoryService.addhistory($scope,function (data) {
            if(data[0].RESULT !=0)
            {
                logger.log('History  added successfully');
                $scope.historydata.Description="";
                $scope.loadHistory();
            }
            else
            {
                logger.logError('Something  went wrong !! Please try again');

            }
        });
    };
    $scope.UpdateHistory=function(htmldata,status){
        var formData={
            "CategoryName" :htmldata.CategoryName,
            "PlaceHolderID":htmldata.PlaceHolderID,
            "Description":htmldata.Description,
            "LanguageID":htmldata.LanguageID,
            "Status" :status
        };
        Admin_TempleHistoryService.updatehistory(formData,function (data) {
            if(data[0].RESULT !=0)
            {
                data.isCollapsed=false;
                if(status=='1')
                {
                    logger.logSuccess(htmldata.CategoryName+'  deleted successfully');
                    $scope.loadHistory();
                }
                else
                {
                    logger.logSuccess(htmldata.CategoryName+'  updated successfully');
                }
            }
            else
            {
                logger.logError('Something  went wrong !! Please try again');

            }
        });
    };
    $scope.ValidateSize=function(file) {
        var FileSize = file.size / 1024 ; // in Kb
        if (FileSize > 200) {
            alert('File size exceeds 200 KB');
            return false;
        } else {
            return true;
        }
    }
	//DV148-Automatically create thumbnail for temples-Later -------- start ---------
	$scope.SelectFile = function (e) {
		var reader = new FileReader();
		reader.onload = function (e) {
			//$scope.PreviewImage = e.target.result;
			//$scope.$apply();
			var img = new Image();
			img.src = e.target.result;
			img.onload = function () {
				var height = this.height;
				var width = this.width;
				//alert (height + ' ' + width); 
				if (height > 252 || width > 336) { 
					alert("Thumbnail image is not of width * height as 340 * 250. Will be resized.");
					this.height = 250;
					this.width = 350;
					return false;
				}else{
					return true;
				}
			};
			//alert ($scope.PreviewImage.width + ' ' + $scope.PreviewImage.height);
		};
		reader.readAsDataURL(e.target.files[0]);
	};  
 	//DV148-Automatically create thumbnail for temples-Later -------- end ---------	
    $scope.UploadGalleryImage = function(){
        var file = $scope.myFile;
        var uploadUrl = "/uploadgallery";
        var fd = new FormData();
        fd.append('file', file);
        if($scope.TypeID!=4) {
            if($scope.ValidateSize($scope.myFile)) {
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).success(function (res) {
                    $scope.path = res.path;
                    $scope.path = $scope.path.replace(/\W+/g, '/');
                    $scope.path = $scope.path.substring(16, $scope.path.length)
                    $scope.path = $scope.path.substring(0, $scope.path.lastIndexOf('/')) + '.' + $scope.path.substring($scope.path.lastIndexOf('/') + 1, $scope.path.length);
                    //$scope.path = $scope.path.substring(1, $scope.path.length);
                    var formData = {
                        'TypeID': $scope.TypeID,
                        'Title': $scope.ImageTitle,
                        'Desc': $scope.ImageDescription,
                        'imageUrl': $scope.path
                    };
                    $http.post('/admin/image/add', formData).success(function (response) {
                        if (response[0].result == "1") {
                            $scope.LoadImages();
                            logger.log('Image uploaded successfully');
                            document.gallery.reset();

                            $scope.ImagePath = "";
                            $scope.image = "";
                            $scope.image = $scope.tempImage;
                            $('.file-input-name').html("");
                            $scope.path = '';
                        }
                        else {
                            logger.logError('error while uploading image');
                        }
                    })
                });
            }
        } else {
            function getId(url) {
                var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                var match = url.match(regExp);

                if (match && match[2].length == 11) {
                    return match[2];
                } else {
                    logger.log('Invalid URL');
                    return 'error';
                }
            }
    if(getId($scope.Url)!='error') {
        var formData = {
            'TypeID': $scope.TypeID,
            'Title': $scope.ImageTitle,
            'Desc': $scope.ImageDescription,
            'imageUrl': 'http://www.youtube.com/embed/'+getId($scope.Url)
        };
        $http.post('/admin/image/add', formData).success(function (response) {
            if (response[0].result == "1") {
                $scope.LoadImages();
                logger.log('Video added to gallery');
                document.gallery.reset();

                $scope.ImagePath = "";
                $scope.image = "";
                $scope.image = $scope.tempImage;
                $('.file-input-name').html("");
                $scope.path = '';
            }
            else {
                logger.logError('error while adding video');
            }
        })
    }
        }
    };
    $scope.deleteImage=function(list){
        var data={
            'ImageType':list.TypeID,
            'ImageID': list.ImageID,
            'ImagePath':list.Url
        }
        $http.post('/admin/image/update',data).success(function(response){
            if(response.affectedRows!=0)
            {
                $scope.LoadImages();
                logger.log('image deleted successfully');

            }
            else
            {
                logger.logError('error while deleting image');
            }
        });
    };
    $scope.addTempleRule=function(data){
        Admin_TempleHistoryService.addtemplerule(data,function (data) {

            if(data[0].RESULT !=0)
            {
                logger.log('Temple Rule  added successfully');
                $scope.loadTempleRule();
            }
            else
            {
                logger.logError('Something  went wrong !! Please try again');

            }

        });

    };
    $scope.updateTempleRule=function(htmldata,status){
        htmldata.status=status;
        Admin_TempleHistoryService.updatetemplerule(htmldata,function (data) {

            htmldata.isCollapsed=false;
            if(data[0].RESULT !=0){
                if(status=='1'){
                    logger.logSuccess('Temple rule deleted successfully');
                    $scope.loadTempleRule();
                }
                else{
                    logger.logSuccess('Temple rule  updated successfully');
                }
            }
            else {
                logger.logError('Something  went wrong !! Please try again');
            }
        });
    };
    $scope.addPujaTime=function(htmldata){

        Admin_TempleHistoryService.addpujatime(htmldata,function (data) {
            if(data[0].RESULT !=0)
            {
                logger.log(htmldata.RitualName+' added successfully');
                $scope.loadPujaTime();
            }
            else
            {
                logger.logError('Something  went wrong !! Please try again');

            }


        });

    };
    $scope.addshippingcharge=function(htmldata){
        if(htmldata.PujaID==undefined||htmldata.PujaID==null) {
            htmldata.PujaID=0;
        }
        if(htmldata.ShippingName!=undefined&&htmldata.ShippingName!=null&&htmldata.ShippingName!=''){
			/*if (htmldata.Amount!=undefined&&htmldata.Amount!=null&&htmldata.Amount!='') {
				htmldata.Amount = 0;
			}
			*/
            if(htmldata.Amount>=0) { // DV163 Allow free shipping witgh zero amount
                Admin_TempleHistoryService.addshippingcharge(htmldata, function (data) {
                    if (data[0].RESULT != 0) {
                        logger.log(htmldata.ShippingName + ' added successfully');
                        $scope.loadshippingcharge();
                    }
                    else {
                        logger.logError('Something  went wrong !! Please try again');

                    }


                });
            }else{
                logger.log('Enter valid Shipping Amount');
            }}else{
            logger.log('Enter Shipping Option');
        }
    };

    $scope.updateshippingcharge=function(htmldata,status){
		
		//alert ('check update');

        htmldata.status=status;
        Admin_TempleHistoryService.updateshippingcharge(htmldata,function (data) {

            htmldata.isCollapsed=false;
            if(data[0].RESULT !=0){
                if(status=='1'){
                    logger.logSuccess('shipping charge deleted successfully');
                    $scope.loadshippingcharge();
                }
                else{
                    $scope.loadshippingcharge();
                    logger.logSuccess('shipping charge updated successfully');
                }
            }
            else {
                logger.logError('Something  went wrong !! Please try again');
            }
        });
    };


    $scope.updatePujaTime=function(htmldata,status){

        htmldata.status=status;
        Admin_TempleHistoryService.updatepujatime(htmldata,function (data) {

            htmldata.isCollapsed=false;
            if(data[0].RESULT !=0){
                if(status=='1'){
                    logger.logSuccess('Puja schedule deleted successfully');
                    $scope.loadPujaTime();
                }
                else{
                    logger.logSuccess('Puja schedule updated successfully');
                }
            }
            else {
                logger.logError('Something  went wrong !! Please try again');
            }
        });
    };
    $scope.setInputLanguage=function(category,row){
        if(category=='PujaSchedule'){
            row.isCollapsed=true;
            var genID='pujaScheduleInput_'+row.LanguageID+'_'+row.ScheduleID;
            if(row.languages_code!="en"){
                //google_transliteration.makeTransliteratable([genID]);
                //google_transliteration.setLanguagePair(google.elements.transliteration.LanguageCode.ENGLISH, row.languages_code);
            }
        }
        else if(category=='TempleRule'){
            row.isCollapsed=true;
            var genID='templeRuleInput_'+row.LanguageID+'_'+row.IndexID;
            if(row.languages_code!="en"){
                //google_transliteration.makeTransliteratable([genID]);
                //google_transliteration.setLanguagePair(google.elements.transliteration.LanguageCode.ENGLISH, row.languages_code);
            }
        }
        else if(category=='TempleAbout'){
            row.isCollapsed=true;
            var genID='templeAboutInput_'+row.LanguageID+'_'+row.PlaceHolderID+'_'+row.CategoryDisplaySequence;
            if(row.languages_code!="en"){
                //google_transliteration.makeTransliteratable([genID]);
                //google_transliteration.setLanguagePair(google.elements.transliteration.LanguageCode.ENGLISH, row.languages_code);
            }
        };
    };
}]).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);