// <copyright file="Admin_NewUserController.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Admin_NewUser Functions </summary>

'use strict';

DevRoute.controller('Admin_NewUserController',['$http','$scope','Admin_NewUserService','$rootScope','logger', function ($http,$scope,Admin_NewUserService,$rootScope,logger) {
    Admin_NewUserService.loaddesignation(function (data) {
        $rootScope.DesignationValues=data;
    });
    Admin_NewUserService.loadaccesstype(function (data) {
        $rootScope.AccessTypeValues=data;
    });
    Admin_NewUserService.loadusers(function (data) {
        $rootScope.adminusers=data;
        $rootScope.adminusers.forEach(function(user){
            $rootScope.DesignationValues.forEach(function(row){
                if(row.TitleID==user.JobTitleID){
                    user.designation=row.Title;
                }
            });
            $rootScope.AccessTypeValues.forEach(function(row){

                if(row.AccessTypeID==user.AccessTypeID){
                    user.accesstype=row.TypeName;
                }
            });
        })
    });

    $scope.AddUserAdmin=function(){
        //alert("try");
        if($scope.userPassword==$scope.userConfirmPassword)
        {

            if($scope.userPassword.length < 6)
            {
                $scope.PasswordStatus="Password must contain at least six characters!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }

            if($scope.userPassword == $scope.userLoginID) {
                $scope.PasswordStatus= "Password must be different from LoginID!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }
            var re = /[0-9]/;
            if(!re.test($scope.userPassword)) {
                $scope.PasswordStatus=" password must contain at least one number (0-9)!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }
            var re = /[a-z]/;
            if(!re.test($scope.userPassword)) {
                $scope.PasswordStatus=" password must contain at least one lowercase letter (a-z)!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }
            var re = /[A-Z]/;
            if(!re.test($scope.userPassword)) {
                $scope.PasswordStatus="password must contain at least one uppercase letter (A-Z)!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }

        }
        else {
            $scope.PasswordStatus="Password Not Match !";
            $scope.userPassword="";
            $scope.userConfirmPassword=""
            return false;
        }




        //alert("try");
        var formData = {
            'LoginID' : $scope.userLoginID ,
            'Password' : $scope.userPassword ,
            'FirstName' : $scope.userFirstName,
            'LastName' : $scope.userLastName ,
            'TitleID':$scope.userTitleID,
            'AccessType':$scope.userAccessType,
            'HouseNumber' : $scope.userHouseNumber,
            'HouseName' : $scope.userHouseName ,
            'StreetName' :$scope.userStreetName ,
            'PostOffice' : $scope.userPostOffice ,
            'Country' : $scope.userCountry ,
            'State' : $scope.userState,
            'District' : $scope.userDistrict,
            'PIN':$scope.userPIN,
            'PhoneWired':$scope.userPhoneMobile,
            'PhoneMobile':$scope.userPhoneMobile,
            'Status':1

        };
        //alert("try");
        //alert(JSON.stringify(formData));
        Admin_NewUserService.adduseradmin(formData,function (data) {

            if(data[0].RESULT !=0)
            {
                logger.log('New user  added successfully');
                $scope.userLoginID ='';
                $scope.userPassword ='';
                $scope.userFirstName ='';
                $scope.userLastName ='';
                $scope.userTitleID ='';
                $scope.userAccessType ='';
                $scope.userHouseNumber ='';
                $scope.userHouseName ='';
                $scope.userStreetName ='';
                $scope.userPostOffice ='';
                $scope.userCountry ='';
                $scope.userState ='';
                $scope.userDistrict ='';
                $scope.userPIN ='';
                $scope.userPhoneWired ='';
                $scope.userPhoneMobile ='';
                $scope.userStatus ='';
                $scope.userConfirmPassword='';

            }
            else
            {
                logger.logError('Something  went wrong !! Please try again');

            }
        });
    };
    $scope.CheckUsernameAvailability=function(){

        if($scope.userLoginID !=null)
        {
            var formData = {

                'LoginID' : $scope.userLoginID
            };

            Admin_NewUserService.checkusernameavailability(formData,function (data) {


                if(data[0].RESULT !=0)
                {
                    logger.logError('Username Already Taken');

                }
                else
                {
                    logger.logSuccess('Username Available');

                }

            });




        }
        Admin_NewUserService.CheckUsernameAvailability($scope);
    };
    $scope.ValidatePassword=function(){
        if($scope.userPassword==$scope.userConfirmPassword)
        {

            if($scope.userPassword.length < 6)
            {
                logger.logError("Password must contain at least six characters!");
                $scope.PasswordStatus="Password must contain at least six characters!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }

            if($scope.userPassword == $scope.userLoginID) {
                logger.logError("Password must be different from LoginID!");
                $scope.PasswordStatus= "Password must be different from LoginID!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }
            var re = /[0-9]/;
            if(!re.test($scope.userPassword)) {
                logger.logError("password must contain at least one number (0-9)!");
                $scope.PasswordStatus=" password must contain at least one number (0-9)!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }
            var re = /[a-z]/;
            if(!re.test($scope.userPassword)) {
                logger.logError(" password must contain at least one lowercase letter (a-z)!");
                $scope.PasswordStatus=" password must contain at least one lowercase letter (a-z)!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }
            var re = /[A-Z]/;
            if(!re.test($scope.userPassword)) {
                logger.logError("password must contain at least one uppercase letter (A-Z)!");
                $scope.PasswordStatus="password must contain at least one uppercase letter (A-Z)!";
                $scope.userPassword="";
                $scope.userConfirmPassword=""
                return false;
            }

        }
        else {
            logger.logError("Password Not Match !");
            $scope.PasswordStatus="Password Not Match !";
            $scope.userPassword="";
            $scope.userConfirmPassword=""
            return false;
        }


    };
}]);
