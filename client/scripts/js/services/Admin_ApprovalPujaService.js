// <copyright file="Admin_PujaService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Admin_PujaServiceFunctions </summary>

'use strict';

DevRoute.factory('Admin_ApprovalPujaService',function($http,$location,logger,$rootScope)
{
    return{
        approvalpuja: function(doCallback) {
            $http.post('/api/admin/LoadPujaApproval')
                .success(function (response) {
                    doCallback(response);
                })
        },
        ApprovalPuja:function($scope)
        {


            $http.post('/api/admin/LoadPujaApproval').success(function(response) {
                if(response[0].length==0)
                {
                    $scope.NoPujaApprovalListText='No puja to list for approval  !';
                    $scope.NoPujaApprovalListTextHide=false;
                    $scope.PujaApprovalCount=0;
                    $scope.PujaApprovalOrders='';
                    $scope.PujaApprovalList='';
                }
                else
                {
                    $scope.ApprovalStatus=[];
                    $scope.NoPujaApprovalListTextHide=true;
                    $scope.PujaApprovalOrders=response[0];
                    $scope.PujaApprovalList=response[1];
                    $scope.PujaApprovalCount=$scope.PujaApprovalList.length;
                    $scope.PujaApprovalList.forEach(function(puja){
                        puja.ApprovalStatus='1';
                    });
                    logger.log($scope.PujaApprovalCount+' Puja waiting for your approval');
                }
            });
        },
        IncompletedPuja:function($scope)
        {
            $http.post('/api/admin/LoadIncompletedPuja').success(function(response) {
                if(response.length !=0)
                {
                    $scope.NoIncompletedPujaTextHide=true;
                    $scope.IncompletedPujaList=response;
                    $scope.IncompletedPujaListCount=$scope.IncompletedPujaList.length
                }
                else
                {
                    $scope.NoIncompletedPujaText='No puja to list !';
                    $scope.IncompletedPujaList='';
                    $scope.NoIncompletedPujaTextHide=false;
                    $scope.IncompletedPujaListCount=0;
                }
            });
        },


        ChangePujaStatus:function($scope,puja)
        {
           var PujaDate=new Date(puja.PujaDate);
               PujaDate = PujaDate.getFullYear() +"-"+(+PujaDate.getMonth()+1)+"-"+PujaDate.getDate();
            var formdata={
                'CustomerID': puja.CustomerID,
                'TrackingNo': puja.TrackingNo,
                'TempleID': puja.TempleID,
                'ReceiptNo': puja.ReceiptNo,
                'BillAmount': puja.BillAmount,
                'Email': puja.CustomerEmail,
                'FirstName': puja.FirstName,
                'LastName': puja.LastName,
                'PujaStatus': puja.PujaStatus,
                'PujaName':puja.PujaName,
                'PujaID':puja.PujaID,
                'PujaDate':PujaDate,
                'DevoteeName':puja.DevoteeName,
                'PujaStatusMessage': puja.PujaStatusMessage
            }
            $http.post('/api/admin/ChangePujaStatus',formdata).success(function(response) {
                if(response[0].RESULT =='0')
                {
                    logger.logError('Puja status change failed, Please try again ');
                }
                else
                {
                    logger.logSuccess('Puja status changed successfully');
                    $scope.loadIncompletedPuja();
                }
            });
        }
    }});