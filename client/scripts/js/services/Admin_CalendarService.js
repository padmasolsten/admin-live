// <copyright file="Admin_CalendarService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Admin_CalendarService Functions </summary>



'use strict';

DevRoute.factory('Admin_CalendarService',function($http,logger,$rootScope)
{
    return{
        addannouncement: function(formdata,doCallback) {
            $http.post('/admin/announcement/add',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadannouncement: function(formdata,doCallback) {
            $http.post('/admin/announcement/load',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        getannouncementdatabylanguage: function(formdata,doCallback) {
            $http.post('/admin/announcement/language',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        updateannouncement: function(formdata,doCallback) {
            $http.post('/admin/announcement/update',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        addevent: function(formdata,doCallback) {
            $http.post('/admin/event/add',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadevent: function(formdata,doCallback) {
            $http.post('/admin/event/load',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        geteventdatabylanguage: function(formdata,doCallback) {
            $http.post('/admin/event/language',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
       updateevent: function(formdata,doCallback) {
            $http.post('/admin/event/update',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        addfestival: function(formdata,doCallback) {
            $http.post('/admin/festival/add',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
       addsubevent: function(formdata,doCallback) {
            $http.post('/admin/event/add/sub',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        }
    }});