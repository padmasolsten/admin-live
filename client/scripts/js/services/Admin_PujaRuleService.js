// <copyright file="Admin_PujaRuleService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Admin_PujaRuleServiceFunctions </summary>

'use strict';

DevRoute.factory('Admin_PujaRuleService',function($http,$location)
{
    return{
        LoadPuja:function($scope){
            $http.post('/admin/puja/load').success(function(response) {
                response.forEach(function(user) {
                    $scope.PujaValues=response;
                });
            });
        },
        GetExclusionDetails : function($scope)
        {var formData = {
            'PujaID' : $scope.PujaRule_PujaID,
            'ExcludedPujaID':$scope.PujaRule_ExcludedPujaID
        };
            $http.post('/api/admin/GetExclusionDetails',formData).success(function(response) {

                response.forEach(function(Exclusion) {

                    $scope.PujaRule_ExclusionEligibilityCount=Exclusion.ExclusionEligibilityCount;


                });

            });




        },

        GetPujaRuleDetails : function($scope)
        {
            var formData = {

                'PujaID' : $scope.PujaRule_PujaID
            };

            $http.post('/api/admin/GetPujaRuleDetails',formData).success(function(response) {

                response.forEach(function(PujaRule) {

                    $scope.PujaRule_DisableOnlineListing=PujaRule.DisableOnlineListing;
                    $scope.PujaRule_DisableOnlineBooking=PujaRule.DisableOnlineBooking;
                    $scope.PujaRule_CeilingNormalDay=PujaRule.CeilingNormalDay;
                    $scope.PujaRule_CeilingFestivalDay=PujaRule.CeilingFestivalDay;
                    $scope.PujaRule_BookingStartDate=PujaRule.BookingStartDate;
                    $scope.PujaRule_BookingEndDate=PujaRule.BookingEndDate;
                    $scope.PujaRule_MinAdvanceBookingPeriod=PujaRule.MinAdvanceBookingPeriod;
                    $scope.PujaRule_ReOccuringDay=PujaRule.ReOccuringDay;
                    $scope.PujaRule_ReOccuringStar=PujaRule.ReOccuringStar;
                    $scope.PujaRule_ReOccuringStarName=PujaRule.StarName;

                    $scope.PujaRule_SkipDevoteeNameStar=PujaRule.SkipDevoteeNameStar;
                    $scope.PujaRule_NotAFixedRate=PujaRule.NotAFixedRate;
                    $scope.PujaRule_Timing=PujaRule.Timing;
                    $scope.PujaRule_Duration=PujaRule.Duration;
                    $scope.PujaRule_PujaStartTime=PujaRule.PujaStartTime;
                    $scope.PujaRule_PujaEndTime=PujaRule.PujaEndTime;


                });

            });




        },
        Block : function($scope)
        {
            var formData = {
                'PujaID':$scope.PujaID,

                'UNAVAILABLE_DATE' : $scope.UNAVAILABLE_DATE



            };
//            $scope.UNAVAILABLE_DATE ='';




            $http.post('/api/admin/Block',formData).success(function(response){
                response.forEach(function(user) {


                });


            });
        },
        Exclusion : function($scope)
        {
            var formData = {
                'PujaID':$scope.PujaID,

                'ExcludedPujaID' : $scope.ExcludedPujaID,
                'ExclusionEligibilityCount':$scope.ExclusionEligibilityCount



            };
//            $scope.UNAVAILABLE_DATE ='';




            $http.post('/api/admin/Exclusion',formData).success(function(response){
                response.forEach(function(user) {


                });


            });
        },
        ExclusionUpdate : function($scope)
        {
            var formData = {
                'PujaID':$scope.PujaRule_PujaID,

                'ExcludedPujaID' : $scope.PujaRule_ExcludedPujaID,
                'ExclusionEligibilityCount' : $scope.PujaRule_ExclusionEligibilityCount



            };



            $http.post('/api/admin/ExclusionUpdate',formData).success(function(response){
                response.forEach(function(user) {
                    $scope.UpdateStatus=user.RESULT;
                    console.log("Connection Success fully");
                });


            });
        },

        PujaRuleUpdate : function($scope)
        {
            var formData = {
                'PujaID':$scope.PujaRule_PujaID,

                'DisableOnlineListing' : $scope.PujaRule_DisableOnlineListing,
                'DisableOnlineBooking' : $scope.PujaRule_DisableOnlineBooking,
                'CeilingNormalDay':$scope.PujaRule_CeilingNormalDay,
                'CeilingFestivalDay' : $scope.PujaRule_CeilingFestivalDay,
                'BookingStartDate' : $scope.PujaRule_BookingStartDate,
                'BookingEndDate':$scope.PujaRule_BookingEndDate,
                'MinAdvanceBookingPeriod' : $scope.PujaRule_MinAdvanceBookingPeriod,
                'ReOccuringDay' : $scope.PujaRule_ReOccuringDay,
                'ReOccuringStar':$scope.PujaRule_ReOccuringStar,
                'SkipDevoteeNameStar' : $scope.PujaRule_SkipDevoteeNameStar,
                'NotAFixedRate' : $scope.PujaRule_NotAFixedRate,
                'Timing':$scope.PujaRule_Timing,
                'Duration' : $scope.PujaRule_Duration,
                'PujaStartTime' : $scope.PujaRule_PujaStartTime,
                'PujaEndTime':$scope.PujaRule_PujaEndTime


            };
            $scope.PujaRule_DisableOnlineListing ='';
            $scope.PujaRule_CeilingNormalDay='' ;
            $scope.PujaRule_CeilingNormalDay ='';
            $scope.PujaRule_CeilingFestivalDay='' ;
            $scope.PujaRule_BookingStartDate ='';
            $scope.PujaRule_BookingEndDate='' ;
            $scope.PujaRule_MinAdvanceBookingPeriod ='';
            $scope.PujaRule_ReOccuringDay='' ;
            $scope.PujaRule_ReOccuringStar ='';
            $scope.PujaRule_SkipDevoteeNameStar='' ;
            $scope.PujaRule_NotAFixedRate ='';
            $scope.PujaRule_Timing='' ;
            $scope.PujaRule_Duration ='';
            $scope.PujaRule_PujaStartTime='' ;
            $scope.PujaRule_PujaEndTime ='';



            $http.post('/api/admin/PujaRuleUpdate',formData).success(function(response){
                response.forEach(function(user) {
                    $scope.UpdateStatus=user.RESULT;
                    console.log("Connection Success fully");
                });


            });
        },
        AddPujaRule:function($scope)
        {

            alert("dfhsdiutg");
            var formData={

                'TempleID' : $scope.TempleID ,
                'PujaID' : $scope.PujaID,
                'DisableOnlineListing' :$scope.DisableOnlineListing,
                'DisableOnlineBooking':$scope.DisableOnlineBooking,
                'CeilingNormalDay':$scope.CeilingNormalDay,
                'CeilingFestivalDay':$scope.CeilingFestivalDay,
                'BookingStartDate':$scope.BookingStartDate,
                'BookingEndDate':$scope.BookingEndDate,
                'MinAdvanceBookingPeriod':$scope.MinAdvanceBookingPeriod,
                'ReOccuringDay':$scope.ReOccuringDay,
                'ReOccuringStar':$scope.ReOccuringStar,
                'SkipDevoteeNameStar':$scope.SkipDevoteeNameStar,
                'NotAFixedRate':$scope.NotAFixedRate,
                'Duration':$scope.Duration,
                'Timing':$scope.Timing,
                'PujaStartTime':$scope.PujaStartTime,
                'PujaEndTime':$scope.PujaEndTime



            };
            $scope.TempleID ='';
            $scope.PujaID='' ;
            $scope.DisableOnlineListing='';
            $scope.DisableOnlineBooking='';
            $scope.CeilingNormalDay='';
            $scope.CeilingFestivalDay ='';
            $scope.BookingStartDate='' ;
            $scope.BookingEndDate='';
            $scope.MinAdvanceBookingPeriod='';
            $scope.ReOccuringDay='';
            $scope.ReOccuringStar='' ;
            $scope.Duration='';
            $scope.Timing='';
            $scope.PujaStartTime='';
            $scope.PujaEndTime='';


            $http.post('/api/admin/AddPujaRule',formData).success(function(response) {
                response.forEach(function(user) {
//                    $scope.Result=user.RESULT;



                });

            });

        }



    }});