// <copyright file="Admin_CalendarService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Admin_CalendarService Functions </summary>

'use strict';

DevRoute.factory('DashboardService',function($http,$rootScope,logger)
{
    return{
        loadDashboard:function($scope){
            $http.post('/admin/dashboard/load').success(function(response) {
                if(response.length!=0){
                       $rootScope.dashboardData=response[0][0];
                }
            });
        },
        getPujaReport:function($scope,dates,log){
            dates.LanguageID=$rootScope.SessionLanguageID;
            $http.post('/admin/order/puja/report',dates).success(function(response){
                if(response[0].length!=0){
                    if(dates.search==undefined){
                        $rootScope.PujaReport=response[0];
                    }
                    else
                    {
                        $rootScope.pujaReportSearchResult=response[0];
                    }
                }
                else{

                    if(dates.search==undefined){
                        $rootScope.PujaReport='';
                    }
                    else
                    {
                        $rootScope.pujaReportSearchResult='';

                    }
                    if(log==1){
                        logger.logWarning('No puja available for the selected dates')
                    }

                }
            });

        },
        getBookingReport:function($scope,dates,log){
            dates.LanguageID=$rootScope.SessionLanguageID;
            $http.post('/admin/order/booking/report',dates).success(function(response){
                if(response[0].length!=0){
                    $rootScope.BookingReport=response[0];
                    $scope.AllBookingDetails=response[1];
                    $scope.isCollapsed=true;
                    $rootScope.BookingReport.forEach(function(booking){
                        console.log ('booking row : ' + booking)
                        booking.bookingdetails=[];
                        $scope.AllBookingDetails.forEach(function(row){
                            if(booking.TrackingNo==row.TrackingNo){
                                booking.bookingdetails.push(row)
                            }
                        });
                    });
                }
                else{
                    $rootScope.BookingReport='';
                    $rootScope.BookingDetails='';
                    if(log==1){
                        logger.logWarning('No Booking available for the selected dates')
                    }
                }
            });
        },
        getDonationReport:function($scope,dates,log){
            dates.LanguageID=$rootScope.SessionLanguageID;
            $http.post('/admin/order/donation/report',dates).success(function(response){
                if(response[0].length!=0){
                    return  $rootScope.DonationReport=response[0];
                }
                else{
                    if(log==1){
                        logger.log('No donation received for the selected dates');
                    }
                }
            });
        },
        getPostalAddressReport:function($scope,dates,log){
            dates.LanguageID=$rootScope.SessionLanguageID;
            $http.post('/admin/order/postaladdress/report',dates).success(function(response){
                if(response[0].length!=0){
                    return  $rootScope.PostalAddressReport=response[0];
                }
                else{
                    if(log==1){
                        logger.log('No shipment for the selected dates');
                    }
                }
            });
        },
        getRevenueReport:function($scope,dates,log){
            dates.LanguageID=$rootScope.SessionLanguageID;
            $http.post('/admin/order/revenue/report',dates).success(function(response){
                if(response[0].length!=0){
                    $rootScope.pujaRevenueReport=response[0];
                    $rootScope.showRevenueReport=true;
                }
                else{
                    $rootScope.pujaRevenueReport='';
                    if(log==1){
                        logger.log('No booking for the selected dates');
                    }
                }
            });
            $http.post('/admin/order/donation/report',dates).success(function(response){
                if(response[0].length!=0){
                    $rootScope.DonationRevenueReport=response[0];
                    $rootScope.showRevenueReport=true;
                }
                else{
                    $rootScope.DonationRevenueReport='';
                }
            });
            if(($rootScope.DonationRevenueReport=='')&&($rootScope.pujaRevenueReport=='')){
                logger.logWarning('No revenue to list for the selected dates');
                $rootScope.showRevenueReport=false;
            }
        },
        getPaymentReport:function($scope,dates,log){
            dates.LanguageID=$rootScope.SessionLanguageID;
            $http.post('/admin/order/payment/report',dates).success(function(response){
                if(response[0].length!=0){
                    $rootScope.pujaPaymentReport=response[0];
                    $rootScope.showPaymentReport=true;
                }
                else{
                    $rootScope.showPaymentReport='';
                    if(log==1){
                        logger.log('No payment received for the selected dates');
                    }
                }
            });
         },
        getApprovalReport:function($scope,dates,log){
            dates.LanguageID=$rootScope.SessionLanguageID;
            $http.post('/admin/order/approval/report',dates).success(function(response){
                if(response[0].length!=0){
                    $rootScope.pujaApprovalReport=response[0];
                    $rootScope.showApprovalReport=true;
                }
                else{
                    $rootScope.showApprovalReport='';
                    if(log==1){
                        logger.log('No approvals for the selected dates');
                    }
                }
            });
        },
        printPostalDetails:function($scope,logger,printReceipt){
            var FromDate=new Date($scope.dateFrom).getDate()+"/"+(new Date($scope.dateFrom).getMonth()+1)+"/"+new Date($scope.dateFrom).getFullYear();
            var ToDate=new Date($scope.dateFrom).getDate()+"/"+(new Date($scope.dateFrom).getMonth()+1)+"/"+new Date($scope.dateFrom).getFullYear();
            printReceipt.printPostalReport($scope.tempPostalDetails,'Postal Details',{FromDate:FromDate,ToDate:ToDate,Today:$scope.varToday,PostalReport:true});
        }
    }});