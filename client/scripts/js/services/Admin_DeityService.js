// <copyright file="Admin_DeityService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Admin_DeityService Functions </summary>

'use strict';

DevRoute.factory('Admin_DeityService',function($http,$rootScope,logger)
{
    return{
        loadmasterdeity: function(doCallback) {
            $http.post('/admin/master/deity',{LanguageID: $rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        adddeity: function(formData,doCallback) {
            $http.post('/admin/deity/add',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        removedeity: function(formData,doCallback) {
            $http.post('/admin/deity/remove',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        loaddeity: function(doCallback) {
            $http.post('/admin/deity/load',{LanguageID: $rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        AddDeity:function($scope){
            var formData={
                'DeityMasterID':$scope.DeityData.DeityID,
                'Category' : $scope.Category
            };
            $scope.DeityData ='';
            $scope.Category='' ;
            $http.post('/admin/deity/add',formData).success(function(response) {
                if(response.affectedRows==undefined){
                    logger.logSuccess("failed to add new deity");
                }
               else
                {
                    logger.logSuccess("successfully added new deity");
                    $rootScope.LoadDeity();
                }
            });
        },
        removedeityold : function($scope,puja,newstatus) {
            var formData = {
                'DeityID':puja.DeityID,
                'TempleID':puja.TempleID,
                'NewStatus':newstatus
            };

            $http.post('/admin/deity/remove',formData).success(function(response){
                if (response.RESULT="1"){
                    $scope.LoadDeity();
                    logger.log("deity removed successfully");
                }
                else
                {
                    logger.log("user operation failed");
                }
            });
        }

    }});