/**
 * Created by saneesh on 2/22/2017.
 */
// <copyright file="LoginService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing LoginService Functions </summary>

'use strict';

DevRoute.factory('adminservice',function($http,$location,$rootScope,logger,$window,base64,$cookies){
    return{
        getpujareport: function(formdata,doCallback) {
            $http.post('/admin/order/puja/report',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        getbookingreport: function(formdata,doCallback) {
            $http.post('/admin/order/booking/report',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        getdonationreport: function(formdata,doCallback) {
            $http.post('/admin/order/donation/report',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        getpostaladdressreport: function(formdata,doCallback) {
            $http.post('/admin/order/postaladdress/report',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
		//DV161 - Postal status update ---------- start -----------
        postalAction: function(formData,doCallback) {
            $http.post('admin/order/postal/complete',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },		
		//DV161 - Postal status update ---------- end -------------
        getpaymentreport: function(formdata,doCallback) {
            $http.post('/admin/order/payment/report',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        printPostalDetails:function($scope,logger,printReceipt){
            var FromDate=new Date($scope.dateFrom).getDate()+"/"+(new Date($scope.dateFrom).getMonth()+1)+"/"+new Date($scope.dateFrom).getFullYear();
            var ToDate=new Date($scope.dateFrom).getDate()+"/"+(new Date($scope.dateFrom).getMonth()+1)+"/"+new Date($scope.dateFrom).getFullYear();
            printReceipt.printPostalReport($scope.tempPostalDetails,'Postal Details',{FromDate:FromDate,ToDate:ToDate,Today:$scope.varToday,PostalReport:true});
        },
        //settlement
        loadsettlements: function(data,doCallback){
            $http.post('/api/Temple/Listtransactions', data)
                .success(function (response) {
                    doCallback(response);
                });
        },
        generatesettlementfile: function(data,doCallback){
            $http.post('/api/Temple/sendsettlementdata', data)
                .success(function (response) {
                    doCallback(response);
                });
        },
        updatesettlementstatus:function(data,doCallback){
            $http.post('/api/Temple/updatesettlmentstatus', data)
                .success(function (response) {
                    doCallback(response);
                });
        },
        LoadTemples: function(data,doCallback){
            $http.post('/api/Temple/ListTemples', data)
                .success(function (response) {
                    doCallback(response);
                });
        },
        loaddevotees: function(formdata,doCallback) {
            $http.post('/admin/getdevoteelist',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        } ,
        addreview: function(formdata,doCallback) {
            $http.post('/admin/addreview',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadreviews: function(formdata,doCallback) {
            $http.post('/admin/getreviews',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        } ,
        updatereview: function(formdata,doCallback) {
            $http.post('/admin/updatereview',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        sendmail: function(formdata,doCallback) {
        $http.post('/admin/sendnotification',formdata)
            .success(function (response) {
                doCallback(response);
            })
    },
        updatedevotee: function(formdata,doCallback) {
        $http.post('/admin/updatedevotee',formdata)
            .success(function (response) {
                doCallback(response);
            })
    },
        loaddashboard: function(doCallback) {
            $http.post('/admin/loaddashboard')
                .success(function (response) {
                    doCallback(response);
                })
        },
        addpuja: function(formdata,doCallback) {
            $http.post('/admin/master/addpuja',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        deletemasterpuja: function(formdata,doCallback) {
            $http.post('/admin/master/deletepuja',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        updatemasterpuja: function(formdata,doCallback) {
            $http.post('/admin/master/updatepuja',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadmasterpuja: function(doCallback) {
            $http.post('/admin/master/puja',{LanguageID:$rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadmasterdeity: function(doCallback) {
            $http.post('/admin/master/deity',{LanguageID:$rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        onboardtemple: function(formdata,doCallback) {
        $http.post('/admin/master/onboard',formdata)
            .success(function (response) {
                doCallback(response);
            })
        },
        generalcall: function(formdata,doCallback) {
        $http.post('/admin/master/generalcall',formdata)
            .success(function (response) {
                doCallback(response);
            })
        }
    }
});

