// <copyright file="Admin_PujaService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Admin_PujaServiceFunctions </summary>

'use strict';

DevRoute.factory('Admin_TaskService',function($http,$location,logger,$rootScope)
{
    return{
        approvalpuja: function(formdata,doCallback) {
            $http.post('/admin/order/puja/request',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        incompletedpuja: function(doCallback) {
            $http.post('/admin/order/puja/pending',{LanguageID: $rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        approvalaction: function(formData,doCallback) {
            $http.post('admin/order/puja/approve',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        changepujastatus: function(formData,doCallback) {
            $http.post('/admin/order/puja/status',formData)
                .success(function (response) {
                    doCallback(response);
                })
        }
    }});