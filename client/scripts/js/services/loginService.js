// <copyright file="LoginService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing LoginService Functions </summary>

'use strict';

DevRoute.factory('loginService',function($http,$location,$rootScope,logger,$window,base64,$cookies){
    return{
        loadlanguages: function(doCallback) {
            $http.get('/languages')
                .success(function (response) {
                    doCallback(response);
                })
        },
        login: function(formdata,doCallback) {
            $http.post('/admin/login',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        checkauthenticatedadmin: function(doCallback) {
            $http.post('/checkAuthenticatedAdmin')
                .success(function (response) {
                    doCallback(response);
                })
        },
        logout: function(doCallback) {
            $http.post('/admin/logout')
                .success(function (response) {
                    doCallback(response);
                })
        }
    }
    });
