// <copyright file="Admin_TempleContactService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Admin_TempleContactService Functions </summary>
'use strict';

DevRoute.factory('Admin_TempleContactService',function($http,$location,logger)
{
    return{
        loaddesignation: function(doCallback) {
            $http.post('/admin/management/designation')
                .success(function (response) {
                    doCallback(response);
                })
        },
        addcontact: function(formData,doCallback) {
			//DV-115 - Remove alert on Add contact
            //alert(JSON.stringify(formData))
            $http.post('/admin/management/add',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        listcontact: function(data,doCallback) {
            $http.post('/admin/management/listcontact',data)
                .success(function (response) {
                    doCallback(response);
                })
        }
    }});






