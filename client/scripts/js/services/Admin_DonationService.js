// <copyright file="Admin_DonationService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Admin_DonationService Functions </summary>

'use strict';

DevRoute.factory('Admin_DonationService',function($http,$location,logger,$rootScope)
{

    return{
        loadmasterdonation: function(doCallback) {
            $http.post('/admin/master/donation',{LanguageID: $rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        loaddonation: function(doCallback) {
            $http.post('/admin/donation/load',{LanguageID: $rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        getdonationdatabylanguage: function(formData,doCallback) {
            $http.post('/admin/donation/language',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        updatedonation: function(formData,doCallback) {
            $http.post('/admin/donation/update',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        changedonationstatus: function(formData,doCallback) {
            $http.post('/admin/donation/update',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        adddonation: function(formData,doCallback) {
            $http.post('/admin/donation/add',formData)
                .success(function (response) {
                    doCallback(response);
                })
        }

    }});