

DevRoute.factory('Admin_PujaService',function($http,$location,logger,$rootScope)
{

    return{
        loadmasterpuja: function(doCallback) {
            $http.post('/admin/master/puja',{LanguageID:$rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadpuja: function(doCallback) {
            $http.post('/admin/puja/load',{LanguageID:$rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadpujatypes: function(doCallback) {
            $http.post('/admin/puja/types',{LanguageID:$rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadstars: function(doCallback) {
            $http.post('/stars',{LanguageID:$rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        getpujadatebylanguage: function(formData,doCallback) {
            $http.post('/admin/puja/language',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        pujaupdate: function(formData,doCallback) {
            $http.post('/admin/puja/update',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        changepujastatus: function(formdata,doCallback) {
            $http.post('/admin/puja/update',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        addpuja: function(formdata,doCallback) {
            $http.post('/admin/puja/add',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        generaltab: function(formdata,doCallback) {
            $http.post('/admin/generaltab',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        addsubpuja: function(formdata,doCallback) {
            $http.post('/admin/puja/sub/add',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        loaddeity: function(doCallback) {
            $http.post('/admin/deity/load',{LanguageID: $rootScope.SessionLanguageID})
                .success(function (response) {
                    doCallback(response);
                })
        },
        load_packages: function(data,doCallback){
            $http.post('/admin/PujaPackages', data)
                .success(function (response) {
                    doCallback(response);
                });
        },
    }});