// <copyright file="Admin_NewUserService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Admin_NewUserServiceFunctions </summary>


'use strict';

DevRoute.factory('Admin_NewUserService',function($http,$rootScope,$location,logger){
    return{
        loaddesignation: function(doCallback) {
            $http.post('/admin/management/designation')
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadaccesstype: function(doCallback) {
            $http.post('/admin/user/types')
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadusers: function(doCallback) {
            $http.post('/admin/management/loadusers')
                .success(function (response) {
                    doCallback(response);
                })
        },
        adduseradmin: function(formData,doCallback) {
            $http.post('/admin/user/add',formData)
                .success(function (response) {
                    doCallback(response);
                })
        },
        checkusernameavailability: function(formData,doCallback) {
            $http.post('/admin/user/check',formData)
                .success(function (response) {
                    doCallback(response);
                })
        }


    }
});




