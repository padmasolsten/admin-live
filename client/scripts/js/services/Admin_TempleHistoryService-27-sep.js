// <copyright file="Registration_TempleHistoryService.js" company="Devaayanam">
// Copyright (c) 2014 All Right Reserved, http://devaayanam.com/
//
// This source is subject to the Devaayanam Permissive License.
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// All other rights reserved.
//
// </copyright>
//
// <author>Santhosh Poothankurussi</author>
// <email>santhosh.poothankurussi@devaayanam.com</email>
// <date>2014-06-22</date>
// <summary>Contains Javascript methods for Routing Of Registration Temple History Service Functions </summary>

'use strict';

DevRoute.factory('Admin_TempleHistoryService',function($http,logger)
{
    return{
        loadtemplehistory: function(doCallback) {
            $http.post('/admin/about/load')
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadtemplerule: function(doCallback) {
            $http.post('/admin/rule/load')
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadshippingcharge: function(doCallback) {
            $http.post('/admin/shippingcharge/load')
                .success(function (response) {
                    doCallback(response);
                })
        },
        addshippingcharge: function(formdata,doCallback) {
            $http.post('/admin/shippingcharge/add',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        updateshippingcharge: function(formdata,doCallback) {
            $http.post('/admin/shippingcharge/update',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadpujatime: function(doCallback) {
            $http.post('/admin/timings/load')
                .success(function (response) {
                    doCallback(response);
                })
        },
        addhistory: function($scope,doCallback) {
            $http.post('/admin/about/add',$scope.historydata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        updatehistory: function(data,doCallback) {
            $http.post('/admin/about/update',data)
                .success(function (response) {
                    doCallback(response);
                })
        },
        addtemplerule: function(data,doCallback) {
            $http.post('/admin/rule/add',data)
                .success(function (response) {
                    doCallback(response);
                })
        },
        updatetemplerule: function(data,doCallback) {
            $http.post('/admin/rule/update',data)
                .success(function (response) {
                    doCallback(response);
                })
        },
        addpujatime: function(data,doCallback) {
            $http.post('/admin/timings/add',data)
                .success(function (response) {
                    doCallback(response);
                })
        },
        updatepujatime: function(data,doCallback) {
            $http.post('/admin/timings/update',data)
                .success(function (response) {
                    doCallback(response);
                })
        },
        loadimages: function(doCallback) {
            $http.post('/admin/image/load')
                .success(function (response) {
                    doCallback(response);
                })
        },
        generaltab: function(formdata,doCallback) {
            $http.post('/admin/generaltab',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        },
        //DV-41-SAIF
        //Messages for users in payment page
        manage_pg_messages: function(formdata,doCallback) {
            $http.post('/admin/manage_pg_messages',formdata)
                .success(function (response) {
                    doCallback(response);
                })
        }


    }});