/**
 * Created by saifudheen on 7/24/2015.
 */
var options = {
    sourceLanguage: google.elements.transliteration.LanguageCode.ENGLISH,
    destinationLanguage: ['ml'],
    shortcutKey: 'ctrl+g',
    transliterationEnabled: true
};
var google_transliteration =new google.elements.transliteration.TransliterationControl(options);
