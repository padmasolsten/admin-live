
var express = require('express')
    , connect = require('connect')
    , http = require('http')
    , https = require('https')
    , path = require('path')
    , db = require('./server/db.js')
    , cookieParser = require('cookie-parser')
    , session = require('express-session')
    , SessionStore = require('express-mysql-session')
    , morgan = require('morgan')
    , errorHandler = require('errorhandler')
    , bodyParser     = require('body-parser')
    , methodOverride = require('method-override')
    , config = require('./server/database.json')
    , cryptojs=require('./server/rest/utilities/cryptography.js')
    ,emailtemplate=require('./server/rest/emailtemplate.js')
    , fs = require('fs')
    , multer = require('multer');

currentenvironment='dev';
//var minify =require('./server/rest/utilities/minify');
var app = exports.app = express();
var env = app.get('env') == 'development' ? 'configuration' : app.get('env');
var dbConfigFile = __dirname + '/server/database.json';
var data = fs.readFileSync(dbConfigFile, 'utf8');
var dbConfig = JSON.parse(data)[env];
var portno=dbConfig.portno;
//app.set('views', __dirname + '/client/page');
//app.use(express.static(path.join(__dirname, 'Admin')));
//app.use(express.static(path.join(__dirname, 'Registration')));
app.use(express.static(path.join(__dirname, 'client')));
app.set('port', process.env.PORT || portno);
//app.use(morgan('development'));
app.use(errorHandler());
app.use(bodyParser()); 		// pull information from html in POST
app.use(methodOverride());  // simulate DELETE and PUT
app.use(express.json());


// MySQL sessions Store   - START

var decryptedPassword=cryptojs.DecryptString(dbConfig.password);
var decryptedUsername=cryptojs.DecryptString(dbConfig.user);


//var test=cryptojs.DecryptString();
//var test=cryptojs.EncryptString();
//console.log(test);



var options = {
    host: dbConfig.host,
    port: dbConfig.port,
    user: decryptedUsername,
    password: decryptedPassword,
    database: dbConfig.database,
    debug: false,// Whether or not to output debug messages to the console.
    checkExpirationInterval: 900000,// How frequently expired sessions will be cleared; milliseconds.
    expiration: 86400000,// The maximum age of a valid session; milliseconds.
    autoReconnect: true,// Whether or not to re-establish a database connection after a disconnect.
    reconnectDelay: 100,// Time between reconnection attempts; milliseconds
    maxReconnectAttempts: 0// Maximum number of reconnection attempts. Set to 0 for unlimited.
}


app.use(cookieParser());
app.use(session({
    key: 'devaayanam_admin_mysql_session',
    secret:'SMjhEnLWqf8NlwLS1ZVdgd2lfVezTkPptPmqZzhOrww=',
    store: new SessionStore(options)
    // store: new SessionStore(config[env])
}));

//app.use(passport.initialize());
//app.use(passport.session());

// MySQL sessions Store - END
var path = require('path')
var filestorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '../ordering/client/files/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname.replace(/\W+/g, 'a').toLowerCase() + Date.now()+path.extname(file.originalname));
    }
});
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '../ordering/client/images/temple/uploads/')
    },
    filename: function (req, file, cb) {
        console.log("on.."+file.originalname);
        cb(null, file.originalname.replace(/\W+/g, 'a').toLowerCase() + Date.now()+'.jpg')
    }
});
var receiptstorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './client/files/')
    },
    filename: function (req, file, cb) {
        cb(null, 'receipt.jpg')
    }
});
var upload = multer({ storage: storage });
var uploadreceipt = multer({ storage: receiptstorage });
var uploadfiles = multer({ storage: filestorage });
app.post('/uploadgallery', upload.single('file'),function (req,res){
    console.log(req.body);
    res.json(req.file);
});
app.post('/uploadfiles', uploadfiles.single('file'),function (req,res){
    console.log(req.body);
    res.json(req.file);
});
app.post('/uploadreceipt', uploadreceipt.single('file'),function (req,res){
    var pujadetails=JSON.parse(req.body.pujadetails);
    var orderinfo={
        event:'BOOKINGCOMPLETED',
        TrackingNo:pujadetails.TrackingNo,
        addressid:pujadetails.AddressID
    };
    console.log(JSON.stringify(orderinfo));
    emailtemplate.prepare_mail_template(orderinfo);
    res.json(req.file);
});
app.use(app.router);



//IMPORTANT! Routes requires app configuration so do this after app has been been configured.
var routes = require('./server/handlers/routes')(app);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
app.set('view engine', 'jade');
scriptall= ['scripts/app.js'];
app.all('/*',function(req,res,next){
    app.set('views', path.join(__dirname ,'client'));
    var host = req.get("host");
    res.cookie('url',host);
    res.render("index.jade", {
        scripts: scriptall
    });


    //res.sendfile('client/index.html',{route: __dirname });
});


/// error handlers
// development error handler. will print stack trace
//if (app.get('env') === 'development') {
//    app.use(function(err, req, res, next) {
//        res.status(err.status || 500);
//        res.render('error', {
//            message: err.message,
//            error: err
//        });
//    });
//}
//// production error handler. no stack traces leaked to user
//app.use(function(err, req, res, next) {
//    res.status(err.status || 500);
//    res.render('error', {
//        message: err.message,
//        error: {}
//    });
//});

// Create HTTP and HTTPS Web server using Express and Listen

//var options = {
//    key: fs.readFileSync('privatekey.pem'),
//    cert: fs.readFileSync('certificate.pem')
//};
//https.createServer(options, app).listen(443);
// create HTTP service to redirect to HTTPS
//http.createServer(express().use(function(req,res){
//    res.redirect('https://localhost:443' + req.url);
//})).listen(8888);
http.createServer(app).listen(app.get('port'), function ()
{
    console.log("Express server listening on port " + app.get('port'));
});
// Create HTTP and HTTPS Web server using Express and Listen